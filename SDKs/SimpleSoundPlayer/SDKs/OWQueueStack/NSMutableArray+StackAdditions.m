//
//  NSMutableArray+StackAdditions.m
//
//  Created by Brant Young on 1/3/14.
//  Copyright (c) 2014 Codinn Studio. All rights reserved.
//

#import "NSMutableArray+StackAdditions.h"

@implementation NSMutableArray (StackAdditions)

- (void)stack_push:(id)anObject
{
    [self addObject: anObject];
}

- (id)stack_pop
{
    if(self.count == 0) {
        return nil;
    }
    
    NSUInteger idx = self.count - 1;
    id object = self[idx];
    
    // Removing an object here may result in a potential thread of the object
    // being released unexpectedly.
    [self removeObjectAtIndex:idx];
    return object;
}

- (id)stack_peek:(int)index
{
    if(index >= self.count) {
        return nil;
    }
    
    return self[index];
}

- (id)stack_peekHead
{
    return [self lastObject];
}

- (id)stack_peekTail
{
	return [self stack_peek:0];
}

- (BOOL)stack_empty
{
    return self.count==0;
}

@end
