//
//  SoundPlayer.h
//
//  Created by Josh Savage on 26/9/13.
//  Copyright (c) 2013 WowWee Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "FISoundEngine.h"

@protocol SimpleSoundPlayerDelegate;

@interface SimpleSoundPlayer : NSObject <AVAudioPlayerDelegate>

@property (nonatomic, weak) id<SimpleSoundPlayerDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *audioPlayers;

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

@property (nonatomic, strong) NSMutableDictionary *alSoundBuffer;

+(id)sharedInstance;

/** Sound Info methods **/
+ (double) soundDuration:(NSString *)soundFile;

/** Plays sound with shared instance */
+ (AVAudioPlayer *)playSoundQueue: (NSArray *)soundQueue;
+ (AVAudioPlayer *)playSoundQueue: (NSArray *)soundQueue appendQueue:(bool)appendQueue;
+ (AVAudioPlayer *)playSound: (NSString *)soundFile;
+ (AVAudioPlayer *)playSound: (NSString *)soundFile delegate:(id)someDelegate;
+ (AVAudioPlayer *)playSound: (NSString *)soundFile repeats:(bool)repeats;
+ (void)vibratePhone;
+ (void)vibratePhoneWithRepeatingInterval:(NSTimeInterval)repeatInterval;
+ (void)stopVibrating;
+ (void)stopAll;

/** Controls for OpenAL Sound */
+ (void)loadALSound:(NSString *)soundFile maxPolyphony:(NSUInteger)maxPolyphone;
+ (void)removeALSound:(NSString *)soundFile;
+ (void)clearALSoundBuffer;
+ (NSTimeInterval)playALSound:(NSString *)soundFile;
+ (NSTimeInterval)playALSound:(NSString *)soundFile pitch:(float)pitch;
+ (void)playALSoundQueue: (NSArray *)soundQueue withDelay:(NSTimeInterval)delay;
+ (void)playALSoundQueue: (NSArray *)soundQueue withDelay:(NSTimeInterval)delay appendQueue:(bool)appendQueue;

@end

@protocol SimpleSoundPlayerDelegate <NSObject>

- (void) simpleSoundPlayer:(SimpleSoundPlayer *)player soundDidFinish:(NSString *)sound;

@end
