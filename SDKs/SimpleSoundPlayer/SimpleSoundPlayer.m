//
//  SoundPlayer.m
//
//  Created by Josh Savage on 26/9/13.
//  Copyright (c) 2013 WowWee Group Limited. All rights reserved.
//

#import "SimpleSoundPlayer.h"
#import "MSWeakTimer.h"
#import "NSMutableArray+QueueAdditions.h"

@interface SimpleSoundPlayer()

@property (nonatomic, strong) MSWeakTimer *repeatTimer;
@property (nonatomic, strong) NSMutableArray *soundQueue;
@property (nonatomic, strong) MSWeakTimer *soundQueuePlayingTimer;
@property (nonatomic, assign) NSTimeInterval soundQueueDelay;
@property (nonatomic, strong) NSMutableArray *currentlyPlayingALSounds;
@property (nonatomic, strong) NSArray *preloadedSounds;

- (AVAudioPlayer *)playSound: (NSString *)soundFile;
- (AVAudioPlayer *)playSound: (NSString *)soundFile repeats:(bool)repeats;
- (void)vibratePhone;
- (void)vibratePhoneWithRepeatingInterval:(NSTimeInterval)repeatInterval;
- (void)stopVibrating;
- (void)stopAll;

@end

@implementation SimpleSoundPlayer

static BOOL useinside = NO;
static id _sharedObject = nil;

+(id) alloc {
    if (!useinside) {
        @throw [NSException exceptionWithName:@"Singleton Vialotaion" reason:@"You are violating the singleton class usage. Please call +sharedInstance method" userInfo:nil];
    }
    else {
        return [super alloc];
    }
}

+(id)sharedInstance {
    static dispatch_once_t p = 0;
    dispatch_once(&p, ^{
        useinside = YES;
        _sharedObject = [[SimpleSoundPlayer alloc] init];
        useinside = NO;
    });
    // returns the same object each time
    return _sharedObject;
}

- (id)init {
    if(self = [super init]){
        self.soundQueue = [NSMutableArray new];
        NSError *error = nil;
        AVAudioSession *session = [AVAudioSession sharedInstance];
//        [session setCategory:AVAudioSessionCategoryPlayback error:&error];
        [session setCategory:AVAudioSessionCategoryAmbient error:&error];
        
        NSAssert(error == nil, @"Failed to set audio session category.");
        
        [session setActive:YES error:&error];
        NSAssert(error == nil, @"Failed to activate audio session.");
        
        // Initialize FISoundEngine
        [FISoundEngine sharedEngine];
        
        // Initiialize OpenAL Audio Buffer
        _alSoundBuffer = [NSMutableDictionary new];
        
        // Initialize our currently playing AL sound pointer array
        self.currentlyPlayingALSounds = [NSMutableArray new];
    }
    return  self;
}

#pragma mark - AL audio buffer
+ (void)loadALSound:(NSString *)soundFile maxPolyphony:(NSUInteger)maxPolyphone {
    [[SimpleSoundPlayer sharedInstance] loadALSound:soundFile maxPolyphony:maxPolyphone];
}

+ (void)removeALSound:(NSString *)soundFile {
    [[SimpleSoundPlayer sharedInstance] removeALSound:soundFile];
}

+ (void)clearALSoundBuffer {
    [[SimpleSoundPlayer sharedInstance] clearALSoundBuffer];
}

- (void)loadALSound:(NSString *)soundFile maxPolyphony:(NSUInteger)maxPolyphone {
    NSAssert(soundFile || [soundFile length] > 0, @"ERROR: loadALSound - cannot play a blank or nil file");
    
    if (![[soundFile pathExtension] isEqualToString:@"wav"]) {
        NSLog(@"SimpleSoundPlayer: ERROR - Cannot play sound with %@ extension, only wav files are supported", [soundFile lastPathComponent]);
        return;
    }

    FISound *alSound = [_alSoundBuffer objectForKey:soundFile];

    // If we are loading the sound with different maxPolyphone then reload it
    if (alSound) {
        if (alSound.maxPolyphony != maxPolyphone) {
            [_alSoundBuffer removeObjectForKey:soundFile];
            alSound = nil;
        }
    }
    
    if (!alSound) {
        alSound = [[FISoundEngine sharedEngine] soundNamed:soundFile maxPolyphony:maxPolyphone error:NULL];
        if (alSound) {
            [_alSoundBuffer setObject:alSound forKey:soundFile];
        } else {
            NSLog(@"ERROR: loadALSound - could not load sound file: %@", soundFile);
            return;
        }
    }
}

- (void)removeALSound:(NSString *)soundFile {
    [_alSoundBuffer removeObjectForKey:soundFile];
}

- (void)clearALSoundBuffer {
    [_alSoundBuffer removeAllObjects];
}

#pragma mark - AL Sound control
+ (NSTimeInterval)playALSound:(NSString *)soundFile {
    return [[SimpleSoundPlayer sharedInstance] playALSound:soundFile pitch:1.0f];
}

+ (NSTimeInterval)playALSound:(NSString *)soundFile pitch:(float)pitch {
    return [[SimpleSoundPlayer sharedInstance] playALSound:soundFile pitch:pitch];
}

- (NSTimeInterval)playALSound:(NSString *)soundFile pitch:(float)pitch {
    NSAssert(soundFile && [soundFile length] > 0, @"SimpleSoundPlayer: ERROR - Cannot play blank sound files!");
    
    if (![[soundFile pathExtension] isEqualToString:@"wav"]) {
        NSLog(@"SimpleSoundPlayer: ERROR - Cannot play sound with %@ extension, only wav files are supported", [soundFile lastPathComponent]);
        return 0;
    }
    
    FISound* sound = [_alSoundBuffer objectForKey:soundFile];
    if (sound) {
        [sound setPitch:pitch];
        [sound play];
        // NOTE: Ideally this timer should be added to an NSArray and should be an MSWeakTimer because I don't know what happens if this class is dealloc'd then the timer fires and tries to run the method, it's not really an issue in this case because the class is a singleton but it's good practise to just add it to an array
        // Kick off a (strong) timer to fire when the sound finishes so that we can know when it's finished playing
        [NSTimer scheduledTimerWithTimeInterval:sound.duration target:self selector:@selector(alSoundDidFinish:) userInfo:@{@"sound": sound} repeats:NO];
        return sound.duration;
    } else {
        NSLog(@"SimpleSoundPlayer: WARNING - %@ OpenAL sound not loaded to buffer yet, auto load into buffer with maxPolyphone = 1", soundFile);
        [self loadALSound:soundFile maxPolyphony:1];
        return [self playALSound:soundFile pitch:pitch];
    }
}

+ (void)playALSoundQueue: (NSArray *)soundQueue withDelay:(NSTimeInterval)delay {
    [SimpleSoundPlayer playALSoundQueue:soundQueue withDelay:delay appendQueue:NO];
}

+ (void)playALSoundQueue: (NSArray *)soundQueue withDelay:(NSTimeInterval)delay appendQueue:(bool)appendQueue {
    SimpleSoundPlayer *soundPlayer = [SimpleSoundPlayer sharedInstance];
    if (!appendQueue) {
        if (soundPlayer.soundQueuePlayingTimer) {
            [soundPlayer.soundQueuePlayingTimer invalidate];
        }
        [soundPlayer stopAllCurrentlyPlayingALSounds];
        soundPlayer.soundQueueDelay = delay;
        soundPlayer.soundQueue = soundQueue.mutableCopy;
        [soundPlayer playNextALSoundFromQueue];
    } else {
        [soundPlayer.soundQueue addObjectsFromArray:soundQueue];
    }
}

- (void)playNextALSoundFromQueue {
    NSTimeInterval length = [self playALSound:[self.soundQueue dequeue] pitch:1.0f] + self.soundQueueDelay;
    if ([self.soundQueue count] > 0) {
        self.soundQueuePlayingTimer = [MSWeakTimer scheduledTimerWithTimeInterval:length target:self selector:@selector(playNextALSoundFromQueue) userInfo:nil repeats:NO dispatchQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    }
}

// When the alSound finishes playing, remove it from our currently playing sounds queue
-(void) alSoundDidFinish:(NSTimer *)timer {
    if (!timer.userInfo ||
        [timer.userInfo count] == 0 ||
        !timer.userInfo[@"sound"] ||
        ![timer.userInfo[@"sound"] isKindOfClass:[FISound class]] ||
        !self.currentlyPlayingALSounds ||
        [self.currentlyPlayingALSounds count] == 0) {
        return;
    }
    
    [self.currentlyPlayingALSounds removeObject:timer.userInfo[@"sound"]];
}

#pragma mark - AV Sound control
//+ (AVAudioPlayer *)playSoundArray:(NSArray *)soundFileArray {
//    return [SimpleSoundPlayer playSound:soundFile repeats:NO];
//}

+ (AVAudioPlayer *)playSoundQueue: (NSArray *)soundQueue {
    return [[SimpleSoundPlayer sharedInstance] playSoundQueue:soundQueue appendQueue:NO];
}

+ (AVAudioPlayer *)playSoundQueue: (NSArray *)soundQueue appendQueue:(bool)appendQueue {
    return [[SimpleSoundPlayer sharedInstance] playSoundQueue:soundQueue appendQueue:appendQueue];
}

- (AVAudioPlayer *)playSoundQueue: (NSArray *)soundQueue appendQueue:(bool)appendQueue {
    if (appendQueue) {
        self.soundQueue = [[self.soundQueue arrayByAddingObjectsFromArray:soundQueue] mutableCopy];
    } else {
        self.soundQueue = [soundQueue mutableCopy];
    }
    
    return [self playSound:[self.soundQueue dequeue] repeats:NO];
}

+ (AVAudioPlayer *)playSound: (NSString *)soundFile {
    return [SimpleSoundPlayer playSound:soundFile repeats:NO];
}

+ (AVAudioPlayer *)playSound: (NSString *)soundFile repeats:(bool)repeats {
    return [[self sharedInstance] playSound:soundFile repeats:repeats];
}

+ (AVAudioPlayer *)playSound: (NSString *)soundFile delegate:(id)someDelegate {
    return [self playSound:soundFile];
}

- (AVAudioPlayer *)playSound: (NSString *)soundFile {
    return [self playSound:soundFile repeats:NO];
}

- (AVAudioPlayer *)playSound: (NSString *)soundFile repeats:(bool)repeats {
    NSURL *url = [[NSBundle mainBundle] URLForResource:soundFile withExtension: nil];
    if (!url) {
        NSLog(@"SimpleSoundPlayer: ERROR - Cannot find sound file %@", soundFile);
    }
    
    [self.audioPlayer stop];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: nil];
    [self.audioPlayer setDelegate: self];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
    
    if (repeats) {
        self.audioPlayer.numberOfLoops = -1;
    }

    //[self.audioPlayers addObject: audioPlayer];
    
    return self.audioPlayer;
}

+ (double) soundDuration:(NSString *)soundFile {
    NSURL *url = [[NSBundle mainBundle] URLForResource:soundFile withExtension: nil];
    if (!url) {
        NSLog(@"SimpleSoundPlayer: ERROR - Cannot find sound file %@", soundFile);
    }
    
    NSError *error;
    // Create a new AVAudioPlayer so we don't interrupt the current one
    AVAudioPlayer* avAudioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];

    return avAudioPlayer.duration;
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    //[self.audioPlayers removeObject: player];
    
    if ([self.soundQueue count] > 0) {
        [self playSound:[self.soundQueue dequeue]];
    } else {
        if ([self.delegate respondsToSelector:@selector(simpleSoundPlayer:soundDidFinish:)]) {
            [self.delegate simpleSoundPlayer:self soundDidFinish:[[player.url path] lastPathComponent]];
        }
    }
}

+ (void)stopAll {
    [[SimpleSoundPlayer sharedInstance] stopAll];
}

- (void)stopAll {
    for(int i = 0; i < self.audioPlayers.count; i++){
        AVAudioPlayer *player = [self.audioPlayers objectAtIndex: i];
        [player stop];
    }
    [self.audioPlayers removeAllObjects];
    [self.repeatTimer invalidate];
    self.repeatTimer = nil;
    if(self.audioPlayer) {
        [self.audioPlayer stop];
    }
    // Stop AL files
    if (self.soundQueuePlayingTimer) {
        [self.soundQueuePlayingTimer invalidate];
    }
    // Clear AL audio queue
    [self.soundQueue removeAllObjects];
    [self stopAllCurrentlyPlayingALSounds];
}

- (void) stopAllCurrentlyPlayingALSounds {
    if ([self.currentlyPlayingALSounds count] > 0) {
        for (FISound *sound in self.currentlyPlayingALSounds) {
            if (sound != nil) {
                [sound stop];
            }
        }
        [self.currentlyPlayingALSounds removeAllObjects];
    }
}

#pragma mark - Vibrate
+ (void)vibratePhone {
    [[SimpleSoundPlayer sharedInstance] vibratePhone];
}

+ (void)vibratePhoneWithRepeatingInterval:(NSTimeInterval)repeatInterval {
    [[SimpleSoundPlayer sharedInstance] vibratePhoneWithRepeatingInterval:repeatInterval];
}

- (void)vibratePhone {
    if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    } else {
        //NSLog(@"SimpleSoundPlayer: Cannot vibrate because this device does not support it");
        [self stopVibrating];
    }
}

- (void)vibratePhoneWithRepeatingInterval:(NSTimeInterval)repeatInterval {
    [self.repeatTimer invalidate];
    self.repeatTimer = nil;
    [self vibratePhone];
    if (repeatInterval > 0) {
        self.repeatTimer = [MSWeakTimer scheduledTimerWithTimeInterval:repeatInterval target:self selector:@selector(vibratePhone) userInfo:nil repeats:YES dispatchQueue:dispatch_get_main_queue()];
    }
}

- (void)stopVibrating {
    [self.repeatTimer invalidate];
    self.repeatTimer = nil;
}

+ (void)stopVibrating {
    [[SimpleSoundPlayer sharedInstance] stopVibrating];
}

@end
