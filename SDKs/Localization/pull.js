#!/usr/bin/env node

var S = require('string'),
	keychain = require('keychain'),
	argv = require('optimist').argv,
	async = require('async'),
	path = require('path'),
	util = require('util'),
	fs  = require("fs");

var languages = {
				'en': {
					'name': 'English',
					'offset': 1
				},
				'jp': {
					'name': 'Japanese',
					'offset': 2
				},
				'zh_cn': {
					'name': 'Chinese (Simplified)',
					'offset': 3
				},
				'de': {
					'name': 'German',
					'offset': 4
				},
				'it': {
					'name': 'Italian',
					'offset': 5
				}
			};


var stringsFileName = 'strings_';
var outputDir = "textfiles";
var googleDocKey = "1c-T-Fj9LM3_36JAgX_EZUzCZSIM4odhFeKynMS9jxP0";

var GoogleClientLogin = require("googleclientlogin").GoogleClientLogin;
var GoogleSpreadsheets = require("google-spreadsheets");

var username = 'katypun@wowwee.com.hk';

var languageData = {};
for (var langCode in languages) {
	if (languages.hasOwnProperty(langCode)) {
		languageData[langCode] = [];
	}
}

String.prototype.replaceAll = function (find, replace) {
	var str = this;
	return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
};

function getLanguageCodeForRowNumber(rowNumber) {
	for (var langCode in languages) {
		if (languages.hasOwnProperty(langCode)) {
			if (languages[langCode].offset == (rowNumber - 1)) {
				return langCode;
			}
		}
	}
	return null;
}

function checkValue(value) {
	if (value.slice(-1) == '"' && value.substr(0, 1) == '"') {
		value = value.substr(1, (value.length - 1));
	}

	//value = value.replaceAll('"', '\"');

	if (value.indexOf('"') != -1) {
		value = value.replace('"','\\"');
	}

	return value;
}

function checkAndDeleteFile(filePath, callback) {
	 if (fs.existsSync(filePath)) {
	    fs.unlinkSync(filePath);
	}

	callback();
}

// Read username/password from keychain
keychain.getPassword({ account: username, service: 'Google Docs' }, function(err, pass) {

	var googleAuth = new GoogleClientLogin({
	  email: username,
	  password: pass,
	  service: 'spreadsheets',
	  accountType: GoogleClientLogin.accountTypes.google
	});

	var range = util.format('A1:F300');

	googleAuth.on(GoogleClientLogin.events.login, function(){
	    GoogleSpreadsheets({
	        key: googleDocKey,
	        auth: googleAuth.getAuthId()
	    }, function(err, spreadsheet) {
	        spreadsheet.worksheets[0].cells({
	            range: range
	        }, function(err, cells) {
				for (var rowNumber in cells['cells']) {
					if (rowNumber == '1') {
						continue;
					}

				    if (cells['cells'].hasOwnProperty(rowNumber)) {
						var row = cells['cells'][rowNumber];
						for (var columnNumber in row) {
							if (row.hasOwnProperty(columnNumber)) {
								var languageCode = getLanguageCodeForRowNumber(columnNumber);
								if (languageCode == null) {
									continue;
								}
								var value = checkValue(row[columnNumber]['value']);
								languageData[languageCode].push({'value': value, 'key': row['1']['value']});
							}
						}
				    }
				}



				for (var langCode in languageData) {
					if (languageData.hasOwnProperty(langCode)) {

						console.log(langCode);

						var fileName = util.format('%s%s.txt', stringsFileName, langCode);
						var filePath = path.join(__dirname, outputDir, fileName);

						checkAndDeleteFile(filePath, function() {
							console.log(filePath);

							var writeLine = "";

							for (var i = 0; i < languageData[langCode].length; i++) {
								var line = languageData[langCode][i];
							    writeLine += util.format("\"%s\" = \"%s\";\n", line.key, line.value);


							}

							fs.writeFileSync(filePath, writeLine);
							console.log('Successfully wrote file',fileName);
						});
					}
				}


	        });
	    });
	});

	googleAuth.login();

});
