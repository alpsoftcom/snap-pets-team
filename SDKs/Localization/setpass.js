#!/usr/bin/env node

var argv = require('optimist').argv,
    keychain = require('keychain'),
    read = require('read');

var serviceName = 'Google Docs';

console.log('##### Set Google Docs Keychain User/Pass ######');

read({ prompt: 'Google Docs Email: ', silent: false }, function(err, username) {
	if (err) {
		return console.log(err);
	}

	read({ prompt: 'Google Docs Password: ', silent: true }, function(err, password) {
		if (err) {
			return console.log(err);
		}

		keychain.setPassword({ account: username, service: serviceName, password: password}, function(err) {
			if (err) {
				return console.log(err);
			}
			console.log('Password successfully set');
		});
	});
});
