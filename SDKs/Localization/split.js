#!/usr/bin/env node

var S = require('string'),
	LineByLineReader = require('line-by-line'),
	keychain = require('keychain'),
	argv = require('optimist').argv,
	Spreadsheet = require('edit-google-spreadsheet'),
	async = require('async'),
	node_find_files = require("node-find-files"),
	path = require('path');

var output = [];
var languages = {
					'en': {
						'name': 'English',
						'offset': 0
					}
				};

var inputFile = "strings_en.txt";
var googleDocKey = "1c-T-Fj9LM3_36JAgX_EZUzCZSIM4odhFeKynMS9jxP0";

function getStringsFromFile(inputFile, callback) {
	console.log('Reading file',inputFile);

	// Read this file
	var lr = new LineByLineReader(inputFile);

	lr.on('error', function (err) {
	    // 'err' contains error object
	});

	lr.on('line', function (rawLine) {
		var line = S(rawLine).trim();
		// Check for comments
		if (line.startsWith('/*') || line.startsWith('//')) {
			return;
		}
		// Check for blank lines
		if (line.isEmpty()) {
			return;
		}

		var splitArray = line.s.split('=');

		for (var i = 0; i < splitArray.length; i++) {
			splitArray[i] = S(splitArray[i]).trim().between('"', '"').s;
		}

	    output.push(splitArray);
	});

	lr.on('end', function () {
		callback(null, output);
	});
}

function getAllTranslationFiles(directory, extension, callback) {
	var finder = new node_find_files({
	    rootFolder : path.join(__dirname, directory),
	    filterFunction : function (filePath, stat) {
			return (path.extname(filePath) == ".txt" && S(path.basename(filePath)).left(1) != '.') ? true : false;
		}
	});

	var files = [];

	finder.on("match", function(strPath, stat) {
		var basename = path.basename(strPath, path.extname(strPath));

		var split = basename.split('_');
		if (split.length == 2) {
			files.push([split[1], split[0], path.extname(strPath), strPath]);
		}
	});

	finder.on("complete", function() {
	    callback(null, files);
	});

	finder.on("patherror", function(err, strPath) {
	    callback("Error for Path " + strPath + " " + err);  // Note that an error in accessing a particular file does not stop the whole show
	});

	finder.on("error", function(err) {
	    callback("Global Error " + err);
	});

	finder.startSearch();
}

function writeToGoogleDocs(outputArray, columnOffset, outputGoogleDocKey, callback) {
	var username = 'andysavage@wowwee.com.hk';

	// Read username/password from keychain
	keychain.getPassword({ account: username, service: 'Google Docs' }, function(err, pass) {
		if (err) {
			return callback(err);
		}

		Spreadsheet.load({
			debug: true,
			spreadsheetId: outputGoogleDocKey,
			worksheetId: '0',
			// Choose from 1 of the 3 authentication methods:
			//    1. Username and Password
			username: username,
			password: pass
			}, function sheetReady(err, spreadsheet) {
			  if(err) {
			  	return callback(err);
			  }

			  	var i = 2;

				async.each(output, function( line, callback) {
					var data = {};
					var langColumn = 2;
					data[i] = {
						1: line[0],
					};
					data[i][langColumn] = line[1];
					console.log(data);
					spreadsheet.add(data);
					i++;
					callback();
				}, function(err){
					spreadsheet.send(function(err) {
						callback(err);
				    });
				});
			});
		});
}

getAllTranslationFiles('textfiles', 'txt', function(err, files) {

	// Loop through each file
	for (var i = 0; i < files.length; i++) {
		var langCode = files[i][0];
		var langName = languages[langCode]['name'];
		var langOffset = languages[langCode]['offset'];
		var file = files[i][3];

		if (languages.hasOwnProperty(langCode)) {
			console.log('Processing',langName,'language');
			getStringsFromFile(file, function(err, strings) {
				writeToGoogleDocs(strings, langOffset, googleDocKey, function(err) {
					if (err) {
						return console.log(err);
					}
					console.log('Successfully written',langName,'to google docs');
				});
			});
		} else {
			console.log('Found a file for',files[i][0],'but it is not in our supported languages list');
		}
	}

	//writeToGoogleDocs(output);
});
