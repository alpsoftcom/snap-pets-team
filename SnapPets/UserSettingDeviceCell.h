//
//  UserSettingDeviceCell.h
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>


@interface UserSettingDeviceCell : UITableViewCell {
    
}

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UIView *viewBase;

@property (atomic, strong) IBOutlet UIImageView* viewImg1;
@property (atomic, strong) IBOutlet UIImageView* viewImg2;
@property (atomic, strong) IBOutlet UIImageView* viewImg3;

@property (readwrite) NSInteger colorID;

@end
