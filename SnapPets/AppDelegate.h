//
//  AppDelegate.h
//  MipPhotoTest
//
//  Created by Forrest Chan on 25/4/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoDownloader.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, PhotoDownloaderDelegate> {
    
}

- (void)startScan;

@property (strong, nonatomic) UIWindow *window;
@property (readwrite) UIBackgroundTaskIdentifier backgroundTask;
@property (strong, nonatomic) NSTimer* backgroundTimer;

@end
