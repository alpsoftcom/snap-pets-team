//
//  DeviceCell.h
//  MipPhotoTest
//
//  Created by Forrest Chan on 26/9/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
//#import "SnappetRobot.h"

#define DeviceCellHeight    80.0f

@interface DeviceCell : UITableViewCell

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) SnappetRobot *snappet;
@property (nonatomic, strong) UIView* colorView;

@end
