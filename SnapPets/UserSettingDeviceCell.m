//
//  UserSettingDeviceCell.m
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "UserSettingDeviceCell.h"

@implementation UserSettingDeviceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
        [self setBackgroundColor:[UIColor clearColor]];
        UIFont* font = [UIFont systemFontOfSize:34.0f];
        self.nameLabel.font = font;
    }
    
    self.viewBase.layer.cornerRadius = 10.0f;
    
    NSString* str1 = nil;
    NSString* str2 = nil;
    NSString* str3 = nil;
    NSString* strDevice = @"";
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
        strDevice = @"~ipad";
    }
    
    switch (self.colorID) {
        case kSnappetColorBlue:
            str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme3@2x%@.png", strDevice];
            str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme3@2x%@.png", strDevice];
            str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme3@2x%@.png", strDevice];
            break;
        case kSnappetColorPink:
            str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1@2x%@.png", strDevice];
            str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme1@2x%@.png", strDevice];
            str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme1@2x%@.png", strDevice];
            break;
        case kSnappetColorOrage:
            str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme4@2x%@.png", strDevice];
            str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme4@2x%@.png", strDevice];
            str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme4@2x%@.png", strDevice];
            break;
        case kSnappetColorSilver:
            str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme2@2x%@.png", strDevice];
            str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme2@2x%@.png", strDevice];
            str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme2@2x%@.png", strDevice];
            break;
        case kSnappetAquaBlue:
            str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme5@2x%@.png", strDevice];
            str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme5@2x%@.png", strDevice];
            str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme5@2x%@.png", strDevice];
            break;
        default:
            str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1@2x%@.png", strDevice];
            str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme1@2x%@.png", strDevice];
            str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme1@2x%@.png", strDevice];
            break;
    }
    [self.viewImg1 setImage:[UIImage imageNamed:str1]];
    [self.viewImg2 setImage:[UIImage imageNamed:str2]];
    [self.viewImg3 setImage:[UIImage imageNamed:str3]];
    [self.viewImg1 setHidden:NO];
    [self.viewImg2 setHidden:YES];
    [self.viewImg3 setHidden:YES];
}

@end
