//
//  MagnetBorderView.h
//  Zombiegram_iOS
//
//  Created by sailim on 22/11/12.
//  Copyright (c) 2012 Mangrove Giant Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MagnetBorderView : UIView {
    
    float scale;
    
}

- (void)setScale:(float)s;

@end
