//
//  MagnetScrollView.m
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import "MagnetScrollView.h"
#import "UIImageViewAddition.h"
#import "UIButtonAddition.h"

@implementation MagnetScrollView

@synthesize magnetSet, magnetType, delegate;

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (id)initWithSet:(int)mSet Type:(int)mType
{
    CGRect rect = [[UIScreen mainScreen] applicationFrame];
    if (rect.origin.y+rect.size.height == 1024 || rect.origin.y+rect.size.height == 2048)
        self = [super initWithFrame:CGRectMake(0, 0, 85*2, 85*2)];
    else
        self = [super initWithFrame:CGRectMake(0, 0, 85, 85)];
    
    self.backgroundColor = [UIColor clearColor];
    
    bgImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stickerbackground.png"]];
    bgImg.contentMode = UIViewContentModeScaleToFill;
    bgImg.frame = (CGRect){bgImg.frame.origin, 85, 85};
    [self addSubview:bgImg];
    
    [self loadWithSet:mSet Type:mType];
    
    return self;
}

- (void)loadWithSet:(int)mSet Type:(int)mType
{
    //thumbnail
    
    magnetType = mType;
    magnetSet = mSet;
    UIImage* magnetImg;
    NSArray *stickerSetPrefixName = @[@"48147048_", @"43980651_", @"56210080_"];
    magnetType = mType;
    NSString *resName = [NSString stringWithFormat:@"%@%d@2x.png",[stickerSetPrefixName objectAtIndex:mSet] ,magnetType+1];
    magnetImg = [UIImage imageNamed:resName];
    
//    switch (magnetType) {
//        case Sticker1:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_1.png"];
//            break;
//        case Sticker2:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_2.png"];
//            break;
//        case Sticker3:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_3.png"];
//            break;
//        case Sticker4:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_4.png"];
//            break;
//        case Sticker5:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_5.png"];
//            break;
//        case Sticker6:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_6.png"];
//            break;
//        case Sticker7:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_7.png"];
//            break;
//        case Sticker8:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_8.png"];
//            break;
//        case Sticker9:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_9.png"];
//            break;
//        case Sticker10:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_10.png"];
//            break;
//        case Sticker11:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_11.png"];
//            break;
//        case Sticker12:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_12.png"];
//            break;
//        case Sticker13:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_13.png"];
//            break;
//        case Sticker14:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_14.png"];
//            break;
//        case Sticker15:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_15.png"];
//            break;
//        case Sticker16:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_16.png"];
//            break;
//        case Sticker17:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_17.png"];
//            break;
//        case Sticker18:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_18.png"];
//            break;
//        case Sticker19:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_19.png"];
//            break;
//        case Sticker20:
//            magnetImg = [UIImage imageNamed:@"sticker_thumb_20.png"];
//            break;       
//    }
    
//    magnetView = [[UIImageView alloc] initWithImage:magnetImg];
//    magnetView.contentMode = UIViewContentModeScaleAspectFit;
    magnetView = [UIButton buttonWithImage:magnetImg target:self sel:@selector(magnetAction:)];
    magnetView.contentMode = UIViewContentModeScaleAspectFill;
    magnetView.frame = CGRectMake(5, 0, bgImg.frame.size.width - 10, bgImg.frame.size.height);
//    magnetView.transform = CGAffineTransformMakeScale(0.8f, 0.8f);
    
    [self addSubview:magnetView];    
}

- (void)setButtonEnabled:(BOOL)isE
{
    isEnabled = isE;
    [magnetView setEnabled:isEnabled];
}

#pragma mark - positions
- (void)setPosition:(CGPoint)position
{
    self.frame = CGRectMake(position.x, position.y, self.frame.size.width, self.frame.size.height);
}

- (BOOL)touchOnViewWithPos:(CGPoint)p 
{
    return CGRectContainsPoint(self.frame, p);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



- (void)magnetAction:(id)sender
{
    [delegate shouldAddMagnet:self];
}

//#pragma mark - handle touches
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [super touchesBegan:touches withEvent:event];
//    CGPoint pos = [[touches anyObject] locationInView:self];
//   
//    NSLog(@"Touch on scroll item");
//}
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [super touchesMoved:touches withEvent:event];
//    CGPoint pos = [[touches anyObject] locationInView:self];
//    
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [super touchesEnded:touches withEvent:event];
//    
//}

@end
