//
//  MagnetItemView.m
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import "MagnetItemView.h"
#import "UIImageViewAddition.h"

@implementation MagnetItemView

@synthesize magnetData, orientation, delegate, scale;

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (id)initWithSet:(int)mSet Type:(int)mType
{
    self = [super initWithFrame:CGRectZero];
    
    self.backgroundColor = [UIColor clearColor];
    
    [self loadWithSet:mSet Type:mType];
    
    return self;
}

- (id)initWithMagnetData:(MagnetData*)mData
{
    self = [super initWithFrame:CGRectZero];
    
    self.backgroundColor = [UIColor clearColor];
//    self.backgroundColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.5f];
    
    self.multipleTouchEnabled = true;
    mLastScale = 1.0f;
    
    self.magnetData = mData;
    
    [self loadWithSet:mData.magnetSet Type:magnetData.magnetType];
    
    [self setPosition:CGPointMake(magnetData.posX, magnetData.posY)];
    
    [self setOrientation:magnetData.orientation];
    
    // pinch gesture
    UIPinchGestureRecognizer* pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self addGestureRecognizer:pinchGesture];
    pinchGesture.delegate = self;
//    [pinchGesture release];
    
    // rotation gesture
    UIRotationGestureRecognizer* rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
    [self addGestureRecognizer:rotationRecognizer];
    rotationRecognizer.delegate = self;
//    [rotationRecognizer release];
    
    return self;
}

- (void)loadWithSet:(int)mSet Type:(int)mType
{
    NSArray *stickerSetPrefixName = @[@"48147048_", @"43980651_", @"56210080_"];
    magnetType = mType;
    UIImage* magnetImg;
    NSString *resName = [NSString stringWithFormat:@"%@%d@2x.png",[stickerSetPrefixName objectAtIndex:mSet] ,magnetType+1];
    magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:resName ofType:nil]];
//    switch (magnetType) {
//        case Sticker1:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker1@2x.png" ofType:nil]];
//            break;
//        case Sticker2:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker2@2x.png" ofType:nil]];
//            break;
//        case Sticker3:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker3@2x.png" ofType:nil]];
//            break;
//        case Sticker4:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker4@2x.png" ofType:nil]];
//            break;
//        case Sticker5:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker5@2x.png" ofType:nil]];
//            break;
//        case Sticker6:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker6@2x.png" ofType:nil]];
//            break;
//        case Sticker7:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker7@2x.png" ofType:nil]];
//            break;
//        case Sticker8:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker8@2x.png" ofType:nil]];
//            break;
//        case Sticker9:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker9@2x.png" ofType:nil]];
//            break;
//        case Sticker10:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker10@2x.png" ofType:nil]];
//            break;
//        case Sticker11:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker11@2x.png" ofType:nil]];
//            break;
//        case Sticker12:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker12@2x.png" ofType:nil]];
//            break;
//        case Sticker13:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker13@2x.png" ofType:nil]];
//            break;
//        case Sticker14:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker14@2x.png" ofType:nil]];
//            break;
//        case Sticker15:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker15@2x.png" ofType:nil]];
//            break;
//        case Sticker16:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker16@2x.png" ofType:nil]];
//            break;
//        case Sticker17:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker17@2x.png" ofType:nil]];
//            break;
//        case Sticker18:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker18@2x.png" ofType:nil]];
//            break;
//        case Sticker19:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker19@2x.png" ofType:nil]];
//            break;
//        case Sticker20:
//            magnetImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sticker20@2x.png" ofType:nil]];
//            break;
//    }
    
//    float longerEdge = magnetImg.size.width > magnetImg.size.height ? magnetImg.size.width/2 : magnetImg.size.height/2;
    float longerEdge = magnetImg.size.width > magnetImg.size.height ? magnetImg.size.width/2 : magnetImg.size.height/2;
    longerEdge += 12;
    self.frame = CGRectMake(-6, -6, longerEdge, longerEdge);
    
    // border view
    borderView = [[MagnetBorderView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:borderView];
    
    // image content
    magnetView = [[UIImageView alloc] initWithImage:magnetImg];
//    [magnetView setPosition:CGPointMake(self.frame.size.width/2 - [magnetView width]/2, self.frame.size.height/2 - [magnetView height]/2)];
    [magnetView setFrame:CGRectMake(self.frame.size.width/2-magnetImg.size.width/4,self.frame.size.height/2-magnetImg.size.height/4, magnetImg.size.width/2, magnetImg.size.height/2)];
    magnetView.userInteractionEnabled = true;
    [self addSubview:magnetView];
    
    // overlay view
    overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    overlayView.backgroundColor = [UIColor clearColor];
    [self addSubview:overlayView];
    
    // scale icon
    scaleIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_scale.png"]];
    scaleIcon.backgroundColor = [UIColor clearColor];
    scaleIcon.userInteractionEnabled = true;
    [scaleIcon setFrame:CGRectMake(0, 0, 30, 30)];
    [scaleIcon setPosition:CGPointMake(self.frame.size.width - scaleIcon.frame.size.width, self.frame.size.height - scaleIcon.frame.size.height)];
    [overlayView addSubview:scaleIcon];
    
    // close button
    btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClose setFrame:CGRectMake(0, 0, 30, 30)];
    [btnClose setImage:[UIImage imageNamed:@"btn_close"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [overlayView addSubview:btnClose];
    
    // set default scale
    scale = 1.0f;
    
    isEditMode = true;
}

- (void)setEditMode:(BOOL)m
{
    if(isEditMode != m)
    {
        isEditMode = m;
        if(isEditMode)
        {
//            circleView.alpha = 1.0f;
            overlayView.hidden = false;
            borderView.hidden = false;
        }
        else
        {
//            circleView.alpha = 0.0f;
            overlayView.hidden = true;
            borderView.hidden = true;
        }
    }
}

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    CGPoint p = [overlayView convertPoint:point fromView:self];
//    if(CGRectContainsPoint(btnClose.frame, p))
//    {
//        return btnClose;
//    }
////    else if(CGRectContainsPoint(overlayView.frame, p))
////    {
////        return self;
////    }
//
//    return self;
//}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGPoint p = [overlayView convertPoint:point fromView:self];
    if(CGRectContainsPoint(btnClose.frame, p))
    {
        return btnClose;
    }
    else if(CGRectContainsPoint(overlayView.frame, p))
    {
        return self;
    }
    
    return nil;
}

-(void)handlePinch:(UIPinchGestureRecognizer*)sender {
    
    if(isEditMode)
    {
        /*
         There are different actions to take for the different states of the gesture recognizer.
         * In the Began state, use the pinch location to find the index path of the row with which the pinch is associated, and keep a reference to that in pinchedIndexPath. Then get the current height of that row, and store as the initial pinch height. Finally, update the scale for the pinched row.
         * In the Changed state, update the scale for the pinched row (identified by pinchedIndexPath).
         * In the Ended or Canceled state, set the pinchedIndexPath property to nil.
         */
    //    NSLog(@"latscale = %f", [sender scale]);
        
        float applyScale = 1.0f + ([sender scale] - mLastScale);
        mLastScale = [sender scale];
        
        if (sender.state == UIGestureRecognizerStateEnded)
        {
            mLastScale = 1.0;
        }
        
        [self setScaleAndOrientation:orientation appendScale:applyScale];
    }
}

- (void)handleRotation:(UIRotationGestureRecognizer*)sender
{
//    NSLog(@"handle rotation %.2f", [sender rotation]);
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        pinchOriginalOrientation = orientation;
    }
    float newOrientation = pinchOriginalOrientation + [sender rotation] * 180 / M_PI;
    [self setScaleAndOrientation:newOrientation appendScale:1.0f];
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

#pragma mark - positions
- (CGPoint)position
{
    return self.frame.origin;
}

- (void)setPosition:(CGPoint)position
{
    self.frame = CGRectMake(position.x, position.y, self.frame.size.width, self.frame.size.height);
}

- (void)setOrientation:(float)ori
{
    orientation = ori;
    
//    CGAffineTransform transform = CGAffineTransformConcat(CGAffineTransformMakeRotation((orientation/180.0f)*M_PI), CGAffineTransformMakeTranslation(50, -260));
    CGAffineTransform transform = CGAffineTransformMakeRotation((orientation/180.0f)*M_PI);
//    self.transform = transform;
    magnetView.transform = transform;
    overlayView.transform = transform;
    borderView.transform = transform;
}

- (void)appendScale:(float)s
{
    scale *= s;
    
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    
    magnetView.transform = transform;
    overlayView.transform = transform;
    borderView.transform = transform;
    
    [borderView setScale:(1.0f/scale)];
}

- (void)setScaleAndOrientation:(float)ori appendScale:(float)s
{
    orientation = ori;
    scale *= s;
    CGAffineTransform transform = CGAffineTransformConcat(CGAffineTransformMakeRotation((orientation/180.0f)*M_PI), CGAffineTransformMakeScale(scale, scale));
    magnetView.transform = transform;
    overlayView.transform = transform;
    borderView.transform = transform;
    
    float buttonScale = 1.0f/scale;
    CGAffineTransform buttonTransform = CGAffineTransformMakeScale(buttonScale, buttonScale);
    scaleIcon.transform = buttonTransform;
    btnClose.transform = buttonTransform;
    
    [borderView setScale:buttonScale];
}

- (int)touchOnViewWithPos:(CGPoint)p withEvent:(UIEvent *)event
{
    
//    CGRect scaleIconRect = CGRectMake(self.frame.origin.x + scaleIcon.frame.origin.x,
//                                      self.frame.origin.y-5 + scaleIcon.frame.origin.y-5,
//                                      scaleIcon.frame.size.width+10,
//                                      scaleIcon.frame.size.height+10);
        
    
    
    
    if([scaleIcon pointInside:[scaleIcon convertPoint:p fromView:self.superview] withEvent:event])
//    if([scaleIcon pointInside:p withEvent:event])
//     if([self hitTest:p withEvent:event] == scaleIcon)
//    if(CGRectContainsPoint(scaleIconRect, p))
    {
        if(isEditMode)
        {
            return MagnetTouchOnCircle;
        }
        return MagnetTouchOnItem;
    }
//    else if(CGRectContainsPoint(self.frame, p))
    else if([magnetView pointInside:[magnetView convertPoint:p fromView:self.superview] withEvent:event])
    {
        return MagnetTouchOnItem;
    }
    
//    CGPoint circleMid = CGPointMake(self.frame.origin.x + self.frame.size.width/2, self.frame.origin.y + self.frame.size.height/2);
//    double distance_x = circleMid.x - p.x;
//    double distance_y = circleMid.y - p.y;
//    float distance = sqrt( (distance_x * distance_x) + (distance_y * distance_y));
    
//    if(ABS(distance) < (self.frame.size.width/2 - 9))
//    {
//        return MagnetTouchOnItem;
//    }
//    else if(CGRectContainsPoint(self.frame, p))
//    {
//        if(isEditMode)
//        {
//            return MagnetTouchOnCircle;
//        }
//        return MagnetTouchOnItem;
//    }
    return MagnetTouchOnNone;
}

- (float)getAngleWithPos:(CGPoint)p
{
    CGPoint circleMid = CGPointMake(self.frame.origin.x + self.frame.size.width/2, self.frame.origin.y + self.frame.size.height/2);
    float angle = atan2f(p.y - circleMid.y, p.x - circleMid.x) * 180 / M_PI;
    if(angle < 0)
    {
        angle = 180 + (180 + angle);
    }
    return angle;
}

- (float)getDistanceWithPoint:(CGPoint)pA
{
    CGPoint pB = CGPointMake(self.frame.origin.x + self.frame.size.width/2,
                             self.frame.origin.y + self.frame.size.height/2);
    
    double distance_x = ABS(pA.x - pB.x);
    double distance_y = ABS(pA.y - pB.y);
    float distance = sqrt( (distance_x * distance_x) + (distance_y * distance_y));
    return distance;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    float radius = 5.0f;
//    
//    // Drawing code
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetRGBFillColor(context, 0,0,0,0.75);
//    CGContextSetLineWidth(context, 2.0f);
//    
//    float offset = 8.0f;
//    rect = CGRectMake(rect.origin.x + offset, rect.origin.y + offset, rect.size.width - (2*offset), rect.size.height - (2*offset));
//    
//    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + radius);
//    CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - radius);
//    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + rect.size.height - radius,
//                    radius, M_PI, M_PI / 2, 1); //STS fixed
//    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - radius,
//                            rect.origin.y + rect.size.height);
//    CGContextAddArc(context, rect.origin.x + rect.size.width - radius,
//                    rect.origin.y + rect.size.height - radius, radius, M_PI / 2, 0.0f, 1);
//    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + radius);
//    CGContextAddArc(context, rect.origin.x + rect.size.width - radius, rect.origin.y + radius,
//                    radius, 0.0f, -M_PI / 2, 1);
//    CGContextAddLineToPoint(context, rect.origin.x + radius, rect.origin.y);
//    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + radius, radius,
//                    -M_PI / 2, M_PI, 1);
//    
////    CGContextFillPath(context);  
//    CGContextStrokePath(context);
//}

//- (void)dealloc
//{
//    [magnetView release];
//    self.magnetData = nil;
//    [scaleIcon release];
//    scaleIcon = nil;
//    [overlayView release];
//    overlayView = nil;
//    [btnClose release];
//    btnClose = nil;
//    [borderView release];
//    borderView = nil;
//    [super dealloc];
//}

#pragma mark - Animations
- (void)startShowAnimation
{
    self.transform = CGAffineTransformMakeScale(0.01f, 0.01f);
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.5];
    
    self.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView commitAnimations];
}

- (void)startDismissAnimation
{   
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    
    self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    
    [UIView commitAnimations];
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [delegate shouldRemoveMagnet:self];
}

#pragma mark - Button actions
- (void)closeAction:(id)sender
{
    [self startDismissAnimation];
}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [super touchesBegan:touches withEvent:event];
//    NSLog(@"Magnet touch began");
//}

@end
