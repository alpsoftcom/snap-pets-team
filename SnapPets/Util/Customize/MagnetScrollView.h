//
//  MagnetScrollView.h
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnetData.h"
#import "MagnetItemView.h"

@protocol MagnetScrollViewDelegate <NSObject>

- (void)shouldAddMagnet:(id)sender;

@end

@interface MagnetScrollView : UIView {
    
    int magnetType;
        
    UIButton* magnetView;
    UIImageView* bgImg;
    
    BOOL isEnabled;
    
    id<MagnetScrollViewDelegate> delegate;
}

@property(readonly)int magnetSet;
@property(readonly)int magnetType;
@property(nonatomic, retain)id<MagnetScrollViewDelegate> delegate;

- (id)initWithSet:(int)mSet Type:(int)mType;
- (void)loadWithSet:(int)mSet Type:(int)mType;
- (void)setButtonEnabled:(BOOL)isE;

#pragma mark - positions
- (void)setPosition:(CGPoint)position;
- (BOOL)touchOnViewWithPos:(CGPoint)p;


@end
