//
//  MagnetItemView.h
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnetData.h"
#import "MagnetBorderView.h"

enum MagnetItemType {
    Sticker1 = 0,
    Sticker2,
    Sticker3,
    Sticker4,
    Sticker5,
    Sticker6,
    Sticker7,
    Sticker8,
    Sticker9,
    Sticker10,
    Sticker11,
    Sticker12,
    Sticker13,
    Sticker14,
    Sticker15,
    Sticker16,
    Sticker17,
    Sticker18,
    Sticker19,
    Sticker20,
    Sticker21,
    Sticker22,
    Sticker23,
    Sticker24,
    Sticker25,
    Sticker26,
    Sticker27,
    Sticker28,
    Sticker29,
    Sticker30,
    Sticker31,
    Sticker32,
    Sticker33,
    Sticker34,
    Sticker35,
    Sticker36,
    Sticker37,
    Sticker38
};

enum MagnetTouchType {
    MagnetTouchOnItem = 0,
    MagnetTouchOnCircle,
    MagnetTouchOnNone,
};

@protocol MagnetItemViewDelegate <NSObject>

- (void)shouldRemoveMagnet:(id)sender;

@end

@interface MagnetItemView : UIView<UIGestureRecognizerDelegate> {
    
    int magnetType;
    
    float orientation;
    float pinchOriginalOrientation;
    
    UIImageView* magnetView;
    
    MagnetBorderView* borderView;
    
    MagnetData* magnetData;
    
    UIImageView* scaleIcon;
    UIButton* btnClose;
    
    BOOL isEditMode;
    
    id<MagnetItemViewDelegate> delegate;
    
    UIView* overlayView;
    
    float originalDistance;
    
    float scale;
    
    
    float mLastScale;
}

@property(nonatomic, retain)id<MagnetItemViewDelegate> delegate;

@property(nonatomic, retain)MagnetData* magnetData;
@property(readonly)float orientation;
@property(readonly)float scale;

- (id)initWithSet:(int)mSet Type:(int)mType;
- (id)initWithMagnetData:(MagnetData*)mData;
- (void)loadWithSet:(int)mSet Type:(int)mType;
- (void)setEditMode:(BOOL)m;

#pragma mark - positions
- (CGPoint)position;
- (void)setPosition:(CGPoint)position;
- (void)setOrientation:(float)ori;
- (void)appendScale:(float)s;

- (void)setScaleAndOrientation:(float)ori appendScale:(float)s;

- (int)touchOnViewWithPos:(CGPoint)p withEvent:(UIEvent *)event;
- (float)getAngleWithPos:(CGPoint)p;
- (float)getDistanceWithPoint:(CGPoint)pA;

#pragma mark - Animations
- (void)startShowAnimation;
- (void)startDismissAnimation;

#pragma mark - Button actions
- (void)closeAction:(id)sender;

@end
