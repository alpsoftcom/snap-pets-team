//
//  MagnetBorderView.m
//  Zombiegram_iOS
//
//  Created by sailim on 22/11/12.
//  Copyright (c) 2012 Mangrove Giant Limited. All rights reserved.
//

#import "MagnetBorderView.h"

@implementation MagnetBorderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = false;
        scale = 1.0f;
    }
    return self;
}

- (void)setScale:(float)s
{
    scale = s;
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    float radius = 5.0f*scale;
    
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0,0,0,0.75);
    CGContextSetLineWidth(context, 2.0f*scale);
    CGContextSetAllowsAntialiasing(context, true);
    
    float offset = 8.0f;
    rect = CGRectMake(rect.origin.x + offset, rect.origin.y + offset, rect.size.width - (2*offset), rect.size.height - (2*offset));
    
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + radius);
    CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - radius);
    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + rect.size.height - radius,
                    radius, M_PI, M_PI / 2, 1); //STS fixed
    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - radius,
                            rect.origin.y + rect.size.height);
    CGContextAddArc(context, rect.origin.x + rect.size.width - radius,
                    rect.origin.y + rect.size.height - radius, radius, M_PI / 2, 0.0f, 1);
    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + radius);
    CGContextAddArc(context, rect.origin.x + rect.size.width - radius, rect.origin.y + radius,
                    radius, 0.0f, -M_PI / 2, 1);
    CGContextAddLineToPoint(context, rect.origin.x + radius, rect.origin.y);
    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + radius, radius,
                    -M_PI / 2, M_PI, 1);
    
    //    CGContextFillPath(context);
    CGContextStrokePath(context);
}

@end
