//
//  MagnetData.h
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MagnetData : NSObject<NSCoding> {
    int magnetSet;
    int magnetType;
    float posX;
    float posY;
    float orientation;
    
}
@property(assign)int magnetSet;
@property(assign)int magnetType;
@property(assign)float posX;
@property(assign)float posY;
@property(assign)float orientation;

- (void)copyFrom:(MagnetData*)cData;

@end

