//
//  MagnetView.m
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import "MagnetView.h"
#import "UIImageViewAddition.h"
#import "UIButtonAddition.h"
#import "MagnetData.h"

@implementation MagnetView

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (id)initSmallMangetView
{
    self = [super initWithFrame:CGRectMake(0, 0, 243, 401)];
    
    self.backgroundColor = [UIColor clearColor];
    magnetItemList = [NSMutableArray new];
    self.userInteractionEnabled = NO;
    
    scaleX = 1.0f;
    scaleY = 1.0f;
    
    return self;
}

- (id)initBigMangetView
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 480)];
    
    self.backgroundColor = [UIColor clearColor];
    magnetItemList = [NSMutableArray new];
    self.userInteractionEnabled = NO;
    
    scaleX = 320.0f / 243.0f;
    scaleY = 480.0f / 401.0f;
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//- (void)dealloc
//{
//    [magnetItemList release];
//    [super dealloc];
//}

#pragma mark - control
- (void)refreshMagnetView
{
    for(UIView* v in magnetItemList)
    {
        [v removeFromSuperview];
    }
    [magnetItemList removeAllObjects];
    float scaleMax = scaleX > scaleY ? scaleX : scaleY;
    
    // TODO: update
//    for(MagnetData* d in [DishManager sharedInstance].magnetDataList)
//    {
//        MagnetItemView* mItem = [[MagnetItemView alloc] initWithMagnetData:d];
//        [self addSubview:mItem];
//        [mItem setPosition:CGPointMake([mItem position].x * scaleX, [mItem position].y * scaleY)];
//        mItem.transform = CGAffineTransformMakeScale(scaleMax, scaleMax);
//        [magnetItemList addObject:mItem];
//        [mItem release];
//    }
}

@end
