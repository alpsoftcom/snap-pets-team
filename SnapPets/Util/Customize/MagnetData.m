//
//  MagnetData.m
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import "MagnetData.h"


@implementation MagnetData

@synthesize magnetType, posX, posY, orientation;

- (id)initWithCoder:(NSCoder *)coder 
{
    self = [super init];
    
    magnetType = [coder decodeIntForKey:@"magnetType"];
    posX = [coder decodeFloatForKey:@"posX"];
    posY = [coder decodeFloatForKey:@"posY"];
    orientation = [coder decodeFloatForKey:@"orientation"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder 
{
    [coder encodeInt:magnetType forKey:@"magnetType"];
    [coder encodeFloat:posX forKey:@"posX"];
    [coder encodeFloat:posY forKey:@"posY"];
    [coder encodeFloat:orientation forKey:@"orientation"];
}

- (void)copyFrom:(MagnetData*)cData
{
    magnetType = cData.magnetType;
    posX = cData.posX;
    posY = cData.posY;
    orientation = cData.orientation;
}

@end
