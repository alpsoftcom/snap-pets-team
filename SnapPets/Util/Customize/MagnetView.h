//
//  MagnetView.h
//  maggi_iOS
//
//  Created by Sailim Chan on 09/08/2011.
//  Copyright 2011 1UP Game. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnetItemView.h"

@interface MagnetView : UIView {
    
    NSMutableArray* magnetItemList;
    
    float scaleX;
    float scaleY;
}

- (id)initSmallMangetView;
- (id)initBigMangetView;

#pragma mark - control
- (void)refreshMagnetView;

@end
