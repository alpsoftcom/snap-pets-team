//
//  ConnectedDeviceRecord.m
//  SnapPets
//
//  Created by David Chan on 6/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "ConnectedDeviceRecord.h"

@implementation ConnectedDeviceRecord

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.strID = [decoder decodeObjectForKey:KEY_ID];
        self.strName = [decoder decodeObjectForKey:KEY_NAME];
        self.connectedCount = [decoder decodeInt64ForKey:KEY_CONNECTEDCOUNT];
        self.colorID = [decoder decodeInt64ForKey:KEY_COLORID];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.strID forKey:KEY_ID];
    [encoder encodeObject:self.strName forKey:KEY_NAME];
    [encoder encodeInt64:self.connectedCount forKey:KEY_CONNECTEDCOUNT];
    [encoder encodeInt64:self.colorID forKey:KEY_COLORID];
}

@end
