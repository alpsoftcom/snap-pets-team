//
//  UARTPeripheral.m
//  nRF UART
//
//  Created by Ole Morten on 1/12/13.
//  Copyright (c) 2013 Nordic Semiconductor. All rights reserved.
//

#import "UARTPeripheral.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

//#import "RobotCommand.h"

@interface UARTPeripheral ()
@property CBService *uartService;
@property CBCharacteristic *blobDataCharacteristic;
@property CBCharacteristic *dataLengthCharacteristic;
@property CBCharacteristic *requestCharacteristic;

@property (strong, nonatomic) NSMutableData *imageData;
@property (nonatomic, assign) NSUInteger receivedByteLength;
@property (nonatomic, assign) NSUInteger totalByteLength;
@property (nonatomic, strong) NSMutableString *hexDump;
@property (nonatomic, strong) NSString *previousHex;

@end

@implementation UARTPeripheral
@synthesize peripheral = _peripheral;

@synthesize uartService = _uartService;
@synthesize blobDataCharacteristic = _blobDataCharacteristic;
@synthesize dataLengthCharacteristic = _dataLengthCharacteristic;
@synthesize requestCharacteristic = _requestCharacteristic;

#pragma mark - UUID

+ (CBUUID *) uartServiceUUID {
    return [CBUUID UUIDWithString:@"0000BB8D-36F6-4BA2-B619-48F028AD1680"];
}

+ (CBUUID *) blobDataCharacteristicUUID {
    return [CBUUID UUIDWithString:@"00001681-36F6-4BA2-B619-48F028AD1680"];
}

+ (CBUUID *) dataLengthCharacteristicUUID {
    return [CBUUID UUIDWithString:@"00001682-36F6-4BA2-B619-48F028AD1680"];
}

+ (CBUUID *) requestCharacteristicUUID {
    return [CBUUID UUIDWithString:@"00001680-36F6-4BA2-B619-48F028AD1680"];
}

#pragma mark - setup

- (UARTPeripheral *) initWithPeripheral:(CBPeripheral*)peripheral delegate:(id<UARTPeripheralDelegate>) delegate {
    if (self = [super init]) {
        _peripheral = peripheral;
        _peripheral.delegate = self;
        _delegate = delegate;
        _usePredefinedHeader = false;
    }
    return self;
}

- (void) didConnect {
    [_peripheral discoverServices:@[self.class.uartServiceUUID]];
    NSLog(@"Did start service discovery.");
}

- (void) didDisconnect {
    
}

#pragma mark - private
- (void)sendRobotCommand:(RobotCommand*)command {
    NSData* sendData = [command data];
    
//    NSString *sendDataString = [NSString stringWithFormat:@"%@", sendData];
//    [self.lbCommand setText:[NSString stringWithFormat:@"Send time:%.2f Data:%@", CFAbsoluteTimeGetCurrent(), sendDataString]];
    
    [self writeRawData:sendData];
}

- (void) writeRawData:(NSData *) data {
    if ((self.requestCharacteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) != 0) {
        [self.peripheral writeValue:data forCharacteristic:self.requestCharacteristic type:CBCharacteristicWriteWithoutResponse];
    }
    else if ((self.requestCharacteristic.properties & CBCharacteristicPropertyWrite) != 0) {
        [self.peripheral writeValue:data forCharacteristic:self.requestCharacteristic type:CBCharacteristicWriteWithResponse];
    }
    else {
        NSLog(@"No write property on request characteristic, %u.", self.requestCharacteristic.properties);
    }
}

- (void) writeString:(NSString *) string {
//    NSData *data = [NSData dataWithBytes:string.UTF8String length:string.length];
//    if ((self.txCharacteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) != 0) {
//        [self.peripheral writeValue:data forCharacteristic:self.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
//    }
//    else if ((self.txCharacteristic.properties & CBCharacteristicPropertyWrite) != 0) {
//        [self.peripheral writeValue:data forCharacteristic:self.txCharacteristic type:CBCharacteristicWriteWithResponse];
//    }
//    else {
//        NSLog(@"No write property on TX characteristic, %d.", self.txCharacteristic.properties);
//    }
}

- (void)logToHexDump:(NSString *)dump {
    if(!_hexDump) {
        self.hexDump = [NSMutableString new];
    }
    [self.hexDump appendFormat:@"%@\n", dump];
}

#pragma mark - public 

- (void)getProtocolVersion {
    [self sendRobotCommand:[RobotCommand robotCommandWithUInt8:kSnapGetProtocolVersion]];
}

- (void)getModelName {
    [self sendRobotCommand:[RobotCommand robotCommandWithUInt8:kSnapGetModelName]];
}

- (void)getNumberOfImagesInFlash {
    [self sendRobotCommand:[RobotCommand robotCommandWithUInt8:kSnapGetNumberOfImagesInFlash]];
}

- (void)takePicture {
    [self sendRobotCommand:[RobotCommand robotCommandWithUInt8:kSnapTakePicture]];
}

- (NSString *)getHexDump {
    return self.hexDump;
}

- (NSData *)getImageData {
    return self.imageData;
}

#pragma mark - CBPeripheralDelegate

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (error) {
        NSLog(@"Error discovering services: %@", error);
        return;
    }
    
    for (CBService *s in [peripheral services]) {
        if ([s.UUID isEqual:self.class.uartServiceUUID]) {
            NSLog(@"Found correct service");
            self.uartService = s;
            
            [self.peripheral discoverCharacteristics:@[self.class.blobDataCharacteristicUUID, self.class.dataLengthCharacteristicUUID, self.class.requestCharacteristicUUID] forService:self.uartService];
        }
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if (error) {
        NSLog(@"Error discovering characteristics: %@", error);
        return;
    }
    
    for (CBCharacteristic *c in [service characteristics]) {
        if ([c.UUID isEqual:self.class.blobDataCharacteristicUUID]) {
            NSLog(@"Found blobDataCharacteristicUUID");
            self.blobDataCharacteristic = c;
            [self.peripheral setNotifyValue:YES forCharacteristic:self.blobDataCharacteristic];
        }
        else if ([c.UUID isEqual:self.class.dataLengthCharacteristicUUID]) {
            NSLog(@"Found dataLengthCharacteristicUUID");
            self.dataLengthCharacteristic = c;
            [self.peripheral setNotifyValue:YES forCharacteristic:self.dataLengthCharacteristic];
        }
        else if ([c.UUID isEqual:self.class.requestCharacteristicUUID]) {
            NSLog(@"Found requestCharacteristicUUID");
            self.requestCharacteristic = c;
        }
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        NSLog(@"Error receiving notification for characteristic %@: %@", characteristic, error);
        return;
    }
    
    if (characteristic == self.blobDataCharacteristic) {
        NSData *receivedData = [characteristic value];
        NSString *hex = [NSString stringWithFormat:@"%@", receivedData];
        hex = [hex stringByReplacingOccurrencesOfString:@"<" withString:@""];
        hex = [hex stringByReplacingOccurrencesOfString:@">" withString:@""];
        
//        if(![self.previousHex isEqualToString:hex]) {
            self.previousHex = hex;
            
            self.receivedByteLength += [receivedData length];
//            NSLog(@"Received blobDataCharacteristic = %@, accumulated length = %04lx", receivedData, (unsigned long)self.receivedByteLength);
        
            // Receive blob data
            [self.imageData appendData:receivedData];
        
            if ([self.delegate respondsToSelector:@selector(didProcessHex:)]){
                [self.delegate performSelector:@selector(didProcessHex:) withObject:hex];
            }
        
            // callback byte length received
            if([self.delegate respondsToSelector:@selector(didReceiveByteProgress:fullLength:)]) {
                [self.delegate didReceiveByteProgress:self.receivedByteLength fullLength:self.totalByteLength];
            }
        
        
            // log to hex dump
            [self logToHexDump:[NSString stringWithFormat:@"%04lx: %@", (unsigned long)self.receivedByteLength, hex]];
//        }
//        else {
//            NSLog(@"WARNING: Receive duplicated blob data: %@", hex);
//        }
    }
    else if (characteristic == self.dataLengthCharacteristic) {
        // Receive data length
        NSData *receivedData = [characteristic value];
        uint16_t receivedValue = OSReadLittleInt16([receivedData bytes], 0);
        
        NSLog(@"Received dataLengthCharacteristic: %@, length = %d", receivedData, receivedValue);
        self.receivedByteLength = 0;
        self.totalByteLength = receivedValue;
        
        // Currently we do not have command byte, so I can just assume any value <1000 is the
        // jpg header, i.e. start of an image transfer process. We should add command byte later
        if(receivedValue > 100 && receivedValue < 1000) {
            // new image data
            self.imageData = nil;
            self.imageData = [NSMutableData new];
            self.hexDump = [NSMutableString new];
            
            if([self.delegate respondsToSelector:@selector(didReceiveByteProgress:fullLength:)]) {
                [self.delegate didReceiveByteProgress:self.receivedByteLength fullLength:self.totalByteLength];
            }
        }
        else if(receivedValue == 0) {
            // End of image transfer
            UIImage* image = [UIImage imageWithData:self.imageData];
            if(image) {
                NSLog(@"Get image success!");
                if ([self.delegate respondsToSelector:@selector(didReceiveImage:)]){
                    [self.delegate didReceiveImage:image];
                }
            }
            else {
                NSLog(@"ERROR: Process image data failed!");
                if([self.delegate respondsToSelector:@selector(didProcessImageDataFailed)]) {
                    [self.delegate didProcessImageDataFailed];
                }
            }
        }
        else {
            if(self.usePredefinedHeader) {
                // Try to use preloaded header
                self.imageData = [NSMutableData new];
                NSData *headerData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jpgheader.dat" ofType:nil]];
                [self.imageData appendData:headerData];
            }
            
            if([self.delegate respondsToSelector:@selector(didReceiveByteProgress:fullLength:)]) {
                [self.delegate didReceiveByteProgress:self.receivedByteLength fullLength:self.totalByteLength];
            }
        }
        
        // log to hex dump
        NSString *hex = [NSString stringWithFormat:@"%@", receivedData];
        hex = [hex stringByReplacingOccurrencesOfString:@"<" withString:@""];
        hex = [hex stringByReplacingOccurrencesOfString:@">" withString:@""];
        [self logToHexDump:[NSString stringWithFormat:@"received %@ on 0x1682:", hex]];
    }
    else if (characteristic == self.requestCharacteristic) {
        // TODO: Receive request?
        NSData *receivedData = [characteristic value];
        uint16_t receivedValue = OSReadLittleInt16([receivedData bytes], 0);
        
        NSLog(@"Received requestCharacteristic: %@, length = %d", receivedData, receivedValue);
    }
}

@end
