//
//  BluetoothConnectionManager.h
//  SnapPets
//
//  Created by David Chan on 26/3/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
//#import "SnappetRobot.h"

@interface BluetoothConnectionManager : NSObject <SnappetRobotDelegate> {
}
+ (BluetoothConnectionManager*)sharedInstance;
- (void)connectSnappetIfPossible;
- (void)startScan;
- (void)stopScan;
@property (strong, nonatomic) CBCentralManager *cm;
@property (strong, nonatomic) NSArray *deviceList;
@property (strong, nonatomic) NSMutableDictionary *connectedDeviceList;
@end
