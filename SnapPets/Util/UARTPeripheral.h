//
//  UARTPeripheral.h
//  nRF UART
//
//  Created by Ole Morten on 1/12/13.
//  Copyright (c) 2013 Nordic Semiconductor. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : uint8_t {
    kSnapGetProtocolVersion = 0x01, // kSnapGetProtocolVersion
    kSnapTakePicture = 0x02,
    kSnapGetModelName = 0x10,
    kSnapGetNumberOfImagesInFlash = 0x20,

    /** End firmware extras **/
} kSnapCommand;


@protocol UARTPeripheralDelegate <NSObject>
- (void) didReceiveData:(NSString *) string;
- (void) didReceiveImage:(UIImage *)image;
- (void) didReceiveByteProgress:(int)currentByteLength fullLength:(int)fullByteLength;
- (void) didProcessImageDataFailed;
- (void) didProcessHex:(NSString*)hex;
@end

@interface UARTPeripheral : NSObject <CBPeripheralDelegate>
@property CBPeripheral *peripheral;
@property(nonatomic, strong) id<UARTPeripheralDelegate> delegate;
@property(nonatomic, assign) BOOL usePredefinedHeader;

+ (CBUUID *) uartServiceUUID;

- (UARTPeripheral *) initWithPeripheral:(CBPeripheral*)peripheral delegate:(id<UARTPeripheralDelegate>) delegate;

- (void) writeString:(NSString *) string;
- (void) writeRawData:(NSData *) data;
- (void) didConnect;
- (void) didDisconnect;

#pragma mark - public 
- (void)getProtocolVersion;
- (void)getModelName;
- (void)getNumberOfImagesInFlash;
- (void)takePicture;
- (NSString *)getHexDump;
- (NSData *)getImageData;

@end
