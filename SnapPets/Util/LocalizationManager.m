 //
//  LocalizationManager.m
//  RoboRemoteBlue
//
//  Created by David Chan on 8/4/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import "LocalizationManager.h"
static LocalizationManager* _sharedObject= nil;

@implementation LocalizationManager

+ (LocalizationManager*)getInstance {
    if (_sharedObject == nil)
        _sharedObject = [LocalizationManager new];
    return _sharedObject;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)loadLanguage:(NSString*)strLang {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                     ofType:@"strings"
                                                inDirectory:nil
                                            forLocalization:strLang];
    self.dictLang = [NSDictionary dictionaryWithContentsOfFile:path];
}

- (NSString*)getStringWithKey:(NSString*)strKey {
//    NSLog(@"%@", strKey);
    NSString* obj = [self.dictLang objectForKey:strKey];
//    NSString* obj = strKey;
    return obj;
}

@end
