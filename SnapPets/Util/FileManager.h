//
//  FileManager.h
//  Robosapien
//
//  Created by David Chan on 6/3/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
+ (NSString*)defaultDirectory;
+ (NSArray*)loadDirectoryFilePaths;
+ (NSArray*)loadDirectoryFilePathsInDirectory:(NSString*)_directory;
+ (NSString*)generatePathName:(NSString*)_filename;
+ (NSString*)generatePathName:(NSString*)_filename inDirectory:(NSString*)_directory;
+ (void)deletePathName:(NSString*)_filename;
+ (void)deletePathName:(NSString*)_filename inDirectory:(NSString*)_directory;
@end
