//
//  ConnectionManager.m
//  SnapPets
//
//  Created by Katy Pun on 29/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "ConnectionManager.h"
#import "AppConfig.h"

@implementation ConnectionManager

static ConnectionManager *instance = nil;

+(ConnectionManager*)sharedInstance {
    if (!instance) {
        instance = [ConnectionManager new];
    }
    
    return instance;
}

-(BOOL)isEnableBackgroundDownloading {
//    NSLog(@"Version: %@", self.snappet.bleModuleSoftwareVersion);
//    if (self.snappet != nil && [self getToyFirmwareVersion] > 0.19) {
//        return YES;
//    }
    return NO;
}

-(double)getToyFirmwareVersion {
    NSArray* arr = [self.snappet.bleModuleSoftwareVersion componentsSeparatedByString:@"_"];
    if ([arr count] == 2) {
        NSString* firmwareVersion = [arr objectAtIndex:0];
        firmwareVersion = [firmwareVersion substringFromIndex:1];
        return [firmwareVersion doubleValue];
    }
    return 0;
}

+(UIColor*)getColorFromSnappetColorID:(NSInteger)colorID{
    
    UIColor * petColor;
   
            switch (colorID){
                    
                case kSnappetColorBlue:
                    petColor = [UIColor colorWithRed:20.0f/255.0f green:0.0/255.0f blue:250.0f/255.0f alpha:1.0f];
                    break;
                case kSnappetColorPink:
                    petColor = [UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
                    break;
                case kSnappetColorOrage:
                   petColor = [UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:39.0f/255.0f alpha:1.0f];
                    break;
                case kSnappetColorSilver:
                    petColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
                    break;
                case kSnappetAquaBlue:
                    petColor = [UIColor colorWithRed:12.0f/255.0f green:247.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
                    break;
                default:
                    
                    petColor = [UIColor whiteColor];
                    break;

    }
    
    return  petColor;
 //   Rgb2UIColor(<#r#>, <#g#>, <#b#>)
}
@end
