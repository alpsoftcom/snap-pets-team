//
//  LocalizationManager.h
//  RoboRemoteBlue
//
//  Created by David Chan on 8/4/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalizationManager : NSObject {
    
}

+ (LocalizationManager*)getInstance;
- (void)loadLanguage:(NSString*)strLang;
- (NSString*)getStringWithKey:(NSString*)strKey;

@property (readwrite) NSDictionary* dictLang;

@end
