//
//  UIButtonAddition.h
//  maggi_iOS
//
//  Created by sailim Chan on 21/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


@interface UIButton (Additions)

+ (UIButton*)buttonWithImage:(UIImage*)img target:(id)tar sel:(SEL)act;
- (void)setPosition:(CGPoint)pos;

@end
