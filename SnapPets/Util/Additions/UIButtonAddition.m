//
//  UIButtonAddition.m
//  maggi_iOS
//
//  Created by sailim Chan on 21/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIButtonAddition.h"

@implementation UIButton (Additions)

+ (UIButton*)buttonWithImage:(UIImage*)img target:(id)tar sel:(SEL)act
{
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, img.size.width, img.size.height);
    [btn setImage:img forState:UIControlStateNormal];
    [btn addTarget:tar action:act forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (void)setPosition:(CGPoint)pos
{
    self.frame = CGRectMake(pos.x, pos.y, self.frame.size.width, self.frame.size.height);
}

@end
