//
//  UIImageViewAddition.h
//  maggi_iOS
//
//  Created by sailim Chan on 21/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


@interface UIImageView (Additions)

- (void)setPosition:(CGPoint)pos;
- (CGPoint)getPosition;
- (float)width;
- (float)height;

@end
