//
//  UIImageViewAddition.m
//  maggi_iOS
//
//  Created by sailim Chan on 21/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIImageViewAddition.h"

@implementation UIImageView (Additions)

- (void)setPosition:(CGPoint)pos
{
    self.frame = CGRectMake(pos.x, pos.y, self.frame.size.width, self.frame.size.height);
}

- (CGPoint)getPosition
{
    return self.frame.origin;
}

- (float)width
{
    return self.frame.size.width;
}

- (float)height
{
    return self.frame.size.height;
}

@end
