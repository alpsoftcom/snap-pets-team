//
//  StickersView.h
//  Zombiegram_iOS
//
//  Created by sailim on 20/11/12.
//  Copyright (c) 2012 Mangrove Giant Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnetData.h"
#import "MagnetItemView.h"
#import "MagnetScrollView.h"

#define DEFAULT_STICKERS_COUNT   2

@interface StickersView : UIView<MagnetItemViewDelegate, MagnetScrollViewDelegate> {
    
    UIScrollView* bottomScrollView;
    
    NSMutableArray* magnetItemList;
    
    NSMutableArray* magnetScrollList;
    
    MagnetItemView* currentItem;
    
    BOOL shouldCheckRotate;
    
    CGPoint previousTouchPosition;
    
    float previousOrientation;
    
    float previousDistance;
    
    NSMutableArray* unlockImages;
    
    UIView* targetView;
    
    UITouch* beganTouch;
    
    BOOL isiPhone5;
    
    float hideY;
    float showY;
}

@property(nonatomic, retain)UIView* targetView;
@property(nonatomic, readonly)UIScrollView* bottomScrollView;
@property(nonatomic, readonly)NSMutableArray* magnetItemList;

- (id)initStickersView:(UIView*)superView;

#pragma mark - animation
- (void)animateBottomBar;
- (void)animateHideBottomBar;
- (void)hideBottomBar;
- (BOOL)isHidden;
#pragma mark - setup
- (void)loadStickers;
- (void)removeAllStickers;
- (void)unlock;
- (void)lock;

@end
