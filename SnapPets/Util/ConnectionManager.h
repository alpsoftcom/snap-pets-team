//
//  ConnectionManager.h
//  SnapPets
//
//  Created by Katy Pun on 29/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
//#import "SnappetRobot.h"

@interface ConnectionManager : NSObject

@property (nonatomic, strong) SnappetRobot *snappet;
@property (readwrite) NSString *strSnappetName;

+(ConnectionManager*)sharedInstance;
-(BOOL)isEnableBackgroundDownloading;
+(UIColor*)getColorFromSnappetColorID:(NSInteger)colorID;


@end
