//
//  NSArray+SavePlist.m
//  Robosapien
//
//  Created by David Chan on 5/3/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import "NSArray+SavePlist.h"

@implementation NSArray (SavePlist)

-(BOOL)savePlist:(NSString*)path {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    return [data writeToFile:path atomically:YES];
}

+(NSArray*)loadPlist:(NSString*)path {
    NSData * data = [NSData dataWithContentsOfFile:path];
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

@end
