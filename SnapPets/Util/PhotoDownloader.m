//
//  PhotoDownloader.m
//  SnapPets
//
//  Created by David Chan on 24/3/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "PhotoDownloader.h"
#import "ConnectionManager.h"
#import "PhotoLibraryManager.h"

@interface PhotoDownloader ()
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, assign) int photoLength;
@end

@implementation PhotoDownloader

static PhotoDownloader* _sharedObject = nil;

+ (PhotoDownloader*)sharedInstance {
    if (_sharedObject == nil) {
        _sharedObject = [PhotoDownloader new];
    }
    
    return _sharedObject;
}

- (id)init {
    if (self = [super init]) {
        self.downloaderStatus = PhotoDownloaderStatusIdle;
        self.delegate = nil;
    }
    
    return self;
}

- (void)downloadAllPhotos {
    if ([ConnectionManager sharedInstance].snappet != nil) {
        if (self.downloaderStatus == PhotoDownloaderStatusIdle || self.downloaderStatus == PhotoDownloaderStatusStopped) {
            [ConnectionManager sharedInstance].snappet.delegate = self;
            [[ConnectionManager sharedInstance].snappet snappetReadNumberOfPhotos];
            self.downloaderStatus = PhotoDownloaderStatusDownloading;
        }
    }
}

- (void)stop {
    [[ConnectionManager sharedInstance].snappet snappetStopTransfer];
    self.downloaderStatus = PhotoDownloaderStatusStopped;
}

- (BOOL)isDownloading {
    return self.downloaderStatus == PhotoDownloaderStatusDownloading;
}

#pragma mark - SnappetRobotDelegate
-(void) Snappet:(SnappetRobot *)snappet didReceiveButtonPressed:(kSnappetPressType)pressType {
    if(pressType == kSnappetShortPress) {
        //        [self.view makeToast:@"Short pressed"];
//        NSLog(@"Short press");
    }
    else if(pressType == kSnappetLongPress) {
        //        [self.view makeToast:@"Long pressed"];
//        NSLog(@"Long press");
    }
    
    
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoLength:(int)photoLength checksum:(uint8_t)checksum {
    //    [self.lbLength setText:[NSString stringWithFormat:@"Length: %d", photoLength]];
    //    [self.lbChecksum setText:[NSString stringWithFormat:@"Checksum: %d", checksum]];
    
    self.receivedData = [NSMutableData new];
    self.photoLength = 0;
    
    self.photoLength = photoLength;
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoBlob:(NSData *)blobData {
    if (self.downloaderStatus == PhotoDownloaderStatusDownloading) {
        [_receivedData appendData:blobData];
        int receivedLength = (int)[_receivedData length];
        //    float progress = (float)receivedLength / (float)_photoLength;
        //    [_blobProgress setProgress:progress animated:NO];
        //    [_lbProgress setText:[NSString stringWithFormat:@"%d / %d", receivedLength, _photoLength]];
        //
        
//        NSLog(@"PhotoDownloader received data %d", receivedLength);
        if(receivedLength > 0 && receivedLength == _photoLength) {
            //        NSLog(@"PhotoLibraryViewController Receive photo data finished. Creating image now..");
            
            NSMutableData *finalData = [NSMutableData new];
            NSData *headerData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jpgheader.dat" ofType:nil]];
            [finalData appendData:headerData];
            [finalData appendData:_receivedData];
            
            // Finish receiving photo
            UIImage *image = [UIImage imageWithData:finalData];
            //        UIImage *image = [UIImage imageWithData:_receivedData];
            if(!image) {
//                NSLog(@"PhotoDownloader image = nil");
            }
            
            
            //        [SVProgressHUD dismiss];
            
            //save to app album
            [[PhotoLibraryManager sharedInstance] saveImage:image];
            //delete from snappet
            uint8_t photoIdByte = (uint8_t)(1);
            //        NSLog(@"PhotoLibraryViewController delete photo");
            [[ConnectionManager sharedInstance].snappet snappetDeletePhoto:photoIdByte];
            if (self.delegate != nil) {
                [self.delegate didDownloadImage];
            }
            if ([PhotoLibraryManager sharedInstance].currentPhotoId+1 == [PhotoLibraryManager sharedInstance].numOfPhotoInSnappet){
//                NSLog(@"Done");
                if (self.delegate != nil) {
                    [self.delegate didCompleteDownloadTask];
                }
                
                self.downloaderStatus = PhotoDownloaderStatusIdle;
            }else{
//                NSLog(@"PhotoDownloader Next photo");
                //get next photo from snappet
                if (self.downloaderStatus == PhotoDownloaderStatusDownloading) {
                    [PhotoLibraryManager sharedInstance].currentPhotoId++;
                    
                    //            uint8_t photoIdByte = (uint8_t)([PhotoLibraryManager sharedInstance].currentPhotoId+1);
                    uint8_t photoIdByte = (uint8_t)(1);
                    [[ConnectionManager sharedInstance].snappet snappetGetPhoto:kSnappetPhotoLargeSize photoID:photoIdByte];
                }
            }
            //        [_photoImage setImage:image];
            //
            //        // Save data for debug purpose
            //        //        [self saveReceivedDataAction:nil];
        }
    }
}


-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
    UINavigationController* controller = (UINavigationController*)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    [controller popToRootViewControllerAnimated:NO];
    
    //    [self.view makeToast:@"Snappets Disconnected"];
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveNumberOfPhoto:(int)numberOfPhoto {
    if (self.downloaderStatus == PhotoDownloaderStatusDownloading) {
        [PhotoLibraryManager sharedInstance].numOfPhotoInSnappet = numberOfPhoto;
//        NSLog(@"PhotoDownloader Number of Photo: %d", numberOfPhoto);
        if (numberOfPhoto > 0) {
            [PhotoLibraryManager sharedInstance].currentPhotoId = 0;
            uint8_t photoIdByte = (uint8_t)([PhotoLibraryManager sharedInstance].currentPhotoId+1);
            [[ConnectionManager sharedInstance].snappet snappetGetPhoto:kSnappetPhotoLargeSize photoID:photoIdByte];
        }
        else {
            if (self.delegate != nil) {
                [self.delegate didCompleteDownloadTask];
            }

            self.downloaderStatus = PhotoDownloaderStatusIdle;
        }
    }
}

@end
