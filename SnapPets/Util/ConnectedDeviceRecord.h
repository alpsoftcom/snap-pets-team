//
//  ConnectedDeviceRecord.h
//  SnapPets
//
//  Created by David Chan on 6/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>
#define KEY_NAME @"name"
#define KEY_ID @"id"
#define KEY_CONNECTEDCOUNT @"connectedcount"
#define KEY_COLORID @"colorid"

@interface ConnectedDeviceRecord : NSObject  <NSCoding> {
}

@property (readwrite) NSString* strName;
@property (readwrite) NSString* strID;
@property (readwrite) NSInteger connectedCount;
@property (readwrite) NSInteger colorID;

@end
