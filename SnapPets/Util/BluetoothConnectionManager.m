//
//  BluetoothConnectionManager.m
//  SnapPets
//
//  Created by David Chan on 26/3/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "BluetoothConnectionManager.h"
//#import "SnappetRobotFinder.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>


@interface BluetoothConnectionManager ()

@end

@implementation BluetoothConnectionManager

static BluetoothConnectionManager* _sharedObject = nil;

+ (BluetoothConnectionManager*)sharedInstance {
    if (_sharedObject == nil)
        _sharedObject = [BluetoothConnectionManager new];
    return _sharedObject;
}


- (id)init {
    if (self = [super init]) {
//        self.cm = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }
    
    return self;
}

- (void)connectSnappetIfPossible {
    NSLog(@"connectSnappetIfPossible");
    if ([self.deviceList count] > 0) {
        for (int i=0; i<[self.deviceList count]; i++) {
            SnappetRobot* robot = (SnappetRobot*)[self.deviceList objectAtIndex:i];
            if ([[robot.name lowercaseString] containsString:@"snappets"]) {
                if ([self.connectedDeviceList objectForKey:robot.name] == nil) {
                    [self.connectedDeviceList setObject:robot forKey:robot.name];
                    [robot connect];
                    robot.delegate = self;
                    break;
                }
            }
        }
    }
}

#pragma mark - private
- (void)startScan {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSnappetRobotFinderNotification:) name:SnappetRobotFinderNotificationID object:nil];
    self.connectedDeviceList = [NSMutableDictionary dictionary];
//    [[SnappetRobotFinder sharedInstance] cbCentralManagerState];
    [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
    [[SnappetRobotFinder sharedInstance] scanForSnappetsInBackground];
    
    
//    if ([NSThread isMainThread])
//        [self _startScan];
//    else
//        [self performSelectorOnMainThread:@selector(_startScan) withObject:nil waitUntilDone:NO];
}

- (void)_startScan {
//    NSLog(@"Start scan");
    NSArray *serviceArray =
    @[[CBUUID UUIDWithString:[NSString stringWithFormat:@"%x", kSnappetPhotoServiceUUID]],
      [CBUUID UUIDWithString:[NSString stringWithFormat:@"%x", kSnappetSettingsServiceUUID]],
      [CBUUID UUIDWithString:[NSString stringWithFormat:@"%x", kSnappetSecurityServiceUUID]],
      [CBUUID UUIDWithString:[NSString stringWithFormat:@"%x", kSnappetModuleServiceUUID]],
      [CBUUID UUIDWithString:[NSString stringWithFormat:@"%x", kSnappetModuleParameterServiceUUID]]];
    BOOL filterByServices = YES;
    NSArray *services = nil;
    if (filterByServices) {
        services = serviceArray;
    }
    
    [self.cm scanForPeripheralsWithServices:serviceArray options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @(NO)}];
}

- (void)stopScan {
//    [self.cm stopScan];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SnappetRobotFinderNotificationID object:nil];
    [[SnappetRobotFinder sharedInstance] stopScanForSnappets];
}

#pragma mark - SnappetRobotFinder callback
- (void)onSnappetRobotFinderNotification: (NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    if(info){
        NSNumber *code = [info objectForKey: @"code"];
        //id data = [info objectForKey: @"data"];
        if (code.intValue == SnappetRobotFinderNote_SnappetFound){
            self.deviceList = [[SnappetRobotFinder sharedInstance] snappetsFound];
        } else if (code.intValue == SnappetRobotFinderNote_SnappetListCleared) {
            self.deviceList = [[SnappetRobotFinder sharedInstance] snappetsFound];
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothError) {
//            NSLog(@"Bluetooth error!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsOff) {
//            NSLog(@"Bluetooth is off!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsAvailable) {
//            NSLog(@"SnappetRobotFinderNote_BluetoothIsAvailable");
        }
    }
}

#pragma mark - SnappetRobotDelegate
-(void) SnappetDeviceReady:(SnappetRobot *)snappet {
    NSLog(@"SnappetDeviceReady %@", snappet.name);
    //    SelectCamController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCamController"];
    //    controller.snappet = snappet;
    //    [self.navigationController pushViewController:controller animated:YES];
    
    [NSThread sleepForTimeInterval:2];
    [snappet disconnect];
}

/*
#pragma mark - CBCentralManagerDelegate
- (void) centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state == CBCentralManagerStatePoweredOn) {
        [self startScan];
    }
}

- (NSArray *)retrievePeripheralsWithIdentifiers:(NSArray *)identifiers {
    NSLog(@"retrievePeripheralsWithIdentifiers");
    return identifiers;
}

- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Did discover peripheral %@", peripheral.name);
    
    //    [self.cm stopScan];
    //    self.currentPeripheral = [[UARTPeripheral alloc] initWithPeripheral:peripheral delegate:self];
    //    [self.cm connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: [NSNumber numberWithBool:YES]}];
    
    //    self.scanLabel.hidden = YES;
    //    [self.deviceList addObject:peripheral];
    //    [self.deviceTable reloadData];
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Did connect peripheral %@", peripheral.name);
    
    if ([self.currentPeripheral.peripheral isEqual:peripheral]) {
        [self.currentPeripheral didConnect];
    }
    
//    SelectCamController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCamController"];
    //    controller.currentPeripheral = self.currentPeripheral;
//    self.currentPeripheral.delegate = controller;
//    [self.navigationController pushViewController:controller animated:YES];
}

- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Did disconnect peripheral %@", peripheral.name);
    if ([self.currentPeripheral.peripheral isEqual:peripheral]) {
        [self.currentPeripheral didDisconnect];
        self.currentPeripheral = nil;
    }
    
//    if([self.navigationController.topViewController isKindOfClass:[SelectCamController class]]) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
}
*/
@end
