//
//  PhotoDownloader.h
//  SnapPets
//
//  Created by David Chan on 24/3/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

typedef enum {
    PhotoDownloaderStatusIdle = 0,
    PhotoDownloaderStatusDownloading = 1,
    PhotoDownloaderStatusStopped = 2,
} PhotoDownloaderStatus;

@protocol PhotoDownloaderDelegate

- (void)didDownloadImage;
- (void)didCompleteDownloadTask;

@end

@interface PhotoDownloader : NSObject <SnappetRobotDelegate> {
    
}
@property (readwrite) PhotoDownloaderStatus downloaderStatus;
@property (readwrite) id<PhotoDownloaderDelegate> delegate;
+ (PhotoDownloader*)sharedInstance;
- (void)downloadAllPhotos;
- (void)stop;
- (BOOL)isDownloading;

@end
