//
//  NSArray+SavePlist.h
//  Robosapien
//
//  Created by David Chan on 5/3/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSArray(SavePlist)
-(BOOL)savePlist:(NSString*)path;
+(NSArray*)loadPlist:(NSString*)path;
@end
