//
//  StickersView.m
//  Zombiegram_iOS
//
//  Created by sailim on 20/11/12.
//  Copyright (c) 2012 Mangrove Giant Limited. All rights reserved.
//

#import "StickersView.h"
//#import "EditController.h"
//#import "UnlockController.h"

@implementation StickersView

@synthesize targetView, bottomScrollView, magnetItemList;

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        
//        
//    }
//    return self;
//}

- (id)initStickersView:(UIView*)superView
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    self = [super initWithFrame:CGRectMake(0, 0, screenBounds.size.width, screenBounds.size.height - 30)];
    
    self.userInteractionEnabled = true;
    
    int stickerBgH = 70;
    
    // check to iPhone 5
    
    if (screenBounds.size.height == 1024)
    {
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenBounds.size.height-4-152-20-156, screenBounds.size.width, 152)];
        hideY = screenBounds.size.height;
        showY = screenBounds.size.height-4-152-90+10;
    }
    else if (screenBounds.size.height == 667)
    {
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenBounds.size.height-4-76-20, screenBounds.size.width, 80)];
        hideY = screenBounds.size.height;
        showY = screenBounds.size.height-126;
    }
    else if (screenBounds.size.height > 568)
    {
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenBounds.size.height-4-76-20, screenBounds.size.width, 76)];
        hideY = screenBounds.size.height;
        showY = screenBounds.size.height-4-76-85+10;
    }
    else if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        isiPhone5 = YES;
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenBounds.size.height-76-20, screenBounds.size.width, 76)];
//        hideY = 568;
//        showY = 440;
        hideY = screenBounds.size.height;
        showY = screenBounds.size.height-76-55+5;
    }
    else if (screenBounds.size.height == 480) {
        // code for 3.5-inch screen
        isiPhone5 = NO;
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, screenBounds.size.height-4-76, screenBounds.size.width, 76)];
//        hideY = 480;
//        showY = 480 - 55 - stickerBgH;
        hideY = screenBounds.size.height;
        showY = screenBounds.size.height-4-55-76+5;
    }
    
    // Initialization code
    magnetItemList = [NSMutableArray new];
    magnetScrollList = [NSMutableArray new];
    unlockImages = [NSMutableArray new];
    
//    NSLog(@"init stickersview %p", self);
    
    // add scroll view
    bottomScrollView.backgroundColor = [UIColor clearColor];
    bottomScrollView.showsHorizontalScrollIndicator = false;
    bottomScrollView.showsVerticalScrollIndicator = false;
//    [self addSubview:bottomScrollView];
    [superView addSubview:bottomScrollView];
    
//    NSLog(@"init bottomScrollView %p", bottomScrollView);
    
    // load filter buttons
    [self loadStickers];
    
    return self;
}

//- (void)dealloc
//{
//    [bottomScrollView release];
//    bottomScrollView = nil;
//    [magnetItemList release];
//    magnetItemList = nil;
//    [magnetScrollList release];
//    magnetScrollList = nil;
//    [unlockImages release];
//    unlockImages = nil;
//    self.targetView = nil;
//    [super dealloc];
//}

//- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
//    for (UIView *view in self.subviews) {
//        if([view isKindOfClass:[MagnetItemView class]])
//        {
//            int checkTouchState = [(MagnetItemView*)view touchOnViewWithPos:point withEvent:event];
//            if(checkTouchState != MagnetTouchOnNone)
//            {
//                return YES;
//            }
//        }
//        else if (!view.hidden && [view pointInside:[self convertPoint:point toView:view] withEvent:event])
//        {
//            return YES;
//        }
//    }
//    return NO;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - animation
- (void)animateBottomBar
{
    bottomScrollView.hidden = false;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 1024)
        [bottomScrollView setFrame:CGRectMake(0, hideY, screenBounds.size.width, 152)];
    else {
        if (screenBounds.size.height == 568 || screenBounds.size.height == 480)
            [bottomScrollView setFrame:CGRectMake(0, hideY, screenBounds.size.width, 76)];
        else
            [bottomScrollView setFrame:CGRectMake(0, hideY, screenBounds.size.width, 100)];
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    if (screenBounds.size.height == 1024)
        [bottomScrollView setFrame:CGRectMake(0, showY, screenBounds.size.width, 152)];
    else{
        if (screenBounds.size.height == 568 || screenBounds.size.height == 480)
            [bottomScrollView setFrame:CGRectMake(0, showY, screenBounds.size.width, 76)];
        else
            [bottomScrollView setFrame:CGRectMake(0, showY, screenBounds.size.width, 100)];
    }
    
    [UIView commitAnimations];
}

- (void)animateHideBottomBar
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    bottomScrollView.hidden = false;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 1024)
        [bottomScrollView setFrame:CGRectMake(0, hideY, screenBounds.size.width, 152)];
    else {
        if (screenBounds.size.height == 568 || screenBounds.size.height == 480)
            [bottomScrollView setFrame:CGRectMake(0, hideY, screenBounds.size.width, 76)];
        else
            [bottomScrollView setFrame:CGRectMake(0, hideY, screenBounds.size.width, 100)];
    }
    
    [UIView commitAnimations];
}

- (void)hideBottomBar
{    
    // first all edit mode off
    for(MagnetItemView* v in magnetItemList)
    {
        if(v != currentItem)
        {
            [v setEditMode:NO];
        }
    }
    
    bottomScrollView.hidden = true;
}

- (BOOL)isHidden {
    return bottomScrollView.hidden;
}

#pragma mark - setup
- (void)loadStickers
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    float contentWidth = 0;
    int index = 0;
    for(int i=0;i<38;i++)
    {
        MagnetScrollView* mScroll = [[MagnetScrollView alloc] initWithSet:0 Type:i];
        if (screenBounds.size.height == 1024)
            [mScroll setPosition:CGPointMake(i*142, 0)];
        else {
            if (screenBounds.size.height == 568 || screenBounds.size.height == 480 || screenBounds.size.height == 667)
                [mScroll setPosition:CGPointMake(i*85, 0)];
            else
                [mScroll setPosition:CGPointMake(i*85, 0)];
        }
        mScroll.delegate = self;
        [bottomScrollView addSubview:mScroll];
        [magnetScrollList addObject:mScroll];
        if (screenBounds.size.height == 1024)
            contentWidth += 142;
        else {
            if (screenBounds.size.height == 568 || screenBounds.size.height == 480 || screenBounds.size.height == 667)
                contentWidth += 85;
            else
                contentWidth += 95;
        }
        
        // lock
//        if(i >= DEFAULT_FILTERS_COUNT)
//        {
//            // lock icon
//            UIImageView* lockImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 21, 40, 40)];
//            lockImg.contentMode = UIViewContentModeScaleAspectFit;
//            [lockImg setImage:[UIImage imageNamed:@"lock.png"]];
//            [mScroll addSubview:lockImg];
//            [unlockImages addObject:lockImg];
//            
//            if([EditController sharedInstance].unlocked)
//            {
//                lockImg.hidden = true;
//            }
//            
//            [lockImg release];
//            lockImg = nil;
//        }
        
//        [mScroll release];
        mScroll = nil;
    }
    index+=38;
    for(int i=0;i<31;i++)
    {
        MagnetScrollView* mScroll = [[MagnetScrollView alloc] initWithSet:1 Type:i];
        if (screenBounds.size.height == 1024)
            [mScroll setPosition:CGPointMake((index+i)*142, 0)];
        else {
            if (screenBounds.size.height == 568 || screenBounds.size.height == 480 || screenBounds.size.height == 667)
                [mScroll setPosition:CGPointMake((i+index)*85, 0)];
            else
                [mScroll setPosition:CGPointMake((i+index)*95, 0)];
        }
        mScroll.delegate = self;
        [bottomScrollView addSubview:mScroll];
        [magnetScrollList addObject:mScroll];
        if (screenBounds.size.height == 1024)
            contentWidth += 142;
        else {
            if (screenBounds.size.height == 568 || screenBounds.size.height == 480 || screenBounds.size.height == 667)
                contentWidth += 85;
            else
                contentWidth += 95;
        }
        
        // lock
        //        if(i >= DEFAULT_FILTERS_COUNT)
        //        {
        //            // lock icon
        //            UIImageView* lockImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 21, 40, 40)];
        //            lockImg.contentMode = UIViewContentModeScaleAspectFit;
        //            [lockImg setImage:[UIImage imageNamed:@"lock.png"]];
        //            [mScroll addSubview:lockImg];
        //            [unlockImages addObject:lockImg];
        //
        //            if([EditController sharedInstance].unlocked)
        //            {
        //                lockImg.hidden = true;
        //            }
        //
        //            [lockImg release];
        //            lockImg = nil;
        //        }
        
        //        [mScroll release];
        mScroll = nil;
    }
    index+=31;
    for(int i=0;i<34;i++)
    {
        MagnetScrollView* mScroll = [[MagnetScrollView alloc] initWithSet:2 Type:i];
        if (screenBounds.size.height == 1024)
            [mScroll setPosition:CGPointMake((index+i)*142, 0)];
        else {
            if (screenBounds.size.height == 568 || screenBounds.size.height == 480 || screenBounds.size.height == 667)
                [mScroll setPosition:CGPointMake((index+i)*85, 0)];
            else
                [mScroll setPosition:CGPointMake((index+i)*95, 0)];
        }
        mScroll.delegate = self;
        [bottomScrollView addSubview:mScroll];
        [magnetScrollList addObject:mScroll];
        if (screenBounds.size.height == 1024)
            contentWidth += 142;
        else {
            if (screenBounds.size.height == 568 || screenBounds.size.height == 480 || screenBounds.size.height == 667)
                contentWidth += 85;
            else
                contentWidth += 95;
        }
        
        // lock
        //        if(i >= DEFAULT_FILTERS_COUNT)
        //        {
        //            // lock icon
        //            UIImageView* lockImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 21, 40, 40)];
        //            lockImg.contentMode = UIViewContentModeScaleAspectFit;
        //            [lockImg setImage:[UIImage imageNamed:@"lock.png"]];
        //            [mScroll addSubview:lockImg];
        //            [unlockImages addObject:lockImg];
        //
        //            if([EditController sharedInstance].unlocked)
        //            {
        //                lockImg.hidden = true;
        //            }
        //
        //            [lockImg release];
        //            lockImg = nil;
        //        }
        
        //        [mScroll release];
        mScroll = nil;
    }
    
    bottomScrollView.contentSize = CGSizeMake(contentWidth, bottomScrollView.frame.size.height);
}

- (void)removeAllStickers
{
    for(MagnetItemView* v in magnetItemList)
    {
        [v removeFromSuperview];
    }
    [magnetItemList removeAllObjects];
}

- (void)unlock
{
    for(UIView* v in unlockImages)
    {
//        [v removeFromSuperview];
        v.hidden = true;
    }
//    [unlockImages removeAllObjects];
}

- (void)lock
{
    for(UIView* v in unlockImages)
    {
        v.hidden = false;
    }
}

#pragma mark - button actions
- (void)stickersAction:(id)sender
{
    
}

#pragma mark - MagnetItemViewDelegate
- (void)shouldRemoveMagnet:(id)sender
{
    UIView* senderView = (UIView*)sender;
    if(senderView.superview)
    {
        [senderView removeFromSuperview];
    }
    [magnetItemList removeObject:sender];
//    [self refreshScrollListItems];
}

#pragma mark - MagnetScrollViewDelegate
- (void)shouldAddMagnet:(id)sender
{
    
//    if(!([EditController sharedInstance].unlocked) && ((MagnetScrollView*)sender).magnetType >= DEFAULT_FILTERS_COUNT)
//    {
////        [[EditController sharedInstance] presentModalViewController:[UnlockController sharedInstance] animated:YES];
//    }
//    else
//    {
        // first all edit mode off
        for(MagnetItemView* v in magnetItemList)
        {
            if(v != currentItem)
            {
                [v setEditMode:NO];
            }
        }
        int magnetSet = ((MagnetScrollView*)sender).magnetSet;
        int magnetType = ((MagnetScrollView*)sender).magnetType;
        MagnetData* d = [MagnetData new];
        d.magnetSet = magnetSet;
        d.magnetType = magnetType;
        d.posX = 25;
        d.posY = 250;
        d.orientation = 0;
        MagnetItemView* mItem = [[MagnetItemView alloc] initWithMagnetData:d];
        [mItem setPosition:CGPointMake(25, 250)];
        mItem.userInteractionEnabled = YES;
        mItem.delegate = self;
//        mItem.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
        [self addSubview:mItem];
//        [targetView addSubview:mItem];
        [magnetItemList addObject:mItem];
        [mItem startShowAnimation];
//        [mItem release];
//        [d release];
//        [self refreshScrollListItems];


//    }
}

#pragma mark - handle touches
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    UITouch* t = [touches anyObject];
    CGPoint pos = [t locationInView:self];
    shouldCheckRotate = NO;
    beganTouch = nil;
    
    currentItem = nil;
    
    //Check touch with added items
    for(MagnetItemView* v in magnetItemList)
    {
        int checkTouchState = [v touchOnViewWithPos:pos withEvent:event];
        if(checkTouchState == MagnetTouchOnItem)
        {
            currentItem = v;
            [currentItem setEditMode:YES];
            previousTouchPosition = pos;
            beganTouch = t;
            break;
        }
        else if(checkTouchState == MagnetTouchOnCircle)
        {
            currentItem = v;
            [currentItem setEditMode:YES];
//            previousTouchPosition = pos;
            previousOrientation = [v getAngleWithPos:pos];
            shouldCheckRotate = YES;
            previousDistance = [v getDistanceWithPoint:pos];
            
            break;
        }
    }
    for(MagnetItemView* v in magnetItemList)
    {
        if(v != currentItem)
        {
            [v setEditMode:NO];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    UITouch* moveTouch = [touches anyObject];
    CGPoint pos = [moveTouch locationInView:self];
    
    //Update touch position
    if(currentItem)
    {
        if(shouldCheckRotate)       //Check rotate
        {
            // do rotate
            float touchOri = [currentItem getAngleWithPos:pos];
            float deltaAngle = touchOri - previousOrientation;
//            [currentItem setOrientation:(currentItem.orientation + deltaAngle)];
            previousOrientation = touchOri;
            
            // do scale
            float newDis = [currentItem getDistanceWithPoint:pos];
            float appendScale = newDis / previousDistance;
//            [currentItem appendScale:appendScale];
            [currentItem setScaleAndOrientation:(currentItem.orientation + deltaAngle) appendScale:appendScale];
            previousDistance = newDis;
            
        }
        else                        //Check move
        {
            if(beganTouch == moveTouch)
            {
                float offX = pos.x - previousTouchPosition.x;
                float offY = pos.y - previousTouchPosition.y;
                CGPoint cPos = currentItem.frame.origin;
                cPos.x += offX;
                cPos.y += offY;
                [currentItem setPosition:cPos];
                previousTouchPosition = pos;
            }
        }
    }
    
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    if(currentItem)
    {
        if(!shouldCheckRotate)
        {
//            if([currentItem position].x < 30 || [currentItem position].x > 210 || [currentItem position].y < 60 || [currentItem position].y > 390)
//            {
//                [currentItem startDismissAnimation];
//            }
        }
    }
    currentItem = nil;
}


@end
