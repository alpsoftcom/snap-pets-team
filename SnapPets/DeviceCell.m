//
//  DeviceCell.m
//  MipPhotoTest
//
//  Created by Forrest Chan on 26/9/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "DeviceCell.h"

@implementation DeviceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.backgroundColor = [UIColor clearColor];
    
    // Round corner bg
    float cellHeight = DeviceCellHeight - 10;
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(20, 5, 280, cellHeight)];
    bgView.userInteractionEnabled = NO;
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.cornerRadius = 11.0f;
    bgView.layer.masksToBounds = YES;
    [self addSubview:bgView];
    
    UIImageView *spIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"connect_icon"]];
    spIcon.contentMode = UIViewContentModeScaleAspectFit;
    [spIcon setFrame:CGRectMake(0, 0, cellHeight, cellHeight)];
    [bgView addSubview:spIcon];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, bgView.frame.size.width - 80, cellHeight)];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor darkGrayColor];
    self.nameLabel.font = [UIFont boldSystemFontOfSize:16];
    self.nameLabel.text = @"Snap Pets";
    [bgView addSubview:self.nameLabel];
    
    self.colorView = [[UIView alloc] initWithFrame:CGRectMake(100, 40, 20, 20)];
    
    [self addSubview:self.colorView];
    
    return self;
}

- (void)setPeripheral:(CBPeripheral *)peripheral {
    _peripheral = peripheral;
    self.nameLabel.text = peripheral.name;
}

- (void)setSnappet:(SnappetRobot *)snappet {
    _snappet = snappet;
    [self.nameLabel setNumberOfLines:0];
    [self setColorGrid];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ \n       (%@)", snappet.name, [self colorIdName:snappet.colorId]];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setColorGrid{

    switch (_snappet.colorId){
           
        case kSnappetColorBlue:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f green:0.0/255.0f blue:250.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetColorPink:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:255.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetColorOrage:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:39.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetColorSilver:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetAquaBlue:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:12.0f/255.0f green:247.0f/255.0f blue:246.0f/255.0f alpha:1.0f]];
            break;
        default:
            
            break;
    }
}

- (NSString*)colorIdName:(NSInteger)colorId{
    switch (colorId) {
        case kSnappetColorBlue:
            return @"深蓝色 Blue";
            break;
        case kSnappetColorPink:
            return @"粉红色 Pink";
            break;
        case kSnappetColorOrage:
            return @"橙色 Orange";
            break;
        case kSnappetColorSilver:
            return @"黑色 Black";
            break;
        case kSnappetAquaBlue:
            return @"水蓝色 Aqua blue";
            break;
        default:
            return @"无颜色 No colorId";
            break;
    }
}

@end
