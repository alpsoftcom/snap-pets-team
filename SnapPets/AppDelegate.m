//
//  AppDelegate.m
//  MipPhotoTest
//
//  Created by Forrest Chan on 25/4/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "AppDelegate.h"
#import <DropboxSDK/DropboxSDK.h>
#import "SimpleSoundPlayer.h"
#import "ConnectionManager.h"
#import "PhotoLibraryManager.h"
#import "PhotoDownloader.h"
#import "ConnectionController.h"
#import "BluetoothConnectionManager.h"
#import "SVProgressHUD.h"
#import "Flurry.h"
#import "TakePhotoWithSnapPetsScannerController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()

@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, assign) int photoLength;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Start APP");
    UIView* statusBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, 20)];
    statusBg.backgroundColor = [UIColor colorWithWhite:1 alpha:.7];
    
    //Add the view behind the status bar
    [self.window.rootViewController.view addSubview:statusBg];

    // Dropbox
    DBSession *dbSession = [[DBSession alloc]
                            initWithAppKey:@"4y7c5jjjy1x1yt5"
                            appSecret:@"eu93sngd6sb63tr"
                            root:kDBRootAppFolder]; // either kDBRootAppFolder or kDBRootDropbox
    [DBSession setSharedSession:dbSession];
   // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    // Init SimpleAudioPlayer
    [SimpleSoundPlayer sharedInstance];
//    [Flurry startSession:@"PYS3YHNPW9KFGDV5GYBD"];
//    [Fabric with:@[CrashlyticsKit]];
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url sourceApplication:(NSString *)source annotation:(id)annotation {
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
            NSLog(@"App linked successfully!");
            // At this point you can start making API calls
        }
        return YES;
    }
    // Add whatever other url handling code your app requires here
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[ConnectionManager sharedInstance].snappet disconnect];
    [ConnectionManager sharedInstance].snappet = nil;
//    UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
//    UIViewController* controller = [navigationController presentedViewController];
//    if ([controller isKindOfClass:[TakePhotoWithSnapPetsScannerController class]]) {
//        TakePhotoWithSnapPetsScannerController* takePhotoController = (TakePhotoWithSnapPetsScannerController*)controller;
//        [takePhotoController viewUnloadAnimation];
//    }
    
    return;
    
    NSLog(@"Enter Background Start");
    
//    if ([ConnectionManager sharedInstance].snappet == nil) {
//        self.backgroundTimer = [NSTimer timerWithTimeInterval:5.0f target:self selector:@selector(startScan) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop]addTimer:self.backgroundTimer forMode:NSRunLoopCommonModes];
//    }
    [SVProgressHUD dismiss];
    self.backgroundTask = [application beginBackgroundTaskWithName:@"wowwee.backgroundTask" expirationHandler:^ {
        NSLog(@"Terminated");
        
        //Clean up code. Tell the system that we are done.
        [application endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        if ([ConnectionManager sharedInstance].snappet != nil) {
            if ([[ConnectionManager sharedInstance] isEnableBackgroundDownloading]) {
                if (![[PhotoDownloader sharedInstance] isDownloading]) {
                    [[PhotoDownloader sharedInstance] setDelegate:self];
                    [[PhotoDownloader sharedInstance] downloadAllPhotos];
                }
            }
            else {
                if ([[PhotoDownloader sharedInstance] isDownloading]) {
                    if ([NSThread isMainThread])
                        [[PhotoDownloader sharedInstance] stop];
                    else
                        [[PhotoDownloader sharedInstance] performSelectorOnMainThread:@selector(stop) withObject:nil waitUntilDone:YES];
                }
                if ([NSThread isMainThread])
                    [[ConnectionManager sharedInstance].snappet disconnect];
                else
                    [[ConnectionManager sharedInstance].snappet performSelectorOnMainThread:@selector(disconnect) withObject:nil waitUntilDone:YES];
                [ConnectionManager sharedInstance].snappet = nil;
            }
        }
        else {
//            [self startScan];
        }
        
//        NSTimer* t = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(checkSnappetReconnection) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
//        [[NSRunLoop currentRunLoop] run];
        
        [application endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    });
    
    
    NSLog(@"Enter Background End");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//    NSLog(@"Stop scan.");
    
//    UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
//    UIViewController* controller = [navigationController presentedViewController];
//    if ([controller isKindOfClass:[TakePhotoWithSnapPetsScannerController class]]) {
//        TakePhotoWithSnapPetsScannerController* takePhotoController = (TakePhotoWithSnapPetsScannerController*)controller;
//        [takePhotoController viewUnloadAnimation];
//        [takePhotoController startScan];
//    }
    return;
    
    [self.backgroundTimer invalidate];
    self.backgroundTimer = nil;
//    [[BluetoothConnectionManager sharedInstance] stopScan];
    
    if ([ConnectionManager sharedInstance].snappet != nil) {
        if ([[ConnectionManager sharedInstance] isEnableBackgroundDownloading]) {
            if ([[PhotoDownloader sharedInstance] isDownloading]) {
                [[PhotoDownloader sharedInstance] stop];
                [[ConnectionManager sharedInstance].snappet disconnect];
                [ConnectionManager sharedInstance].snappet = nil;
            }
            else {
                [[ConnectionManager sharedInstance].snappet disconnect];
                [ConnectionManager sharedInstance].snappet = nil;
            }
        }
        else {
            [[ConnectionManager sharedInstance].snappet disconnect];
            [ConnectionManager sharedInstance].snappet = nil;
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    UINavigationController* controller = (UINavigationController*)self.window.rootViewController;
    if (controller.topViewController != nil && [controller.topViewController isKindOfClass:[ConnectionController class]]) {
        ConnectionController* connectionController = (ConnectionController*)controller.topViewController;
        [connectionController refreshAction:nil];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
//    // Get topmost/visible view controller
//    UIViewController *currentViewController = [self topViewController];
//    
//    // Check whether it implements a dummy methods called canRotate
//    SEL selector = NSSelectorFromString(@"canRotate");
//    if ([currentViewController respondsToSelector:selector]) {
//        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:
//                                    [[currentViewController class] instanceMethodSignatureForSelector:selector]];
//        [invocation setSelector:selector];
//        [invocation setTarget:currentViewController];
//        [invocation invoke];
//        BOOL returnValue;
//        [invocation getReturnValue:&returnValue];
//        if (returnValue){
//            return UIInterfaceOrientationMaskAllButUpsideDown;
//        }
//    }
//    
//    // Only allow portrait (standard behaviour)
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//
//- (UIViewController*)topViewController {
//    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
//}
//
//- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
//    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
//        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
//        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
//    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
//        UINavigationController* navigationController = (UINavigationController*)rootViewController;
//        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
//    } else if (rootViewController.presentedViewController) {
//        UIViewController* presentedViewController = rootViewController.presentedViewController;
//        return [self topViewControllerWithRootViewController:presentedViewController];
//    } else {
//        return rootViewController;
//    }
//}

- (void)startScan {
    [[SnappetRobotFinder sharedInstance] stopScanForSnappets];
    [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
    
    [[BluetoothConnectionManager sharedInstance] stopScan];
    [[BluetoothConnectionManager sharedInstance] startScan];
}

- (void)checkSnappetReconnection {
    [[BluetoothConnectionManager sharedInstance] connectSnappetIfPossible];
}

#pragma mark PhotoDownloaderDelegate
- (void)didDownloadImage {
    
}

- (void)didCompleteDownloadTask {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        [[ConnectionManager sharedInstance].snappet disconnect];
        [ConnectionManager sharedInstance].snappet = nil;
        UINavigationController* controller = (UINavigationController*)self.window.rootViewController;
        [controller popToRootViewControllerAnimated:NO];

//        [self startScan];
//        self.backgroundTimer = [NSTimer timerWithTimeInterval:5.0f target:self selector:@selector(startScan) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop]addTimer:self.backgroundTimer forMode:NSRunLoopCommonModes];
        NSLog(@"Disconnect");
    }
}

@end
