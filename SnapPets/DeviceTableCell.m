//
//  DeviceTableCell.m
//  SnapPets
//
//  Created by David Chan on 30/4/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "DeviceTableCell.h"
#import "LocalizationManager.h"
#import "ConnectionManager.h"

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
@implementation DeviceTableCell

- (void)updateView:(int)imgIndex {
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
    self.viewWhiteCorner.layer.cornerRadius = 10.0f;
    self.viewPinkCorner.layer.cornerRadius = 10.0f;
    [self.viewPinkCorner.layer setBorderWidth:2.0f];
   petcolor = [ConnectionManager getColorFromSnappetColorID:self.snappet.colorId];
    [self.labelText setTextColor:petcolor];
    [self.viewPinkCorner.layer setBorderColor:petcolor.CGColor];
    // [self.viewWhiteCorner.layer setBorderColor:[UIColor redColor].CGColor];
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* font = [UIFont systemFontOfSize:30.0f];
        self.labelText.font = font;
    }
    
    self.clipsToBounds = YES;
    
    if (self.snappet != nil) {
        self.viewImg1.hidden = YES;
        self.viewImg2.hidden = YES;
        self.viewImg3.hidden = YES;
        NSString* str1 = nil;
        NSString* str2 = nil;
        NSString* str3 = nil;
        NSString* strDevice = @"@2x";
        if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
            strDevice = @"@2x~ipad";
        }
        strDevice = @"";

        switch (self.snappet.colorId) {
            case kSnappetColorBlue:
                str1 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme3%@.png", strDevice];
                str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme3%@.png", strDevice];
                str3 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme3%@.png", strDevice];
                break;
            case kSnappetColorPink:
                str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1%@.png", strDevice];
                str2 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1%@.png", strDevice];
                str3 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1%@.png", strDevice];
                break;
            case kSnappetColorOrage:
                str1 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme4%@.png", strDevice];
                str2 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme4%@.png", strDevice];
                str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme4%@.png", strDevice];
                break;
            case kSnappetColorSilver:
                str1 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme2%@.png", strDevice];
                str2 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme2%@.png", strDevice];
                str3 = [NSString stringWithFormat:@"connect_icon_pet_cat_theme2%@.png", strDevice];
                break;
            case kSnappetAquaBlue:
                str1 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme5%@.png", strDevice];
                str2 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme5%@.png", strDevice];
                str3 = [NSString stringWithFormat:@"connect_icon_pet_dog_theme5%@.png", strDevice];
                break;
            default:
                str1 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1%@.png", strDevice];
                str2 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1%@.png", strDevice];
                str3 = [NSString stringWithFormat:@"connect_icon_pet_bunny_theme1%@.png", strDevice];
                break;
        }
        switch (imgIndex) {
            case 0:
                [self.viewImg1 setImage:[UIImage imageNamed:str1]];
                self.viewImg1.hidden = NO;
                break;
            case 1:
                [self.viewImg2 setImage:[UIImage imageNamed:str2]];
                self.viewImg2.hidden = NO;
                break;
            case 2:
                [self.viewImg3 setImage:[UIImage imageNamed:str3]];
                self.viewImg3.hidden = NO;
                break;
            default:
                break;
        }
    
//      self.labelText.text = [NSString stringWithFormat:@"%@ (%@)", self.snappet.name, [self colorIdName:self.snappet.colorId]];
        self.labelText.text = [NSString stringWithFormat:@"%@", self.snappet.name];
    }
    else {
        NSString* str1 = nil;
        self.viewImg1.hidden = YES;
        self.viewImg2.hidden = YES;
        self.viewImg3.hidden = YES;
        switch (imgIndex) {
            case 0:
                self.labelText.text = [[LocalizationManager getInstance] getStringWithKey:@"Buy a SnapPet"];
                self.viewImg1.hidden = NO;
                str1 = [NSString stringWithFormat:@"btn_shop.png"];
                self.viewImg2.hidden = YES;
                self.viewImg3.hidden = YES;
                break;
            default:
                break;
        }
        [self.viewImg1 setImage:[UIImage imageNamed:str1]];
    }
}

- (NSString*)colorIdName:(NSInteger)colorId{
    switch (colorId) {
        case kSnappetColorBlue:
            return @"深蓝色 Blue";
            break;
        case kSnappetColorPink:
            return @"粉红色 Pink";
            break;
        case kSnappetColorOrage:
            return @"橙色 Orange";
            break;
        case kSnappetColorSilver:
            return @"黑色 Black";
            break;
        case kSnappetAquaBlue:
            return @"水蓝色 Aqua blue";
            break;
        default:
            return @"无颜色 No colorId";
            break;
    }
}

@end
