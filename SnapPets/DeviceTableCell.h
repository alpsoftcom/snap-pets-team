//
//  DeviceTableCell.h
//  SnapPets
//
//  Created by David Chan on 30/4/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

@interface DeviceTableCell : UITableViewCell {
  
    UIColor * petcolor;
}

- (void)updateView:(int)imgIndex;

@property (atomic, strong) IBOutlet UIView* viewWhiteCorner;
@property (atomic, strong) IBOutlet UIView* viewPinkCorner;

@property (atomic, strong) IBOutlet UIImageView* viewImg1;
@property (atomic, strong) IBOutlet UIImageView* viewImg2;
@property (atomic, strong) IBOutlet UIImageView* viewImg3;

@property (atomic, strong) IBOutlet UILabel* labelText;

@property (readwrite) IBOutlet SnappetRobot* snappet;

@end
