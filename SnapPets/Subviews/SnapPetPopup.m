//
//  SnapPetPopup.m
//  SnapPets
//
//  Created by David Chan on 19/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "SnapPetPopup.h"
#import "LocalizationManager.h"
@implementation SnapPetPopup

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)show {
    [self setHidden:NO];
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* fontLabel50 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50.0f];
        self.label.font = fontLabel50;
    }
    if (timerAnimateTutorial == nil)
        timerAnimateTutorial = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(animateTutorial) userInfo:nil repeats:YES];
}

- (void)showQuitSettingView {
    [self show];
    mode = 0;
    
    self.label.text = [[LocalizationManager getInstance] getStringWithKey:@"Are you sure to setup the SnapPet later?"];
    
    [self.btnFullCancel setHidden:YES];
    [self.btnNext setHidden:NO];
    [self.btnCancel setHidden:NO];
    
    [self.imgFullCancel setHidden:YES];
    [self.imgNext setHidden:NO];
    [self.imgCancel setHidden:NO];
}

- (void)showQuitTutorialView {
    [self show];
    mode = 1;
    
    self.label.text = [[LocalizationManager getInstance] getStringWithKey:@"Are you sure to quit the tutorial?"];
    
    [self.btnFullCancel setHidden:YES];
    [self.btnNext setHidden:NO];
    [self.btnCancel setHidden:NO];
    
    [self.imgFullCancel setHidden:YES];
    [self.imgNext setHidden:NO];
    [self.imgCancel setHidden:NO];
}

- (void)hide {
    [self setHidden:YES];
}

- (void)animateTutorial {
    NSString* str = nil;
    NSString* strTitle = @"anime_popup_loading-";
    CGSize screenBounds = [self bounds].size;
    if (self.imgView.tag == 0) {
        [self.imgView setTag:1];
    }
    else if (self.imgView.tag == 1) {
        [self.imgView setTag:2];
    }
    else if (self.imgView.tag == 2) {
        [self.imgView setTag:3];
    }
    if (screenBounds.height == 1024 || screenBounds.height == 2048)
        str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @""];
    else
        str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @""];
    str = [NSString stringWithFormat:@"%@%d.png", strTitle, (int)self.imgView.tag];
    [self.imgView setImage:[UIImage imageNamed:str]];
    if (self.imgView.tag == 3) {
        [self.imgView setTag:0];
    }
}


- (IBAction)buttonAction:(id)sender {
    switch (mode) {
        case 0:
            if (sender == self.btnFullCancel) {
                [self.delegate popupViewSelectedCenterFullButton];
            }
            else if (sender == self.btnNext) {
                [self.delegate popupViewSelectedRightButton];
            }
            else if (sender == self.btnCancel) {
                [self.delegate popupViewSelectedLeftButton];
            }
            break;
        case 1:
            if (sender == self.btnFullCancel) {
                [self.delegate popupViewSelectedCenterFullButton];
            }
            else if (sender == self.btnNext) {
                [self.delegate popupViewSelectedRightButton];
            }
            else if (sender == self.btnCancel) {
                [self.delegate popupViewSelectedLeftButton];
            }
            break;
        default:
            break;
    }
}

@end
