//
//  SnapPetPopup.h
//  SnapPets
//
//  Created by David Chan on 19/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SnapPetPopupDelegate

@optional
- (void)popupViewSelectedLeftButton;
- (void)popupViewSelectedRightButton;
- (void)popupViewSelectedCenterFullButton;

@end

@interface SnapPetPopup : UIView {
    int mode;
    NSTimer* timerAnimateTutorial;
}

- (void)showQuitSettingView;
- (void)showQuitTutorialView;
- (void)hide;

@property (readwrite) id<SnapPetPopupDelegate> delegate;
@property (nonatomic, assign) IBOutlet UILabel* label;
@property (nonatomic, assign) IBOutlet UIImageView* imgView;

@property (nonatomic, strong) IBOutlet UIButton* btnCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnNext;
@property (nonatomic, strong) IBOutlet UIButton* btnFullCancel;

@property (nonatomic, strong) IBOutlet UIImageView* imgCancel;
@property (nonatomic, strong) IBOutlet UIImageView* imgNext;
@property (nonatomic, strong) IBOutlet UIImageView* imgFullCancel;
@end
