//
//  TakePhotoOverlayView.m
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "TakePhotoOverlayView.h"
#import "UIImage+Resize.h"
#import "SimpleSoundPlayer.h"
#import "NewSnapPetEnterNameView.h"
#import "ConnectionManager.h"
#import "PhotoLibraryManager.h"
#import "AppConfig.h"
#import "Flurry.h"
#import "LocalizationManager.h"

NSString *const pressedSound = @"button2.wav";

@interface TakePhotoOverlayView()

@property (weak, nonatomic) IBOutlet UIView *timerAndSeriesView;
@property (nonatomic, assign) BOOL isNoFlashBtn;
@property (nonatomic, assign) BOOL isDeviceNoFlash;
@property (weak, nonatomic) IBOutlet UIView *timerPressedView;
@property (weak, nonatomic) IBOutlet UIButton *firstSecButton;
@property (weak, nonatomic) IBOutlet UIButton *secondSecButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdSecButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UILabel *timerTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *mintTimerButton;
@property (weak, nonatomic) IBOutlet UIButton *mintSeriesButton;
@property (weak, nonatomic) IBOutlet UIButton *mintViewTypeTitleButton;
@property (weak, nonatomic) IBOutlet UIButton *mintViewCloseButton;
@property (weak, nonatomic) IBOutlet UIButton *mintViewThirdButton;
@property (weak, nonatomic) IBOutlet UIButton *mintViewSecondButton;
@property (weak, nonatomic) IBOutlet UIButton *mintViewFirstButton;
@property (weak, nonatomic) IBOutlet UILabel *mintViewTitleLabel;

@end
@implementation TakePhotoOverlayView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - initView
- (id)init
{
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"OverlayView_autolayout" owner:self options:nil];
    self = (TakePhotoOverlayView*)[nibViews objectAtIndex:0];
    self.translatesAutoresizingMaskIntoConstraints = YES;
    [self setFrame:[[UIScreen mainScreen] bounds]];
    
    [self setCameraButton:NO];
    
    self.isPhoneShutterMode = YES;
    self.isSelfie = NO;
    self.timerLevel = TimerLevel_0;
    self.flashMode = FlashModeOff;
    
    [self setMode];
    [self setSelfie];
    [self setFlash];
    [self setTimer];
    
    [self setPhotoSettingBtnViewWithMode:TakePhotoSettingBtnViewModeMain];
    
    self.blobProgress.hidden = YES;
    self.flashView.hidden = YES;
    self.snapptOkBtn.hidden = YES;
    self.snapptRetakeBtn.hidden = YES;
    self.photoOkBtn.hidden = YES;
    self.deleteBtn.hidden = YES;
    self.takenPhotoSettingBtnView.hidden = YES;
    self.deleteSnappetBtn.hidden = YES;
    
    self.btnCancel.hidden = YES;
    self.btnContinue.hidden = YES;
    
    self.btnCancel.layer.cornerRadius = 10.0f;
    self.btnContinue.layer.cornerRadius = 10.0f;
    self.isInUserSettingController = NO;
    
    self.timerSnappetBtn.hidden = YES;
    
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.btnContinue setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnContinue"] forState:UIControlStateNormal];
    [self.btnCancel setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnCancel"] forState:UIControlStateNormal];
    [self.labelSnapPetDownloading setText:[[LocalizationManager getInstance] getStringWithKey:@"Loading images from Snap Pet"]];
    
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* font40 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:40];
        UIFont* font30 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:30];
        UIFont* font50 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50];
        self.labelName.font = font40;
        self.progressLabel.font = font30;
        self.progress2Label.font = font30;
        self.labelSnapPetDownloading.font = font50;
        self.btnCancel.titleLabel.font = font40;
        self.btnContinue.titleLabel.font = font40;
    }
    else if (screenBounds.height == 480) {
        //self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen_640.png"];
//        self.constraintBottomViewHeight.constant = 80.0f;
        if (self.isPhoneShutterMode){
            self.constraintBottomViewHeight.constant = 124.0f;
            self.constraintTopViewHeight.constant = 100.0f;
        }
        else {
            self.constraintBottomViewHeight.constant = 80.0f;
            self.constraintTopViewHeight.constant = 64.0f;
        }
    }
    else if (screenBounds.height == 568) {

    }
    else if (screenBounds.height == 736) {
        self.constraintTopBarHeight.constant = 161;
        self.constraintBottomBarHeight.constant = 161;
        self.constraintTopViewHeight.constant = 161;
        self.constraintBottomViewHeight.constant = 161;
        self.constraintPinkBarYFromBottom.constant= 161;
        self.constraintCameraImageY.constant = -161;
        self.constraintSnappetDelete.constant = -180.0f;
        self.constraintSnappetEdit.constant = -180.0f;
        self.constraintSnappetShare.constant = -180.0f;
        self.constraintSnappetTimer.constant = -180.0f;
    }
    else if (screenBounds.height == 667) {
        self.constraintTopBarHeight.constant = 146;
        self.constraintBottomBarHeight.constant = 146;
        self.constraintTopViewHeight.constant = 146;
        self.constraintBottomViewHeight.constant = 146;
        self.constraintPinkBarYFromBottom.constant= 146;
        self.constraintCameraImageY.constant = -146;
        self.constraintSnappetDelete.constant = -165.0f;
        self.constraintSnappetEdit.constant = -165.0f;
        self.constraintSnappetShare.constant = -165.0f;
        self.constraintSnappetTimer.constant = -160.5f;
//        self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen_667.png"];
        self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen.png"];
       // self.topView.image = [UIImage imageNamed:@"bg_top.png"];
        self.bottomView.image = [UIImage imageNamed:@"bg_bottom.png"];
//        self.topView.image = nil;
//        self.bottomView.image = nil;
    }
}

- (void)showConnectionScreen {
    self.isUnderTutorial = NO;
    
    [self.btnCancel setHidden:YES];
    [self.btnContinue setHidden:YES];
    [self.bottomBtnUIViewCover setHidden:YES];
    [self.closeBtn setHidden:YES];
    [self.settingBtn setHidden:YES];
    
    [self.labelName setHidden:YES];
    //[self.viewImgLogo setHidden:NO];
    
    [self.scanBtn setHidden:NO];
    self.scanBtn.tag = 0;
    [self.scanBtn setAlpha:1.0f];
    [self.modeBtn setHidden:YES];
    [self.camImgView setHidden:NO];
    
    [self.viewNewSnapPets setHidden:YES];
}

- (void)showSnappetTakePhotoScreen {
    self.isUnderTutorial = NO;
    
    [self.btnCancel setHidden:YES];
    [self.btnContinue setHidden:YES];
    [self.closeBtn setHidden:YES];
    [self.settingBtn setHidden:NO];
    
   // [self.labelName setHidden:NO];
   // [self.viewImgLogo setHidden:YES];
    
    [self.scanBtn setHidden:YES];
    [self.modeBtn setHidden:NO];
    [self.camImgView setHidden:NO];
    
    self.viewNewSnapPets.tutorialDelegate = self;
    [self.viewNewSnapPets setHidden:YES];
}

- (void)showNewSnappetScreen {
    self.isUnderTutorial = YES;
    
    self.viewTutorial.tutorialDelegate = self;
    self.viewEnterName.tutorialDelegate = self;
    
    [self.viewTutorial showSnapPetsModeTutorial];
    
    self.btnCancel.hidden = NO;
    self.btnContinue.hidden = NO;
    [self.btnCancel setTag:0];
    [self.btnContinue setTag:0];
    [self.camImgView setHidden:YES];
    
    [self.settingBtn setHidden:YES];
    [self.closeBtn setHidden:YES];
    
    [self.modeBtn setHidden:YES];
    [self.libBtn setHidden:YES];
}

- (void)retakePhoto{
    self.phoneTakePhoto.hidden = YES;
    [self setCameraButton:NO];
    //    self.btnGroupView.hidden = YES;
    self.photoOkBtn.hidden = YES;
    self.deleteBtn.hidden = YES;
    self.takenPhotoSettingBtnView.hidden = YES;
    self.blackview.hidden = YES;
}

#pragma mark - IBAction

- (IBAction)sharePhoto:(id)sender {
    [self.deleagte sharePressed];
}

- (IBAction)deletePhoto:(id)sender {
    [self.deleagte deletePressed];
    [self retakePhoto];
    if (self.isPhoneShutterMode){
    }
    else{
        self.viewSnappetTutorial.hidden = NO;
        self.deleteSnappetBtn.hidden = YES;
        self.snapptOkBtn.hidden = YES;
        self.snapptRetakeBtn.hidden = YES;
        [self.snappetTakenPhoto setImage:nil];
    }
}

- (IBAction)takePhoto:(id)sender
{
    [SimpleSoundPlayer playSound:pressedSound];
    
//    if (!self.isPhoneShutterMode){
//        self.blobProgress.hidden = YES;
//        self.blobProgress.progress = 0;
//    }
//    [self setUserInteractionEnabled:NO];
//    self.camImgView.enabled = NO;
    [self.deleagte takePhoto];
//    self.camImgView.enabled = YES;
}

- (IBAction)buttonSelector:(id)sender {
    if (sender == self.scanBtn) {
        [self.deleagte scannerPressed];
    }
}

- (IBAction)okPressed:(id)sender
{
    [self.phoneTakePhoto setImage:nil];
    [self.deleagte okPressed];
}

- (IBAction)retakePressed:(id)sender
{
    if (self.isPhoneShutterMode) {
        self.phoneTakePhoto.hidden = YES;
        [self.phoneTakePhoto setImage:nil];
        [self setCameraButton:NO];
    //    self.btnGroupView.hidden = YES;
        self.photoOkBtn.hidden = YES;
        self.deleteBtn.hidden = YES;
        self.takenPhotoSettingBtnView.hidden = YES;
        self.blackview.hidden = YES;
        [self.closeBtn setHidden:YES];
    }
    else {
        [self.snappetTakenPhoto setImage:nil];
        self.snapptOkBtn.hidden = YES;
        self.snapptRetakeBtn.hidden = YES;
        self.deleteSnappetBtn.hidden = YES;
        self.viewSnappetTutorial.hidden = NO;
        [self.closeBtn setHidden:YES];
    }
    [self.bottomBtnUIViewCover setHidden:YES];
    [self.deleagte retakePressed];
}

- (IBAction)backPressed:(id)sender
{
    [SimpleSoundPlayer playSound:pressedSound];
    [self retakePressed:nil];
    
//    [self.deleagte backPressed];
}

- (IBAction)libraryPressed:(id)sender
{
    [SimpleSoundPlayer playSound:pressedSound];
    
    [self.deleagte libraryPressed];
}

- (IBAction)modePressed:(id)sender {
    [SimpleSoundPlayer playSound:pressedSound];
    
    self.isPhoneShutterMode = !self.isPhoneShutterMode;
    
    [self setCameraButton:NO];
    [self setTimer];
    [self.phoneTakePhoto setImage:nil];
    [self.snappetTakenPhoto setImage:nil];
    if (self.isPhoneShutterMode){
        self.photoOkBtn.hidden = YES;
        self.deleteBtn.hidden = YES;
        self.takenPhotoSettingBtnView.hidden = YES;
        
        CGSize screenBounds = [UIScreen mainScreen].bounds.size;
        if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        }
        else if (screenBounds.height == 480) {
            self.constraintBottomViewHeight.constant = 124.0f;
            self.constraintTopViewHeight.constant = 100.0f;
        }
        else if (screenBounds.height == 667) {
            self.constraintTopViewHeight.constant = 161;
            self.constraintBottomViewHeight.constant = 161;
        }
    }
    else {
        CGSize screenBounds = [UIScreen mainScreen].bounds.size;
        if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        }
        else if (screenBounds.height == 480) {
            self.constraintBottomViewHeight.constant = 80.0f;
            self.constraintTopViewHeight.constant = 64.0f;
        }
        else if (screenBounds.height == 568) {
            self.constraintSnappetTimer.constant = -133.0f;
        }
        else if (screenBounds.height == 667) {
            self.constraintSnappetDelete.constant = -165.0f;
            self.constraintSnappetEdit.constant = -165.0f;
            self.constraintSnappetShare.constant = -165.0f;
            self.constraintSnappetTimer.constant = -160.5f;
        }
        else if (screenBounds.height == 736) {
            self.constraintSnappetDelete.constant = -180.0f;
            self.constraintSnappetEdit.constant = -180.0f;
            self.constraintSnappetShare.constant = -180.0f;
            self.constraintSnappetTimer.constant = -180.0f;
        }
    }
    [self setMode];
    [self.deleagte modePressed];
}

- (void)setupViewUI:(PressedButtonType)type
{
    self.pressedButtonType = type;
    if ( type == kPressedButtonTypeTimer )
    {
//        [self.titleButton setTitle:@"Timer" forState:UIControlStateNormal];
        self.timerTitleLabel.text = @"Take a picture in:";
        [self.firstSecButton setTitle:@"3S" forState:UIControlStateNormal];
        self.firstSecButton.tag = 3;
        [self.secondSecButton setTitle:@"7S" forState:UIControlStateNormal];
        self.secondSecButton.tag = 7;
        [self.thirdSecButton setTitle:@"10S" forState:UIControlStateNormal];
        self.thirdSecButton.tag = 10;
      
    }
    else if ( type == kPressedButtonTypeSeries )
    {
//        [self.titleButton setTitle:@"Photo" forState:UIControlStateNormal];
        self.timerTitleLabel.text = @"Take a picture every:";
        [self.firstSecButton setTitle:@"30S" forState:UIControlStateNormal];
        [self.secondSecButton setTitle:@"2min" forState:UIControlStateNormal];
        [self.thirdSecButton setTitle:@"5min" forState:UIControlStateNormal];
        self.firstSecButton.tag = 30;
        self.secondSecButton.tag = 120;
        self.thirdSecButton.tag = 300;
    }
}

- (IBAction)photoSetPressed:(id)sender
{
    [self setupViewUI:kPressedButtonTypeSeries];
    self.viewPhotoModePinkBar.hidden = YES;
    self.takePhotoSettingBtnView.hidden = YES;
    self.bottomBtnUIView.hidden = YES;
    self.timerPressedView.hidden = NO;
}

- (IBAction)timerPressed:(id)sender {
    
    [self setupViewUI:kPressedButtonTypeTimer];

    self.viewPhotoModePinkBar.hidden = YES;
    self.takePhotoSettingBtnView.hidden = YES;
//    self.bottomBtnUIView.hidden = YES;
    self.timerPressedView.hidden = NO;
    [SimpleSoundPlayer playSound:pressedSound];
//    [self setPhotoSettingBtnViewWithMode:TakePhotoSettingBtnViewModeMainTimer];
//    self.timerLabel.hidden = YES;
//    
//    switch (self.timerLevel){
//        case TimerLevel_0:
//            self.timerLevel = TimerLevel_2;
//            break;
//        case TimerLevel_2:
//            self.timerLevel = TimerLevel_5;
//            break;
//        case TimerLevel_5:
//            self.timerLevel = TimerLevel_10;
//            break;
//        case TimerLevel_10:
//            self.timerLevel = TimerLevel_0;
//            break;
//    }
//    [self setTimer];
//    [self.deleagte timerPressed];
}

- (IBAction)firstSecButtonPressed:(id)sender
{
    self.takePhotoSettingBtnView.hidden = NO;
    self.bottomBtnUIView.hidden = NO;
    self.timerPressedView.hidden = YES;
    
    [SimpleSoundPlayer playSound:pressedSound];
    
    _timerHandler(((UIButton *)sender).tag, self.pressedButtonType);
}

- (IBAction)secondSecButtonPressed:(id)sender
{
    self.takePhotoSettingBtnView.hidden = NO;
    self.bottomBtnUIView.hidden = NO;
    self.timerPressedView.hidden = YES;
    
    [SimpleSoundPlayer playSound:pressedSound];
    
     _timerHandler(((UIButton *)sender).tag, self.pressedButtonType);
}

- (IBAction)thirdSecButtonPressed:(id)sender
{
    self.takePhotoSettingBtnView.hidden = NO;
    self.bottomBtnUIView.hidden = NO;
    self.timerPressedView.hidden = YES;
    
    [SimpleSoundPlayer playSound:pressedSound];
    
     _timerHandler(((UIButton *)sender).tag, self.pressedButtonType);
}

- (IBAction)cancelButtonPressed:(id)sender
{
    [self.titleButton setTitle:@"" forState:UIControlStateNormal];
    self.timerTitleLabel.text = @"";
    [self.firstSecButton setTitle:@"" forState:UIControlStateNormal];
    [self.secondSecButton setTitle:@"" forState:UIControlStateNormal];
    [self.thirdSecButton setTitle:@"" forState:UIControlStateNormal];
    
    self.takePhotoSettingBtnView.hidden = NO;
    self.bottomBtnUIView.hidden = NO;
    self.timerPressedView.hidden = YES;
    
    [SimpleSoundPlayer playSound:pressedSound];
    
     _timerHandler(((UIButton *)sender).tag, self.pressedButtonType);
    
    NSLog(@"Cancel");
}

- (IBAction)flashPressed:(id)sender {
    [SimpleSoundPlayer playSound:pressedSound];
    switch (self.flashMode){
        case FlashModeOn:
            self.flashMode = FlashModeOff;
            break;
        case FlashModeAuto:
            self.flashMode = FlashModeOn;
            break;
        case FlashModeOff:
            self.flashMode = FlashModeAuto;
            break;
    }
    [self setFlash];
    [self.deleagte flashPressed];
}

- (IBAction)selfiePressed:(id)sender {
    [SimpleSoundPlayer playSound:pressedSound];
    self.timerLabelSnap.hidden = YES;
    self.timerLabel.hidden = YES;
    self.isSelfie = !self.isSelfie;
    if (self.isSelfie){
        self.flashBtn.alpha = 0.5f;
        self.flashBtn.enabled = NO;
    }else{
        self.flashBtn.alpha = 1.0f;
        self.flashBtn.enabled = YES;
    }
    [self.deleagte selfiePressed];
}

- (IBAction)settingPressed:(id)sender {
    self.isInUserSettingController = YES;
    [SimpleSoundPlayer playSound:pressedSound];
    
    [self.deleagte settingPressed];
}

- (IBAction)cancelPressed:(id)sender {
    [SimpleSoundPlayer playSound:pressedSound];
    
    if (self.btnCancel.tag == 0) {
        [self showQuitSettingPopup];
//        [[ConnectionManager sharedInstance].snappet disconnect];
//        [ConnectionManager sharedInstance].snappet = nil;
//        [self.snapptRetakeBtn setEnabled:YES];
//        [self.snapptOkBtn setEnabled:YES];
//        [self.deleteSnappetBtn setEnabled:YES];
//        [self tutorialCanceled];
//        [self showConnectionScreen];
    }
    else if (self.btnCancel.tag == 1) {
        [self showQuitSettingPopup];
//        [[ConnectionManager sharedInstance].snappet disconnect];
//        [ConnectionManager sharedInstance].snappet = nil;
//        [self.snapptRetakeBtn setEnabled:YES];
//        [self.snapptOkBtn setEnabled:YES];
//        [self.deleteSnappetBtn setEnabled:YES];
//        [self tutorialCanceled];
//        [self showConnectionScreen];
    }
    else if (self.btnCancel.tag == 2) {
        [self showQuitTutorialPopup];
//        [[ConnectionManager sharedInstance].snappet disconnect];
//        [ConnectionManager sharedInstance].snappet = nil;
//        [self.snapptRetakeBtn setEnabled:YES];
//        [self.snapptOkBtn setEnabled:YES];
//        [self.deleteSnappetBtn setEnabled:YES];
//        [self tutorialCanceled];
//        [self showSnappetTakePhotoScreen];
    }
}

- (IBAction)continuePressed:(id)sender {
    [SimpleSoundPlayer playSound:pressedSound];
    
    if (self.btnContinue.tag == 1) {
        [self.viewNewSnapPets.viewTutorial showDeviceModeTutorial];
    }
    else if (self.btnContinue.tag == 2) {
        [self.viewNewSnapPets.viewTutorial showCompletedModeTutorial];
        self.btnCancel.hidden = YES;
        self.btnContinue.hidden = YES;
        [self.retakeBtn setEnabled:YES];
        [self.photoOkBtn setEnabled:YES];
        [self.deleteBtn setEnabled:YES];
        [self.snapptRetakeBtn setEnabled:YES];
        [self.snapptOkBtn setEnabled:YES];
        [self.deleteSnappetBtn setEnabled:YES];
        [self.camImgView setHidden:NO];
    }
}

- (void)setupMintView
{
    
}

- (IBAction)mintTimerSelected:(id)sender
{
    [self.mintTimerButton setTitle:[NSString stringWithFormat:@"%lis", (long)((UIButton *)sender).tag] forState:UIControlStateNormal];
    _timerHandler(((UIButton *)sender).tag, self.pressedButtonType);
    [self setupTimerView];
}

- (void)setupTimerView
{
    self.mintTimerButton.hidden = !self.mintTimerButton.hidden;
    self.mintSeriesButton.hidden = !self.mintSeriesButton.hidden;
    self.mintViewTitleLabel.hidden = !self.mintViewTitleLabel.hidden;
    self.mintViewCloseButton.hidden = !self.mintViewCloseButton.hidden;
    self.mintViewFirstButton.hidden = !self.mintViewFirstButton.hidden;
    self.mintViewTypeTitleButton.hidden = !self.mintViewTypeTitleButton.hidden;
    self.mintViewSecondButton.hidden = !self.mintViewSecondButton.hidden;
    self.mintViewThirdButton.hidden = !self.mintViewThirdButton.hidden;
}

- (IBAction)mintTimerCloseButtonPressed:(id)sender
{
    [self.mintTimerButton setTitle:@"x" forState:UIControlStateNormal];
    [SimpleSoundPlayer playSound:pressedSound];
    _timerHandler(((UIButton *)sender).tag, self.pressedButtonType);
    [self setupTimerView];
}

- (IBAction)mintTimerButtonPressed:(id)sender
{
    [self setupTimerView];
    
//    [self.mintViewTypeTitleButton setTitle:@"Timer" forState:UIControlStateNormal];
    self.mintViewTitleLabel.text = @"Take a picture in:";
    [self.mintViewFirstButton setTitle:@"3S" forState:UIControlStateNormal];
    self.mintViewFirstButton.tag = 3;
    [self.mintViewSecondButton setTitle:@"7S" forState:UIControlStateNormal];
    self.mintViewSecondButton.tag = 7;
    [self.mintViewThirdButton setTitle:@"10S" forState:UIControlStateNormal];
    self.mintViewThirdButton.tag = 10;
    
//    self.timerAndSeriesView.hidden = !self.timerAndSeriesView.hidden;
//    if ( self.mintTimerButton.hidden )
//    {
//        self.mintTimerButton.hidden = NO;
//    }
//    else
//    {
//        self.mintTimerButton.hidden = YES;
////        self.takePhotoSettingBtnView.hidden = YES;
////        self.bottomBtnUIView.hidden = YES;
//        self.timerPressedView.hidden = NO;
////        [self setupViewUI:kPressedButtonTypeTimer];
//
//        self.titleButton.hidden = NO;
//        self.timerTitleLabel.hidden = NO;
//        self.firstSecButton.hidden = NO;
//        self.secondSecButton.hidden = NO;
//        self.thirdSecButton.hidden = NO;
//        [self.titleButton setTitle:@"Timer" forState:UIControlStateNormal];
//        self.timerTitleLabel.text = @"Take a picture in:";
//        [self.firstSecButton setTitle:@"3S" forState:UIControlStateNormal];
//        [self.secondSecButton setTitle:@"7S" forState:UIControlStateNormal];
//        [self.thirdSecButton setTitle:@"10S" forState:UIControlStateNormal];
//
//    }
}

- (IBAction)mintSeriesButtonPressed:(id)sender
{
    
}

#pragma mark - Other
- (void)clearPreview {
    [self.phoneTakePhoto setImage:nil];
    [self.bottomBtnUIViewCover setHidden:YES];
}

- (void)clearSnappetPreview {
    [self.snappetTakenPhoto setImage:nil];
    [self.viewSnappetTutorial setHidden:NO];
//    [self.timerSnappetBtn setHidden:NO];
    [self.snapptRetakeBtn setHidden:YES];
    [self.snapptOkBtn setHidden:YES];
    [self.deleteSnappetBtn setHidden:YES];
}

- (void)initView
{
    if (!self.isDeviceNoFlash){
        self.isNoFlashBtn = NO;
    }
    [self setMode];
    [self setCameraButton:NO];
}

- (void)disableSettingButton {
    self.settingBtn.hidden = YES;
}

- (void)disableCloseButton {
    self.closeBtn.hidden = YES;
}

- (void)animateScanButton {
    if (self.scanBtn.tag != 1) {
//        [self.scanBtn setImage:[UIImage imageNamed:@"btn_connect_1.png"] forState:UIControlStateNormal];
//        [self.scanBtn setAlpha:1.0f];
        self.scanBtn.tag = 1;
        
        //fade in
        [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse|UIViewAnimationOptionAllowUserInteraction animations:^{
            [self.scanBtn setAlpha:0.2f];
        } completion:nil];
    }
    
/*
    NSString* str = [NSString stringWithFormat:@"btn_connect_1.png"];
    if (self.scanBtn.tag == 1) {
        [self.scanBtn setTag:0];
    }
    else {
        [self.scanBtn setTag:1];
    }
    [self.scanBtn setImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
 */
}

- (void)animateSnapPetsTutorial {
   

    
    NSInteger animationImageCount =  3; //38;
    NSString * str;
    number ++;
    if(number > animationImageCount){
        number = 0;
        [timerAnimatingSnappetTutorial invalidate];
        return;
    }
    
    if(number < 10)
        str = [NSString stringWithFormat:@"Setup-Complete-animation_0000%d", number];
    else
        str = [NSString stringWithFormat:@"Setup-Complete-animation_000%d", number];
    
    [self.viewSnappetTutorial setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:str ofType:@"png"]]];

}

- (void)animateSnapPetsDownloading {
    CGSize screenBounds = [self bounds].size;
    NSString* str = nil;
    if (self.imgViewSnapPetDownloading.tag == 1) {
//        if (screenBounds.height == 1024 || screenBounds.height == 2048)
//            str = [NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 1, @"2x~ipad"];
//        else
//            str = [NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 1, @"2x"];
        str = [NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d.png", 1];
        [self.imgViewSnapPetDownloading setTag:0];
    }
    else {
//        if (screenBounds.height == 1024 || screenBounds.height == 2048)
//            str = [NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 2, @"2x~ipad"];
//        else
//            str = [NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 2, @"2x"];
        str = [NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d.png", 2];
        [self.imgViewSnapPetDownloading setTag:1];
    }
    [self.imgViewSnapPetDownloading setImage:[UIImage imageNamed:str]];
}

- (void)setMode {
    if (self.isPhoneShutterMode){
        [self.modeBtn setImage:[UIImage imageNamed:@"btn_cammode_1"] forState:UIControlStateNormal];
        self.snappetModeView.hidden = YES;
        self.cameraModeView.hidden = NO;
        self.phoneTakePhoto.hidden = YES;
        self.btnGroupView.hidden = NO;
        [timerAnimatingSnappetTutorial invalidate];
        timerAnimatingSnappetTutorial = nil;
        self.blackview.hidden = YES;
        
    }else{
        [self.modeBtn setImage:[UIImage imageNamed:@"btn_cammode_2"] forState:UIControlStateNormal];
        self.snappetModeView.hidden = NO;
        self.cameraModeView.hidden = YES;
        [self.snappetTakenPhoto setImage:nil];
        [self.viewSnappetTutorial setHidden:NO];
//        [self.timerSnappetBtn setHidden:NO];
        //[self animateSnapPetsTutorial];
        if (timerAnimatingSnappetTutorial == nil){
            [ self.viewSnappetTutorial setTag:0];
           // [self animateSnapPetsTutorial];
            timerAnimatingSnappetTutorial = [NSTimer scheduledTimerWithTimeInterval:0.07f target:self selector:@selector(animateSnapPetsTutorial) userInfo:nil repeats:YES];
        }
        
    }
}
- (void)setPhotoSet {

    if (self.isPhoneShutterMode) {
        NSString *timerImgName = @"x";
        if (self.photoSetLevel == PhotoSetLevel_0){
            timerImgName = @"x";
        }else if (self.photoSetLevel == PhotoSetLevel_30){
            timerImgName = @"30s";
        }else if (self.photoSetLevel == PhotoSetLevel_120){
            timerImgName = @"2min";
        }else if (self.photoSetLevel == PhotoSetLevel_300){
            timerImgName = @"5min";
            
        }
        [self.photosetBtn setTitle:timerImgName forState:UIControlStateNormal];
        //  [self.timerBtn setImage:[UIImage imageNamed:timerImgName] forState:UIControlStateNormal];
    }
//    else {
//        NSString *timerImgName = @"btn_timer_0";
//        if (self.timerLevel == TimerLevel_0){
//            timerImgName = @"x";
//        }else if (self.timerLevel == TimerLevel_3){
//            timerImgName = @"2s";
//        }else if (self.timerLevel == TimerLevel_7){
//            timerImgName = @"5s";
//        }else if (self.timerLevel == TimerLevel_10){
//            timerImgName = @"10s";
//        }
    
        //[self.timerSnappetBtn setImage:[UIImage imageNamed:timerImgName] forState:UIControlStateNormal];
    

}
- (void)setTimer {

    if (self.isPhoneShutterMode) {
        NSString *timerImgName = @"x";
        if (self.timerLevel == TimerLevel_0){
            timerImgName = @"x";
        }else if (self.timerLevel == TimerLevel_3){
            timerImgName = @"3s";
        }else if (self.timerLevel == TimerLevel_7){
            timerImgName = @"7s";
        }else if (self.timerLevel == TimerLevel_10){
            timerImgName = @"10s";

        }
        [self.timerBtn setTitle:timerImgName forState:UIControlStateNormal];
      //  [self.timerBtn setImage:[UIImage imageNamed:timerImgName] forState:UIControlStateNormal];
    }
    else {
        NSString *timerImgName = @"btn_timer_0";
        if (self.timerLevel == TimerLevel_0){
            timerImgName = @"x";
        }else if (self.timerLevel == TimerLevel_3){
            timerImgName = @"2s";
        }else if (self.timerLevel == TimerLevel_7){
            timerImgName = @"5s";
        }else if (self.timerLevel == TimerLevel_10){
            timerImgName = @"10s";
        }
  
        //[self.timerSnappetBtn setImage:[UIImage imageNamed:timerImgName] forState:UIControlStateNormal];
    }
   
}

- (void)setFlash{
    NSString *flashImgName = @"btn_flash_off";
    if (self.flashMode == FlashModeOff){
        flashImgName = @"btn_flash_off";
    }else if (self.flashMode == FlashModeAuto){
        flashImgName = @"btn_flash_auto";
    }else if (self.flashMode == FlashModeOn){
        flashImgName = @"btn_flash_on";
    }
    [self.flashBtn setImage:[UIImage imageNamed:flashImgName] forState:UIControlStateNormal];
}

- (void)setSelfie{

}

- (void)noFlashBtn {
    self.flashBtn.hidden = YES;
    
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    
//    CGRect rect = self.timerBtn.frame;
//    rect.origin.x = (screenBounds.width / 2 - rect.size.width) / 2;
//    self.timerBtn.frame = rect;
//    
//    rect = self.selfieBtn.frame;
//    rect.origin.x = screenBounds.width / 2 + (screenBounds.width / 2 - rect.size.width) / 2;
//    self.selfieBtn.frame = rect;
    
    self.isDeviceNoFlash = YES;
    self.isNoFlashBtn = YES;
}

- (void)setCameraButton :(BOOL)isShowPreview
{
    if (isShowPreview) {
        self.flashBtn.hidden = YES;
        self.selfieBtn.hidden = YES;
        self.timerBtn.hidden = YES;
        self.takePhotoSettingBtnView.hidden = YES;
        self.retakeBtn.hidden = NO;
        self.deleteSnappetBtn.hidden = NO;
        if (!self.isUnderTutorial)
            self.closeBtn.hidden = NO;
    }else{
        self.closeBtn.hidden = YES;
        
        if (self.isPhoneShutterMode) {
            // Camrea Mode Before Take Photo Button
            if (!self.isNoFlashBtn){
                self.flashBtn.hidden = NO;
                self.flashBtn.alpha = 1.0f;
                self.flashBtn.enabled = YES;
            }
            
            self.timerBtn.hidden = NO;
            self.takePhotoSettingBtnView.hidden = NO;
            
            self.selfieBtn.hidden = NO;
            if (self.isSelfie)
                self.flashBtn.enabled = NO;
            
            // Camera Mode After Taken Photo Button
            self.takenPhotoSettingBtnView.hidden = YES;
            self.retakeBtn.hidden = YES;
            self.photoOkBtn.hidden = YES;
            self.deleteBtn.hidden = YES;
        }
        else {
            self.snappetModeView.hidden = NO;
            self.cameraModeView.hidden = YES;
            [self.snappetTakenPhoto setImage:nil];
            [self.viewSnappetTutorial setHidden:NO];
//            [self.timerSnappetBtn setHidden:NO];
            self.snappetTakenPhoto.hidden = YES;
            
            // SnapPet Mode After Taken Photo Button
            self.snapptRetakeBtn.hidden = YES;
            self.snapptOkBtn.hidden = YES;
            self.deleteSnappetBtn.hidden = YES;
        }
    }
}

- (void)showPreview:(UIImage*)img
{
    self.phoneTakePhoto.hidden = NO;
    [self setCameraButton:YES];
    
//    self.btnGroupView.hidden = YES;
    self.btnGroupView.hidden = NO;

    UIImage *resizeImage = img;
    
    float width = [UIScreen mainScreen].bounds.size.width;
    float height = width;
    
    
    
    float widthRatio = width / resizeImage.size.width;
    float heightRatio = height / resizeImage.size.height;
//    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
    
    width = resizeImage.size.width * widthRatio;
    height = resizeImage.size.height * widthRatio;
    
    CGSize newSize = CGSizeMake(width,height);
    UIGraphicsBeginImageContextWithOptions(newSize,NO,0.0);
    CGRect rect = CGRectMake(0, 0, (int)width, (int)height);
    
//    rect.size.width  = width;
//    rect.size.height = height;
    
    //indent in case of width or height difference
//    float offset = (width - height) / 2;
//    if (offset > 0) {
//        rect.origin.y = offset;
//    }
//    else {
//        rect.origin.x = -offset;
//    }
    
    [resizeImage drawInRect: rect];
    
//    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    self.phoneTakePhoto.image = resizeImage;
    self.phoneTakePhoto.contentMode = UIViewContentModeScaleAspectFit;
    self.tempImage = resizeImage;
    
//    CGSize newSize = CGSizeMake(width,width);
//    UIGraphicsBeginImageContextWithOptions(newSize,NO,0.0);
//    CGRect rect = CGRectMake(0, 0, width, height);
//    
//    float widthRatio = resizeImage.size.width / width;
//    float heightRatio = resizeImage.size.height / height;
//    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
//    
//    width = resizeImage.size.width / divisor;
//    height = resizeImage.size.height / divisor;
//    
//    rect.size.width  = width;
//    rect.size.height = height;
//    
//    //indent in case of width or height difference
//    float offset = (width - height) / 2;
//    if (offset > 0) {
//        rect.origin.y = offset;
//    }
//    else {
//        rect.origin.x = -offset;
//    }
//    
//    [resizeImage drawInRect: rect];
//    
//    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    self.previewImage.image = smallImage;
//    self.previewImage.contentMode = UIViewContentModeScaleAspectFit;
    
//    UIImage* scaledImgV = [img resizedImageToFitInSize:self.previewImage.bounds.size scaleIfSmaller:NO];
//    [self.previewImage setFrame:CGRectMake(0, 0, scaledImgV.size.width, scaledImgV.size.height)];
//    self.previewImage.image = scaledImgV;
//
//    UIImageView * view = [[UIImageView alloc] initWithImage:scaledImgV];
    
    
//    UIImageView *view = [[UIImageView alloc] initWithImage:img];
//    
//    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
//    CGFloat cameraAspectRatio = 4.0f/3.0f;
//    CGFloat camViewHeight = screenBounds.width * cameraAspectRatio;
//    CGFloat scale = screenBounds.height / camViewHeight;
////    view.transform = CGAffineTransformMakeTranslation(0, (screenBounds.height - camViewHeight) / 2.0);
//    view.transform = CGAffineTransformMakeTranslation((img.size.width*scale - img.size.width)/2, (img.size.height*scale-img.size.height)/2);
//    view.transform = CGAffineTransformScale(view.transform, scale, scale);
////
////    self.previewImage = view;
//[self addSubview:view];
    
//    UIImageView *view = [[UIImageView alloc] initWithImage:img];
//    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
//    CGFloat cameraAspectRatio = 4.0f/3.0f;
//    CGFloat camViewHeight = screenBounds.width * cameraAspectRatio;
//    CGFloat scale = screenBounds.height / camViewHeight;
//    CGFloat imgScaleX = screenBounds.width * scale / img.size.width;
//    CGFloat imgScaleY = screenBounds.height * scale / img.size.height;
////    view.transform = CGAffineTransformMakeTranslation(0, (screenBounds.height - camViewHeight) / 2.0);
//    view.transform = CGAffineTransformMakeTranslation(-(img.size.width*scale - img.size.width)/2, -(img.size.height*scale-img.size.height)/2);
//    view.transform = CGAffineTransformScale(view.transform, imgScaleX, imgScaleY);
////    view.transform = CGAffineTransformMakeTranslation((screenBounds.width - img.size.width) / 2.0, (screenBounds.height - img.size.height) / 2.0);
////    view.transform = CGAffineTransformScale(view.transform, imgScaleX, imgScaleY);
//
//    view.contentMode = UIViewContentModeScaleAspectFit;
//    [self addSubview:view];
    
    
//    UIImage *resizeImage = img;
//    
//    float width = screenBounds.width;
//    float height = screenBounds.height;
//    
//    CGSize newSize = CGSizeMake(width * scale,height * scale);
//    UIGraphicsBeginImageContextWithOptions(newSize,NO,0.0);
//    CGRect rect = CGRectMake(0, 0, width * scale, height * scale);
//    
//    float widthRatio = resizeImage.size.width / width;
//    float heightRatio = resizeImage.size.height / height;
//    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
////
//    width = resizeImage.size.width / divisor;
//    height = resizeImage.size.height / divisor;
//    
//    rect.size.width  = width;
//    rect.size.height = height;
//    
////    indent in case of width or height difference
//        float offset = (width - height) / 2;
//        if (offset > 0) {
//            rect.origin.y = offset;
//        }
//        else {
//            rect.origin.x = -offset;
//        }
//    
//    [resizeImage drawInRect: rect];
//    
//    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    self.previewImage.image = smallImage;
//    self.previewImage.contentMode = UIViewContentModeScaleAspectFit;

    
//    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
//    CGFloat ratio = screenBounds.width / img.size.width;
//    self.previewImage.transform = CGAffineTransformScale(self.previewImage.transform, ratio, ratio);
////    CGFloat cameraAspectRatio = 4.0f/3.0f;
//    CGFloat cameraAspectRatio = 3.0f/2.0f;
//    CGFloat camViewHeight = self.previewImage.image.size.width * cameraAspectRatio;
//    CGFloat scale = screenBounds.width / self.previewImage.image.size.width;
//    self.previewImage.transform = CGAffineTransformMakeTranslation(0, (screenBounds.height - camViewHeight) / 2.0);
//    self.previewImage.transform = CGAffineTransformScale(self.previewImage.transform, screenBounds.width / self.previewImage.image.size.width, screenBounds.height / self.previewImage.image.size.height);
    
//    self.previewImage.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)tutorial1Completed {
    self.btnCancel.hidden = NO;
    self.btnContinue.hidden = NO;
    [self.snapptRetakeBtn setEnabled:NO];
    [self.snapptOkBtn setEnabled:NO];
    [self.deleteSnappetBtn setEnabled:NO];
    
    [self.modeBtn setHidden:YES];
    [self.libBtn setHidden:YES];
    
    [self.btnCancel setTag:1];
    [self.btnContinue setTag:1];
    self.labelName.text = [AppConfig sharedInstance].strRobotName;
}

- (void)tutorial2Completed {
    self.btnCancel.hidden = NO;
    self.btnContinue.hidden = NO;
    
    [self.retakeBtn setEnabled:NO];
    [self.photoOkBtn setEnabled:NO];
    [self.deleteBtn setEnabled:NO];
    
    [self.modeBtn setHidden:YES];
    [self.libBtn setHidden:YES];
    
    [self.btnCancel setTag:2];
    [self.btnContinue setTag:2];
}

- (void)tutorial3Completed {
    self.btnCancel.hidden = YES;
    self.btnContinue.hidden = YES;
    
    [self.modeBtn setHidden:NO];
    [self.libBtn setHidden:NO];
    
    [self.snapptRetakeBtn setEnabled:YES];
    [self.snapptOkBtn setEnabled:YES];
    [self.deleteSnappetBtn setEnabled:YES];
    [self.camImgView setHidden:NO];
    
    [self.closeBtn setHidden:NO];
    [self.settingBtn setHidden:NO];
    
    [self.deleagte takePhoto];
    
    self.isUnderTutorial = NO;
    [Flurry logEvent:@"Tutorial Completed"];
}

- (void)showQuitSettingPopup {
    [self.viewPopup showQuitSettingView];
    self.viewPopup.delegate = self;
}

- (void)showQuitTutorialPopup {
    [self.viewPopup showQuitTutorialView];
    self.viewPopup.delegate = self;
}

- (void)tutorialCanceled {
    self.btnCancel.hidden = YES;
    self.btnContinue.hidden = YES;

    if (self.viewTutorial.mode == SnapPetsMode) {
        [self.retakeBtn setEnabled:YES];
        [self.photoOkBtn setEnabled:YES];
        [self.deleteBtn setEnabled:YES];
        [self.snapptRetakeBtn setEnabled:YES];
        [self.snapptOkBtn setEnabled:YES];
        [self.deleteSnappetBtn setEnabled:YES];
        
        if (!self.isPhoneShutterMode)
            [self modePressed:nil];
        [self.camImgView setHidden:NO];
        [self.snappetTakenPhoto setImage:nil];
        
        [self.modeBtn setHidden:YES];
        [self.libBtn setHidden:NO];
        
        [self.labelName setHidden:YES];
       // [self.viewImgLogo setHidden:NO];
        self.isUnderTutorial = NO;
        [Flurry logEvent:@"Tutorial Canceled in SnapPet Setting Page"];
    }
    else if (self.viewTutorial.mode == DeviceMode) {
        self.btnCancel.hidden = YES;
        self.btnContinue.hidden = YES;
        
        [self.modeBtn setHidden:NO];
        [self.libBtn setHidden:NO];
        
        [self.snapptRetakeBtn setEnabled:YES];
        [self.snapptOkBtn setEnabled:YES];
        [self.deleteSnappetBtn setEnabled:YES];
        [self.camImgView setHidden:NO];
        
        [self.closeBtn setHidden:NO];
        [self.settingBtn setHidden:NO];
        
        if (self.phoneTakePhoto.image != nil)
            [self.deleagte takePhoto];
        
        self.isUnderTutorial = NO;
        [Flurry logEvent:@"Tutorial Canceled in Tutorial Page"];
    }
}

- (void)modePressed {
    [self modePressed:self.modeBtn];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.deleagte cancelTimerPressed];
}

#pragma mark - SnapPetPopupDelegate
- (void)popupViewSelectedLeftButton {
    switch (self.viewTutorial.mode) {
        case SnapPetsMode:
            if (self.viewTutorial.counter == 0) {
                [self.viewPopup hide];
                [self.viewTutorial resumeTutorial];
            }
            else if (self.viewTutorial.counter == 1) {
                [self.viewPopup hide];
                [self.viewEnterName show];
            }
            else if (self.viewTutorial.counter == 2) {
                [self.viewPopup hide];
                [self.viewTutorial resumeTutorial];
            }
            else if (self.viewTutorial.counter == 4) {
                [self.viewPopup hide];
            }
            break;
        case DeviceMode:
            if (self.viewTutorial.counter == 0) {
                [self.viewPopup hide];
                [self.viewTutorial resumeTutorial];
            }
            else if (self.viewTutorial.counter == 1) {
                [self.viewPopup hide];
                [self.viewTutorial resumeTutorial];
            }
            else if (self.viewTutorial.counter == 3) {
                [self.viewPopup hide];
            }
            break;
        default:
            break;
    }
}

- (void)popupViewSelectedRightButton {
    switch (self.viewTutorial.mode) {
        case SnapPetsMode:
            if (self.viewTutorial.counter == 0) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            else if (self.viewTutorial.counter == 1) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            else if (self.viewTutorial.counter == 2) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            else if (self.viewTutorial.counter == 4) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            [[ConnectionManager sharedInstance].snappet disconnect];
            [ConnectionManager sharedInstance].snappet = nil;
            [self.scanBtn setHidden:NO];
            self.scanBtn.tag = 0;
            [self.scanBtn setAlpha:1.0f];
            break;
        case DeviceMode:
            if (self.viewTutorial.counter == 0) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            else if (self.viewTutorial.counter == 1) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            else if (self.viewTutorial.counter == 3) {
                [self.viewPopup hide];
                [self tutorialCanceled];
            }
            break;
        default:
            break;
    }
}

- (void)popupViewSelectedCenterFullButton {
    
}

-(void)setPhotoSettingBtnViewWithMode:(TakePhotoSettingBtnViewMode)mode{
    UIColor * purpleColor = Rgb2UIColor(135, 50, 153);
    UIColor * lightGreenColor = Rgb2UIColor(43, 211, 198);
    /*
     TakePhotoSettingBtnViewModeMain = 1,
     TakePhotoSettingBtnViewModeMainTimer,
     TakePhotoSettingBtnViewModeMainPhotoSet,
     TakePhotoSettingBtnViewModeMainWithPet,
     TakePhotoSettingBtnViewModeMainWithPetTimer,
     TakePhotoSettingBtnViewModeMainWithPetPhotoSet,
     */
    switch (mode) {
        case TakePhotoSettingBtnViewModeMain:
        {
            [self.takePhotoSettingBtnView setBackgroundColor:purpleColor];
        }
            break;
        case TakePhotoSettingBtnViewModeMainTimer:
        {
            [self.takePhotoSettingBtnView setBackgroundColor:purpleColor];
            
        }
            break;
        case TakePhotoSettingBtnViewModeMainPhotoSet:
        {
            [self.takePhotoSettingBtnView setBackgroundColor:purpleColor];
        }
            break;
        case TakePhotoSettingBtnViewModeMainWithPet:
        {
            [self.takePhotoSettingBtnView setBackgroundColor:lightGreenColor];
        }
            break;
        case TakePhotoSettingBtnViewModeMainWithPetTimer:
        {
           [self.takePhotoSettingBtnView setBackgroundColor:lightGreenColor];
        }
            break;
        case TakePhotoSettingBtnViewModeMainWithPetPhotoSet:
        {
            [self.takePhotoSettingBtnView setBackgroundColor:lightGreenColor];
        }
            break;
            
        default:
            break;
    }
    
    
}
@end
