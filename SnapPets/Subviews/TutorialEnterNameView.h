//
//  TutorialEnterNameView.h
//  SnapPets
//
//  Created by David Chan on 13/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TutorialDelegate.h"

@class TutorialSnapPetView;

@interface TutorialEnterNameView : UIView {
    NSTimer* timerAnimateNewSnapPet;
}

- (void)show;
- (void)hide;
- (void)animateNewSnapPet;
- (IBAction)buttonAction:(id)sender;

@property (nonatomic, strong) IBOutlet UITextField* tfName;

@property (nonatomic, strong) IBOutlet UIView* imgBaseView;
@property (nonatomic, strong) IBOutlet UIImageView* imgView;
@property (nonatomic, strong) IBOutlet UILabel* labelText;

@property (nonatomic, strong) IBOutlet UIButton* btnCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnNext;

@property (nonatomic, strong) IBOutlet UIImageView* imgCancel;
@property (nonatomic, strong) IBOutlet UIImageView* imgNext;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintY;

@property (readwrite) TutorialSnapPetView* viewTutorial;

@property (readwrite) NSString* strRobotName;
@property (nonatomic, weak) id<TutorialDelegate> tutorialDelegate;

@end
