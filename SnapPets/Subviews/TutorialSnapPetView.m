//
//  TutorialSnapPetView.m
//  SnapPets
//
//  Created by David Chan on 12/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "TutorialSnapPetView.h"
#import "LocalizationManager.h"
#import "ConnectionManager.h"
#import "SimpleSoundPlayer.h"
#import "TutorialEnterNameView.h"
#import "ConnectionManager.h"
#import "AppConfig.h"
#import "Flurry.h"
@implementation TutorialSnapPetView
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imgBaseView.layer.cornerRadius = 5.0f;
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        self.imgBaseView.layer.cornerRadius = 10.0f;
    }
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* fontLabel50 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50.0f];
        self.labelText.font = fontLabel50;
    }
    
//    [self.btnNext setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnNext"] forState:UIControlStateNormal];
//    [self.btnCancel setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnCancel"] forState:UIControlStateNormal];
}

- (void)resumeTutorial {
    [self setHidden:NO];
}

- (void)showSnapPetsModeTutorial {
    [self setHidden:NO];
    
    self.counter = 0;
    self.mode = SnapPetsMode;
    [self _enterSnapPetsModeTutorial1View];
}

- (void)showAndContinueSnapPetsModeTutorial {
    [self setHidden:NO];
    [self _enterSnapPetsModeTutorial2View];
}

- (void)showDeviceModeTutorial {
    [self setHidden:NO];
    
    self.counter = 0;
    self.mode = DeviceMode;
    [self _enterDeviceModeTutorial1View];
}

- (void)showCompletedModeTutorial {
    [self setHidden:NO];
    
    self.counter = 0;
    self.mode = CompletedMode;
    [self _enterCompletedModeTutorialView];
}

- (void)hide {
    [self setHidden:YES];
}

- (void)animateTutorial {
    switch (self.mode) {
        case SnapPetsMode:
            if (self.counter == 0) {
                NSString* str = nil;
                NSString* strTitle = @"anime_popup_loading-";
                CGSize screenBounds = [self bounds].size;
                if (self.imgView.tag == 0) {
                    [self.imgView setTag:1];
                }
                else if (self.imgView.tag == 1) {
                    [self.imgView setTag:2];
                }
                else if (self.imgView.tag == 2) {
                    [self.imgView setTag:3];
                }
//                if (screenBounds.height == 1024 || screenBounds.height == 2048)
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x~ipad"];
//                else
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x"];
                str = [NSString stringWithFormat:@"%@%d@2x.png", strTitle, (int)self.imgView.tag];
                [self.imgView setImage:[UIImage imageNamed:str]];
                if (self.imgView.tag == 3) {
                    [self.imgView setTag:0];
                }
            }
            else if (self.counter == 2) {
                NSString* str = nil;
                NSString* strTitle = @"anime_popup_test-snap-pet-";
                CGSize screenBounds = [self bounds].size;
                if (self.imgView.tag == 0) {
                    [self.imgView setTag:1];
                }
                else if (self.imgView.tag == 1) {
                    [self.imgView setTag:2];
                }
//                if (screenBounds.height == 1024 || screenBounds.height == 2048)
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x~ipad"];
//                else
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x"];
                str = [NSString stringWithFormat:@"%@%d@2x.png", strTitle, (int)self.imgView.tag];
                [self.imgView setImage:[UIImage imageNamed:str]];
                if (self.imgView.tag == 2) {
                    [self.imgView setTag:0];
                }
            }
            else if (self.counter == 3) {
                NSString* str = nil;
                NSString* strTitle = @"anime_popup_loading-";
                CGSize screenBounds = [self bounds].size;
                if (self.imgView.tag == 0) {
                    [self.imgView setTag:1];
                }
                else if (self.imgView.tag == 1) {
                    [self.imgView setTag:2];
                }
                else if (self.imgView.tag == 2) {
                    [self.imgView setTag:3];
                }
//                if (screenBounds.height == 1024 || screenBounds.height == 2048)
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x~ipad"];
//                else
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x"];
                str = [NSString stringWithFormat:@"%@%d@2x.png", strTitle, (int)self.imgView.tag];
                [self.imgView setImage:[UIImage imageNamed:str]];
                if (self.imgView.tag == 3) {
                    [self.imgView setTag:0];
                }
            }
            break;
        case DeviceMode:
            if (self.counter == 0) {
                CGSize screenBounds = [self bounds].size;
                if (screenBounds.height == 1024 || screenBounds.height == 2048) {
                    if (self.imgView.tag == 0) {
                        self.constraintBtnWidth.constant = 180.0f*2;
                        [self.imgView setTag:1];
                    }
                    else {
                        self.constraintBtnWidth.constant = 180.0f*0.8f*2;
                        [self.imgView setTag:0];
                    }
                    self.constraintBtnHeight.constant = self.constraintBtnWidth.constant;
                }
                else {
                    if (self.imgView.tag == 0) {
                        self.constraintBtnWidth.constant = 180.0f;
                        [self.imgView setTag:1];
                    }
                    else {
                        self.constraintBtnWidth.constant = 180.0f*0.8f;
                        [self.imgView setTag:0];
                    }
                    self.constraintBtnHeight.constant = self.constraintBtnWidth.constant;
                }
            }
            else if (self.counter == 1) {
                NSString* str = nil;
                NSString* strTitle = @"anime_popup_press-snap-pet-mobile-";
                CGSize screenBounds = [self bounds].size;
                if (self.imgView.tag == 0) {
                    [self.imgView setTag:1];
                }
                else if (self.imgView.tag == 1) {
                    [self.imgView setTag:2];
                }
//                if (screenBounds.height == 1024 || screenBounds.height == 2048)
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x~ipad"];
//                else
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x"];
                str = [NSString stringWithFormat:@"%@%d@2x.png", strTitle, (int)self.imgView.tag];
                UIImage* img = [UIImage imageNamed:str];
                [self.imgView setImage:img];
                if (self.imgView.tag == 2) {
                    [self.imgView setTag:0];
                }
            }
            else if (self.counter == 2) {
                NSString* str = nil;
                NSString* strTitle = @"anime_popup_loading-";
                CGSize screenBounds = [self bounds].size;
                if (self.imgView.tag == 0) {
                    [self.imgView setTag:1];
                }
                else if (self.imgView.tag == 1) {
                    [self.imgView setTag:2];
                }
                else if (self.imgView.tag == 2) {
                    [self.imgView setTag:3];
                }
//                if (screenBounds.height == 1024 || screenBounds.height == 2048)
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x~ipad"];
//                else
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x"];
                str = [NSString stringWithFormat:@"%@%d@2x.png", strTitle, (int)self.imgView.tag];
                [self.imgView setImage:[UIImage imageNamed:str]];
                if (self.imgView.tag == 3) {
                    [self.imgView setTag:0];
                }
            }
            break;
        case CompletedMode:
            if (self.counter == 0) {
                NSString* str = nil;
                NSString* strTitle = @"anime_popup_loading-";
                CGSize screenBounds = [self bounds].size;
                if (self.imgView.tag == 0) {
                    [self.imgView setTag:1];
                }
                else if (self.imgView.tag == 1) {
                    [self.imgView setTag:2];
                }
                else if (self.imgView.tag == 2) {
                    [self.imgView setTag:3];
                }
//                if (screenBounds.height == 1024 || screenBounds.height == 2048)
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x~ipad"];
//                else
//                    str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgView.tag, @"2x"];
                str = [NSString stringWithFormat:@"%@%d@2x.png", strTitle, (int)self.imgView.tag];
                [self.imgView setImage:[UIImage imageNamed:str]];
                if (self.imgView.tag == 3) {
                    [self.imgView setTag:0];
                }
            }
            break;
        default:
            break;
    }
}

- (void)_enterSnapPetsModeTutorial1View {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Set up Snap Pet"]];
    [self.imgView setImage:[UIImage imageNamed:@""]];
    
    if (timerAnimateTutorial == nil)
        timerAnimateTutorial = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(animateTutorial) userInfo:nil repeats:YES];

    [self animateTutorial];
    
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048)
        self.constraintImgView.constant = -40;
    else
        self.constraintImgView.constant = -20;
    [self.btnNext setHidden:NO];
    [self.btnCancel setHidden:NO];
    [self.btnFullCancel setHidden:YES];
    [self.imgNext setHidden:NO];
    [self.imgCancel setHidden:NO];
    [self.imgFullCancel setHidden:YES];
    [self.btnToggle setHidden:YES];
    [self.imgView setHidden:NO];
    
    [self.btnNext setEnabled:YES];
    
    [ConnectionManager sharedInstance].snappet.autoReconnect = NO;
}

- (void)_enterSnapPetsModeTutorial2View {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Press button to capture image from Snap Pet"]];
    [self.imgView setImage:[UIImage imageNamed:@""]];
    self.counter = 2;
    
    if (timerAnimateTutorial != nil) {
        [timerAnimateTutorial invalidate];
        timerAnimateTutorial = nil;
        timerAnimateTutorial = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(animateTutorial) userInfo:nil repeats:YES];
    }
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048)
        self.constraintImgView.constant = 0;
    else
        self.constraintImgView.constant = 0;
    
    [self animateTutorial];
    
    [self.btnNext setHidden:NO];
    [self.btnCancel setHidden:NO];
    [self.btnFullCancel setHidden:NO];
    [self.imgNext setHidden:YES];
    [self.imgCancel setHidden:YES];
    [self.imgFullCancel setHidden:NO];
    [self.btnToggle setHidden:YES];
    
    [self.btnNext setEnabled:NO];
#if TARGET_IPHONE_SIMULATOR
    if (timerClosePopup != nil) {
        [timerClosePopup invalidate];
        timerClosePopup = nil;
    }
    timerClosePopup = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(_enterSnapPetsModeTutorial3View) userInfo:nil repeats:NO];
#endif
}

- (void)_enterSnapPetsModeTutorial3View {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Loading images from Snap Pet"]];
    [self.imgView setImage:[UIImage imageNamed:@""]];
    self.counter = 3;
    
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048)
        self.constraintImgView.constant = -40;
    else
        self.constraintImgView.constant = -20;
    
    [self.btnNext setHidden:YES];
    [self.btnCancel setHidden:YES];
    [self.btnFullCancel setHidden:NO];
    [self.imgNext setHidden:YES];
    [self.imgCancel setHidden:YES];
    [self.imgFullCancel setHidden:NO];
    [self.btnToggle setHidden:YES];
}

- (void)_enterDeviceModeTutorial1View {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Press toogle button to change cam view"]];
    [self.imgView setImage:nil];
    
    [self.btnNext setHidden:YES];
    [self.btnCancel setHidden:YES];
    [self.btnFullCancel setHidden:NO];
    [self.imgNext setHidden:YES];
    [self.imgCancel setHidden:YES];
    [self.imgFullCancel setHidden:NO];
    [self.btnToggle setHidden:NO];
    [self.imgView setHidden:YES];
    
    [self.btnNext setEnabled:NO];
    if (![[AppConfig sharedInstance].strRobotName isEqualToString:[ConnectionManager sharedInstance].snappet.name])
        [Flurry logEvent:@"Snappet Renamed"];
    
    [[ConnectionManager sharedInstance].snappet snappetWriteDisplayName:[AppConfig sharedInstance].strRobotName];
    [[ConnectionManager sharedInstance].snappet snappetSetUserPhoneName:[[UIDevice currentDevice] name]];
    [ConnectionManager sharedInstance].snappet.autoReconnect = YES;
}

- (void)_enterDeviceModeTutorial2View {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Press toggle button to capture image from the mobile"]];
    
    if (timerAnimateTutorial == nil)
        timerAnimateTutorial = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(animateTutorial) userInfo:nil repeats:YES];
    self.counter = 1;
    
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048)
        self.constraintImgView.constant = 20;
    else
        self.constraintImgView.constant = 40;
    [self animateTutorial];
    
    [self.btnNext setHidden:YES];
    [self.btnCancel setHidden:YES];
    [self.btnFullCancel setHidden:NO];
    [self.imgNext setHidden:YES];
    [self.imgCancel setHidden:YES];
    [self.imgFullCancel setHidden:NO];
    [self.btnToggle setHidden:YES];
    [self.imgView setHidden:NO];
    
    [self.btnNext setEnabled:YES];
    
#if TARGET_IPHONE_SIMULATOR
    if (timerClosePopup != nil) {
        [timerClosePopup invalidate];
        timerClosePopup = nil;
    }
    timerClosePopup = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(_enterDeviceModeTutorial3View) userInfo:nil repeats:NO];
#endif
}

- (void)_enterDeviceModeTutorial3View {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Loading images from Camera"]];
    [self.imgView setImage:[UIImage imageNamed:@""]];
    self.counter = 2;
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048)
        self.constraintImgView.constant = -40;
    else
        self.constraintImgView.constant = -20;
    
    [self.btnNext setHidden:YES];
    [self.btnCancel setHidden:YES];
    [self.btnFullCancel setHidden:NO];
    [self.imgNext setHidden:YES];
    [self.imgCancel setHidden:YES];
    [self.imgFullCancel setHidden:NO];
    [self.btnToggle setHidden:YES];
}

- (void)_enterCompletedModeTutorialView {
    [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Finish setting! Let's snap with your pet!"]];
    [self.imgView setImage:[UIImage imageNamed:@""]];
    CGSize screenBounds = [self bounds].size;
//    if (screenBounds.height == 1024 || screenBounds.height == 2048)
//        [self.imgFullCancel setImage:[UIImage imageNamed:@"btn_ok@2x~ipad.png"]];
//    else
//        [self.imgFullCancel setImage:[UIImage imageNamed:@"btn_ok@2x.png"]];
    [self.imgFullCancel setImage:[UIImage imageNamed:@"btn_ok.png"]];
    self.counter = 0;
    
    [self.btnNext setHidden:YES];
    [self.btnCancel setHidden:YES];
    [self.btnFullCancel setHidden:NO];
    [self.imgNext setHidden:YES];
    [self.imgCancel setHidden:YES];
    [self.imgFullCancel setHidden:NO];
    [self.btnToggle setHidden:YES];
}

- (IBAction)buttonAction:(id)sender {
    if (sender == self.btnCancel) {
        [self setHidden:YES];
        switch (self.mode) {
            case SnapPetsMode:
                if (self.counter == 0) {
                    [self.tutorialDelegate showQuitSettingPopup];
                }
                else if (self.counter == 2) {
                    [self.tutorialDelegate showQuitSettingPopup];
                }
                else if (self.counter == 3) {
                    [self.tutorialDelegate showQuitSettingPopup];
                }
                break;
            case DeviceMode:
                if (self.counter == 0) {
                }
                else if (self.counter == 2) {
                    [self _enterDeviceModeTutorial3View];
                }
                else if (self.counter == 3) {
//                    [self.tutorialDelegate showQuitTutorialPopup];
                    [self hide];
                    [[ConnectionManager sharedInstance].snappet disconnect];
                    [ConnectionManager sharedInstance].snappet = nil;
                }
                break;
            default:
                break;
        }
//        [[ConnectionManager sharedInstance].snappet disconnect];
//        [ConnectionManager sharedInstance].snappet = nil;
//        [self.tutorialDelegate tutorialCanceled];
    }
    else if (sender == self.btnNext) {
        switch (self.mode) {
            case SnapPetsMode:
                if (self.counter == 0) {
#if TARGET_IPHONE_SIMULATOR
                    [self hide];
                    [self.viewEnterName show];
#else
                    if ([[ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion length] <= 5) {
                        [self showAndContinueSnapPetsModeTutorial];
                    }
                    else {
                        [self hide];
                        [self.viewEnterName show];
                        self.counter = 1;
                    }
#endif
                }
                else if (self.counter == 1) {
                }
                else if (self.counter == 2) {
                }
                break;
            case DeviceMode:
                if (self.counter == 0) {
                    
                }
                else if (self.counter == 1) {
                    [self _enterDeviceModeTutorial3View];
                }
                else if (self.counter == 2) {
                    [self hide];
                    [[ConnectionManager sharedInstance].snappet disconnect];
                    [ConnectionManager sharedInstance].snappet = nil;
                }
                break;
            default:
                break;
        }
    }
    else if (sender == self.btnFullCancel) {
        [self _pressFullCanelBtn];
    }
    else if (sender == self.btnToggle) {
        if (self.counter == 0) {
            [self _enterDeviceModeTutorial2View];
            [self.takePhotoOverlayDelegate modePressed];
        }
    }
}

- (void)_pressFullCanelBtn {
    if (self.counter == 0) {
        switch (self.mode) {
            case SnapPetsMode:
                break;
            case DeviceMode:
                [self hide];
                [self.tutorialDelegate showQuitTutorialPopup];
                break;
            case CompletedMode:
                [self.tutorialDelegate tutorial3Completed];
                break;
            default:
                break;
        }
    }
    else if (self.counter == 1) {
        switch (self.mode) {
            case SnapPetsMode:
                [self hide];
                
                break;
            case DeviceMode:
                [self hide];
                [self.tutorialDelegate showQuitTutorialPopup];
                break;
            default:
                break;
        }
    }
    else if (self.counter == 2) {
        switch (self.mode) {
            case SnapPetsMode:
                [self hide];
                [self.tutorialDelegate showQuitSettingPopup];
                break;
            case DeviceMode:
                self.counter = 3;
                [self.tutorialDelegate tutorial2Completed];
                break;
            default:
                break;
        }
    }
    else if (self.counter == 3) {
        switch (self.mode) {
            case SnapPetsMode:
                [timerClosePopup invalidate];
                timerClosePopup = nil;
                self.counter = 4;
                [self.tutorialDelegate tutorial1Completed];
                break;
            case DeviceMode:
                break;
            default:
                break;
        }
    }
    [self hide];
}

- (void)receiveShortPress {
    switch (self.mode) {
        case SnapPetsMode:
            if (self.counter == 2) {
                if (self.hidden == NO) {
                    [self _enterSnapPetsModeTutorial3View];
                    if (timerClosePopup != nil) {
                        [timerClosePopup invalidate];
                        timerClosePopup = nil;
                    }
                    timerClosePopup = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(_pressFullCanelBtn) userInfo:nil repeats:NO];
                }
            }
            break;
        case DeviceMode:
            if (self.counter == 1) {
                [self _enterDeviceModeTutorial3View];
                if (timerClosePopup != nil) {
                    [timerClosePopup invalidate];
                    timerClosePopup = nil;
                }
                [SimpleSoundPlayer playSound:@"button2.wav"];
                timerClosePopup = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(_pressFullCanelBtn) userInfo:nil repeats:NO];
            }
            break;
        default:
            break;
    }
}

@end
