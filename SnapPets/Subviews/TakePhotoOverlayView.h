//
//  TakePhotoOverlayView.h
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewSnapPetEnterNameView.h"
#import "TakePhotoOverlayDelegate.h"
#import "TutorialEnterNameView.h"
#import "SnapPetPopup.h"

@protocol TakePhotoDelegate;

typedef NS_ENUM(NSInteger, TimerLevel) {
    TimerLevel_0 = 0,
    TimerLevel_2 = 2,
    TimerLevel_3 = 3,
    TimerLevel_5 = 5,
    TimerLevel_7 = 7,
    TimerLevel_10 = 10,
};
typedef NS_ENUM(NSInteger, PhotoSetLevel) {
    PhotoSetLevel_0 = 0,
    PhotoSetLevel_30 = 30,
    PhotoSetLevel_120 = 120,
    PhotoSetLevel_300 = 300,
};


typedef NS_ENUM(NSInteger, FlashMode) {
    FlashModeOff,
    FlashModeOn,
    FlashModeAuto
};

typedef enum {
    TakePhotoSettingBtnViewModeMain = 1,
    TakePhotoSettingBtnViewModeMainTimer,
    TakePhotoSettingBtnViewModeMainPhotoSet,
    TakePhotoSettingBtnViewModeMainWithPet,
    TakePhotoSettingBtnViewModeMainWithPetTimer,
    TakePhotoSettingBtnViewModeMainWithPetPhotoSet,
} TakePhotoSettingBtnViewMode;

typedef NS_ENUM(NSInteger, PressedButtonType)
{
    kPressedButtonTypeTimer,
    kPressedButtonTypeSeries
};

typedef void(^TimerBlock)(NSInteger, PressedButtonType);

@interface TakePhotoOverlayView : UIView <TutorialDelegate, TakePhotoDelegate, SnapPetPopupDelegate> {
    NSTimer* timerAnimatingSnappetTutorial;
    int number;
}

@property (copy, nonatomic) TimerBlock timerHandler;
@property (nonatomic, weak) id<TakePhotoDelegate> deleagte;
@property (nonatomic) UIImagePickerController *imagePickerController;

@property (nonatomic, strong) IBOutlet UIImageView *imgBG;

@property (nonatomic, strong) IBOutlet UIView *overlayView;
@property (nonatomic, strong) IBOutlet UIImageView *phoneTakePhoto;
@property (nonatomic, strong) IBOutlet UIButton *photoOkBtn;
@property (nonatomic, strong) IBOutlet UIButton *deleteBtn;
@property (nonatomic, strong) IBOutlet UIButton *snapptOkBtn;
@property (nonatomic, strong) IBOutlet UIButton *snapptRetakeBtn;
@property (nonatomic, strong) IBOutlet UIButton *retakeBtn;
@property (weak, nonatomic) IBOutlet UIButton *modeBtn;
@property (weak, nonatomic) IBOutlet UIButton *scanBtn;
@property (weak, nonatomic) IBOutlet UIView *snappetModeView;
@property (weak, nonatomic) IBOutlet UIView *cameraModeView;
@property (weak, nonatomic) IBOutlet UIButton *timerBtn;
@property (weak, nonatomic) IBOutlet UIButton *photosetBtn;
@property (weak, nonatomic) IBOutlet UIButton *flashBtn;
@property (weak, nonatomic) IBOutlet UIButton *selfieBtn;
@property (weak, nonatomic) IBOutlet UIImageView *snappetTakenPhoto;
@property (weak, nonatomic) IBOutlet UIView *btnGroupView;
@property (weak, nonatomic) IBOutlet UIImageView *bgTop;
@property (weak, nonatomic) IBOutlet UIImageView *bgBottom;
@property (weak, nonatomic) IBOutlet UIView *snappetUIView;
@property (weak, nonatomic) IBOutlet UIView *bottomBtnUIView;
@property (weak, nonatomic) IBOutlet UIView *bottomBtnUIViewCover;
@property (weak, nonatomic) IBOutlet UIButton *camImgView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UILabel *timerLabelSnap;
@property (weak, nonatomic) IBOutlet UIProgressView *blobProgress;
@property (weak, nonatomic) IBOutlet UIView *flashView;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *progress2Label;
@property (weak, nonatomic) IBOutlet UIButton *libBtn;
@property (weak, nonatomic) IBOutlet UIButton *settingBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteSnappetBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerSnappetBtn;

@property (weak, nonatomic) IBOutlet UIView *takePhotoSettingBtnView;
@property (weak, nonatomic) IBOutlet UIView *takenPhotoSettingBtnView;

@property (weak, nonatomic) IBOutlet UIView *blackview;

@property (weak, nonatomic) IBOutlet UIImageView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomView;

@property (weak, nonatomic) IBOutlet UIImageView *viewSnappetTutorial;
@property (weak, nonatomic) IBOutlet TutorialEnterNameView* viewEnterName;
@property (weak, nonatomic) IBOutlet TutorialSnapPetView* viewTutorial;

@property (readwrite) BOOL isUnderTutorial;

@property (nonatomic) BOOL isPhoneShutterMode;
@property (nonatomic) TimerLevel timerLevel;
@property (nonatomic) PhotoSetLevel photoSetLevel;
@property (nonatomic) FlashMode flashMode;
@property (nonatomic) PressedButtonType pressedButtonType;
@property (nonatomic) BOOL isSelfie;
@property (nonatomic) BOOL isInUserSettingController;
@property (weak, nonatomic) IBOutlet UIImageView *viewImgLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UIView *viewPhotoModePinkBar;

@property (weak, nonatomic) IBOutlet UIView *viewSnapPetDownloading;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSnapPetDownloading;
@property (weak, nonatomic) IBOutlet UILabel *labelSnapPetDownloading;

@property (weak, nonatomic) IBOutlet NewSnapPetEnterNameView *viewNewSnapPets;

@property (nonatomic, strong) IBOutlet UIButton* btnCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnContinue;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintTopViewHeight;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintBottomViewHeight;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintSnappetTimer;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintSnappetShare;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintSnappetEdit;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintSnappetDelete;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintTopBarHeight;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintBottomBarHeight;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintPinkBarYFromBottom;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintCameraImageY;

@property(readwrite) SnapPetPopup* viewPopup;

@property (strong) UIImage * tempImage;
- (void)showConnectionScreen;
- (void)showSnappetTakePhotoScreen;
- (void)showNewSnappetScreen;
- (void)setCameraButton :(BOOL)isShowPreview;
- (IBAction)takePhoto:(id)sender;
- (IBAction)buttonSelector:(id)sender;
- (IBAction)modePressed:(id)sender;
- (void)showPreview:(UIImage*)img;
- (void)clearPreview;
- (void)clearSnappetPreview;
- (void)noFlashBtn;
- (void)setPhotoSet;
- (void)setTimer;
- (void)retakePhoto;
- (void)initView;

- (void)disableSettingButton;
- (void)disableCloseButton;

- (void)animateScanButton;
- (void)animateSnapPetsTutorial;
- (void)animateSnapPetsDownloading;

@end

