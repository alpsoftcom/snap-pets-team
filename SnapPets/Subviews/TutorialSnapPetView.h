//
//  TutorialSnapPetView.h
//  SnapPets
//
//  Created by David Chan on 12/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TakePhotoOverlayDelegate.h"
#import "TutorialDelegate.h"
@class TutorialEnterNameView;

typedef enum {
    SnapPetsMode = 0,
    DeviceMode = 1,
    CompletedMode = 2,
}TutorialMode;

@interface TutorialSnapPetView : UIView {
    NSTimer* timerAnimateTutorial;
    NSTimer* timerClosePopup;
}

- (void)resumeTutorial;
- (void)showSnapPetsModeTutorial;
- (void)showAndContinueSnapPetsModeTutorial;
- (void)showDeviceModeTutorial;
- (void)showCompletedModeTutorial;
- (void)hide;
- (void)animateTutorial;
- (IBAction)buttonAction:(id)sender;

- (void)receiveShortPress;

@property (readwrite) int counter;
@property (readwrite) TutorialMode mode;
@property (nonatomic, strong) IBOutlet UIView* imgBaseView;
@property (nonatomic, strong) IBOutlet UIImageView* imgView;
@property (nonatomic, strong) IBOutlet UILabel* labelText;
@property (nonatomic, strong) IBOutlet UIButton* btnCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnNext;

@property (nonatomic, strong) IBOutlet UIButton* btnFullCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnToggle;
@property (nonatomic, strong) IBOutlet UIImageView* imgCancel;
@property (nonatomic, strong) IBOutlet UIImageView* imgNext;
@property (nonatomic, strong) IBOutlet UIImageView* imgFullCancel;
@property (readwrite) TutorialEnterNameView* viewEnterName;
@property (nonatomic, weak) id<TakePhotoDelegate> deleagte;
@property (nonatomic, weak) id<TutorialDelegate> tutorialDelegate;
@property (nonatomic, weak) id<TakePhotoDelegate> takePhotoOverlayDelegate;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintImgView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintBtnWidth;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintBtnHeight;

@end
