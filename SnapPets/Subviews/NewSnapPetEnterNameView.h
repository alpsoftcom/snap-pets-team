//
//  NewSnapPetEnterNameView.h
//  SnapPets
//
//  Created by David Chan on 12/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialSnapPetView.h"

@interface NewSnapPetEnterNameView : UIView {
    NSTimer* timerAnimateNewSnapPet;
}

- (void)show;
- (void)hide;
- (void)animateNewSnapPet;
- (IBAction)buttonAction:(id)sender;

@property (nonatomic, strong) IBOutlet UIButton* btnBack;
@property (nonatomic, strong) IBOutlet UIButton* btnCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnNext;
@property (nonatomic, strong) IBOutlet UITextField* tfName;
@property (nonatomic, strong) IBOutlet UIImageView* imgViewNewSnappet;
@property (nonatomic, strong) IBOutlet UILabel* labelNewSnapPet;
@property (nonatomic, strong) IBOutlet UILabel* labelEnterName;

@property (readwrite) TutorialSnapPetView* viewTutorial;

@property (readwrite) NSString* strRobotName;
@property (nonatomic, weak) id<TutorialDelegate> tutorialDelegate;

@end
