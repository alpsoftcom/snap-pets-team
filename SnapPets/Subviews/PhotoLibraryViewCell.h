//
//  PhotoLibraryViewCell.h
//  SnapPets
//
//  Created by Katy Pun on 12/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoLibraryViewCell : UICollectionViewCell

- (void)select;
- (void)unselect;
- (BOOL)isSelected;
@property (nonatomic, strong) IBOutlet UIView *selectedView;
@property (nonatomic, strong) IBOutlet UIImageView *photoImgView;
@property (nonatomic, strong) IBOutlet UIImageView *tickView;
@property (readwrite) UIGestureRecognizer* gesture;
@end
