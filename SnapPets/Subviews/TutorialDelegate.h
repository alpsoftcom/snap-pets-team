//
//  TutorialDelegate.h
//  SnapPets
//
//  Created by David Chan on 13/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TutorialDelegate

@optional
- (void)tutorial1Completed;
- (void)tutorial2Completed;
- (void)tutorial3Completed;
- (void)showQuitSettingPopup;
- (void)showQuitTutorialPopup;
- (void)tutorialCanceled;
@end

@interface TutorialDelegate : NSObject

@end
