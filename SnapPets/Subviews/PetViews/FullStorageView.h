//
//  FullStorageView.h
//  SnapPets
//
//  Created by Surik Sarkisyan on 17.09.15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FullStorageBlock)();

@interface FullStorageView : UIView

@property (copy, nonatomic) FullStorageBlock fullStorageBlock;

@end
