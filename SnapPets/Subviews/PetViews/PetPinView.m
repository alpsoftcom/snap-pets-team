//
//  PetPinView.m
//  SnapPets
//
//  Created by Andrew Medvedev on 17/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "PetPinView.h"
#import "ConnectionManager.h"
#import "UARTPeripheral.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

@interface PetPinView ()<SnappetRobotDelegate>
@property (strong, nonatomic) SnappetRobot * snappet;

@property (weak, nonatomic) IBOutlet UITextField *petPinTextField;
@property (weak, nonatomic) IBOutlet UILabel * youNeedToRemebmerThis;

@end

@implementation PetPinView

- (void)checkConnectionAndPinStatus
{
    [self.youNeedToRemebmerThis setHidden:YES];
    self.snappet = [ConnectionManager sharedInstance].snappet;
    self.snappet.delegate = self;
    [self.snappet snappetReadASCIIPinCodeEnabled];
    NSLog(@"123");
}

- (IBAction)backButtonPressed:(id)sender
{
    _petPinBlock(nil, YES);
//    _petPinBlock(_petPinTextField.text);
}

- (IBAction)applyButtonPressed:(id)sender
{
//    [self.snappet snappetResetASCIIPinCodeAndEraseFlash];
//    return;
        
    if(self.needGeneratePin){
        int value0 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:0]] intValue];
        int value1 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:1]] intValue];
        int value2 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:2]] intValue];
        int value3 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:3]] intValue];
 

       [self.snappet snappetSetASCIIPinCode:value0 digit2:value1 digit3:value2 digit4:value3];
           [NSThread sleepForTimeInterval:2];
        [self.snappet snappetSetASCIIPinCodeEnabled:YES];
         _petPinBlock(@"response", YES);
        
    }
    else
    {
        int value0 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:0]] intValue];
        int value1 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:1]] intValue];
        int value2 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:2]] intValue];
        int value3 = [[NSString stringWithFormat:@"%c", [_petPinTextField.text characterAtIndex:3]] intValue];

            
            [self.snappet snappetAuthenticateWithASCIIPinCode:value0 digit2:value1 digit3:value2 digit4:value3];
            
            //        [self performSelector:@selector(authenPIN) withObject:nil];
        }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //    if(self.type == SettingsCellTypeSnapPetPin)
    if ( _petPinTextField.text.length == 4 && string.length > 0 )
    {
        return NO;
    }
    
    if ( (_petPinTextField.text.length + string.length) == 4 && _petPinTextField.text.length != 4 )
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self endEditing:YES];
        });
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)gennerateAndShowPin{
     [self.youNeedToRemebmerThis setHidden:NO];
    NSString * geteratedPin = [[NSString alloc]init];
    for(int i = 0 ; i < 4 ; i++)
    {
       geteratedPin = [NSString stringWithFormat:@"%@%@", geteratedPin,[[NSNumber numberWithInt:arc4random_uniform(10)] stringValue]];
        
    }
    
    [_petPinTextField setText:geteratedPin];
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingChargingStatus:(kSnappetChargingStatus)chargingStatus{
    NSLog(@"");
    if (chargingStatus == kSnappetNotCharging){
       NSLog(@"not charging");
    }else if (chargingStatus == kSnappetCharging){
           NSLog(@"charging");
    }else if (chargingStatus == kSnappetFullyCharge){
       NSLog(@"power full");
    }
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingChargingResponse:(int)response{
    NSLog(@"");
    if (response == kSnappetNotCharging){
        NSLog(@"not charging");
    }else if (response == kSnappetCharging){
        NSLog(@"charging");
    }else if (response == kSnappetFullyCharge){
        NSLog(@"power full");
    }

}

-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityPinEnable:(int)enable{
    BOOL isAuthen;
    enable == 0x00 ? (isAuthen = NO) : (isAuthen = YES);

    
    if ( enable == 0x00){
        NSLog(@"no pin");
        self.needGeneratePin = YES;
        [self gennerateAndShowPin];
        
    }
    // self.isEnablePin = self.isAuthen;
    if (enable == 0x01){
        NSLog(@"yes pin");
        self.needGeneratePin = NO;
        
            }
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityAuthenticateResponse:(int)response{
    BOOL pinAuthen;
    
   // _petPinBlock(_petPinTextField.text, YES);
   // [self.snappet snappetReadChargingStatus ];
    
    
    if (response == 0){
        _petPinBlock(nil, NO);
    }else{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString * petUUID = [NSString stringWithFormat:@"%@",snappet.uuid.UUIDString];
        [defaults setObject:petUUID forKey:@"LastConnectedPet"];
        [defaults setObject:_petPinTextField.text forKey:@"LastConnectedPetPIN"];
        NSString * newPetName = [defaults objectForKey:@"LastConnectedPetName"];
        [defaults synchronize];
        [snappet snappetWriteDisplayName:newPetName];
        _petPinBlock(@"response", YES);
    }
    if (response == 0x00) {
        //authen fail
         pinAuthen = NO;
    }else {
        //authen success

        pinAuthen = NO;
        
   // [self.snappet snappetSetASCIIPinCodeEnabled:NO];
    
    }

}

@end