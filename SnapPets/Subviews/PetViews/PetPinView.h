//
//  PetPinView.h
//  SnapPets
//
//  Created by Andrew Medvedev on 17/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PetPinBlock)(NSString *petPin, BOOL access);

@interface PetPinView : UIView

@property (copy, nonatomic) PetPinBlock petPinBlock;
@property (assign, nonatomic) BOOL needGeneratePin;
- (void)checkConnectionAndPinStatus;

@end
