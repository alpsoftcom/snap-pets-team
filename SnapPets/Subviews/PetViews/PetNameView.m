//
//  PetNameView.m
//  SnapPets
//
//  Created by Andrew Medvedev on 17/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "PetNameView.h"
#import "ConnectionManager.h"
#import "UARTPeripheral.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "AppConfig.h"

@interface PetNameView () <SnappetRobotDelegate>

@property (weak, nonatomic) IBOutlet UITextField *petNameTextField;

@end

@implementation PetNameView

- (void)awakeFromNib
{
    self.petNameTextField.text = @"My SnapPet";
}

- (IBAction)applyButtonPressed:(id)sender
{
    NSString *newPetName = _petNameTextField.text;
    
    SnappetRobot * petrobot = [ConnectionManager sharedInstance].snappet;
    petrobot.delegate = self;

    [petrobot snappetWriteDisplayName:newPetName];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:newPetName forKey:@"LastConnectedPetName"];
    [defaults synchronize];

    
    [[AppConfig sharedInstance] saveDeviceWithName:newPetName Identity:[petrobot.uuid UUIDString] ColorID:petrobot.colorId];
    _petNameBlock(newPetName);
 
}

@end
