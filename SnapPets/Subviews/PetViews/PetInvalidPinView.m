//
//  PetInvalidPinView.m
//  SnapPets
//
//  Created by Surik Sarkisyan on 17.09.15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "PetInvalidPinView.h"

@interface PetInvalidPinView ()

@property (weak, nonatomic) IBOutlet UILabel *petNameLabel;

@end

@implementation PetInvalidPinView

- (IBAction)backButtonPressed:(id)sender
{
    _petInvalidPinBlock();
}

@end
