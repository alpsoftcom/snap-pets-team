//
//  PetNameView.h
//  SnapPets
//
//  Created by Andrew Medvedev on 17/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PetNameBlock)(NSString *petName);

@interface PetNameView : UIView 

@property (copy, nonatomic) PetNameBlock petNameBlock;

@end
