//
//  TakePhotoOverlayDelegate.h
//  SnapPets
//
//  Created by David Chan on 12/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TakePhotoDelegate

@optional
- (void)takePhoto;
- (void)okPressed;
- (void)retakePressed;
- (void)backPressed;
- (void)modePressed;
- (void)libraryPressed;
- (void)selfiePressed;
- (void)flashPressed;
- (void)timerPressed;
- (void)settingPressed;
- (void)scannerPressed;
- (void)sharePressed;
- (void)deletePressed;
- (void)cancelTimerPressed;
@end

@interface TakePhotoOverlayDelegate : NSObject

@end
