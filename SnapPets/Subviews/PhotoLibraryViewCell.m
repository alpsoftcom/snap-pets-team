//
//  PhotoLibraryViewCell.m
//  SnapPets
//
//  Created by Katy Pun on 12/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "PhotoLibraryViewCell.h"


@implementation PhotoLibraryViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.photoImgView = (UIImageView *)[self viewWithTag:100];
        [self.tickView setHidden:YES];
//        [self addSubview:self.photoImgView];
    }
    
    return self;
}

- (void)select {
    self.selectedView.hidden = NO;
    [self.tickView setHidden:NO];
}

- (void)unselect {
    self.selectedView.hidden = YES;
    [self.tickView setHidden:YES];
}

- (BOOL)isSelected {
    return self.selectedView.hidden == NO;
}

@end
