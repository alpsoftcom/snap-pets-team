//
//  TutorialEnterNameView.m
//  SnapPets
//
//  Created by David Chan on 13/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "TutorialEnterNameView.h"
#import "ConnectionManager.h"
#import "TutorialSnapPetView.h"
#import "LocalizationManager.h"
#import "AppConfig.h"

@implementation TutorialEnterNameView
#define MAXROBOTNAMELENGTH 15
- (void)layoutSubviews {
    [super layoutSubviews];
    self.imgBaseView.layer.cornerRadius = 5.0f;
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        self.imgBaseView.layer.cornerRadius = 10.0f;
    }
    
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        //        UIFont* fontButton = [UIFont systemFontOfSize:50.0f weight:2.0f];
        //        UIFont* fontLabel80 = [UIFont systemFontOfSize:80.0f weight:2.0f];
        //        UIFont* fontLabel50 = [UIFont systemFontOfSize:50.0f weight:2.0f];
        UIFont* fontLabel80 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:80.0f];
        UIFont* fontButton = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50.0f];
        UIFont* fontLabel50 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50.0f];
        UIFont* fontLabel30 = [UIFont systemFontOfSize:30.0f];
        self.labelText.font = fontLabel80;
        self.tfName.font = fontLabel30;
    }
}

- (void)show {
    [self setHidden:NO];
    if (timerAnimateNewSnapPet == nil)
        timerAnimateNewSnapPet = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(animateNewSnapPet) userInfo:nil repeats:YES];
    [self animateNewSnapPet];
    [self.tfName setText:[ConnectionManager sharedInstance].snappet.name];
    self.strRobotName = [ConnectionManager sharedInstance].snappet.name;

//    NSLog(@"%@", [ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion);
    if ([[ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion length] <= 5) {
        [self.tfName setEnabled:NO];
        [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Your SnapPet name"]];
    }
    else {
        [self.labelText setText:[[LocalizationManager getInstance] getStringWithKey:@"Rename your SnapPet"]];
    }
    [self.tfName becomeFirstResponder];
//    if ([ConnectionManager sharedInstance].snappet.snappetFirmwareVersionId)
}

- (void)hide {
    [self setHidden:YES];
    [timerAnimateNewSnapPet invalidate];
    timerAnimateNewSnapPet = nil;
}

- (void)animateNewSnapPet {
    CGSize screenBounds = [self bounds].size;
    NSString* str = nil;
    if (self.imgView.tag == 1) {
//        if (screenBounds.height == 1024 || screenBounds.height == 2048)
//            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 1, @"2x~ipad"];
//        else
//            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 1, @"2x"];
        str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d.png", 1];
        [self.imgView setTag:0];
    }
    else {
//        if (screenBounds.height == 1024 || screenBounds.height == 2048)
//            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 2, @"2x~ipad"];
//        else
//            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 2, @"2x"];
        str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d.png", 2];
        [self.imgView setTag:1];
    }
    [self.imgView setImage:[UIImage imageNamed:str]];
}

- (IBAction)buttonAction:(id)sender {
    if (sender == self.btnCancel) {
        [self hide];
        [self.tutorialDelegate showQuitSettingPopup];
//        [[ConnectionManager sharedInstance].snappet disconnect];
//        [ConnectionManager sharedInstance].snappet = nil;
//        [self.tutorialDelegate tutorialCanceled];
    }
    else if (sender == self.btnNext) {
#if TARGET_IPHONE_SIMULATOR
        [self hide];
        [self.viewTutorial showAndContinueSnapPetsModeTutorial];
#else
        if ([self.strRobotName length] > 0) {
            [AppConfig sharedInstance].strRobotName = self.strRobotName;
            [self hide];
            [self.viewTutorial showAndContinueSnapPetsModeTutorial];
            [self.tfName resignFirstResponder];
            //            [self.viewTutorial showDeviceModeTutorial];
        }
#endif
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.tfName resignFirstResponder];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.constraintY.constant += 100.0f;

    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.constraintY.constant -= 100.0f;
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-_ "] invertedSet];
    
    if ([string rangeOfCharacterFromSet:set].location != NSNotFound) {
        return NO;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    if (newLength <= MAXROBOTNAMELENGTH || returnKey || newLength < oldLength) {
        self.strRobotName = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return YES;
    }
    return NO;
}

@end
