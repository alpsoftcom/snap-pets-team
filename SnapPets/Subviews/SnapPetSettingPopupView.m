//
//  SnapPetSettingPopupView.m
//  SnapPets
//
//  Created by David Chan on 11/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "SnapPetSettingPopupView.h"
#import "ConnectionManager.h"
#import "LocalizationManager.h"
#import "AppConfig.h"

#define MAXROBOTNAMELENGTH 16

@implementation SnapPetSettingPopupView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews {
    [super layoutSubviews];
    
}

- (void)setVersion:(NSString*)strVersion {
    [self.versionLabel setText:strVersion];
}

- (void)setName:(NSString*)strName {
    [self.nameLabel setText:strName];
}

- (void)showBaseMenu {
    self.mode = SettingPopupViewModeBaseMenu;

    self.baseView.layer.cornerRadius = 5.0f;
    self.baseView.layer.masksToBounds = YES;
    
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* font = [UIFont systemFontOfSize:36.0f];
        self.firstButton.titleLabel.font = font;
        self.secondButton.titleLabel.font = font;
        self.thirdButton.titleLabel.font = font;
        self.fourthButton.titleLabel.font = font;
        self.versionLabel.font = font;
        self.nameLabel.font = font;
    }
    
    [self setHidden:NO];
    
    [self.firstButton setHidden:NO];
    [self.secondButton setHidden:NO];
    [self.thirdButton setHidden:NO];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:NO];
    
    [self.firstButton setEnabled:YES];
    [self.secondButton setEnabled:YES];
    [self.thirdButton setEnabled:YES];
    [self.fourthButton setEnabled:YES];
    
    [self.nameLabel setHidden:NO];
    [self.imgSnappetView setHidden:NO];
    [self.textfield setHidden:YES];
    [self.nameLabel setText:[ConnectionManager sharedInstance].snappet.name];
    
    [self.webView setHidden:YES];
    
    [self.imgSettingFrame1View setHidden:YES];
    [self.imgSettingFrame2View setHidden:YES];
    
    [self.firstButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Info"] forState:UIControlStateNormal];
    [self.secondButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Settings"] forState:UIControlStateNormal];
//    [self.thirdButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Check for update"] forState:UIControlStateNormal];
    if ([ConnectionManager sharedInstance].snappet.takePhotoMode == kSnappetTakePhotoMode_Normal)
        [self.thirdButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Timer"] forState:UIControlStateNormal];
    else if ([ConnectionManager sharedInstance].snappet.takePhotoMode == kSnappetTakePhotoMode_Timelapse)
        [self.thirdButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Disable Timer"] forState:UIControlStateNormal];
    [self.fourthButton setTitle:@"" forState:UIControlStateNormal];
    
    if (timerAnimateTutorial == nil)
        timerAnimateTutorial = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(animateTutorial) userInfo:nil repeats:YES];
    
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* fontLabel30 = [UIFont systemFontOfSize:30.0f];
        self.textfield.font = fontLabel30;
    }
    [self setName:[AppConfig sharedInstance].strRobotName];
}

- (void)showDFUMenu {
    self.mode = SettingPopupViewModeDFUMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:YES];
    [self.secondButton setHidden:NO];
    [self.thirdButton setHidden:NO];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
    [self.webView setHidden:YES];
    
    [self.firstButton setTitle:@"" forState:UIControlStateNormal];
    [self.secondButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Update Now"] forState:UIControlStateNormal];
    [self.thirdButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Update Later"] forState:UIControlStateNormal];
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Notes"] forState:UIControlStateNormal];
}

- (void)showUpdatingMenu {
    self.mode = SettingPopupViewModeUpdatingMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:YES];
    [self.secondButton setHidden:YES];
    [self.thirdButton setHidden:YES];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
    [self.fourthButton setEnabled:NO];
    
    [self.nameLabel setHidden:YES];
    [self.imgSnappetView setHidden:YES];
    
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
//        self.imgSettingFrame1View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 1, @"2x~ipad"]];
//        self.imgSettingFrame2View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 2, @"2x~ipad"]];
    }
    else {
//        self.imgSettingFrame1View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 1, @"2x"]];
//        self.imgSettingFrame2View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d@%@.png", 2, @"2x"]];
    }
    self.imgSettingFrame1View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d.png", 1]];
    self.imgSettingFrame2View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_setting-snap-pet-%d.png", 2]];
    
    [self.imgSettingFrame1View setHidden:NO];
    [self.imgSettingFrame2View setHidden:YES];
    
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Updating"] forState:UIControlStateNormal];
    
    timerDFUModeUpdating = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(updateDFUModeUpdatingAnimation) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(showDoneMenu) userInfo:nil repeats:NO];
}

- (void)showDoneMenu {
    self.mode = SettingPopupViewModeDoneMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:YES];
    [self.secondButton setHidden:YES];
    [self.thirdButton setHidden:YES];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
//    [timerDFUModeUpdating invalidate];
    CGSize screenBounds = [self bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
//        self.imgSettingFrame1View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 1, @"2x~ipad"]];
//        self.imgSettingFrame2View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 2, @"2x~ipad"]];
    }
    else {
//        self.imgSettingFrame1View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 1, @"2x"]];
//        self.imgSettingFrame2View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 2, @"2x"]];
    }
    self.imgSettingFrame1View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_new-pet-setting-%d.png", 1]];
    self.imgSettingFrame2View.image = [UIImage imageNamed:[NSString stringWithFormat:@"anime_popup_new-pet-setting-%d.png", 2]];
    
    [self.imgSettingFrame1View setHidden:NO];
    [self.imgSettingFrame2View setHidden:YES];
    
    [self.fourthButton setEnabled:YES];
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Done"] forState:UIControlStateNormal];
}

- (void)showSettingMenu {
    self.mode = SettingPopupViewModeSettingMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:YES];
    [self.secondButton setHidden:YES];
    [self.thirdButton setHidden:YES];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
    [self.nameLabel setHidden:YES];
    [self.textfield setHidden:NO];
    
    [self.firstButton setTitle:@"" forState:UIControlStateNormal];
    [self.secondButton setTitle:@"" forState:UIControlStateNormal];
    [self.thirdButton setTitle:@"" forState:UIControlStateNormal];
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Back"] forState:UIControlStateNormal];
    
    [self.textfield setText:[AppConfig sharedInstance].strRobotName];
    if ([[ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion length] <= 5) {
        [self.textfield setEnabled:NO];
    }
}

- (void)showInfoMenu {
    self.mode = SettingPopupViewModeInfoMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:NO];
    [self.secondButton setHidden:NO];
    [self.thirdButton setHidden:NO];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
    [self.firstButton setEnabled:NO];
    [self.secondButton setEnabled:NO];
    [self.thirdButton setEnabled:NO];
    
    [self.nameLabel setHidden:NO];
    [self.textfield setHidden:YES];
    
    NSString* strVersion = [ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion;
    [self.firstButton setTitle:[NSString stringWithFormat:@"%@: %@", [[LocalizationManager getInstance] getStringWithKey:@"Firmware Version"], strVersion] forState:UIControlStateNormal];
    [self.secondButton setTitle:@"" forState:UIControlStateNormal];
    [self.thirdButton setTitle:@"" forState:UIControlStateNormal];
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Back"] forState:UIControlStateNormal];
}

- (void)showNotesMenu {
    self.mode = SettingPopupViewModeNotesMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:YES];
    [self.secondButton setHidden:YES];
    [self.thirdButton setHidden:YES];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
    [self.webView setHidden:NO];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com"]]];
    
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Back"] forState:UIControlStateNormal];
}

- (void)showTimerMenu {
    self.mode = SettingPopupViewModeTimerMenu;
    
    [self setHidden:NO];
    [self.firstButton setHidden:NO];
    [self.secondButton setHidden:NO];
    [self.thirdButton setHidden:NO];
    [self.fourthButton setHidden:NO];
    [self.closeImage setHidden:YES];
    
    [self.firstButton setEnabled:YES];
    [self.secondButton setEnabled:NO];
    [self.thirdButton setEnabled:NO];
    
    [self.nameLabel setHidden:NO];
    [self.textfield setHidden:YES];
    
    NSString* strVersion = [ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion;
    [self.firstButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Start Timer Mode"] forState:UIControlStateNormal];
    [self.secondButton setTitle:@"" forState:UIControlStateNormal];
    [self.thirdButton setTitle:@"" forState:UIControlStateNormal];
    [self.fourthButton setTitle:[[LocalizationManager getInstance] getStringWithKey:@"Back"] forState:UIControlStateNormal];
}

- (void)hide {
    [self setHidden:YES];
    if (timerDFUModeUpdating != nil)
        [timerDFUModeUpdating invalidate];
    
    [self.imgSettingFrame1View setHidden:YES];
    [self.imgSettingFrame2View setHidden:YES];
    [timerAnimateTutorial invalidate];
    timerAnimateTutorial = nil;
    
    [self.delegate hided];
}

- (void)animateTutorial {
    NSString* str = nil;
    NSString* strTitle = @"anime_popup_loading-";
    CGSize screenBounds = [self bounds].size;
    if (self.imgSnappetView.tag == 0) {
        [self.imgSnappetView setTag:1];
    }
    else if (self.imgSnappetView.tag == 1) {
        [self.imgSnappetView setTag:2];
    }
    else if (self.imgSnappetView.tag == 2) {
        [self.imgSnappetView setTag:3];
    }
//    if (screenBounds.height == 1024 || screenBounds.height == 2048)
//        str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgSnappetView.tag, @"2x~ipad"];
//    else
//        str = [NSString stringWithFormat:@"%@%d@%@.png", strTitle, (int)self.imgSnappetView.tag, @"2x"];
    str = [NSString stringWithFormat:@"%@%d.png", strTitle, (int)self.imgSnappetView.tag];
    [self.imgSnappetView setImage:[UIImage imageNamed:str]];
    if (self.imgSnappetView.tag == 3) {
        [self.imgSnappetView setTag:0];
    }
}

- (void)updateDFUModeUpdatingAnimation {
    if (self.imgSettingFrame1View.hidden) {
        [self.imgSettingFrame1View setHidden:NO];
        [self.imgSettingFrame2View setHidden:YES];
    }
    else {
        [self.imgSettingFrame1View setHidden:YES];
        [self.imgSettingFrame2View setHidden:NO];
    }
}

- (IBAction)firstButtonAction:(id)sender {
    switch (self.mode) {
        case SettingPopupViewModeBaseMenu:
            [self showInfoMenu];
            break;
        case SettingPopupViewModeDFUMenu:
            break;
        case SettingPopupViewModeUpdatingMenu:
            break;
        case SettingPopupViewModeDoneMenu:
            break;
        case SettingPopupViewModeTimerMenu:
            [[ConnectionManager sharedInstance].snappet snappetTakePhotoWithTimerMode];
        default:
            break;
    }
}

- (IBAction)secondButtonAction:(id)sender {
    switch (self.mode) {
        case SettingPopupViewModeBaseMenu:
            [self showSettingMenu];
            break;
        case SettingPopupViewModeDFUMenu:
            [self showUpdatingMenu];
            break;
        case SettingPopupViewModeUpdatingMenu:
            break;
        case SettingPopupViewModeDoneMenu:
            break;
        default:
            break;
    }
}

- (IBAction)thirdButtonAction:(id)sender {
    switch (self.mode) {
        case SettingPopupViewModeBaseMenu:
//            [self showDFUMenu];
            if ([ConnectionManager sharedInstance].snappet.takePhotoMode == kSnappetTakePhotoMode_Normal)
                [self showTimerMenu];
            else if ([ConnectionManager sharedInstance].snappet.takePhotoMode == kSnappetTakePhotoMode_Timelapse)
                [[ConnectionManager sharedInstance].snappet snappetStopTimeMode];
            break;
        case SettingPopupViewModeDFUMenu:
            [self hide];
            break;
        case SettingPopupViewModeUpdatingMenu:
            break;
        case SettingPopupViewModeDoneMenu:
            break;
        default:
            break;
    }
}

- (IBAction)fourthButtonAction:(id)sender {
    switch (self.mode) {
        case SettingPopupViewModeBaseMenu:
            [self hide];
            break;
        case SettingPopupViewModeDFUMenu:
            [self showNotesMenu];
            break;
        case SettingPopupViewModeUpdatingMenu:
            break;
        case SettingPopupViewModeDoneMenu:
            [self hide];
            break;
        case SettingPopupViewModeSettingMenu:
            [self showBaseMenu];
            break;
        case SettingPopupViewModeInfoMenu:
            [self showBaseMenu];
            break;
        case SettingPopupViewModeNotesMenu:
            [self showDFUMenu];
            break;
        case SettingPopupViewModeTimerMenu:
            [self showBaseMenu];
            break;
        default:
            break;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.textfield resignFirstResponder];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.constraintY.constant += 100.0f;
    
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.constraintY.constant -= 100.0f;
    if ([textField.text length] > 0) {
        NSString* strName = textField.text;
        [[ConnectionManager sharedInstance].snappet snappetWriteDisplayName:strName];
        [AppConfig sharedInstance].strRobotName = strName;
    }
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
//    if ([self.strRobotName length] > 0) {
//        NSString* strName = textField.text;
//        [[ConnectionManager sharedInstance].snappet snappetWriteDisplayName:strName];
//    }
    
    [self showBaseMenu];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-_ "] invertedSet];
    
    if ([string rangeOfCharacterFromSet:set].location != NSNotFound) {
        return NO;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    if (newLength <= MAXROBOTNAMELENGTH || returnKey || newLength < oldLength) {
        self.strRobotName = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return YES;
    }
    return NO;
}

@end
