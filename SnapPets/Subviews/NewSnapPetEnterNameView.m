//
//  NewSnapPetEnterNameView.m
//  SnapPets
//
//  Created by David Chan on 12/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "NewSnapPetEnterNameView.h"
#import "ConnectionManager.h"
#import "TutorialSnapPetView.h"
#import "LocalizationManager.h"

#define MAXROBOTNAMELENGTH 16

@implementation NewSnapPetEnterNameView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.btnCancel.layer.cornerRadius = 10.0f;
    self.btnNext.layer.cornerRadius = 10.0f;
    
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
//        UIFont* fontButton = [UIFont systemFontOfSize:50.0f weight:2.0f];
//        UIFont* fontLabel80 = [UIFont systemFontOfSize:80.0f weight:2.0f];
//        UIFont* fontLabel50 = [UIFont systemFontOfSize:50.0f weight:2.0f];
        UIFont* fontLabel80 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:80.0f];
        UIFont* fontButton = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50.0f];
        UIFont* fontLabel50 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:50.0f];
        self.btnCancel.titleLabel.font = fontButton;
        self.btnNext.titleLabel.font = fontButton;
        self.labelEnterName.font = fontLabel50;
        self.labelNewSnapPet.font = fontLabel80;
    }
    
    [self.btnNext setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnContinue"] forState:UIControlStateNormal];
    [self.btnCancel setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnCancel"] forState:UIControlStateNormal];

}

- (void)show {
    [self setHidden:NO];
    if (timerAnimateNewSnapPet == nil)
        timerAnimateNewSnapPet = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(animateNewSnapPet) userInfo:nil repeats:YES];
    
    [self.tfName setText:[ConnectionManager sharedInstance].snappet.name];
    self.strRobotName = [ConnectionManager sharedInstance].snappet.name;
}

- (void)hide {
    [self setHidden:YES];
    [timerAnimateNewSnapPet invalidate];
    timerAnimateNewSnapPet = nil;
}

- (void)animateNewSnapPet {
    CGSize screenBounds = [self bounds].size;
    NSString* str = nil;
    if (self.imgViewNewSnappet.tag == 1) {
        if (screenBounds.height == 1024 || screenBounds.height == 2048)
            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 1, @"2x~ipad"];
        else
            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 1, @"2x"];
        str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d.png", 1];
        [self.imgViewNewSnappet setTag:0];
    }
    else {
        if (screenBounds.height == 1024 || screenBounds.height == 2048)
            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 2, @"2x~ipad"];
        else
            str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d@%@.png", 2, @"2x"];
        str = [NSString stringWithFormat:@"anime_popup_new-pet-setting-%d.png", 2];
        [self.imgViewNewSnappet setTag:1];
    }
    [self.imgViewNewSnappet setImage:[UIImage imageNamed:str]];
}

- (IBAction)buttonAction:(id)sender {
    if (sender == self.btnBack) {
        [self hide];
        [[ConnectionManager sharedInstance].snappet disconnect];
        [ConnectionManager sharedInstance].snappet = nil;
        [self.tutorialDelegate tutorialCanceled];
    }
    else if (sender == self.btnCancel) {
        [self hide];
        [[ConnectionManager sharedInstance].snappet disconnect];
        [ConnectionManager sharedInstance].snappet = nil;
        [self.tutorialDelegate tutorialCanceled];
    }
    else if (sender == self.btnNext) {
        if ([self.strRobotName length] > 0) {
            [[ConnectionManager sharedInstance].snappet snappetWriteDisplayName:self.strRobotName];
            [self hide];
            [self.viewTutorial showSnapPetsModeTutorial];
//            [self.viewTutorial showDeviceModeTutorial];
        }
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-_ "] invertedSet];
    
    if ([string rangeOfCharacterFromSet:set].location != NSNotFound) {
        return NO;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    if (newLength <= MAXROBOTNAMELENGTH || returnKey || newLength < oldLength) {
        self.strRobotName = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return YES;
    }
    return NO;
}

@end
