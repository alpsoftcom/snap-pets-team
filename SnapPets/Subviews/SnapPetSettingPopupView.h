//
//  SnapPetSettingPopupView.h
//  SnapPets
//
//  Created by David Chan on 11/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SettingPopupViewModeBaseMenu = 0,
    SettingPopupViewModeDFUMenu = 1,
    SettingPopupViewModeUpdatingMenu = 2,
    SettingPopupViewModeDoneMenu = 3,
    SettingPopupViewModeSettingMenu = 4,
    SettingPopupViewModeInfoMenu = 5,
    SettingPopupViewModeNotesMenu = 6,
    SettingPopupViewModeTimerMenu = 7,
} SettingPopupViewMode;

@protocol SnapPetSettingPopupViewDelegate

@optional
- (void)hided;
@end

@interface SnapPetSettingPopupView : UIView {
    NSTimer* timerDFUModeUpdating;
    NSTimer* timerAnimateTutorial;
}

@property (nonatomic, strong) IBOutlet UIView* baseView;

@property (nonatomic, strong) IBOutlet UILabel* versionLabel;
@property (nonatomic, strong) IBOutlet UILabel* nameLabel;

@property (nonatomic, strong) IBOutlet UITextField* textfield;

@property (nonatomic, strong) IBOutlet UIWebView* webView;

@property (nonatomic, strong) IBOutlet UIImageView* imgSnappetView;

@property (nonatomic, strong) IBOutlet UIImageView* imgSettingFrame1View;
@property (nonatomic, strong) IBOutlet UIImageView* imgSettingFrame2View;
@property (nonatomic, strong) IBOutlet UIImageView* imgSettingFrameDoneView;

@property (nonatomic, strong) IBOutlet UIButton* firstButton;
@property (nonatomic, strong) IBOutlet UIButton* secondButton;
@property (nonatomic, strong) IBOutlet UIButton* thirdButton;
@property (nonatomic, strong) IBOutlet UIButton* fourthButton;
@property (nonatomic, strong) IBOutlet UIImageView* closeImage;
@property (readwrite) SettingPopupViewMode mode;
@property (readwrite) NSString* strRobotName;
@property (readwrite) id<SnapPetSettingPopupViewDelegate> delegate;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintY;

- (void)setVersion:(NSString*)strVersion;
- (void)setName:(NSString*)strName;
- (void)showBaseMenu;
- (void)showDFUMenu;
- (void)showUpdatingMenu;
- (void)showDoneMenu;
- (void)showSettingMenu;
- (void)showInfoMenu;
- (void)showTimerMenu;
- (void)hide;

@end
