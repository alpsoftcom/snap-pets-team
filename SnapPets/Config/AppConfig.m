//
//  AppConfig.m
//  RoboRemoteBlue
//
//  Created by David Chan on 1/4/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import "AppConfig.h"
#import "ConnectionManager.h"
#import "NSArray+SavePlist.h"
#import "FileManager.h"
#import "LocalizationManager.h"

// Please add Localization-jp/fr/de/es...
// Please uncomment arrLanguage and arrDetailLanguage

@implementation AppConfig

static AppConfig* _sharedObject = nil;
+ (AppConfig*)sharedInstance {
    if (_sharedObject == nil)
        _sharedObject = [AppConfig new];
    return _sharedObject;
}

+ (NSString*)applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (id)init {
    if (self = [super init]) {
        [self loadConfig];
        [self reloadLanguage];

        self.arrLanguage = [NSMutableArray new];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"English (US)"]];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"English (U.K.)"]];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"French"]];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"German"]];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"Italian"]];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"Japanese"]];
//        [self.arrLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"Spanish"]];
        
        self.arrDetailLanguage = [NSMutableArray new];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"English (US)"]];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"English (U.K.)"]];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"French_en"]];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"German_en"]];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"Italian_en"]];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"Japanese_en"]];
//        [self.arrDetailLanguage addObject:[[LocalizationManager getInstance] getStringWithKey:@"Spanish_en"]];
    }
    
    return self;
}

- (void)loadConfig {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"language"]) {
        self.indexLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    }
    else {
        if ([[[NSLocale preferredLanguages] firstObject] isEqualToString:@"en"]) {
            self.indexLanguage = 0;
        }
        else if ([[[NSLocale preferredLanguages] firstObject] isEqualToString:@"fr"]) {
            self.indexLanguage = 2;
        }
//        else if ([[[NSLocale preferredLanguages] firstObject] isEqualToString:@"de"]) {
//            self.indexLanguage = 3;
//        }
//        else if ([[[NSLocale preferredLanguages] firstObject] isEqualToString:@"it"]) {
//            self.indexLanguage = 4;
//        }
        else if ([[[NSLocale preferredLanguages] firstObject] isEqualToString:@"ja"]) {
            self.indexLanguage = 5;
        }
        else if ([[[NSLocale preferredLanguages] firstObject] isEqualToString:@"es"]) {
            self.indexLanguage = 6;
        }
        else
            self.indexLanguage = 0;
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"theme"]) {
        self.indexTheme = [[NSUserDefaults standardUserDefaults] integerForKey:@"theme"];
    }
    else {
        self.indexTheme = 1;
    }
    
    NSString* strPath = [FileManager generatePathName:CONNECTEDDEVICE_PLIST_FILENAME];
    self.arrConnectedDevice = [NSMutableArray arrayWithArray:[NSArray loadPlist:strPath]];
    if (self.arrConnectedDevice == nil)
        self.arrConnectedDevice = [NSMutableArray new];
}

- (void)saveConfig {
    [[NSUserDefaults standardUserDefaults] setInteger:self.indexLanguage forKey:@"language"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.indexTheme forKey:@"theme"];
}

- (void)reloadLanguage {
    switch (self.indexLanguage) {
        case AppConfigLanguageEnglishUS:
            [[LocalizationManager getInstance] loadLanguage:@"en"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"English (U.K.)"}];
            break;
        case AppConfigLanguageEnglishUK:
//            [[LocalizationManager getInstance] loadLanguage:@"en"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"English (US)"}];
            break;
        case AppConfigLanguageFrench:
            [[LocalizationManager getInstance] loadLanguage:@"en"];
//            [[LocalizationManager getInstance] loadLanguage:@"fr"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"French"}];
            break;
        case AppConfigLanguageGerman:
//            [[LocalizationManager getInstance] loadLanguage:@"de"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"German"}];
            break;
        case AppConfigLanguageItalian:
//            [[LocalizationManager getInstance] loadLanguage:@"it"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"Italian"}];
            break;
        case AppConfigLanguageJapanese:
            [[LocalizationManager getInstance] loadLanguage:@"en"];
//            [[LocalizationManager getInstance] loadLanguage:@"ja"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"Japanese"}];
            break;
        case AppConfigLanguageSpanish:
            [[LocalizationManager getInstance] loadLanguage:@"en"];
//            [[LocalizationManager getInstance] loadLanguage:@"es"];
//            [Flurry logEvent:@"Language Selected" withParameters:@{@"Robot type" : @"Robosapien", @"Language" : @"Spanish"}];
            break;
            
        default:
            break;
    }
}

- (NSString*)getCurrentLanguage {
    return [self.arrLanguage objectAtIndex:self.indexLanguage];
}

- (void)activated {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"activated"];
}

- (BOOL)isActivated {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"activated"] != nil && [[NSUserDefaults standardUserDefaults] boolForKey:@"activated"];
}

- (void)saveDeviceWithName:(NSString*)strName Identity:(NSString*)strID ColorID:(NSInteger)colorID {
    ConnectedDeviceRecord* dict = nil;
    for (int i=0;i<[self.arrConnectedDevice count];i++) {
        ConnectedDeviceRecord* record = [self.arrConnectedDevice objectAtIndex:i];
        if ([strName isEqualToString:record.strName] && [strID isEqualToString:record.strID]) {
            dict = record;
        }
        else if ([strID isEqualToString:record.strID]) {
            dict = record;
            dict.strName = strName;
            dict.colorID = colorID;
        }
    }
    if (dict == nil) {
        dict = [ConnectedDeviceRecord new];
        dict.strName = strName;
        dict.strID = strID;
        dict.connectedCount = 0;
        dict.colorID = colorID;
        [self.arrConnectedDevice addObject:dict];
    }
    else {
        dict.strName = strName;
        dict.colorID = colorID;
        dict.connectedCount += 1;
    }
    NSString* strPath = [FileManager generatePathName:CONNECTEDDEVICE_PLIST_FILENAME];
    [self.arrConnectedDevice savePlist:strPath];
}

@end
