//
//  AppConfig.h
//  RoboRemoteBlue
//
//  Created by David Chan on 1/4/15.
//  Copyright (c) 2015 wowwee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectedDeviceRecord.h"

typedef enum {
    AppConfigLanguageEnglishUS = 0,
    AppConfigLanguageEnglishUK = 1,
    AppConfigLanguageFrench = 2,
    AppConfigLanguageGerman = 3,
    AppConfigLanguageItalian = 4,
    AppConfigLanguageJapanese = 5,
    AppConfigLanguageSpanish = 6,
}AppConfigLanguage;

#define CONNECTEDDEVICE_PLIST_FILENAME @"connecteddevice"
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface AppConfig : NSObject {

}

+ (AppConfig*)sharedInstance;
+ (NSString*)applicationDocumentsDirectory;
- (void)loadConfig;
- (void)saveConfig;
- (void)reloadLanguage;
- (NSString*)getCurrentLanguage;
- (void)activated;
- (BOOL)isActivated;

- (void)saveDeviceWithName:(NSString*)strName Identity:(NSString*)strID ColorID:(NSInteger)colorID;

@property (assign, nonatomic) NSInteger donePhotoCount;
@property (readwrite) NSUInteger indexTheme;
@property (readwrite) NSUInteger indexLanguage;
@property (readwrite) NSMutableArray* arrLanguage;
@property (readwrite) NSMutableArray* arrDetailLanguage;
@property (readwrite) NSMutableArray* arrConnectedDevice;
@property (readwrite) NSString* strRobotName;
@end
