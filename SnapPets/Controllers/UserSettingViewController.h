//
//  UserSettingViewController.h
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnapPetSettingPopupView.h"
#import "ParentViewController.h"

@interface UserSettingViewController : ParentViewController {
    
}
- (void)updateView;

@property (nonatomic, strong) IBOutlet UILabel* titleLanguageLabel;
@property (nonatomic, strong) IBOutlet UILabel* titleSnapPetsLabel;
@property (nonatomic, strong) IBOutlet UILabel* titleThemeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imgBG;
@property (atomic, strong) IBOutlet UIView* viewLanguageSelection;
@property (atomic, strong) IBOutlet UIView* viewThemeSelection;
@property (atomic, strong) IBOutlet UIButton* buttonLanguageEng;
@property (atomic, strong) IBOutlet UIButton* buttonLanguageJP;
@property (atomic, strong) IBOutlet UIButton* buttonLanguageES;
@property (atomic, strong) IBOutlet UIButton* buttonLanguageFR;
@property (atomic, strong) IBOutlet UIButton* buttonTheme1;
@property (atomic, strong) IBOutlet UIButton* buttonTheme2;
@property (atomic, strong) IBOutlet UIButton* buttonTheme3;
@property (atomic, strong) IBOutlet UIButton* buttonTheme4;
@property (atomic, strong) IBOutlet UIButton* buttonTheme5;

@property (atomic, strong) IBOutlet UIButton* buttonDisconnect;

@property (atomic, strong) IBOutlet SnapPetSettingPopupView* viewSettingPopup;

@property (atomic, strong) NSMutableArray* arrConnectedDevice;

@property (atomic, strong) IBOutlet UITableView* tableView;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintHeaderHeight;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintListHeight;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintTableViewHeight;

@end
