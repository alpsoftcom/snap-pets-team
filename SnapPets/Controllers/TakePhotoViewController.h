//
//  TakePhotoViewController.h
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "TakePhotoOverlayView.h"
#import "SnapPetPopup.h"
#import "ParentViewController.h"

@interface TakePhotoViewController : ParentViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, TakePhotoDelegate, SnappetRobotDelegate> {
    NSTimer* timerAnimatingDownloadImages;
    
    BOOL isUnderTutorial;
    BOOL isInUserSettingController;
    BOOL isInCurrentViewController;
}
- (void)viewUnloadAnimation;
- (void)disableSettingButton;
- (void)disableCloseButton;

@property (nonatomic, strong) IBOutlet UIView *cameraView;
@property (nonatomic, strong) TakePhotoOverlayView *takephotoOverlayView;
@property (nonatomic, strong) IBOutlet TutorialSnapPetView *tutorialView;
@property (nonatomic, strong) IBOutlet TutorialEnterNameView *enterNameView;
@property (nonatomic, strong) IBOutlet SnapPetPopup *popupView;

@end
