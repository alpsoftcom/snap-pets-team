//
//  ModalScreensViewController.m
//  SnapPets
//
//  Created by Andrew Medvedev on 15/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "ModalScreensViewController.h"
#import "PetPinView.h"
#import "PetNameView.h"
#import "PetInvalidPinView.h"
#import "ConnectionManager.h"
#import "LowBatteryView.h"
#import "FullStorageView.h"

@interface ModalScreensViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *modalBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rateView;
@property (weak, nonatomic) IBOutlet UIView *noPetView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet PetPinView *petPinView;
@property (weak, nonatomic) IBOutlet PetNameView *petNameView;
@property (weak, nonatomic) IBOutlet PetInvalidPinView *invalidPinView;
@property (weak, nonatomic) IBOutlet LowBatteryView *lowBatteryView;
@property (weak, nonatomic) IBOutlet FullStorageView *fullStorageView;

@end

@implementation ModalScreensViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    UITapGestureRecognizer *gest = [[UITapGestureRecognizer alloc]initWithTarget:self.view action:@selector(endEditing:)];
    gest.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gest];
    
    self.view.backgroundColor = [UIColor clearColor];
    if ( IS_IOS7 )
    {
        self.modalPresentationStyle = UIModalPresentationFormSheet;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.modalBackgroundView.layer.cornerRadius = self.modalBackgroundView.frame.size.height / 16;
    self.modalBackgroundView.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self hideAllViews];
}

- (void)hideAllViews
{
    self.rateView.hidden = YES;
    self.noPetView.hidden = YES;
    self.petPinView.hidden = YES;
    self.petNameView.hidden = YES;
    self.invalidPinView.hidden = YES;
    self.lowBatteryView.hidden = YES;
    self.fullStorageView.hidden = YES;
}

- (void)setModalScreenType:(enum ModalScreenType)modalScreenType
{
    
    switch (modalScreenType) {
        case kModalScreenTypeRate:
        {
            self.rateView.hidden = NO;
            self.closeButton.hidden = NO;
        }
            break;
        case kModalScreenTypeBuyPet:
        {
            self.noPetView.hidden = NO;
            self.closeButton.hidden = NO;
        }
            break;
        case kModalScreenTypePetFullStorage:
        {
            self.fullStorageView.hidden = NO;
            self.closeButton.hidden = NO;
            
            __weak typeof (self) wself = self;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                wself.fullStorageView.fullStorageBlock = ^{
                    NSLog(@"FULL STORAGE");
                };
            });
        }
            break;
        case kModalScreenTypePetLowBattery:
        {
            self.lowBatteryView.hidden = NO;
            self.closeButton.hidden = NO;
        }
            break;
        case kModalScreenTypePetConnecting:
            break;
        case kModalScreenTypePetSearching:
            break;
        case kModalScreenTypePetPin:
        {
            self.petPinView.hidden = NO;
            
            [self.petPinView checkConnectionAndPinStatus];
            
            __weak typeof (self) wself = self;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                wself.petPinView.petPinBlock = ^(NSString *petPin, BOOL access){
                    if ( petPin.length )
                    {
                        wself.callBackHandler(YES);
                        [wself dismissViewControllerAnimated:YES completion:nil];
                    }
                    else
                    {
                        wself.petPinView.hidden = YES;
                        wself.modalScreenType = access ? kModalScreenTypePetName : kModalScreenTypePetInvalidPin;
                    }
                };
            });
        }
            break;
        case kModalScreenTypePetName:
        {
            self.petNameView.hidden = NO;
            
            __weak typeof (self) wself = self;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                wself.petNameView.petNameBlock = ^(NSString *petName){

                    wself.petNameView.hidden = YES;
                    wself.modalScreenType = kModalScreenTypePetPin;
                };
            });
        }
            break;
        case kModalScreenTypePetInvalidPin:
        {
            self.invalidPinView.hidden = NO;
            
            __weak typeof (self) wself = self;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                wself.invalidPinView.petInvalidPinBlock = ^{
                    wself.invalidPinView.hidden = YES;
                    wself.modalScreenType = kModalScreenTypePetPin;
                };
            });
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Actions
- (IBAction)yesButtonPressed:(id)sender
{
    if ( IS_IPAD )
    {
        
    }
    else
    {
//        itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=960996059&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8
        NSString *iTunesLink = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=960996059&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    NSLog(@"YES");
}

- (IBAction)noButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"NO");
}

#pragma mark - KeyboardNotifications
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if ( SCREEN_HEIGHT - (self.modalBackgroundView.frame.origin.y + self.modalBackgroundView.frame.size.height) < kbRect.size.height )
    {
        [self.scrollView setContentOffset:(CGPoint){0, kbRect.size.height - (SCREEN_HEIGHT - (self.modalBackgroundView.frame.origin.y + self.modalBackgroundView.frame.size.height)) + 10} animated:YES];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Dealloc
- (void)dealloc
{
    NSLog(@"modalscreensvc DEALLOC");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
