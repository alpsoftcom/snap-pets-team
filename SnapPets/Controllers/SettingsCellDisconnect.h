//
//  UserSettingDeviceCell.h
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>


@interface SettingsCellDisconnect : UITableViewCell {
    
}


@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

-(void)setLastTypeCell;

@end
