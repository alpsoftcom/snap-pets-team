//
//  TestServiceViewController.m
//  SnapPets
//
//  Created by Katy Pun on 18/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "TestServiceViewController.h"
#import "CamTestController.h"
#import "ConnectionManager.h"
//#import "SnappetRobotConstants.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

@interface TestServiceViewController ()
@property (nonatomic) BOOL isAuthen;
@property (nonatomic) BOOL isEnablePin;
@property (nonatomic) BOOL isSwitchOnPin;
@end

@implementation TestServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isAuthen = NO;
    self.snappet = [ConnectionManager sharedInstance].snappet;
    
    [self.snappet snappetSetBroadcastData:[NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:kSnappetColorPink] forKey:[NSNumber numberWithInteger:0x00]]];
}

- (void)viewWillAppear:(BOOL)animated{
    self.snappet.delegate = self;
//    [self.snappet snappetReadThumbnailQuality];
//    [self.snappet snappetReadThumbnailResolution];
    
    
//    [self.snappet snappetReadPhotoQuality];
//    [self.snappet snappetReadPhotoResolution];
//    [self.snappet snappetReadLongPressAction];
//    [self.snappet snappetReadLongPressDuration];
//    [self.snappet snappetReadCmosPoweroffTime];
//    [self.snappet snappetReadDeviceName];
    
    [self.snappet snappetReadAppendPhotoHeader];
    [self.snappet snappetReadOverridePhotoOnFlash];
    [self.snappet snappetReadShortPressAction];
    [self.snappet snappetReadLEDEnabled];
    [self.snappet snappetReadActivationStatus];
    [self.snappet snappetReadCmosSensorFrequency];
//    [self.snappet snappetReadChargingStatus];
    [self.snappet snappetReadASCIIPinCodeEnabled];
//    [self.snappet snappetReadSleepTime];
    
    [self.snappet snappetReadTimeout];
    [self.snappet snappetReadLargeSizePhotoInfo];
    

    //only support firmware after pp
    if (self.snappet.bleModuleSoftwareVersion.length > 5){
        [self.snappet snappetReadDisplayName];
        [self.snappet snappetReadUserPhoneName];
        [self.snappet snappetReadBroadcastData];
    }
    

    [self.snappet snappetReadProductIdentifier];
    
    
    [self.snappet snappetReadChargingStatus];
    self.settingView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 568);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)updatePressed:(id)sender {
    
//    if ([self.thumbnailQuality isFirstResponder]){
//        if (self.thumbnailQuality.text.length > 0 ){
//            [self.snappet snappetSetThumbnailQuality:(uint8_t)[self.thumbnailQuality.text intValue]];
//        }
//    }else if ([self.thumbnailResolution isFirstResponder]){
//        if (self.thumbnailResolution.text.length > 0){
//            [self.snappet snappetSetThumbnailResolution:[self.thumbnailResolution.text intValue]];
//        }
//    }else if ([self.photoQuality isFirstResponder]){
//        if (self.photoQuality.text.length > 0){
//            [self.snappet snappetSetPhotoQuality:[self.photoQuality.text intValue]];
//            [self.snappet snappetReadPhotoQuality];
//        }
//    }else if ([self.photoResolution isFirstResponder]){
//        if (self.photoResolution.text.length > 0){
//            [self.snappet snappetSetPhotoResolution:[self.photoResolution.text intValue]];
//        }
//    }else if ([self.longPress isFirstResponder]){
//        if (self.longPress.text.length > 0){
//            [self.snappet snappetSetLongPressAction:[self.longPress.text intValue]];
//        }
//    }else if ([self.longPressDuration isFirstResponder]){
//        if (self.longPressDuration.text.length > 0){
//            [self.snappet snappetSetLongPressDuration:[self.longPressDuration.text intValue]];
//        }
//    }
    if ([self.shortPress isFirstResponder]){
        if (self.shortPress.text.length > 0){
            [self.snappet snappetSetShortPressAction:[self.shortPress.text intValue]];
        }
    }else if ([self.activationStatus isFirstResponder]){
        if (self.activationStatus.text.length > 0){
//            [self.snappet snappetSetProductActivated];
//            [self.snappet snappetSetProductActivationUploaded];
            [self.snappet snappetResetProductActivation];
        }
    }else if ([self.cmosFrequency isFirstResponder]){
        if (self.cmosFrequency.text.length > 0){
            [self.snappet snappetSetCmosSensorFrequency:[self.cmosFrequency.text intValue]];
        }
    }else if ([self.pinAuthen isFirstResponder]){
        if (self.pinAuthen.text.length > 0){
            
        }
    }else if ([self.deviceNameTextField isFirstResponder]) {
        if (self.deviceNameTextField.text.length > 0){
//            [self.snappet snappetWriteDeviceName:self.deviceNameTextField.text];
            [self.snappet snappetWriteDisplayName:self.deviceNameTextField.text];
//            [self.snappet snappetSetSleepTime:[self.deviceNameTextField.text intValue]];
        }
    }else if ([self.photoQuality isFirstResponder]){
        if (self.photoQuality.text.length > 0){
            kSnappetPhotoResolution resolution;
            if (self.photoResolution.text.length > 0){
                resolution = [self.photoResolution.text intValue];
            }else{
                resolution = kSnappetPhotoResolutionVGA;
            }
            [self.snappet snappetSetLargeSizePhotoInfo:[self.photoQuality.text intValue] resolution:resolution];

        }
    }else if ([self.photoResolution isFirstResponder]){
        if (self.photoResolution.text.length > 0){
            kSnappetPhotoQuality quality;
            if (self.photoQuality.text.length > 0){
                quality = [self.photoQuality.text intValue];
            }else{
                quality = kSnappetPhotoQualityLow;
            }
            [self.snappet snappetSetLargeSizePhotoInfo:quality resolution:[self.photoResolution.text intValue]];
        }
    }
    
    [self.view endEditing:YES];
}

- (IBAction)backPressed:(id)sender {
//    [self.snappet disconnect];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchOnOff:(id)sender {
    UISwitch *switcher = (UISwitch *)sender;
    if ([switcher isEqual:self.appendPhotoHeader]) {
//        [self.snappet snappetAppendPhotoHeader:self.appendPhotoHeader.isOn];
        [self.snappet snappetReboot:kSnappetRebootApplication];
    }else if ([switcher isEqual:self.overridePhotoOnFlash]) {
        [self.snappet snappetOverridePhotosOnFlash:self.overridePhotoOnFlash.isOn];
    }else if ([switcher isEqual:self.enableLED]) {
        [self.snappet snappetSetLEDEnabled:self.enableLED.isOn];
//        [self.snappet snappetReboot:kSnappetRebootDFU];
    }
    else if ([switcher isEqual:self.pinEnable]){
        self.pinSetupView.hidden = NO;
        self.settingView.hidden = YES;
        
        if (self.pinEnable.isOn) {
            self.isSwitchOnPin = YES;
        }else{
            self.isSwitchOnPin = NO;
        }
//        if (self.pinEnable.isOn){
//            [self.snappet snappetSetASCIIPinCode:0 digit2:0 digit3:0 digit4:0];
//        }
//        [self.snappet snappetSetASCIIPinCodeEnabled:self.pinEnable.isOn];
    }
}

- (IBAction)resetPin:(id)sender {
    [self.snappet snappetResetASCIIPinCodeAndEraseFlash];
}

- (IBAction)camTestPressed:(id)sender {
    CamTestController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CamTestController"];
    controller.snappet = self.snappet;
    controller.snappet.delegate = controller;
    controller.snappet.autoReconnect = NO;     // Disable auto reconnect for now
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)dismissPinSetup:(id)sender {
    self.pinSetupView.hidden = YES;
    self.settingView.hidden = NO;
    
    if (self.isAuthen) {
        [self backPressed:nil];
    }else{
        if (self.isEnablePin){
            [self.pinEnable setOn: YES];
        }else{
            [self.pinEnable setOn: NO];
        }
    }
}

- (IBAction)submitPinSetup:(id)sender {
    
    if (self.pinTextField.text.length >= 4){
        
//        short val=(short)( ((17&0xFF)<<8) | (17&0xFF) );
        
        int value0 = [[NSString stringWithFormat:@"%c", [self.pinTextField.text characterAtIndex:0]] intValue];
        int value1 = [[NSString stringWithFormat:@"%c", [self.pinTextField.text characterAtIndex:1]] intValue];
        int value2 = [[NSString stringWithFormat:@"%c", [self.pinTextField.text characterAtIndex:2]] intValue];
        int value3 = [[NSString stringWithFormat:@"%c", [self.pinTextField.text characterAtIndex:3]] intValue];
     
        if (self.isAuthen) {
            //authen pin
            [self.snappet snappetAuthenticateWithASCIIPinCode:value0 digit2:value1 digit3:value2 digit4:value3];
        }else{
            self.pinSetupView.hidden = YES;
            self.settingView.hidden = NO;
            
            [self.snappet snappetSetASCIIPinCode:value0 digit2:value1 digit3:value2 digit4:value3];
            if (self.isEnablePin){
                self.isEnablePin = NO;
                [self.snappet snappetSetASCIIPinCodeEnabled:NO];
            }else{
                self.isEnablePin = YES;
                [self.snappet snappetSetASCIIPinCodeEnabled:YES];
            }
            
            

        }
    }
   
    [self.view endEditing:YES];

}

- (void)authenPIN {
    [self.snappet snappetAuthenticateWithASCIIPinCode:0 digit2:0 digit3:0 digit4:0];
}

#pragma mark - SnappetRobotDelegate

-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsThumbnailQuality:(int)thumbnailQuality{
    self.thumbnailQuality.text = [NSString stringWithFormat:@"%d", thumbnailQuality];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsThumbnailResolution:(int)resolution{
    self.thumbnailResolution.text = [NSString stringWithFormat:@"%d", resolution];
}
//-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsPhotoQuality:(int)photoQuality{
//    self.photoQuality.text = [NSString stringWithFormat:@"%d", photoQuality];
//}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsPhotoResolution:(int)photoResolution{
    self.photoResolution.text = [NSString stringWithFormat:@"%d", photoResolution];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsLongPress:(int)state{
    self.longPress.text = [NSString stringWithFormat:@"%d", state];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsLongPressDuration:(int)duration{
    self.longPressDuration.text = [NSString stringWithFormat:@"%d", duration];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsSleepTime:(int)sleepTime{
    self.deviceNameTextField.text = [NSString stringWithFormat:@"%d", sleepTime];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingCmosPoweroffTime:(int)cmosPoweroffTime{
    self.cmosPoweroffTextField.text = [NSString stringWithFormat:@"%d", cmosPoweroffTime];
}


-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsAppendPhotoHeader:(int)appendHeader{
    [self.appendPhotoHeader setOn:(appendHeader == 0x00 ? NO : YES)];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsPhotoOnFlash:(int)override{
    [self.overridePhotoOnFlash setOn:(override == 0x00 ? NO : YES)];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsShortPress:(int)state{
    self.shortPress.text = [NSString stringWithFormat:@"%d", state];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsEnableLED:(int)enableLED{
    [self.enableLED setOn:(enableLED == 0x01 ? YES : NO)];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsActivationStatus:(int)status{
    self.activationStatus.text = [NSString stringWithFormat:@"%d", status];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsCmosFrequency:(int)frequency{
    self.cmosFrequency.text = [NSString stringWithFormat:@"%d", frequency];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingChargingStatus:(kSnappetChargingStatus)chargingStatus{
    if (chargingStatus == kSnappetNotCharging){
        self.chargingStatusLabel.text = @"not charging";
    }else if (chargingStatus == kSnappetCharging){
        self.chargingStatusLabel.text = @"charging";
    }else if (chargingStatus == kSnappetFullyCharge){
        self.chargingStatusLabel.text = @"power full";
    }
    
    [self.chargingStatusLabel sizeToFit];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingLargePhotoInfo:(kSnappetPhotoQuality)quality resolution:(kSnappetPhotoResolution)resolution{
    self.photoQuality.text = [NSString stringWithFormat:@"%d", quality];
    self.photoResolution.text = [NSString stringWithFormat:@"%d", resolution];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingTimeout:(int)connectModeTimeout cmosPoweroffTimeout:(int)cmosPoweroffTimeout{
    self.cmosPoweroffTextField.text = [NSString stringWithFormat:@"%d", cmosPoweroffTimeout];
    
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingChargingResponse:(int)response{
    if (response == kSnappetNotCharging){
        self.chargingStatusLabel.text = @"not charging";
    }else if (response == kSnappetCharging){
        self.chargingStatusLabel.text = @"charging";
    }else if (response == kSnappetFullyCharge){
        self.chargingStatusLabel.text = @"power full";
    }
    [self.chargingStatusLabel sizeToFit];
}


-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityPinEnable:(int)enable{
    enable == 0x00 ? (self.isAuthen = NO) : (self.isAuthen = YES);
    [self.pinEnable setOn:(enable == 0x00 ? NO : YES)];
    self.isEnablePin = self.isAuthen;
    if (enable == 0x01){
        self.pinSetupView.hidden = NO;
        self.settingView.hidden = YES;
        
//        [self performSelector:@selector(authenPIN) withObject:nil];
    }
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityAuthenticateResponse:(int)response{
    if (response == 0){
        self.pinAuthen.text = @"fail";
    }else{
        self.pinAuthen.text = @"success";
    }
    if (response == 0x00) {
        //authen fail
        self.authenFailLabel.hidden = NO;
    }else {
        //authen success
        self.pinSetupView.hidden = YES;
        self.settingView.hidden = NO;
        self.isAuthen = NO;

        if (self.isEnablePin) {
            [self.pinEnable setOn:YES];
        }else {
            [self.pinEnable setOn:NO];
            [self.snappet snappetSetASCIIPinCodeEnabled:NO];
        }
    }
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveGenericAccessDeviceName:(NSString *)deviceName{
    if (deviceName && ![deviceName isEqual:[NSNull null]]){
        self.deviceNameTextField.text = deviceName;
    }
}

-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
    [self.navigationController popViewControllerAnimated:YES];
    [snappet connect];
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveModuleParameterDeviceName:(NSString *)deviceName{
    self.deviceNameTextField.text = deviceName;
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveModuleParameterCustomBroadcastData:(NSDictionary *)customBroadcastData{
    NSLog(@"%@",[customBroadcastData objectForKey:[NSNumber numberWithInteger:0x00]]);   //[NSNumber numberWithInteger:kSnappetColorPink]
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveModuleParameterProductId:(NSUInteger)productId{
    NSLog(@"%d", productId);
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveModuleParameterUserPhoneName:(NSString *)userPhoneName{
    self.userDeviceName.text = userPhoneName;
    if (userPhoneName == nil || [userPhoneName isEqualToString:@""]){
        self.userDeviceName.text = [[UIDevice currentDevice] name];
        [self.snappet snappetSetUserPhoneName:self.userDeviceName.text];
    }
}

@end
