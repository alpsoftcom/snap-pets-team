//
//  UserSettingDeviceCell.m
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "SettingsCellHeader.h"

@implementation SettingsCellHeader

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    return;
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
