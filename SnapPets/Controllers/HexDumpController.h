//
//  HexDumpController.h
//  MipPhotoTest
//
//  Created by Forrest Chan on 5/8/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface HexDumpController : ParentViewController

@property (nonatomic, weak) IBOutlet UITextView *hexDumpView;
@property (nonatomic, strong) NSString *hexDump;

@end
