//
//  SettingsViewController.m
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "SettingsViewController.h"
#import "ConnectionManager.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ConnectionManager sharedInstance].snappet.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)deleteAllPhotos:(id)sender {
    [[ConnectionManager sharedInstance].snappet snappetDeleteAllPhoto];
}

- (IBAction)powerOff:(id)sender {
    
    [[ConnectionManager sharedInstance].snappet snappetReboot:kSnappetRebootPowerOff];
    [[ConnectionManager sharedInstance].snappet disconnect];
}

-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
