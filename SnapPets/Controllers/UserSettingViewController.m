//
//  UserSettingViewController.m
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "UserSettingViewController.h"
#import "UserSettingDeviceCell.h"
#import "AppConfig.h"
#import "ConnectionManager.h"
#import "LocalizationManager.h"

@interface UserSettingViewController ()

@end

@implementation UserSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewLanguageSelection.layer.cornerRadius = 10.0f;
    self.viewThemeSelection.layer.cornerRadius = 10.0f;
    // Do any additional setup after loading the view.

#if TARGET_IPHONE_SIMULATOR
#else
    self.arrConnectedDevice = [AppConfig sharedInstance].arrConnectedDevice;
    SnappetRobot* robot = [ConnectionManager sharedInstance].snappet;
    ConnectedDeviceRecord* record = nil;
    
    for (int i=0;i<[self.arrConnectedDevice count];i++) {
        ConnectedDeviceRecord* dict = [self.arrConnectedDevice objectAtIndex:i];
        if ([dict.strID isEqualToString:[robot.uuid UUIDString]]) {
            record = dict;
            break;
        }
    }

    [self.arrConnectedDevice removeObject:record];
    [self.arrConnectedDevice insertObject:record atIndex:0];
#endif
    [self.viewSettingPopup setHidden:YES];
    
    CGSize screenBounds = [self.view bounds].size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* font = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:36];
        self.titleLanguageLabel.font = font;
        self.titleSnapPetsLabel.font = font;
        self.titleThemeLabel.font = font;
    }
    else if (screenBounds.height == 480) {
        self.constraintHeaderHeight.constant = 70.0f;
        self.constraintListHeight.constant = 90.0f;
        self.constraintTableViewHeight.constant = 60.0f;
        self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen.png"];
    }
    else if (screenBounds.height == 667) {
        self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen.png"];
    }
    
    
    // Temp
    [self.viewLanguageSelection setHidden:YES];
    [self.viewThemeSelection setHidden:YES];
    
    
    [self.buttonDisconnect setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnDisconnect"] forState:UIControlStateNormal];
    [self.titleLanguageLabel setText:[[LocalizationManager getInstance] getStringWithKey:@"SettingTitleLanguage"]];
    [self.titleThemeLabel setText:[[LocalizationManager getInstance] getStringWithKey:@"SettingTitleTheme"]];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.buttonDone.layer.cornerRadius = 10.0f;
    self.buttonDisconnect.layer.cornerRadius = 10.0f;
    
    [self updateView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)updateView {
    [self.buttonLanguageEng setAlpha:0.5f];
    [self.buttonLanguageJP setAlpha:0.5f];
    [self.buttonLanguageES setAlpha:0.5f];
    [self.buttonLanguageFR setAlpha:0.5f];
    
    [self.buttonTheme1 setAlpha:0.5f];
    [self.buttonTheme2 setAlpha:0.5f];
    [self.buttonTheme3 setAlpha:0.5f];
    [self.buttonTheme4 setAlpha:0.5f];
    [self.buttonTheme5 setAlpha:0.5f];
    
    switch ([AppConfig sharedInstance].indexLanguage) {
        case AppConfigLanguageEnglishUS:
            [self.buttonLanguageEng setAlpha:1.0f];
            break;
        case AppConfigLanguageJapanese:
            [self.buttonLanguageJP setAlpha:1.0f];
            break;
        case AppConfigLanguageSpanish:
            [self.buttonLanguageES setAlpha:1.0f];
            break;
        case AppConfigLanguageFrench:
            [self.buttonLanguageFR setAlpha:1.0f];
            break;
        default:
            break;
    }
    
    switch ([AppConfig sharedInstance].indexTheme) {
        case 1:
            [self.buttonTheme1 setAlpha:1.0f];
            break;
        case 2:
            [self.buttonTheme2 setAlpha:1.0f];
            break;
        case 3:
            [self.buttonTheme3 setAlpha:1.0f];
            break;
        case 4:
            [self.buttonTheme4 setAlpha:1.0f];
            break;
        case 5:
            [self.buttonTheme5 setAlpha:1.0f];
            break;
        default:
            break;
    }
    
    [self.buttonLanguageJP setEnabled:NO];
    [self.buttonLanguageES setEnabled:NO];
    [self.buttonLanguageFR setEnabled:NO];

    [self.buttonTheme2 setEnabled:NO];
    [self.buttonTheme3 setEnabled:NO];
    [self.buttonTheme4 setEnabled:NO];
    [self.buttonTheme5 setEnabled:NO];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)languageSelectonAction:(id)sender {
    if (sender == self.buttonLanguageEng) {
        [AppConfig sharedInstance].indexLanguage = (int)AppConfigLanguageEnglishUS;
    }
    else if (sender == self.buttonLanguageJP) {
        [AppConfig sharedInstance].indexLanguage = (int)AppConfigLanguageJapanese;
    }
    else if (sender == self.buttonLanguageES) {
        [AppConfig sharedInstance].indexLanguage = (int)AppConfigLanguageSpanish;
    }
    else if (sender == self.buttonLanguageFR) {
        [AppConfig sharedInstance].indexLanguage = (int)AppConfigLanguageFrench;
    }
    
    [[AppConfig sharedInstance] saveConfig];
    [self updateView];
}

- (IBAction)themeSelectionAction:(id)sender {
    if (sender == self.buttonTheme1) {
        [AppConfig sharedInstance].indexTheme = 1;
    }
    else if (sender == self.buttonTheme2) {
        [AppConfig sharedInstance].indexTheme = 2;
    }
    else if (sender == self.buttonTheme3) {
        [AppConfig sharedInstance].indexTheme = 3;
    }
    else if (sender == self.buttonTheme4) {
        [AppConfig sharedInstance].indexTheme = 4;
    }
    else if (sender == self.buttonTheme5) {
        [AppConfig sharedInstance].indexTheme = 5;
    }
    
    [[AppConfig sharedInstance] saveConfig];
    [self updateView];
}

- (IBAction)buttonAction:(id)sender {
    if (sender == self.buttonDisconnect) {
        [[ConnectionManager sharedInstance].snappet disconnect];
        [ConnectionManager sharedInstance].snappet = nil;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
//    return [[AppConfig sharedInstance].arrConnectedDevice count];
    //    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserSettingDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userSettingDeviceCell"];
    cell.userInteractionEnabled = NO;
#if TARGET_IPHONE_SIMULATOR
    [cell.nameLabel setText:@"iphone simulator"];
    cell.colorID = 1;
    cell.userInteractionEnabled = YES;
#else
    ConnectedDeviceRecord* dict = [[AppConfig sharedInstance].arrConnectedDevice objectAtIndex:indexPath.row];
    if (indexPath.row == 0)
        [cell.nameLabel setText:[AppConfig sharedInstance].strRobotName];
    else
        [cell.nameLabel setText:dict.strName];
    SnappetRobot* robot = [ConnectionManager sharedInstance].snappet;
    cell.colorID = dict.colorID;
    if ([dict.strID isEqualToString:[robot.uuid UUIDString]]){
        cell.userInteractionEnabled = YES;
    }
#endif
    if (cell.userInteractionEnabled) {
        [cell.nameLabel setTextColor:[UIColor colorWithRed:163.0f/255.0f green:58.0f/255.0f blue:186.0f/255.0f alpha:1.0f]];
    }
    else {
        [cell.nameLabel setTextColor:[UIColor grayColor]];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGRect rect = [self.view bounds];
    if (rect.size.height == 1024 || rect.size.height == 2048)
        return 88.0f;
    else
        return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.viewSettingPopup showBaseMenu];
    self.viewSettingPopup.delegate = self;
    NSString *shortversionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    [self.viewSettingPopup setVersion:[NSString stringWithFormat:@"%@: %@  (%@)",[[LocalizationManager getInstance] getStringWithKey:@"Version"], shortversionString, versionString]];
    
    [self.viewSettingPopup setName:[AppConfig sharedInstance].strRobotName];
    
}

- (void)hided {
    [self.tableView reloadData];
}

@end