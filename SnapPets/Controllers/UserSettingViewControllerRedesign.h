//
//  UserSettingViewController.h
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnapPetSettingPopupView.h"
#import "ParentViewController.h"

typedef enum {
    SettingsCellTypeGeneral = 0,
   // SettingsCellTypeTutorial = 1,
//    SettingsCellTypeLanguage = 2,//1
    SettingsCellTypeReset = 1 ,
    SettingsCellTypeCustomization = 2,
    SettingsCellTypeSnapPetName = 3,
    SettingsCellTypeSnapPetPin=4,
//    SettingsCellTypeSnapPetColor = 7,
    SettingsCellTypeUpdates = 5,
    SettingsCellTypeSnapPetVersion = 6,
    SettingsCellTypeDissconnect = 7
} SettingsCellType;

@interface UserSettingViewControllerRedesign : ParentViewController <UITableViewDelegate, UITableViewDataSource> {
    
}
- (void)updateView;

@property (nonatomic,weak) IBOutlet UITableView * tableView;

-(IBAction)backButtonPressed:(id)sender;
@end
