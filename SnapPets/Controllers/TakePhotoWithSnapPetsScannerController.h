//
//  TakePhotoWithSnapPetsScannerController.h
//  SnapPets
//
//  Created by David Chan on 21/4/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TakePhotoViewController.h"

@interface TakePhotoWithSnapPetsScannerController : TakePhotoViewController <UITableViewDelegate, UITableViewDataSource> {
    NSTimer*    timerAnimatingScannerButton;
    NSArray*    m_arrSorted;
    
    BOOL        m_isSnapPetFound;
}
- (void)startScan;
@property (atomic, strong) IBOutlet UIView* viewScannerBase;
@property (atomic, strong) IBOutlet UIView* viewScanner;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleScanner;
@property (weak, nonatomic) IBOutlet UILabel *snapPetLabel;
@property (atomic, strong) IBOutlet UIButton* btnClose;
@property (atomic, strong) IBOutlet UITableView* tableView;
@property (nonatomic, strong) NSTimer* connectionTimer;
@property (strong, nonatomic) NSMutableArray *deviceList;
@property (weak, nonatomic) IBOutlet UIImageView *conntectingImageView;
@property (weak, nonatomic) IBOutlet UIImageView *viewImgLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelName;

@end