//
//  UserSettingViewController.m
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "AppConfig.h"
#import "ConnectionManager.h"
#import "LocalizationManager.h"
#import "UserPetSettingViewController.h"
#import "ConnectionManager.h"

#define PINENGTH 4

@interface UserPetSettingViewController ()

@end

@implementation UserPetSettingViewController

#pragma mark - View Configure

- (void)viewDidLoad {
    
    [super viewDidLoad];
    robot = [ConnectionManager sharedInstance].snappet;
    robot.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self setPetSettingsMode:self.type];
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.namepintextField.frame.size.height-2, self.namepintextField.frame.size.width, 2)];
    bottomLineView.backgroundColor = [UIColor greenColor];
    [self.namepintextField addSubview:bottomLineView];
    
}

-(void)setPetSettingsMode:(SettingsCellType)celltype{
    
    NSLog(@"celltype %u",celltype);
    
    switch (celltype) {
//        case SettingsCellTypeTutorial:{
//            [self.titleLabel setText:@"General"];
//            [self.checkMarkButton setHidden:YES];
//        }
//        case SettingsCellTypeLanguage:{
//            [self.titleLabel setText:@"General"];
//            [self.checkMarkButton setHidden:YES];
//        }
          //  break;
        case SettingsCellTypeReset:{
            [self.titleLabel setText:@"General"];
            [self.checkMarkButton setHidden:YES];
        }
            break;
        case SettingsCellTypeSnapPetName:{
            [self.titleLabel setText:@"Customization"];
            [self.checkMarkButton setHidden:NO];
            [self.mainImageView setImage:[UIImage imageNamed:@"bg_pet_settings_pet"]];
            [self.mainImageView setHidden:NO];
            [self.namepintextField setHidden:NO];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString * newPetName = [defaults objectForKey:@"LastConnectedPetName"];

            [self.namepintextField setText:newPetName];
            
        }
            break;
        case SettingsCellTypeSnapPetPin:{
            [self.titleLabel setText:@"Customization"];
            [self.checkMarkButton setHidden:NO];
            [self.mainImageView setImage:[UIImage imageNamed:@"bg_pin_settings_lock"]];
            [self.mainImageView setHidden:NO];
            [self.namepintextField setHidden:NO];
            [self.namepintextField setText:nil];
            [self.namepintextField setPlaceholder:@"Ender PIN"];
            [robot snappetSetASCIIPinCodeEnabled:YES];
            [self.namepintextField setKeyboardType:UIKeyboardTypeNumberPad];
        }
            break;
//        case SettingsCellTypeSnapPetColor:{
//            [self.titleLabel setText:@"Customization"];
//            [self.checkMarkButton setHidden:NO];
//        }
//            break;
        case SettingsCellTypeSnapPetVersion:{
            [self.titleLabel setText:@"Update"];
            [self.checkMarkButton setHidden:NO];
            [self.updateLabel setText:[NSString stringWithFormat:@"%@ %@",robot.bleModuleSoftwareVersion, robot.bleSystemId]];
            [self.updateView setHidden:NO];
            [self.updateCancelButton setHidden:NO];
            //[self setUpdateAnimationResource];
            // [self.updateImageView startAnimating];
            [NSTimer scheduledTimerWithTimeInterval:0.02f target:self selector:@selector(animateUpdatePet) userInfo:nil repeats:YES];
            
            
        }
            break;
            
        default:
            break;
    }
    
}
- (void)animateUpdatePet {
    
    
    
    NSInteger animationImageCount = 1;//119;
    NSString * str;
    number ++;
    if(number > animationImageCount){
        number = 0;
        //[timerAnimatingSnappetTutorial invalidate];
        //return;
    }
    
    if(number < 10)
        str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"Importing Animation_0000%d", number] ofType: @"png"];
    if((number > 9)&&(number <100))
    {
        str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"Importing Animation_000%d", number] ofType: @"png"];
    }
    if(number > 99)
        str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"Importing Animation_00%d", number] ofType: @"png"];
    //   dispatch_async(dispatch_get_main_queue(), ^{
    
    UIImage *frameImage = [UIImage imageWithContentsOfFile: str];
    UIGraphicsBeginImageContext(frameImage.size);
    CGRect rect = CGRectMake(0, 0, frameImage.size.width, frameImage.size.height);
    [frameImage drawInRect:rect];
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.updateImageView setImage:renderedImage];
    // });
    
    
}
-(void)setUpdateAnimationResource{
    
    NSMutableArray *menuanimationImages = [[NSMutableArray alloc] init];
    NSInteger animationImageCount = 59;
    for (int aniCount = 0; aniCount < animationImageCount; aniCount++) {
        NSString * str;
        if(aniCount < 10)
            str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"Importing Animation_0000%d", aniCount] ofType: @"png"];
        if((aniCount > 9)&&(aniCount <100))
        {
            str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"Importing Animation_000%d", aniCount] ofType: @"png"];
        }
        if(aniCount > 99)
            str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"Importing Animation_00%d", aniCount] ofType: @"png"];
        
        // here is the code to pre-render the image
        UIImage *frameImage = [UIImage imageWithContentsOfFile: str];
        UIGraphicsBeginImageContext(frameImage.size);
        CGRect rect = CGRectMake(0, 0, frameImage.size.width, frameImage.size.height);
        [frameImage drawInRect:rect];
        UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [menuanimationImages addObject:renderedImage];
    }
    
    self.updateImageView.animationImages = menuanimationImages;
    self.updateImageView.animationDuration = 2.0;
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}
#pragma mark - Buttons Action

-(IBAction)backButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)checkmarkButton:(id)sender{
      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  
    if(self.type == SettingsCellTypeSnapPetPin)
    {

        int value0 = [[NSString stringWithFormat:@"%c", [self.namepintextField.text characterAtIndex:0]] intValue];
        int value1 = [[NSString stringWithFormat:@"%c", [self.namepintextField.text characterAtIndex:1]] intValue];
        int value2 = [[NSString stringWithFormat:@"%c", [self.namepintextField.text characterAtIndex:2]] intValue];
        int value3 = [[NSString stringWithFormat:@"%c", [self.namepintextField.text characterAtIndex:3]] intValue];
        
//        
//        [robot snappetSetASCIIPinCode:value0 digit2:value1 digit3:value2 digit4:value3];
//        
//        [NSThread sleepForTimeInterval:2];
//        [robot snappetSetASCIIPinCodeEnabled:YES];
//        
//        NSString *lastConnectedPetPIN = [defaults objectForKey:@"LastConnectedPetPIN"];
//        [defaults setObject:self.namepintextField.text forKey:@"LastConnectedPetPIN"];

        // [robot snappetAuthenticateWithASCIIPinCode:5 digit2:5 digit3:5 digit4:5   ];
    }
    else
    {
    
        NSString * newPetName = self.namepintextField.text;
        [defaults setObject:newPetName forKey:@"LastConnectedPetName"];
        [robot snappetWriteDisplayName:newPetName];
    }
     [defaults synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityPinEnable:(int)enable{
   if ( enable == 0x00){
    NSLog(@"no pin");
   }
   // self.isEnablePin = self.isAuthen;
    if (enable == 0x01){
        NSLog(@"yes pin");
        
        //        [self performSelector:@selector(authenPIN) withObject:nil];
    }
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityAuthenticateResponse:(int)response{
    if (response == 0){
        NSLog(@"fail");
    }else{
        NSLog(@"succes");
    }
    if (response == 0x00) {
        //authen fail
         NSLog(@"fail");
    }else {
        //authen success
        NSLog(@"succes");
    }
}

#pragma mark - TextField And Keyboard

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(self.type == SettingsCellTypeSnapPetPin)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        if(newLength == 4)
        {
            [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(textfieldEndEdit) userInfo:nil repeats:NO];
            
        }
        return newLength <= PINENGTH || returnKey;
    }
    return YES;
}

-(void)textfieldEndEdit{
    [self.namepintextField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}
-(void)dismissKeyboard {
    [self.namepintextField resignFirstResponder];
}
@end