//
//  GeneralSettingViewController.m
//  SnapPets
//
//  Created by Andrew Medvedev on 14/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "GeneralSettingViewController.h"
#import "SettingsCell.h"
#import "SettingsCellHeader.h" 
#import "ConnectionManager.h"

static NSString *SettingsCellCellIdentifier = @"SettingsCell";
static NSString *SettingsCellHeaderCellIdentifier = @"SettingsCellHeader";

@interface GeneralSettingViewController ()  <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSArray *languagesArray;
@property (weak, nonatomic) IBOutlet UIImageView *resetScreenPetsImageView;

@end

@implementation GeneralSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.languagesArray = @[@"English", @"Japanese", @"Chinese (simplified)", @"Spanish", @"Italian", @"French", @"German", @"Dutch"];
    
    if ( _cellType == SettingsCellTypeReset )
    {
        _resetScreenPetsImageView.hidden = NO;
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate
////---------------------------------------------------------------------
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ( !(_cellType == SettingsCellTypeReset) )
    {
        return 200;
    }
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ( !(_cellType == SettingsCellTypeReset) )
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:(CGRect){0, 0, [UIScreen mainScreen].bounds.size.width, 200}];
        imageView.image = [UIImage imageNamed:@"bg_three_pets.png"];
        
        return imageView;
    }
    
    return nil;
}

//
//---------------------------------------------------------------------
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

//---------------------------------------------------------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.row == 0 )
    {
        return 64.;
    }
    
    if ( indexPath.row == 2 && _cellType == SettingsCellTypeReset )
    {
        return 22.;
    }
    return 44.;
}

#pragma mark - UITableView DataSoruce
//---------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if ( _cellType == SettingsCellTypeLanguage )
//    {
//        return _languagesArray.count + 1;
//    }
//    else
    
    if ( _cellType == SettingsCellTypeReset )
    {
        return 4.;
    }
    
    return 0;
}

//---------------------------------------------------------------------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( indexPath.row == 0 )
    {
        SettingsCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellHeaderCellIdentifier
                                                                   forIndexPath:indexPath];
        
//        [cell.titleLabel setText:_cellType == SettingsCellTypeLanguage ? @"App Language" : _cellType == SettingsCellTypeReset ? @"Reset" : @""];
        [cell.titleLabel setText:_cellType == SettingsCellTypeReset ? @"Reset" : @""];
        return cell;
    }
    else
    {
        if ( _cellType == SettingsCellTypeReset )
        {
            if ( indexPath.row == 2 )
            {
                UITableViewCell *cell = [[UITableViewCell alloc] init];
                cell.userInteractionEnabled = NO;
                return cell;
            }
            else
            {
                SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
                                                                     forIndexPath:indexPath];
                [cell setBorderedTypeCell];
                cell.titleLabel.text = indexPath.row == 1 ? @"Reset all settings" : indexPath.row == 3 ? @"Reset Snap Pet" : @"";
                
                return cell;
            }
        }
//        else if ( _cellType == SettingsCellTypeLanguage )
//        {
//            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
//                                                                 forIndexPath:indexPath];
//            if ( indexPath.row == 1 )
//            {
//                [cell setFirstTypeCell];
//                [cell.titleLabel setText:[_languagesArray firstObject]];
//            }
//            else if ( indexPath.row == _languagesArray.count )
//            {
//                [cell setLastTypeCell];
//                [cell.titleLabel setText:[_languagesArray lastObject]];
//            }
//            else
//            {
//                [cell setMiddleTypeCell];
//                [cell.titleLabel setText:_languagesArray[indexPath.row - 1]];
//            }
//            
//            return cell;
//        }
    }
    
    return nil;
}

#pragma mark DidSelectRow
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( _cellType == SettingsCellTypeReset )
    {
        NSLog(@"VOT 4TO TI NAJAL: %@", ((SettingsCell *)[tableView cellForRowAtIndexPath:indexPath]).titleLabel.text);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset" message:@"Please press the button on pet top and OK at the same time." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    else
    {
        _generalSettingsHandler(((SettingsCell *)[tableView cellForRowAtIndexPath:indexPath]).titleLabel.text);
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
             [[ConnectionManager sharedInstance].snappet snappetResetASCIIPinCodeAndEraseFlash];
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
            
        default:
            break;
    }
}
@end
