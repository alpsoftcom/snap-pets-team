//
//  PhotoPreviewViewController.h
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "StickersView.h"

@interface PhotoPreviewViewController : BaseViewController <UIScrollViewDelegate> {
    int orginalBottomActionBarY;
}

- (IBAction)cancelEditAction:(id)sender;
- (IBAction)doneEditAction:(id)sender;
+ (UIImage*)saveSnappetOutputImageWithSource:(UIImage*)img;
@property (nonatomic, strong) IBOutlet UIImageView *imgBG;

@property (nonatomic, strong) IBOutlet UIImageView *previewImage;
@property (nonatomic, strong) IBOutlet UIButton *shareBtn;
@property (nonatomic, strong) IBOutlet UIButton *stickersBtn;
@property (nonatomic, strong) IBOutlet UIButton *textBtn;
@property (nonatomic, strong) IBOutlet UIButton *filterBtn;
@property (nonatomic, strong) IBOutlet UIButton *editBtn;


@property (nonatomic, strong) UIImage* selectedImg;
@property (nonatomic) long selectedPhotoIndex;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollImgView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *bottomActionBar;

@property (nonatomic) BOOL isPreviewAll;

@property (nonatomic) BOOL isFromCaptureMode;

@property (nonatomic, strong) NSString* selectedPhotoPath;

@property (nonatomic, strong) IBOutlet UIView* headerView;

@property (nonatomic, strong) IBOutlet UIView* deleteView;
@property (nonatomic, strong) IBOutlet UIView* deleteContentView;
@property (nonatomic, strong) IBOutlet UIButton* deleteConfirmBtn;
@property (nonatomic, strong) IBOutlet UIButton* cancelBtn;
@property (nonatomic, strong) IBOutlet UIButton* deleteBtn;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintImageY;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintDeleteView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintHeightScrollView;

@property (nonatomic, strong) IBOutlet UIButton* cancelEditBtn;
@property (nonatomic, strong) IBOutlet UIButton* doneEditBtn;
@property (weak, nonatomic) IBOutlet UILabel *deleteContent;

@end
