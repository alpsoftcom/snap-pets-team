//
//  PhotoPreviewViewController.m
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "PhotoPreviewViewController.h"
#import "TakePhotoViewController.h"
#import "PhotoLibraryManager.h"
#import "Flurry.h"
#import "LocalizationManager.h"

//#define SCREEN_OFFSET 50
#define SCREEN_OFFSET 0


@interface PhotoPreviewViewController ()
{
    // stickers view
    StickersView* stickersView;
    
    UIView* magnetView;
    
    CGRect photoFrame;
    
    UIImageView *photoImgView;
    
    BOOL isSnappetSizePhoto;
    
    int photoOffset;
    
    int scrollCurrentPage;
    
    int initSubviewSize;
}

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *canceButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *cancelView;

@end

@implementation PhotoPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainImageView.image = _selectedImg;
    
    CGRect rect = self.bottomActionBar.frame;
    orginalBottomActionBarY = rect.origin.y;
    
//    self.previewImage.image = [UIImage imageWithContentsOfFile:self.selectedPhotoPath];
//    self.previewImage.image = self.selectedImg;
    self.scrollImgView.delegate = self;
    
//    self.previewImage.image = [[PhotoLibraryManager sharedInstance].imageList objectAtIndex:self.selectedPhotoIndex];
    self.scrollImgView.pagingEnabled = YES;
    
    [[PhotoLibraryManager sharedInstance] loadPhoto];
    
    [self.deleteContent setText:[[LocalizationManager getInstance] getStringWithKey:@"BtnDeleteContent"]];
    [self.cancelBtn setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnCancel"] forState:UIControlStateNormal];
   
//}

//- (void)viewDidLayoutSubviews
//
//{
    self.isFromCaptureMode = YES;
    self.constraintDeleteView.constant = -200.0f;
    self.deleteContentView.layer.cornerRadius = 5.0f;
    self.cancelBtn.layer.cornerRadius = 5.0f;
    
    int imgHeight = 0;
    int imgWidth = 0;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];

    if (self.isPreviewAll){
        initSubviewSize = [[self.scrollImgView subviews] count];
       
        for (int i=0;i<[[self.scrollImgView subviews] count]; i++) {
            UIView* view = [[self.scrollImgView subviews] objectAtIndex:i];
            [view removeFromSuperview];
        }
        [self.scrollImgView addSubview:[UIImageView new]];
        for (int i = 0; i < [[PhotoLibraryManager sharedInstance].imageList count]; i++) {
            UIImage *img = [[PhotoLibraryManager sharedInstance].imageList objectAtIndex:i];
            
            UIImage *smallImage = [self resizeImg:img];
            
            UIImageView *imgView = [[UIImageView alloc] initWithImage:smallImage];
            
            photoFrame = imgView.frame;
            CGRect arect = self.scrollImgView.frame;
            CGRect viewFrame = imgView.frame;
            viewFrame.origin.x = viewFrame.size.width * i;
            viewFrame.origin.y = (self.scrollImgView.frame.size.height-viewFrame.size.height)/2;
            imgView.frame = viewFrame;
            [self.scrollImgView addSubview:imgView];
            imgHeight = smallImage.size.height;
            imgWidth = smallImage.size.width;
        }
        
        self.scrollImgView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*[[PhotoLibraryManager sharedInstance].imageList count], imgHeight);
        
        [self.scrollImgView setContentOffset:CGPointMake(imgWidth * self.selectedPhotoIndex, 0)];
        
        UIImage *smallImage = [self resizeImg:self.selectedImg];
        if (smallImage.size.height < smallImage.size.width){
            //is snappet image
            
            CGRect previewImgRect = photoImgView.frame;
            previewImgRect.origin.y -= (self.scrollImgView.frame.size.height - smallImage.size.height)/2;
            previewImgRect.size.height = smallImage.size.height;
            photoImgView.frame = previewImgRect;
            if (screenBounds.size.height == 1024)
                self.constraintImageY.constant = 0;
            else if (screenBounds.size.height == 480)
                self.constraintImageY.constant = 0;
        }
        
        if (screenBounds.size.height == 1024 || screenBounds.size.height == 2048) {
            photoOffset = 0;
        }
        else if (screenBounds.size.height == 568)
        {
            photoOffset = 0;
        }else{
            if (smallImage.size.height < smallImage.size.width){
                //is snappet image
                photoOffset = 0;
                
            }else{
                photoOffset = SCREEN_OFFSET;
            }
            
//            CGRect photoRect = self.scrollImgView.frame;
//            photoRect.origin.y -= photoOffset;
//            self.scrollImgView.frame = photoRect;
            
//            CGRect rect = self.bottomActionBar.frame;
//            rect.origin.y = orginalBottomActionBarY-(568-480);
//            self.bottomActionBar.frame = rect;
        }
        
        self.bottomActionBar.hidden = NO;
        self.stickersBtn.enabled = YES;
        self.shareBtn.enabled = YES;
        self.saveButton.hidden = YES;
        self.editBtn.hidden = NO;
        
        [self.deleteBtn setHidden:NO];
        [self.shareBtn setHidden:NO];
        [self.cancelEditBtn setHidden:YES];
        [self.doneEditBtn setHidden:YES];
        
        scrollCurrentPage = (int)self.selectedPhotoIndex;
        
    }else{
        if (photoImgView == nil){
            if (self.selectedImg){
                CGRect screenBounds = [[UIScreen mainScreen] bounds];
                if (screenBounds.size.height == 1024 || screenBounds.size.height == 2048) {
                    self.scrollImgView.frame = CGRectMake(0, (1024-768)/2, 768, 768);
                    self.constraintImageY.constant = -40.0f;
                }
                else if (screenBounds.size.height == 480) {
                    self.scrollImgView.frame = CGRectMake(0, (480-320)/2, 320, 320);
                    self.constraintImageY.constant = -20.0f;
                }
                else if (screenBounds.size.height == 667) {
//                    self.constraintHeightScrollView.constant = 375.0f;
                    self.scrollImgView.frame = CGRectMake(0, (667-375)/2, 375, 375);
                }
                else if (screenBounds.size.height == 736) {
//                    self.constraintHeightScrollView.constant = 414.0f;
                    self.scrollImgView.frame = CGRectMake(0, (736-414)/2, 414, 414);
                }
                else {
                    self.scrollImgView.frame = CGRectMake(0, (568-320)/2, 320, 320);
                }
                
                self.isFromCaptureMode = YES;
                UIImage *smallImage = [self resizeImg:self.selectedImg];
                
                
                photoImgView = [[UIImageView alloc] initWithImage:smallImage];
                
                photoFrame = photoImgView.frame;
                
                if (smallImage.size.height < smallImage.size.width){
                    //is snappet image
                    
                    CGRect previewImgRect = photoImgView.frame;
                    previewImgRect.origin.y = (self.scrollImgView.frame.size.height - smallImage.size.height)/2;
                    previewImgRect.size.height = smallImage.size.height;
                    photoImgView.frame = previewImgRect;
                    if (screenBounds.size.height == 1024)
                        self.constraintImageY.constant = 0;
                    else if (screenBounds.size.height == 480)
                        self.constraintImageY.constant = 0;
                }
                
                if (screenBounds.size.height == 1024 || screenBounds.size.height == 2048) {
                    photoOffset = 0;
                }
                else if (screenBounds.size.height == 568)
                {
                    photoOffset = 0;
                }else{
                    if (smallImage.size.height < smallImage.size.width){
                        //is snappet image
                        photoOffset = 0;
                        
                    }else{
                        photoOffset = SCREEN_OFFSET;
                    }
                    
//                    CGRect photoRect = self.scrollImgView.frame;
//                    photoRect.origin.y -= photoOffset;
//                    self.scrollImgView.frame = photoRect;
                    
//                    CGRect rect = self.bottomActionBar.frame;
//                    rect.origin.y = orginalBottomActionBarY-(568-480);
//                    self.bottomActionBar.frame = rect;
                }
                
                
                
                [self.scrollImgView addSubview:photoImgView];
                [self.cancelEditBtn setHidden:YES];
            }
            else{
                UIImage *smallImage = [self resizeImg:[[PhotoLibraryManager sharedInstance].imageList objectAtIndex:self.selectedPhotoIndex]];
                photoImgView = [[UIImageView alloc] initWithImage:smallImage];
                [self.scrollImgView addSubview:photoImgView];
                
                CGRect screenBounds = [[UIScreen mainScreen] bounds];
                if (screenBounds.size.height == 1024 || screenBounds.size.height == 2048) {
                    photoOffset = 0;
                }
                else if (screenBounds.size.height == 568)
                {
                    photoOffset = 0;
                }else{
                    if (self.previewImage.image.size.height < self.previewImage.image.size.width){
                        //is snappet image
                        photoOffset = 0;
                    }else{
                        photoOffset = SCREEN_OFFSET;
                    }
                    
//                    CGRect photoRect = self.scrollImgView.frame;
//                    photoRect.origin.y -= SCREEN_OFFSET;
//                    self.scrollImgView.frame = photoRect;
                    
//                    CGRect rect = self.bottomActionBar.frame;
//                    rect.origin.y = orginalBottomActionBarY-(568-480);
//                    self.bottomActionBar.frame = rect;
                    [self.cancelEditBtn setHidden:NO];
                }
            }
            
        }
        
        self.bottomActionBar.hidden = NO;
        [self.stickersBtn setHidden:NO];
        self.shareBtn.enabled = YES;
        self.saveButton.hidden = YES;
        self.editBtn.hidden = YES;

//        [self.deleteBtn setHidden:YES];
        [self.cancelEditBtn setHidden:YES];
        [self.shareBtn setHidden:NO];
        [self.doneEditBtn setHidden:NO];
    }
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (UIImage*)resizeImg:(UIImage*) img
{
    UIImage *resizeImage = img;
    
    float width = [UIScreen mainScreen].bounds.size.width;
    float height = width;
    
    float widthRatio = width / resizeImage.size.width;
    float heightRatio = height / resizeImage.size.height;
    //    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
    
    width = resizeImage.size.width * widthRatio;
    height = resizeImage.size.height * widthRatio;
    
    CGSize newSize = CGSizeMake(width,height);
    UIGraphicsBeginImageContextWithOptions(newSize,NO,0.0);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    rect.size.width  = width;
    rect.size.height = height;
    
    //indent in case of width or height difference
    //    float offset = (width - height) / 2;
    //    if (offset > 0) {
    //        rect.origin.y = offset;
    //    }
    //    else {
    //        rect.origin.x = -offset;
    //    }
    
    [resizeImage drawInRect: rect];
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return smallImage;
}

-(void)scrollViewDidScroll:(UIScrollView *)sv
{
    
   
}

- (IBAction)saveAction:(id)sender{
    UIImage *img = [self saveOutputImage];
//    [[PhotoLibraryManager sharedInstance] saveImage:img];
    
    //save to camera roll with Snappets album.
    [[PhotoLibraryManager sharedInstance] saveToCameraRoll:img];
}
- (IBAction)cancelButtonPressed:(id)sender
{
    self.stickersBtn.selected = NO;
    [stickersView animateHideBottomBar];
    self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant - _bottomActionBar.frame.size.height;
    __weak typeof (self) wself = self;
    [UIView animateWithDuration:0.5 animations:^{
        wself.bottomView.backgroundColor = wself.cancelView.backgroundColor;
        wself.cancelView.hidden = YES;
        [wself.view layoutIfNeeded];
    }];
}

- (IBAction)stickersAction:(id)sender {
    // magnet view
    
    if ( !_stickersBtn.isSelected )
    {
        self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant + _bottomActionBar.frame.size.height;
        __weak typeof (self) wself = self;
        [UIView animateWithDuration:0.5 animations:^{
            wself.bottomView.backgroundColor = [UIColor colorWithRed:43/255. green:211/255. blue:198/255. alpha:1.];
            wself.cancelView.hidden = NO;
            [wself.view layoutIfNeeded];
        }];
        if (!stickersView.userInteractionEnabled){
            magnetView = [[UIView alloc] initWithFrame:photoFrame];
            magnetView.backgroundColor = [UIColor clearColor];
            magnetView.userInteractionEnabled = YES;
            magnetView.clipsToBounds = YES;
            [self.view addSubview:magnetView];
            
            stickersView = [[StickersView alloc] initStickersView:self.view];
            [self.view addSubview:stickersView];
            stickersView.hidden = false;
            stickersView.targetView = magnetView;
            stickersView.userInteractionEnabled = YES;
            [stickersView animateBottomBar];
            [self.view bringSubviewToFront:stickersView.bottomScrollView];
            
            [self.view bringSubviewToFront:self.saveButton];
            [self.view bringSubviewToFront:self.headerView];
            [self.view bringSubviewToFront:self.bottomActionBar];
            [self.view bringSubviewToFront:self.cancelView];
            //        [self.stickersBtn setHidden:YES];
            //        [self.cancelEditBtn setHidden:NO];
            //        [self.shareBtn setHidden:YES];
            //        [self.doneEditBtn setHidden:YES];
        }
        else {
            [stickersView animateBottomBar];
            //        [self.cancelEditBtn setHidden:NO];
            //        [self.stickersBtn setHidden:YES];
            //        [self.shareBtn setHidden:YES];
            //        [self.doneEditBtn setHidden:YES];
            
        }
        
        self.stickersBtn.selected = YES;
    }
//    else {
//        [self saveAction:nil];
//    }
}

- (IBAction)shareAction:(id)sender {
    NSMutableArray* objectsToShare = [NSMutableArray new];
    BOOL isPhotoImgViewEmpty = NO;
    if (photoImgView == nil) {
//        int imgIndex = scrollCurrentPage;
//        NSString* path = [[PhotoLibraryManager sharedInstance].imagenameList objectAtIndex:imgIndex];
//        NSURL *url = [NSURL fileURLWithPath:path];
//        [objectsToShare addObject:url];
        
        int imgIndex = initSubviewSize+scrollCurrentPage-1;
        photoImgView = (UIImageView*)[[self.scrollImgView subviews] objectAtIndex:imgIndex];
        isPhotoImgViewEmpty = YES;
    }
    UIImage *img = [self saveOutputImage];
    NSString *pth = [[PhotoLibraryManager sharedInstance] saveImageToTmp:img];
    
    NSURL *url = [NSURL fileURLWithPath:pth];
    [objectsToShare addObject:url];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    
    if (isPhotoImgViewEmpty) {
        photoImgView = nil;
    }
}

- (IBAction)deleteAction:(id)sender {
    [self.view bringSubviewToFront:self.deleteView];
    self.constraintDeleteView.constant = -200.0f;
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.constraintDeleteView.constant = 0.0f;
                         [self.deleteView layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         self.constraintDeleteView.constant = 0.0f;
                     }
     ];
}

- (IBAction)deleteConfirmAction:(id)sender {
    [[PhotoLibraryManager sharedInstance] deletePhotoWithIndex:self.selectedPhotoIndex];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)cancelAction:(id)sender {
    self.constraintDeleteView.constant = 0.0f;
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.constraintDeleteView.constant = -200.0f;
                         [self.deleteView layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         self.constraintDeleteView.constant = -200.0f;
                     }
     ];
}

- (IBAction)cancelEditAction:(id)sender {
    if (stickersView.userInteractionEnabled && ![stickersView isHidden]){
        if (![stickersView isHidden])
            [stickersView hideBottomBar];
        if (self.isFromCaptureMode) {
            [self.cancelEditBtn setHidden:YES];
            [self.shareBtn setHidden:NO];
            [self.doneEditBtn setHidden:NO];
        }
        [self.stickersBtn setHidden:NO];
    }
    else {
        if (!self.isFromCaptureMode)
            [self.cancelEditBtn setHidden:YES];
        
        for (int i=0; i<[stickersView.magnetItemList count]; i++) {
            UIView* aView = [stickersView.magnetItemList objectAtIndex:i];
            [aView removeFromSuperview];
        }
        [stickersView.magnetItemList removeAllObjects];
        
        [self.stickersBtn setHidden:YES];
        self.shareBtn.enabled = YES;
        //        self.saveButton.hidden = NO;
        self.editBtn.hidden = NO;
        
        [self.deleteBtn setHidden:NO];
        [self.shareBtn setHidden:NO];
        if (self.isFromCaptureMode)
            [self.cancelEditBtn setHidden:YES];
        [self.doneEditBtn setHidden:YES];
        if (![stickersView isHidden])
            [stickersView hideBottomBar];
        
        int imgHeight = 0;
        int imgWidth = 0;
        for (int i=0;i<[[self.scrollImgView subviews] count]; i++) {
            UIView* view = [[self.scrollImgView subviews] objectAtIndex:i];
            [view removeFromSuperview];
        }
        [self.scrollImgView addSubview:[UIImageView new]];
    //    initSubviewSize = [[self.scrollImgView subviews] count];
        for (int i = 0; i < [[PhotoLibraryManager sharedInstance].imageList count]; i++) {
            UIImage *img = [[PhotoLibraryManager sharedInstance].imageList objectAtIndex:i];
            
            UIImage *smallImage = [self resizeImg:img];
            
            UIImageView *imgView = [[UIImageView alloc] initWithImage:smallImage];
            
            photoFrame = imgView.frame;
            
            CGRect viewFrame = imgView.frame;
            viewFrame.origin.x = viewFrame.size.width * i;
            viewFrame.origin.y = (self.scrollImgView.frame.size.height-viewFrame.size.height)/2;
            
            imgView.frame = viewFrame;
            
            
            [self.scrollImgView addSubview:imgView];
            
            imgHeight = smallImage.size.height;
            imgWidth = smallImage.size.width;
        }
        
        self.scrollImgView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*[[PhotoLibraryManager sharedInstance].imageList count], imgHeight);
        
        [self.scrollImgView setContentOffset:CGPointMake(imgWidth * self.selectedPhotoIndex, 0)];
        scrollCurrentPage = (int)self.selectedPhotoIndex;
        self.scrollImgView.scrollEnabled = YES;
    }
}

- (IBAction)doneEditAction:(id)sender {
    [self saveAction:sender];
//    [self cancelEditAction:sender];
}

- (IBAction)editAction:(id)sender {

    int imgIndex = initSubviewSize+scrollCurrentPage-1;
    if (imgIndex < 0)
        imgIndex = 1;
    photoImgView = (UIImageView*)[[self.scrollImgView subviews] objectAtIndex:imgIndex];
    CGRect rect1 = photoImgView.frame;
    photoImgView.frame = rect1;
    self.selectedImg = photoImgView.image;
    

   
    CGRect rect = photoImgView.frame;
    UIImageView *imgView = [[UIImageView alloc] initWithImage:((UIImageView*)[[self.scrollImgView subviews] objectAtIndex:imgIndex]).image];
    imgView.frame = rect;
    
    for (int i = 0/*initSubviewSize-1*/; i < [[self.scrollImgView subviews] count]; i++) {
        [[[self.scrollImgView subviews] objectAtIndex:i] removeFromSuperview];
        --i;
    }
    

//    self.scrollImgView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, imgView.frame.size.height);
    [self.scrollImgView setContentOffset:CGPointMake((imgIndex-1)*[UIScreen mainScreen].bounds.size.width, 0)];
    [self.scrollImgView addSubview:imgView];
    self.scrollImgView.scrollEnabled = NO;
    
    if (self.selectedImg){
        
        photoFrame = photoImgView.frame;
        
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (self.selectedImg.size.height < self.selectedImg.size.width){
            //is snappet image
            
            CGRect previewImgRect = photoImgView.frame;
            previewImgRect.origin.y = (self.scrollImgView.frame.size.height - self.selectedImg.size.height)/2;
            //                    self.previewImage.frame = previewImgRect;
        }
        
        if (screenBounds.size.height == 1024 || screenBounds.size.height == 2048) {
            photoOffset = 0;
        }
        else if (screenBounds.size.height == 568)
        {
            photoOffset = 0;
        }else{
            if (self.selectedImg.size.height < self.selectedImg.size.width){
                //is snappet image
                photoOffset = 0;
                
            }else{
                photoOffset = SCREEN_OFFSET;
            }
            
//            CGRect photoRect = self.scrollImgView.frame;
//            photoRect.origin.y -= photoOffset;
//            self.scrollImgView.frame = photoRect;
            
//            CGRect rect = self.bottomActionBar.frame;
//            rect.origin.y = orginalBottomActionBarY-(568-480);
//            self.bottomActionBar.frame = rect;
        }
        
        self.bottomActionBar.hidden = NO;
        [self.stickersBtn setHidden:NO];
        self.shareBtn.enabled = YES;
//        self.saveButton.hidden = NO;
        self.editBtn.hidden = YES;
        
//        [self.deleteBtn setHidden:YES];
        [self.shareBtn setHidden:YES];
        [self.cancelEditBtn setHidden:NO];
        [self.doneEditBtn setHidden:NO];
    }

}

#pragma mark - output image
- (UIImage*)saveOutputImage
{
    UIImage* outImage = nil;
    //    CGSize imgSize = originalImage.size;
    CGSize imgSize = CGSizeMake(photoImgView.image.size.width, photoImgView.image.size.height);
    UIImage* photo = photoImgView.image;
//    UIImage* watermark = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"watermark.png" ofType:nil]];
    UIImage* watermark = [UIImage imageNamed:@"water_mark.png"];
    UIGraphicsBeginImageContextWithOptions(imgSize, NO, 0.0);
    float photoScale = imgSize.width / photoImgView.frame.size.width;
    
    // draw all elements
    [photo drawInRect:CGRectMake(0, 0, imgSize.width, imgSize.height)];
    if (stickersView.magnetItemList.count > 0)
        [Flurry logEvent:[NSString stringWithFormat:@"Save output image with sticker: %d", stickersView.magnetItemList.count]];
    else
        [Flurry logEvent:[NSString stringWithFormat:@"Save output image with sticker: %d", stickersView.magnetItemList.count]];
    for(MagnetItemView* v in stickersView.magnetItemList)
    {
        [v setEditMode:NO];
        
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        
        CGContextConcatCTM(ctx, CGAffineTransformMakeScale(photoScale, photoScale));
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        CGRect rectScroll = self.scrollImgView.frame;
        int y = [v position].y - rectScroll.origin.y-(rectScroll.size.height-imgSize.height)/2;
        int y1 = [v position].y - rectScroll.origin.y+(rectScroll.size.height-imgSize.height)/2;
        if (screenBounds.size.height == 1024) {
//            if (imgSize.height > rectScroll.size.height)
//                CGContextConcatCTM(ctx, CGAffineTransformMakeTranslation([v position].x, ([v position].y - rectScroll.origin.y)*(imgSize.height/rectScroll.size.height)));
//            else
                CGContextConcatCTM(ctx, CGAffineTransformMakeTranslation([v position].x, [v position].y - rectScroll.origin.y-(rectScroll.size.height-imgSize.height)/2));
        }
        else if (screenBounds.size.height == 568)
        {
            CGContextConcatCTM(ctx, CGAffineTransformMakeTranslation([v position].x, [v position].y - self.scrollImgView.frame.origin.y-(self.scrollImgView.frame.size.height-imgSize.height)/2));//self.scrollImgView.frame.origin.y));
        }
        else
        {
            CGContextConcatCTM(ctx, CGAffineTransformMakeTranslation([v position].x, [v position].y - self.scrollImgView.frame.origin.y-(self.scrollImgView.frame.size.height-imgSize.height)/2));
        }
        
        [v.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        CGContextRestoreGState(ctx);
    }
//    float watermarkHeight = (imgSize.width / watermark.size.width) * watermark.size.height;
//    [watermark drawInRect:CGRectMake(0, imgSize.height - watermarkHeight, imgSize.width, watermarkHeight)];
    
    
    outImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outImage;
}

#pragma mark - output image
+ (UIImage*)saveSnappetOutputImageWithSource:(UIImage*)img
{
    UIImage* outImage = nil;
    //    CGSize imgSize = originalImage.size;
    CGSize imgSize = CGSizeMake(img.size.width, img.size.height);
    UIImage* photo = img;
    //    UIImage* watermark = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"watermark.png" ofType:nil]];
    UIImage* watermark = [UIImage imageNamed:@"water_mark.png"];
    UIGraphicsBeginImageContextWithOptions(imgSize, NO, 0.0);
    float photoScale = imgSize.width / img.size.width;
    
    // draw all elements
    [photo drawInRect:CGRectMake(0, 0, imgSize.width, imgSize.height)];
   
    float watermarkHeight = (imgSize.width / watermark.size.width) * watermark.size.height;
    [watermark drawInRect:CGRectMake(0, imgSize.height - watermarkHeight, imgSize.width, watermarkHeight)];
    
    
    outImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    scrollCurrentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    self.selectedPhotoIndex = scrollCurrentPage;
    //your stuff with index
}

@end
