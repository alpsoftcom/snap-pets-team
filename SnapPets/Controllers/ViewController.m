//
//  ViewController.m
//  MipPhotoTest
//
//  Created by Forrest Chan on 25/4/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) NSTimer *checkPhotoTimer;
@property (assign) long lastDataReceiveTime;
@property (strong, nonatomic) MipRobot *selectedMip;
@property (assign) int shutterCountDown;
@property (strong, nonatomic) NSTimer *countdownTimer;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.mipImage setContentMode:UIViewContentModeScaleAspectFit];
    
    // Register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMipRobotFinderNotification:) name:MipRobotFinderNotificationID object:nil];
    [[MipRobotFinder sharedInstance] cbCentralManagerState];
    ((MipRobotFinder *)[MipRobotFinder sharedInstance]).scanOptionsFlagMask = MRFScanOptionMask_ShowAllDevices;

    // Load sfx
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"shutter.wav" ofType:nil]] error:nil];
    [self.audioPlayer prepareToPlay];
    self.audioPlayer.volume = 1.0f;
    
    // Scan for mips
    [[MipRobotFinder sharedInstance] clearFoundMipList];
    self.mips = [[MipRobotFinder sharedInstance] mipsFound];
    [[MipRobotFinder sharedInstance] scanForMips];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showLoading {
    self.progressView.progress = 0;
    [self.progressView setHidden:true];
    [self.loadingLabel setHidden:true];
    
    self.shutterCountDown = 2;
    [self.countdownLabel setHidden:false];
    [self.countdownLabel setText:@""];
    
//    self.countdownTimer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(countdownTimerAction) userInfo:nil repeats:YES];
    self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(countdownTimerAction) userInfo:nil repeats:YES];
}

- (void)hideLoading {
//    [self.alert dismissWithClickedButtonIndex:0 animated:YES];
    [self.progressView setHidden:true];
    [self.loadingLabel setHidden:true];
    [self.countdownLabel setHidden:true];
}

- (void)refreshCameraButton {
    if([self.mips count] == 1) {
        [self enableCamera];
    }
    else {
        [self disableCamera];
    }
}

- (void)enableCamera {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.50];
    [self.cameraButton setImage:[UIImage imageNamed:@"camera.png"] forState:UIControlStateNormal];
    [self.cameraButton setEnabled:true];
    [self.readyLabel setHidden:false];
    [UIView commitAnimations];
}

- (void)disableCamera {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.50];
    [self.cameraButton setImage:[UIImage imageNamed:@"camera_bw.png"] forState:UIControlStateNormal];
    [self.cameraButton setEnabled:false];
    [self.readyLabel setHidden:true];
    [UIView commitAnimations];
}

#pragma mark - Timer action
- (void)countdownTimerAction {
    self.shutterCountDown--;
    
    NSLog(@"countdownTimerAction process %d", self.shutterCountDown);
    
    if(self.shutterCountDown < 0) {
        // stop countdown
        [self.countdownTimer invalidate];
        self.countdownTimer = nil;
        [self.loadingLabel setText:@"Please wait"];
        [self.progressView setHidden:false];
        [self.loadingLabel setHidden:false];
        [self.countdownLabel setHidden:true];
    }
    else if(self.shutterCountDown == 0) {
        [self.countdownLabel setText:@"Smile"];
        [self.countdownLabel setHidden:false];
        self.audioPlayer.currentTime = 0;
        [self.audioPlayer play];
    }
    else if(self.shutterCountDown == 1) {
        [self.countdownLabel setText:@"Ready"];
        [self.countdownLabel setHidden:false];
    }
    else {
        [self.countdownLabel setText:[NSString stringWithFormat:@"%d", self.shutterCountDown]];
        [self.countdownLabel setHidden:false];
    }
}

#pragma mark - MIP Delegate Callbacks
-(void) MipDeviceConnected:(MipRobot *)mip {
    NSLog(@"MipDeviceConnected show loading");
    // disable auto reconnect
    mip.autoReconnect = false;
    
    [self showLoading];
}

- (void)onMipRobotFinderNotification: (NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    
    if(info){
        NSNumber *code = [info objectForKey: @"code"];
        if (code.intValue == MipRobotFinder_MipFound){
            [self.mipTableView reloadData];
            [self refreshCameraButton];
        } else if (code.intValue == MipRobotFinder_MipListCleared) {
            self.mips = [[MipRobotFinder sharedInstance] mipsFound];
            [self.mipTableView reloadData];
            [self refreshCameraButton];
        } else if (code.intValue == MipRobotFinder_BluetoothError) {
            NSLog(@"Bluetooth error!");
        } else if (code.intValue == MipRobotFinder_BluetoothIsOff) {
            NSLog(@"Bluetooth is off!");
        }
    }
}

#pragma mark - Table Callbacks
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedMip = [self.mips objectAtIndex:indexPath.row];
    
    // stop scan
    [[MipRobotFinder sharedInstance] stopScanForMips];
    
    // loading popup
//    [self showLoading];
    
    // connect and fetch photo
    [self.selectedMip setDelegate:self];
    [self.selectedMip connect];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.mips count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MipCell";
    static int labelTag = 100;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"MipCell"];
    }
    cell.backgroundColor = [UIColor clearColor];
    
    MipRobot *mip = (MipRobot *)[self.mips objectAtIndex:indexPath.row];
    UILabel *mipLabel = (UILabel *)[cell viewWithTag: labelTag];
    if(!mipLabel) {
        mipLabel = [[UILabel alloc] initWithFrame: CGRectMake(20, 20, cell.frame.size.width - 20, cell.frame.size.height)];
        mipLabel.tag = labelTag;
        [mipLabel setFont: [UIFont systemFontOfSize:14]];
        [cell addSubview: mipLabel];
    }
    
    if(mip.name){
        [mipLabel setText: mip.name];
    } else {
        [mipLabel setText: @"Unknown"];
    }
    
    [cell setSelectionStyle: UITableViewCellSelectionStyleBlue];
    [cell setHidden: false];
    
    return cell;
}

#pragma mark - button actions
-(IBAction)imageAction:(id)sender {
    
    // hide loading
    [self hideLoading];
    
    // process image data
    if(self.imageData) {
        
        if([self isJPEGValid:self.imageData]) {
            NSLog(@"is Valid JPEG!");
        }
        else {
            NSLog(@"invalid JPEG!");
        }
        
        UIImage* image = [UIImage imageWithData:self.imageData];
        if(image) {
            NSLog(@"Get image success!");
            [self.mipImage setImage:image];
        }
        else {
            NSLog(@"image is null");
        }
        
        self.imageData = nil;
    }
    
    // disconnect
    if(self.selectedMip != nil) {
        [self.selectedMip disconnect];
        self.selectedMip = nil;
    }
    
    [self refreshCameraButton];
    
}

-(IBAction)refreshAction:(id)sender {
    if(self.selectedMip != nil) {
        // disconnect current mip
        [self.selectedMip disconnect];
        self.selectedMip = nil;
    }
    [self.loadingLabel setHidden:true];
    
    // clear imge
    [self.mipImage setImage:nil];
    self.imageData = nil;
    
    // reset progress
    self.progressView.progress = 0;
    [self.progressView setHidden:true];
    
    // disable camera
    [self disableCamera];
    
    // rescan
    [[MipRobotFinder sharedInstance] stopScanForMips];
    [[MipRobotFinder sharedInstance] clearFoundMipList];
    self.mips = [[MipRobotFinder sharedInstance] mipsFound];
    [[MipRobotFinder sharedInstance] scanForMips];
    [self.mipTableView reloadData];
}

- (IBAction)cameraAction:(id)sender {
    if([self.mips count] == 1) {
        // clear previous image
        [self.mipImage setImage:nil];
        
        self.selectedMip = [self.mips objectAtIndex:0];
        
        // stop scan
        [[MipRobotFinder sharedInstance] stopScanForMips];
        
        // loading popup
//        [self showLoading];
        [self.loadingLabel setText:@"Connecting..."];
        [self.loadingLabel setHidden:false];
        
        // connect and fetch photo
        [self.selectedMip setDelegate:self];
        [self.selectedMip connect];
        
        self.cameraButton.enabled = false;
    }
    else {
        [self disableCamera];
    }
}

#pragma mark - mip
-(void) MipRobotBluetooth:(MipRobot *)mip didReceiveRawCommandData:(NSData *)data {
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    NSUInteger          dataLength  = [data length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
//    NSLog(@"didReceiveRawCommandData %@", hexString);
    
    self.lastDataReceiveTime = CFAbsoluteTimeGetCurrent();
    
    // first data packet
    if(self.imageData == nil) {
        self.imageData = [NSMutableData new];
        self.checkPhotoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(checkImageReceiveFinished) userInfo:nil repeats:YES];
    }
    
    // sometimes receive duplicated data? should be a problem with our iOS bluetooth robot library
    if(self.previousData == nil || ![[self.previousData description] isEqualToString:[data description]]) {
        self.previousData = [NSData dataWithData:data];
        [self.imageData appendData:data];
        
        float progress = [self.imageData length] / 12000.0f;
        if(progress > 1) {
            progress = 1;
        }
        self.progressView.progress = progress;
//        NSLog(@"self.imageData size = %lu", (unsigned long)[self.imageData length]);
    }
}

- (BOOL)isJPEGValid:(NSData *)jpeg {
    if ([jpeg length] < 4) {
        return NO;
    }
    const unsigned char * bytes = (const unsigned char *)[jpeg bytes];
    if (bytes[0] != 0xFF || bytes[1] != 0xD8) {
        return NO;
    }
    
    if (bytes[[jpeg length] - 2] != 0xFF || bytes[[jpeg length] - 1] != 0xD9) {
        return NO;
    }
    
    return YES;
}

- (void)checkImageReceiveFinished {
    // no data for 1 second, assume data receive finished
    if(CFAbsoluteTimeGetCurrent() - self.lastDataReceiveTime > 1.0f) {
        [self imageAction:nil];
        [self.checkPhotoTimer invalidate];
        self.checkPhotoTimer = nil;
    }
}

@end
