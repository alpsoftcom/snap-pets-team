//
//  SelectCamController.m
//  MipPhotoTest
//
//  Created by Forrest Chan on 4/8/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "SelectCamController.h"
#import <DropboxSDK/DropboxSDK.h>
#import "SVProgressHUD.h"
#import "SIAlertView.h"
#import "HexDumpController.h"

typedef enum
{
    IDLE = 0,
    SCANNING,
    CONNECTED,
} ConnectionState;

@interface SelectCamController () <DBRestClientDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) DBRestClient *restClient;
@property (nonatomic, strong) NSMutableArray *uploadedFilenames;
@property (nonatomic, assign) int uploadFileCount;
@property (nonatomic, assign) int previousFullByteLength;
@property (nonatomic, assign) bool isShownSetting;
@property (nonatomic, assign) bool isFullScreen;
@property (nonatomic, assign) CGRect prevFrame;
@property (nonatomic, strong) UIImageView* previewImg;
@property (nonatomic, strong) UIScrollView* previewView;
@property (nonatomic, strong) UIButton* previewButton;
@end

@implementation SelectCamController

//@synthesize currentPeripheral = _currentPeripheral;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // dropbox
    self.restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
    self.restClient.delegate = self;
    self.uploadedFilenames = [NSMutableArray new];
    
    // config image view
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.previousFullByteLength = 0;
    
    self.takePictureButton.enabled = YES;
    
    self.isShownSetting = NO;
    self.settingPageView.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // config peripheral
//    if(self.currentPeripheral) {
//        [self.currentPeripheral setDelegate:self];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)takePicture {
    self.indatorImgView.alpha = 0;
    
    // TEST
    //    [self.currentPeripheral getProtocolVersion];
    //    return;
    
    // Use predefined jpg header?
//    self.currentPeripheral.usePredefinedHeader = self.predefinedHeaderSwitch.isOn;
//    [self.currentPeripheral takePicture];
    
    // Clear image
    [self.imageView setImage:nil];
    
    // disable buttons
    self.dropboxButton.enabled = YES;
    self.hexDumpButton.enabled = YES;
    
    // show loading
    [self.progressBar setProgress:0];
    self.progressBar.hidden = NO;
    [self.progressLabel setText:@"Empty"];
    self.progressLabel.hidden = NO;
}

-(void)imgToFullScreen{
    if (!self.isFullScreen) {
        //save previous frame
        self.prevFrame = self.imageView.frame;
        if (!self.previewImg){
            self.previewImg = [[UIImageView alloc] initWithImage:self.imageView.image];
        }
        if (!self.previewView){
            self.previewView = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            self.previewView.delegate = self;
            self.previewView.minimumZoomScale = 1.0;
            self.previewView.maximumZoomScale = 3.0;
            self.previewView.backgroundColor = [UIColor blackColor];
            self.previewView.contentSize=[[UIScreen mainScreen] bounds].size;
            [self.previewView addSubview:self.previewImg];
            
            
            self.previewView.autoresizesSubviews = YES;
            self.previewView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            self.previewView.scrollEnabled = YES;
            self.previewView.directionalLockEnabled = NO;
            self.previewView.userInteractionEnabled = YES;
        }
        if (!self.previewButton){
            self.previewButton = [[UIButton alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            [self.previewButton addTarget:self action:@selector(imgToFullScreen) forControlEvents:UIControlEventTouchUpInside];
            [self.previewView addSubview:self.previewButton];
        }
        self.previewView.hidden = NO;
        self.previewImg.frame = self.prevFrame;
        self.previewImg.center = self.previewView.center;

        [self.previewImg setImage:self.imageView.image];

        [self.view addSubview:self.previewView];
        self.isFullScreen = true;
        
        self.previewView.alpha = 0;
        [UIView animateWithDuration:0.5 animations:^{
            self.previewView.alpha = 1;
        }];

        return;
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            self.previewView.alpha = 0;
        } completion:^(BOOL finished) {
            self.isFullScreen = false;
            self.previewView.hidden = YES;
            [self.previewView removeFromSuperview];
        }];
        return;
    }
}

//- (BOOL)canRotate{
//    return self.isFullScreen;
//}

#pragma UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.previewImg;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = MAX((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = MAX((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0);
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

#pragma mark - button actions
- (IBAction)takePictureAction:(id)sender {
    [self takePicture];
    
}

- (IBAction)hexDumpAction:(id)sender {
//    NSLog(@"HexDump: %@", [self.currentPeripheral getHexDump]);
//    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HexDumpController"];
//    ((HexDumpController *)controller).hexDump = [self.currentPeripheral getHexDump];
//    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)dropboxAction:(id)sender {
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] linkFromController:self];
    }
    else {
        [self.uploadedFilenames removeAllObjects];
        self.uploadFileCount = 0;
        
//        if(![self.currentPeripheral getImageData] && ![self.currentPeripheral getHexDump]) {
//            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Opps" andMessage:@"No image or hex dump to upload"];
//            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
//            [alertView addButtonWithTitle:@"Close"
//                                     type:SIAlertViewButtonTypeCancel
//                                  handler:^(SIAlertView *alert) {
//                                  }];
//            [alertView show];
//            return;
//        }
        
        // save to dropbox
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"MM-dd_HH-mm-ss"];
        NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
        
        // Write image
        NSString *localDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *destDir = @"/";
//        if([self.currentPeripheral getImageData]) {
//            self.uploadFileCount++;
//            NSData *imageData = [self.currentPeripheral getImageData];
//            NSString *imageFilename = [NSString stringWithFormat:@"%@.jpg", dateString];
//            NSString *imagelocalPath = [localDir stringByAppendingPathComponent:imageFilename];
//            [imageData writeToFile:imagelocalPath atomically:YES];
//            [self.restClient uploadFile:imageFilename toPath:destDir withParentRev:nil fromPath:imagelocalPath];
//        }
        
        // Write hex dump
//        if([self.currentPeripheral getHexDump]) {
//            self.uploadFileCount++;
//            NSString *text = [self.currentPeripheral getHexDump];
//            NSString *filename = [NSString stringWithFormat:@"%@.txt", dateString];
//            NSString *localPath = [localDir stringByAppendingPathComponent:filename];
//            [text writeToFile:localPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
//            [self.restClient uploadFile:filename toPath:destDir withParentRev:nil fromPath:localPath];
//        }
        
        // Show loading
        [SVProgressHUD showWithStatus:@"Upoading to dropbox" maskType:SVProgressHUDMaskTypeBlack];
    }
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)settingPageAction:(id)sender {
    self.isShownSetting = !self.isShownSetting;
    if (self.isShownSetting){
        self.settingPageView.hidden = NO;
    }else{
        self.settingPageView.hidden = YES;
    }
}

- (IBAction)previewCapturedImageAction:(id)sender{
    if (self.imageView.image){
        [self imgToFullScreen];
    }
}

#pragma mark - UARTPeripheralDelegate
- (void) didReceiveData:(NSString *)string {
    NSLog(@"didReceiveData %@", string);
//    [self addTextToConsole:string dataType:RX];
}

- (void) didReceiveImage:(UIImage *)image {
    self.indicatorBg.hidden = YES;
    
    [self.imageView setImage:image];
    
    self.hexDumpButton.enabled = YES;
    self.dropboxButton.enabled = YES;
    
    // hide loading
    self.progressLabel.hidden = YES;
    self.progressBar.hidden = YES;
    
    [self checkForExistingScreenAndInitializeIfPresent];
}

- (void) didReceiveByteProgress:(int)currentByteLength fullLength:(int)fullByteLength {
    float progress = (float)currentByteLength / (float)fullByteLength;
    [self.progressLabel setText:[NSString stringWithFormat:@"%d / %d", currentByteLength, fullByteLength]];
    if(self.previousFullByteLength != fullByteLength) {
        [self.progressBar setProgress:progress animated:NO];
    }
    else {
        [self.progressBar setProgress:progress animated:NO];
    }
    self.previousFullByteLength = fullByteLength;
}

- (void) didProcessImageDataFailed {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Failed" andMessage:@"Process image data failed, see hex dump for details"];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    [alertView addButtonWithTitle:@"Close"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                          }];
    [alertView show];
}

- (void) didProcessHex:(NSString*)hex{
    if (hex && [hex isEqualToString:@"00"]){
        
        [self takePicture];
    }
}

#pragma mark - DBRestClientDelegate
- (void)restClient:(DBRestClient *)client uploadedFile:(NSString *)destPath
              from:(NSString *)srcPath metadata:(DBMetadata *)metadata {
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    [SVProgressHUD dismiss];
    
    [self.uploadedFilenames addObject:[metadata.path stringByReplacingOccurrencesOfString:@"/" withString:@""]];
    if([self.uploadedFilenames count] == self.uploadFileCount && self.uploadFileCount > 0) {
        NSString *filenames = self.uploadedFilenames[0];
        if([self.uploadedFilenames count] > 1) {
            filenames = [filenames stringByAppendingString:[NSString stringWithFormat:@" and %@", self.uploadedFilenames[1]]];
        }
        NSString *successMessage = [NSString stringWithFormat:@"%@ has been save to your dropbox at /App/SnapPetzDemoApp/", filenames];
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Success" andMessage:successMessage];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        [alertView addButtonWithTitle:@"Close"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alert) {
                              }];
        [alertView show];
    }
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error {
    NSLog(@"File upload failed with error: %@", error);
    [SVProgressHUD dismiss];
    
    NSString *errorMessage = [NSString stringWithFormat:@"File upload failed with error: %@", error];
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Opps" andMessage:errorMessage];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    [alertView addButtonWithTitle:@"Close"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                          }];
    [alertView show];
}


#pragma mark - AirPlay and extended display

- (void)checkForExistingScreenAndInitializeIfPresent
{
    if ([[UIScreen screens] count] > 1)
    {
        
        // Get the screen object that represents the external display.
        UIScreen *secondScreen = [[UIScreen screens] lastObject];
        secondScreen.overscanCompensation = 2;//UIScreenOverscanCompensationScale;
        
        // Get the screen's bounds so that you can create a window of the correct size.
        UIScreenMode *screenMode = [[secondScreen availableModes] lastObject];
        CGRect screenBounds = (CGRect){.size = screenMode.size};
        
        self.secondWindow = [[UIWindow alloc] initWithFrame:screenBounds];
        self.secondWindow.screen = secondScreen;
    
//        CGRect screenBounds = CGRectMake(0, 0, 1920, 1080);
    
        // Set up initial content to display...
//        self.secondWindow.backgroundColor = [UIColor redColor];

        UIImage *newImage = [UIImage imageWithCGImage:self.imageView.image.CGImage];
//        UIImageView *imgView = [[UIImageView alloc] initWithImage:self.imageView.image];
        newImage = [self scaleToSizeKeepAspect:newImage.size tvSize:screenBounds.size img:newImage];
        
        UIImageView *newView = [[UIImageView alloc] initWithImage:newImage];
        
        [self.secondWindow addSubview:newView];
        
//        [self.secondWindow addSubview:self.imageView];
        
        
        // Show the window.
        self.secondWindow.hidden = NO;
    }
}

- (UIImage*)scaleToSizeKeepAspect:(CGSize)size tvSize:(CGSize)tvSize img:(UIImage *)img
{
    UIGraphicsBeginImageContext( tvSize );
    
    //    float ws = size.width/tvSize.width;
    //    float hs = size.height/tvSize.height;
    CGFloat ws = tvSize.width/size.width;
    CGFloat hs = tvSize.height/size.height;
    
    CGFloat ratio = MIN(ws, hs);
    [img drawInRect:CGRectMake(tvSize.width/2-(size.width*ratio)/2,
                               tvSize.height/2-(size.height*ratio)/2, size.width*ratio,
                               size.height*ratio)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
