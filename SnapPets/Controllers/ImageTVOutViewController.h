//
//  ImageTVOutViewController.h
//  MipPhotoTest
//
//  Created by Katy Pun on 29/9/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface ImageTVOutViewController : ParentViewController

@property (nonatomic, weak) IBOutlet UIImageView *displayImage;


- (id)initWithFrame:(CGRect)rect;
- (void)setImg:(UIImage *)img;

@end
