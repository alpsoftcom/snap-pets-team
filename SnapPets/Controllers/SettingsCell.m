//
//  UserSettingDeviceCell.m
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "SettingsCell.h"
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
@implementation SettingsCell

- (void)awakeFromNib {
    
    lineColor = Rgb2UIColor(173, 255, 250);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code

}

- (void)setBorderedTypeCell
{
    self.layer.borderWidth = 2.;
    self.layer.borderColor = lineColor.CGColor;
}

-(void)setFirstTypeCell{
    
    UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 2)];
    topLineView.backgroundColor = lineColor;
    [self.contentView addSubview:topLineView];
    
    UIView *leftLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2,  self.bounds.size.height)];
    leftLineView.backgroundColor = lineColor;
    [self.contentView addSubview:leftLineView];
    
    UIView *rightLineView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width-2, 0, 2, self.bounds.size.height)];
    rightLineView.backgroundColor = lineColor;
    [self.contentView addSubview:rightLineView];
    
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(80, self.bounds.size.height-2, self.bounds.size.width, 2)];
    bottomLineView.backgroundColor = lineColor;
    [self.contentView addSubview:bottomLineView];
}
-(void)setMiddleTypeCell{
    
    UIView *leftLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2,  self.bounds.size.height)];
    leftLineView.backgroundColor = lineColor;
    [self.contentView addSubview:leftLineView];
    
    UIView *rightLineView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width-2, 0, 2, self.bounds.size.height)];
    rightLineView.backgroundColor = lineColor;
    [self.contentView addSubview:rightLineView];
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(80, self.bounds.size.height-2, self.bounds.size.width, 2)];
    bottomLineView.backgroundColor = lineColor;
    [self.contentView addSubview:bottomLineView];

}
-(void)setLastTypeCell{
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-2, self.bounds.size.width, 2)];
    bottomLineView.backgroundColor = lineColor;
    [self.contentView addSubview:bottomLineView];
    
    UIView *leftLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2,  self.bounds.size.height)];
    leftLineView.backgroundColor = lineColor;
    [self.contentView addSubview:leftLineView];
    
    UIView *rightLineView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width-2, 0, 2, self.bounds.size.height)];
    rightLineView.backgroundColor = lineColor;
    [self.contentView addSubview:rightLineView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   // [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
