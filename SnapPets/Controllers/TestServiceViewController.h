//
//  TestServiceViewController.h
//  SnapPets
//
//  Created by Katy Pun on 18/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "ParentViewController.h"

@interface TestServiceViewController : ParentViewController <SnappetRobotDelegate>

@property (nonatomic, strong) SnappetRobot *snappet;
@property (weak, nonatomic) IBOutlet UITextField *thumbnailQuality;
@property (weak, nonatomic) IBOutlet UITextField *thumbnailResolution;
@property (weak, nonatomic) IBOutlet UITextField *photoQuality;
@property (weak, nonatomic) IBOutlet UITextField *photoResolution;
@property (weak, nonatomic) IBOutlet UISwitch *appendPhotoHeader;
@property (weak, nonatomic) IBOutlet UISwitch *overridePhotoOnFlash;
@property (weak, nonatomic) IBOutlet UITextField *shortPress;
@property (weak, nonatomic) IBOutlet UITextField *longPress;
@property (weak, nonatomic) IBOutlet UISwitch *enableLED;
@property (weak, nonatomic) IBOutlet UITextField *longPressDuration;
@property (weak, nonatomic) IBOutlet UITextField *activationStatus;
@property (weak, nonatomic) IBOutlet UITextField *cmosFrequency;
@property (weak, nonatomic) IBOutlet UISwitch *pinEnable;
@property (weak, nonatomic) IBOutlet UITextField *pinAuthen;
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UILabel *authenFailLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *settingView;
@property (weak, nonatomic) IBOutlet UIView *pinSetupView;
@property (weak, nonatomic) IBOutlet UITextField *deviceNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *cmosPoweroffTextField;
@property (weak, nonatomic) IBOutlet UILabel *chargingStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *userDeviceName;

@end
