//
//  UserSettingViewController.m
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "UserSettingViewController.h"
#import "UserSettingViewControllerRedesign.h"
#import "AppConfig.h"
#import "ConnectionManager.h"
#import "LocalizationManager.h"
#import "SettingsCellHeader.h"
#import "SettingsCell.h"
#import "SettingsCellDisconnect.h"
#import "UserPetSettingViewController.h"
#import "GeneralSettingViewController.h"

static NSString *SettingsCellHeaderCellIdentifier = @"SettingsCellHeader";
static NSString *SettingsCellCellIdentifier = @"SettingsCell";
static NSString *SettingsCellDisconnectIdentifier = @"SettingsCellDisconnect";
@interface UserSettingViewControllerRedesign ()

@end

@implementation UserSettingViewControllerRedesign

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, -20, 0);
    // Do any additional setup after loading the view.

#if TARGET_IPHONE_SIMULATOR
#else
   // self.arrConnectedDevice = [AppConfig sharedInstance].arrConnectedDevice;
    SnappetRobot* robot = [ConnectionManager sharedInstance].snappet;
    ConnectedDeviceRecord* record = nil;
    
//    for (int i=0;i<[self.arrConnectedDevice count];i++) {
//        ConnectedDeviceRecord* dict = [self.arrConnectedDevice objectAtIndex:i];
//        if ([dict.strID isEqualToString:[robot.uuid UUIDString]]) {
//            record = dict;
//            break;
//        }
//    }

   // [self.arrConnectedDevice removeObject:record];
   // [self.arrConnectedDevice insertObject:record atIndex:0];
#endif
  
    
    
    
    // Temp
//    [self.viewLanguageSelection setHidden:YES];
//    [self.viewThemeSelection setHidden:YES];
//    
//    
//    [self.buttonDisconnect setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnDisconnect"] forState:UIControlStateNormal];
//    [self.titleLanguageLabel setText:[[LocalizationManager getInstance] getStringWithKey:@"SettingTitleLanguage"]];
//    [self.titleThemeLabel setText:[[LocalizationManager getInstance] getStringWithKey:@"SettingTitleTheme"]];
//    
//    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.buttonDone.layer.cornerRadius = 10.0f;
 //   self.buttonDisconnect.layer.cornerRadius = 10.0f;
    
    [self updateView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)updateView {
  
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  /*  SettingsCellTypeGeneral = 0,
    SettingsCellTypeTutorial = 1,
    SettingsCellTypeLanguage = 2,//1
    SettingsCellTypeCustomization = 3,
    SettingsCellTypeSnapPetName = 3,
    SettingsCellTypeSnapPetPin= 4,
    SettingsCellTypeSnapPetColor = 5,
    SettingsCellTypeUpdates = 6,
    SettingsCellTypeSnapPetVersion = 7,
    SettingsCellTypeDissconnect = 8 */
  
    
    switch (indexPath.row) {
        case SettingsCellTypeGeneral:{
            SettingsCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellHeaderCellIdentifier
                                                                       forIndexPath:indexPath];
            
            [cell.titleLabel setText:[NSString stringWithFormat:@"General"]];
            return cell;
        }
            break;
//        case SettingsCellTypeTutorial:{
//            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
//                                                                       forIndexPath:indexPath];
//            [cell setFirstTypeCell];
//            [cell.titleLabel setText:[NSString stringWithFormat:@"App tutorial"]];
//            return cell;
//        }
//            break;

//        case SettingsCellTypeLanguage:{
//            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
//                                                                 forIndexPath:indexPath];
//            [cell setMiddleTypeCell];
//            [cell.titleLabel setText:[NSString stringWithFormat:@"Langue"]];
//                return cell;
//            
//        }
//            break;
            
        case SettingsCellTypeReset:{
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
                                                                 forIndexPath:indexPath];
            [cell setLastTypeCell];
            [cell.titleLabel setText:[NSString stringWithFormat:@"Reset"]];
                return cell;
        }
            break;
           
            ///
        case SettingsCellTypeCustomization:{
            SettingsCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellHeaderCellIdentifier
                                                                       forIndexPath:indexPath];
          [cell.titleLabel setText:[NSString stringWithFormat:@"Customization"]];
                return cell;
        }
            break;
       
        case SettingsCellTypeSnapPetName:{
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
                                                                 forIndexPath:indexPath];
             [cell setFirstTypeCell];
            [cell.titleLabel setText:[NSString stringWithFormat:@"Snap Pet Name"]];
                return cell;
        }
            break;
        case SettingsCellTypeSnapPetPin:{
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
                                                                       forIndexPath:indexPath];
            [cell setMiddleTypeCell];
            [cell.titleLabel setText:[NSString stringWithFormat:@"Snap Pet Pin"]];
                return cell;
        }
            break;

//        case SettingsCellTypeSnapPetColor:{
//            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
//                                                                 forIndexPath:indexPath];
//            [cell setLastTypeCell];
//            [cell.titleLabel setText:[NSString stringWithFormat:@"Color Theme"]];
//                return cell;
//        }
//            break;
//
        case SettingsCellTypeUpdates:{
            SettingsCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellHeaderCellIdentifier
                                                                       forIndexPath:indexPath];
          [cell.titleLabel setText:[NSString stringWithFormat:@"Updates"]];
                return cell;
        }
            break;

        case SettingsCellTypeSnapPetVersion:{
            
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellCellIdentifier
                                                                 forIndexPath:indexPath];
            
             [cell setFirstTypeCell];
            [cell.titleLabel setText:[NSString stringWithFormat:@"%@ Update",  [ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion]];
                return cell;
        }
            
            break;

            //
        case SettingsCellTypeDissconnect:{
            SettingsCellDisconnect *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellDisconnectIdentifier
                                                                       forIndexPath:indexPath];
            [cell setLastTypeCell];
           [cell.titleLabel setText:[NSString stringWithFormat:@"Disconect"]];
            return cell;
        }
            break;

      
        default:
            break;
    }


    return  nil;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGRect rect = [self.view bounds];
    if (rect.size.height == 1024 || rect.size.height == 2048)
        return 88.0f;
    else
        return 44.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return  0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
   
    return  20.f;
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [UIView new];
    return  view;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView setTag:indexPath.row];
    
   
    
    switch (indexPath.row) {
        case SettingsCellTypeGeneral:
        case SettingsCellTypeCustomization:
        case SettingsCellTypeUpdates:
        case SettingsCellTypeSnapPetVersion:
             break;
        case SettingsCellTypeDissconnect:
        {
            SnappetRobot* robot = [ConnectionManager sharedInstance].snappet;
            [robot disconnect];
             [self.navigationController popToRootViewControllerAnimated:NO];
            NSLog(@"here");
        }
            break;
//        case SettingsCellTypeLanguage:
//        {
//            [self performSegueWithIdentifier:@"GeneralSettingViewController" sender:tableView];
//        }
//            break;
        case SettingsCellTypeReset:
        {
            [self performSegueWithIdentifier:@"GeneralSettingViewController" sender:tableView];
        }
            break;
        default:
                [self performSegueWithIdentifier:@"UserPetSettingViewController" sender:tableView];
            break;
    }

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([[segue identifier] isEqualToString:@"UserPetSettingViewController"])
    {
        UITableView *table = (id)sender;
        UserPetSettingViewController *vc = [segue destinationViewController];
        [vc setType:table.tag];
       
    }
    else if ( [[segue identifier] isEqualToString:@"GeneralSettingViewController"] )
    {
        GeneralSettingViewController *vc = segue.destinationViewController;
        vc.generalSettingsHandler = ^(NSString *action)
        {
            NSLog(@"%@", action);
        };
        vc.cellType = ((UITableView *)sender).tag;
    }
}
-(IBAction)backButtonPressed:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end