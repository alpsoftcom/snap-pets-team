//
//  TakePhotoViewController.m
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "TakePhotoViewController.h"
#import "TakePhotoOverlayView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PhotoPreviewViewController.h"
#import "PhotoLibraryManager.h"
#import "SVProgressHUD.h"
#import "UIView+Toast.h"
#import "PhotoLibraryViewController.h"
#import "ConnectionManager.h"
#import "TestServiceViewController.h"
#import "SimpleSoundPlayer.h"
#import "SettingsViewController.h"
#import "PhotoLibraryManager.h"
#import "TakePhotoPickerViewController.h"
#import "CamTestController.h"
#import "BootloaderViewController.h"
#import "UserSettingViewController.h"
#import "Flurry.h"
#import "AppConfig.h"
#import "LocalizationManager.h"
#import "RateAppViewController.h"

#define IS_RATIO_3_2    (( double )[[UIScreen mainScreen] bounds].size.height / [[UIScreen mainScreen] bounds].size.width == ( double )3 / 2)

static int RATIO_3_2_OFFSET_H = 20;
NSString *const soundShutter = @"shutter.wav";
NSString *const soundCountDown = @"countdown.wav";

@interface TakePhotoViewController ()

@property (nonatomic) UIImagePickerController *imagePickerController;
@property (nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *takePictureButton;
@property (nonatomic) BOOL isPhoneShutterMode;
@property (nonatomic, strong) UIImage *capturedImg;
@property (nonatomic, assign) int photoLength;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic) BOOL isInit;
@property (nonatomic, assign) int currentCaptureTime;
@property (nonatomic, strong) NSTimer *cameraTimer;
@property (nonatomic, assign) BOOL isTakingPhotoFromSnap;
@property (nonatomic, assign) BOOL isTakePhoto;
@property (nonatomic, assign) int photoQuality;
@property (nonatomic, strong) UIView* colorView;
@end

@implementation TakePhotoViewController 

- (void)setColorGrid{


    switch ([ConnectionManager sharedInstance].snappet.colorId){
        case kSnappetColorBlue:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f green:0.0/255.0f blue:250.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetColorPink:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:255.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetColorOrage:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:39.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetColorSilver:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
            break;
        case kSnappetAquaBlue:
            [self.colorView setBackgroundColor:[UIColor colorWithRed:12.0f/255.0f green:247.0f/255.0f blue:246.0f/255.0f alpha:1.0f]];
            break;
        default:
            break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isPhoneShutterMode = YES;
    
//    [ConnectionManager sharedInstance].snappet.delegate = self;
    
    [[ConnectionManager sharedInstance].snappet snappetSetShortPressAction:kSnappetPressDoNotify];  //set to use as remote shutter
    
//    [[ConnectionManager sharedInstance].snappet snappetReadNumberOfPhotos];

    self.isInit = YES;
#if !defined(TARGET_IPHONE_SIMULATOR) || TARGET_IPHONE_SIMULATOR == 0
    if (self.isPhoneShutterMode){
        [self loadCameraView];
    }
#else
    self.takephotoOverlayView  = [[TakePhotoOverlayView alloc] init];
    self.takephotoOverlayView.deleagte = self;
    [self.view addSubview:self.takephotoOverlayView];
#endif
    self.takephotoOverlayView.viewNewSnapPets.viewTutorial = self.tutorialView;
    self.takephotoOverlayView.viewEnterName = self.enterNameView;
    self.takephotoOverlayView.viewEnterName.viewTutorial = self.tutorialView;
    self.takephotoOverlayView.viewTutorial = self.tutorialView;
    self.takephotoOverlayView.viewTutorial.viewEnterName = self.enterNameView;
    self.takephotoOverlayView.viewPopup = self.popupView;
    
    __weak typeof (self) wself = self;
    self.takephotoOverlayView.timerHandler = ^(NSInteger timer, PressedButtonType type)
    {
        if(type == kPressedButtonTypeTimer)
        {
            wself.takephotoOverlayView.timerLevel = timer;
            wself.takephotoOverlayView.photoSetLevel = PhotoSetLevel_0;
            [wself.takephotoOverlayView setTimer];
        }
        else
        {
            wself.takephotoOverlayView.timerLevel = timer;
            wself.takephotoOverlayView.photoSetLevel = timer;
            [wself.takephotoOverlayView setPhotoSet];
        }
        NSLog(@"TIMER: %li", (long)timer);
    };
    
    self.colorView = [[UIView alloc] initWithFrame:CGRectMake(60, 40, 40, 40)];
    [self.view addSubview:self.colorView];
    [self setColorGrid];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    isInCurrentViewController = YES;
    [self.takephotoOverlayView.bottomBtnUIViewCover setHidden:YES];
    // To Restore the new SnapPet name
    self.takephotoOverlayView.labelName.text = [AppConfig sharedInstance].strRobotName;
    self.takephotoOverlayView.isInUserSettingController = NO;
    
    // To Restore the CameraDevice Mode and clear the Preview
    if (self.takephotoOverlayView.isPhoneShutterMode){
        if (self.takephotoOverlayView.phoneTakePhoto.image == nil) {
        }
        else {
            [self.takephotoOverlayView clearPreview];
            [self.takephotoOverlayView setCameraButton:NO];
            self.takephotoOverlayView.blackview.hidden = YES;
        }

        if (self.takephotoOverlayView.isSelfie){
            self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            [self.takephotoOverlayView.flashBtn setEnabled:NO];
        }else{
            self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
    }
    
    // To Restore The Previous Flash Mode
    if (self.takephotoOverlayView.flashMode == FlashModeAuto){
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    }else if (self.takephotoOverlayView.flashMode == FlashModeOff){
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    }else if (self.takephotoOverlayView.flashMode == FlashModeOn) {
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
    
    if ([ConnectionManager sharedInstance].snappet == nil) {
        if (!self.takephotoOverlayView.isPhoneShutterMode)
            [self.takephotoOverlayView modePressed:nil];
        
        [self.takephotoOverlayView showConnectionScreen];
    }
}

- (void)viewDidAppear:(BOOL)animated{
//    [self loadCameraView];
    [ConnectionManager sharedInstance].snappet.delegate = self;
    [[ConnectionManager sharedInstance].snappet snappetStopTransfer];
    [[ConnectionManager sharedInstance].snappet snappetReadLargeSizePhotoInfo];
    
    self.isPhoneShutterMode = YES;
    if (self.takephotoOverlayView != nil)
        self.isPhoneShutterMode = self.takephotoOverlayView.isPhoneShutterMode;
    if (self.imagePickerController){
        self.imagePickerController.delegate = self;
    }
    if (self.takephotoOverlayView == nil){
        [self initOverlay];
    }
    
    [self resetPhoto];
    
    [SVProgressHUD dismiss];
    
    [self initVariable];
    
    [self.takephotoOverlayView initView];
    
    if ([ConnectionManager sharedInstance].snappet == nil) {
        [self.takephotoOverlayView.scanBtn setHidden:NO];
//        self.takephotoOverlayView.scanBtn.tag = 0;
        [self.takephotoOverlayView.scanBtn setAlpha:1.0f];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    isInCurrentViewController = NO;
}

- (void)viewUnloadAnimation {
    
}

- (void)initVariable
{
    self.takephotoOverlayView.viewSnapPetDownloading.hidden = YES;
    self.takephotoOverlayView.blobProgress.hidden = YES;
    self.takephotoOverlayView.blobProgress.progress = 0;
    self.takephotoOverlayView.snapptOkBtn.hidden = YES;
    self.takephotoOverlayView.snapptRetakeBtn.hidden = YES;
    self.takephotoOverlayView.deleteSnappetBtn.hidden = YES;
    self.takephotoOverlayView.progressLabel.hidden = YES;
    self.isTakingPhotoFromSnap = NO;
    self.isTakePhoto = NO;
    
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

-(void)loadCameraView {
    if (self.takephotoOverlayView == nil && self.isPhoneShutterMode){
        self.imagePickerController = [[TakePhotoPickerViewController alloc] init];
        self.imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imagePickerController.delegate = self;
        self.imagePickerController.allowsEditing = YES;
        self.imagePickerController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.imagePickerController.showsCameraControls = NO;
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            
        //    self.imagePickerController.cameraViewTransform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1.12412);
        
        //    CGSize screenSize = [[UIScreen mainScreen] bounds].size;   // 320 x 568
        //    float scale = screenSize.height / screenSize.width*3/4;  // screen height divided by the pickerController height ... or:  568 / ( 320*4/3 )
        //    CGAffineTransform translate=CGAffineTransformMakeTranslation(0,(screenSize.height - screenSize.width*4/3)*0.5);
        //    CGAffineTransform fullScreen=CGAffineTransformMakeScale(scale, scale);
        //    self.imagePickerController.cameraViewTransform =CGAffineTransformConcat(fullScreen, translate);
        
        [self.imagePickerController setExtendedLayoutIncludesOpaqueBars:YES];
        self.imagePickerController.edgesForExtendedLayout = UIRectEdgeAll;
        CGSize screenBounds = [self.view bounds].size;
        
        if (IS_RATIO_3_2){
            self.imagePickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0, 100 - RATIO_3_2_OFFSET_H);
        }else{
            if (screenBounds.height == 736)
                self.imagePickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0, 161);
            else if (screenBounds.height == 667)
                self.imagePickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0, 146);
            else
                self.imagePickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0, 124);
        }
        
        
        
        //    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
        //    CGFloat cameraAspectRatio = 4.0f/3.0f;
        //    CGFloat camViewHeight = screenBounds.width * cameraAspectRatio;
        //    CGFloat scale = screenBounds.height / camViewHeight;
        //    self.imagePickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0, (screenBounds.height - camViewHeight) / 2.0);
        //    self.imagePickerController.cameraViewTransform = CGAffineTransformScale(self.imagePickerController.cameraViewTransform, scale, scale);
        
        
        //    self.imagePickerController.cameraViewTransform = CGAffineTransformScale(CGAffineTransformIdentity, 2, 2); //full screen
        
        
        //    [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil];
        //    self.overlayView.frame = self.imagePickerController.cameraOverlayView.frame;
        //    self.imagePickerController.cameraOverlayView = self.overlayView;
        //    self.overlayView = nil;
        
        [self initOverlay];
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        BOOL hasFlash = [device hasFlash];
        //ipad no flash camera
        if (!hasFlash){
            [self.takephotoOverlayView noFlashBtn];
        }
        
    //    [self presentViewController:self.imagePickerController animated:YES completion:nil];
        
        [self.view addSubview:self.imagePickerController.view];
    }
}

- (void) initOverlay {
    self.takephotoOverlayView  = [[TakePhotoOverlayView alloc] init];
    [self.takephotoOverlayView showConnectionScreen];
    self.takephotoOverlayView.deleagte = self;
    //    [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self.takephotoOverlayView options:nil];
    self.takephotoOverlayView.frame = self.imagePickerController.cameraOverlayView.frame;
    self.imagePickerController.cameraOverlayView = self.takephotoOverlayView;
    self.imagePickerController.preferredContentSize = CGSizeMake(320, 320);
    
    //    self.takephotoOverlayView = nil;
    //    self.cameraView.frame = self.imagePickerController.cameraOverlayView.frame;
    //    self.imagePickerController.cameraOverlayView = self.cameraView;
    self.takephotoOverlayView.imagePickerController = self.imagePickerController;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissCameraView {
    [self.imagePickerController dismissViewControllerAnimated:NO completion:nil];
//    self.takephotoOverlayView = nil;
}

- (void)updateCounter:(NSTimer *)theTimer {
    if(self.currentCaptureTime > 0 ){
        self.currentCaptureTime -- ;
        self.takephotoOverlayView.timerLabel.text = [NSString stringWithFormat:@"%d", self.currentCaptureTime];
        self.takephotoOverlayView.timerLabelSnap.text = [NSString stringWithFormat:@"%d", self.currentCaptureTime];
        if (self.currentCaptureTime > 0){
            [SimpleSoundPlayer playALSound:soundCountDown];
        }
    }
    if (self.currentCaptureTime == 0){
        [self.takephotoOverlayView.camImgView setImage:[UIImage imageNamed:@"btn_snap.png"] forState:UIControlStateNormal];
        if (self.isPhoneShutterMode) {
            [SVProgressHUD show];
            self.takephotoOverlayView.timerLabel.hidden = YES;
            
            [self.cameraTimer invalidate];
            self.cameraTimer = nil;
            [self showFlashView];
            [self.imagePickerController takePicture];
            if (self.takephotoOverlayView.flashMode == FlashModeOff)
                self.takephotoOverlayView.blackview.hidden = NO;
            if (self.takephotoOverlayView.isSelfie)
                self.takephotoOverlayView.blackview.hidden = NO;
            [self.takephotoOverlayView.viewPhotoModePinkBar setHidden:YES];
            [self.takephotoOverlayView.takePhotoSettingBtnView setHidden:YES];
            [Flurry logEvent:@"Take Photo" withParameters:@{@"Mode" : @"Camera Counter"}];
        }
        else {
            //            if (self.isTakePhoto){
             self.takephotoOverlayView.timerLabel.hidden = YES;
            self.takephotoOverlayView.timerLabelSnap.hidden = YES;
            self.takephotoOverlayView.viewSnapPetDownloading.hidden = NO;
            self.isTakePhoto = NO;
            self.takephotoOverlayView.blobProgress.hidden = NO;
            self.takephotoOverlayView.viewSnappetTutorial.hidden = YES;
            if (timerAnimatingDownloadImages == nil)
                timerAnimatingDownloadImages = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self.takephotoOverlayView selector:@selector(animateSnapPetsDownloading) userInfo:nil repeats:YES];
            //            }
            
            //            [SVProgressHUD show];
            
            [self.cameraTimer invalidate];
            self.cameraTimer = nil;
            
            self.takephotoOverlayView.timerSnappetBtn.hidden = YES;
            [self.takephotoOverlayView.snappetTakenPhoto setImage:nil];
            
            [SimpleSoundPlayer playSound:soundShutter];
            uint8_t quality = self.photoQuality;
            [[ConnectionManager sharedInstance].snappet snappetTakePhotoWithoutSavingToFlash:kSnappetPhotoResolutionVGA quality:quality];
            
            self.isTakingPhotoFromSnap = YES;
            [Flurry logEvent:@"Take Photo" withParameters:@{@"Mode" : @"Snappet"}];
            NSLog(@"take photo");
        }
    }
}

- (void)showFlashView
{
    self.takephotoOverlayView.flashView.hidden = NO;
    
    [NSTimer scheduledTimerWithTimeInterval:0.05f target:self selector:@selector(flashView) userInfo:nil repeats:NO];
}

- (void)disableSettingButton {
    [(TakePhotoOverlayView*)self.takephotoOverlayView disableSettingButton];
}

- (void)disableCloseButton {
    [(TakePhotoOverlayView*)self.takephotoOverlayView disableCloseButton];
}

#pragma mark - TakePhotoDelegate
- (void)takePhoto{

    
    if(self.cameraTimer)
    {
        [self.cameraTimer invalidate];
        self.cameraTimer  = nil;
        self.takephotoOverlayView.timerLabel.hidden = YES;
        self.takephotoOverlayView.timerLabelSnap.hidden = YES;
        [self.takephotoOverlayView.viewPhotoModePinkBar setHidden:NO];
        [self.takephotoOverlayView.takePhotoSettingBtnView setHidden:NO];
        [self.takephotoOverlayView.camImgView setImage:[UIImage imageNamed:@"btn_snap.png"] forState:UIControlStateNormal];
        return;
    }
    self.isTakePhoto = YES;
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    
    if (self.takephotoOverlayView.isPhoneShutterMode){
        if (self.takephotoOverlayView.phoneTakePhoto.image == nil) {
            if (!self.tutorialView.hidden) {
                [self.tutorialView receiveShortPress];
            }
            [self.takephotoOverlayView.phoneTakePhoto setImage:nil];
            if (self.cameraTimer){
                [self.cameraTimer invalidate];
                self.cameraTimer = nil;
                self.takephotoOverlayView.timerLabel.hidden = YES;
                self.takephotoOverlayView.timerLabelSnap.hidden = YES;
                
            }
            self.currentCaptureTime = self.takephotoOverlayView.timerLevel;
            if (self.currentCaptureTime > 0){
                self.takephotoOverlayView.timerLabel.hidden = NO;
                self.takephotoOverlayView.timerLabelSnap.hidden = NO;
                self.takephotoOverlayView.timerLabel.text = [NSString stringWithFormat:@"%d", self.currentCaptureTime];
                self.takephotoOverlayView.timerLabelSnap.text = [NSString stringWithFormat:@"%d", self.currentCaptureTime];
                self.cameraTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
                 [self.takephotoOverlayView.camImgView setImage:[UIImage imageNamed:@"btn_cancel_timer.png"] forState:UIControlStateNormal];
                [SimpleSoundPlayer playALSound:soundCountDown];
                self.takephotoOverlayView.blackview.hidden = YES;
                [Flurry logEvent:@"Take Photo" withParameters:@{@"Mode" : @"Camera Counter"}];
            }else{
                [SVProgressHUD show];
                
                [self showFlashView];
                
                [self.takephotoOverlayView.bottomBtnUIViewCover setHidden:NO];
                [self.imagePickerController takePicture];
                [Flurry logEvent:@"Take Photo" withParameters:@{@"Mode" : @"Camera Instant"}];
                if (self.takephotoOverlayView.flashMode == FlashModeOff)
                    self.takephotoOverlayView.blackview.hidden = NO;
                if (self.takephotoOverlayView.isSelfie) {
                    [self.takephotoOverlayView.flashBtn setEnabled:NO];
                    self.takephotoOverlayView.blackview.hidden = NO;
                }
            }
            [self.takephotoOverlayView.viewPhotoModePinkBar setHidden:YES];
            [self.takephotoOverlayView.takePhotoSettingBtnView setHidden:YES];
        }
        else {
            [self.takephotoOverlayView clearPreview];
            [self.takephotoOverlayView setCameraButton:NO];
            self.takephotoOverlayView.blackview.hidden = YES;
        }
    }else{
        if (!self.isTakingPhotoFromSnap){
            if (self.takephotoOverlayView.snappetTakenPhoto.image != nil) {
                [self.takephotoOverlayView clearSnappetPreview];
            }
            else {
                if (!self.tutorialView.hidden) {
                    [self.tutorialView receiveShortPress];
                }
                
                self.takephotoOverlayView.viewSnapPetDownloading.hidden = YES;
                self.takephotoOverlayView.blobProgress.hidden = YES;
                self.takephotoOverlayView.blobProgress.progress = 0;
                
                self.takephotoOverlayView.snapptOkBtn.hidden = YES;
                self.takephotoOverlayView.snapptRetakeBtn.hidden = YES;
                self.takephotoOverlayView.deleteSnappetBtn.hidden = YES;
                self.takephotoOverlayView.closeBtn.hidden = YES;
                
                self.currentCaptureTime = self.takephotoOverlayView.timerLevel;
#if TARGET_IPHONE_SIMULATOR
                self.takephotoOverlayView.viewSnappetTutorial.hidden = YES;
                self.takephotoOverlayView.timerSnappetBtn.hidden = YES;
                
                self.takephotoOverlayView.snapptOkBtn.hidden = NO;
                self.takephotoOverlayView.snapptRetakeBtn.hidden = NO;
                self.takephotoOverlayView.deleteSnappetBtn.hidden = NO;
                if (!self.takephotoOverlayView.isUnderTutorial)
                    self.takephotoOverlayView.closeBtn.hidden = NO;
                
                self.isTakingPhotoFromSnap = NO;
                self.takephotoOverlayView.viewSnapPetDownloading.hidden = YES;
#else
                if (self.currentCaptureTime > 0){
                   // self.takephotoOverlayView.viewSnappetTutorial.hidden = YES;
                    self.takephotoOverlayView.timerLabel.hidden = NO;
                    self.takephotoOverlayView.timerLabel.text = [NSString stringWithFormat:@"%d", self.currentCaptureTime];
                    self.takephotoOverlayView.timerLabelSnap.hidden = NO;
                    self.takephotoOverlayView.timerLabelSnap.text = [NSString stringWithFormat:@"%d", self.currentCaptureTime];
                    self.cameraTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
                    [self.takephotoOverlayView.camImgView setImage:[UIImage imageNamed:@"btn_cancel_timer.png"] forState:UIControlStateNormal];
                    [SimpleSoundPlayer playALSound:soundCountDown];
                    
                    [Flurry logEvent:@"Take Photo" withParameters:@{@"Mode" : @"Snappet Counter"}];
                }else{
                    //            if (self.isTakePhoto){
                    [self.takephotoOverlayView.bottomBtnUIViewCover setHidden:NO];
                    self.takephotoOverlayView.viewSnapPetDownloading.hidden = NO;
                    self.isTakePhoto = NO;
                    self.takephotoOverlayView.blobProgress.hidden = NO;
                    self.takephotoOverlayView.viewSnappetTutorial.hidden = YES;
                    if (timerAnimatingDownloadImages == nil)
                        timerAnimatingDownloadImages = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self.takephotoOverlayView selector:@selector(animateSnapPetsDownloading) userInfo:nil repeats:YES];
                    //            }
                
                    self.takephotoOverlayView.timerSnappetBtn.hidden = YES;
                    [self.takephotoOverlayView.snappetTakenPhoto setImage:nil];
                   
                    
                    [SimpleSoundPlayer playSound:soundShutter];
                    uint8_t quality = self.photoQuality;
                    [[ConnectionManager sharedInstance].snappet snappetTakePhotoWithoutSavingToFlash:kSnappetPhotoResolutionVGA quality:quality];
                    
                    self.isTakingPhotoFromSnap = YES;
//                    if (self.takephotoOverlayView.isSelfie)
//                        [self.takephotoOverlayView.flashBtn setEnabled:NO];
                    [Flurry logEvent:@"Take Photo" withParameters:@{@"Mode" : @"Snappet Instant"}];
                    NSLog(@"take photo");
                }
#endif
            }
        }

    }
    
//    self.takephotoOverlayView.deleteBtn.hidden = NO;
//    self.takephotoOverlayView.timerLevel = TimerLevel_0;
//    [self.takephotoOverlayView setTimer];
}

- (void)okPressed
{
    PhotoPreviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoPreviewViewController"];
    
    if (self.takephotoOverlayView.isPhoneShutterMode){
        controller.selectedImg = self.takephotoOverlayView.phoneTakePhoto.image;
    }else{
        controller.selectedImg = self.takephotoOverlayView.snappetTakenPhoto.image;
    }
    if(!controller.selectedImg)
        controller.selectedImg = self.takephotoOverlayView.tempImage;
    [self dismissCameraView];
    
    [[PhotoLibraryManager sharedInstance] checkImageCount];
    controller.selectedPhotoIndex = [PhotoLibraryManager sharedInstance].imageCount-1;
//    controller.selectedPhotoIndex = [[PhotoLibraryManager sharedInstance].imageList count]-1;
//    controller.selectedPhotoIndex = [PhotoLibraryManager sharedInstance].latestPhotoIndex-1;
    controller.isPreviewAll = NO;
//    controller.selectedPhotoPath = [self getCapturedImgPath];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)retakePressed
{
    
}

- (void)backPressed
{
    [[ConnectionManager sharedInstance].snappet disconnect];
    [ConnectionManager sharedInstance].snappet = nil;
    [self.takephotoOverlayView.scanBtn setHidden:NO];
    
}

- (void)backToMain{
    [self.imagePickerController dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)save{
//    NSMutableArray *imageDataList = [NSMutableArray new];
//    for(UIImage *img in _imageList) {
//        [imageDataList addObject:UIImagePNGRepresentation(img)];
//    }
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    [fileManager removeItemAtPath:_imageListPath error:NULL];
//    [imageDataList writeToFile:_imageListPath atomically:YES];
//}

- (void)modePressed
{
    [SVProgressHUD dismiss];
//    self.isPhoneShutterMode = !self.isPhoneShutterMode;
    self.isPhoneShutterMode = self.takephotoOverlayView.isPhoneShutterMode;
    self.takephotoOverlayView.timerLabel.hidden = YES;
    
}

- (void)libraryPressed
{
    [SVProgressHUD dismiss];
    
    [self dismissCameraView];
    [[ConnectionManager sharedInstance].snappet snappetStopTransfer];
    [self initVariable];
    
    PhotoLibraryViewController *pcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoLibraryViewController"];

    [self.navigationController pushViewController:pcontroller animated:YES];
//    [self.view addSubview:pcontroller.view];
}

- (void)selfiePressed
{
    [self.takephotoOverlayView clearPreview];
    if (self.takephotoOverlayView.isSelfie){
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }else{
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
}

- (void)flashPressed
{
    if (self.takephotoOverlayView.flashMode == FlashModeAuto){
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    }else if (self.takephotoOverlayView.flashMode == FlashModeOff){
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    }else if (self.takephotoOverlayView.flashMode == FlashModeOn) {
        self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
}

- (void)timerPressed
{

}

- (void)settingPressed
{
    [self dismissCameraView];
//    TestServiceViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TestServiceViewController"];
//    [self.navigationController pushViewController:controller animated:YES];

    //camtest
//    CamTestController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CamTestController"];
//    controller.snappet = [ConnectionManager sharedInstance].snappet;
//    controller.snappet.delegate = controller;
//    controller.snappet.autoReconnect = NO;     // Disable auto reconnect for now
//    [self.navigationController pushViewController:controller animated:YES];
    
    UserSettingViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"UserSettingViewController"];
    [self.navigationController pushViewController:controller animated:YES];

    
    //bootloader
//    BootloaderViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BootloaderViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
    
//    SettingsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
//
//    [self.view addSubview:controller.view];

//    [self.navigationController pushViewController:controller animated:YES];
}

- (void)sharePressed {
    NSMutableArray* objectsToShare = [NSMutableArray new];
    UIImage *img = nil;
    if (self.takephotoOverlayView.isPhoneShutterMode)
        img = self.takephotoOverlayView.phoneTakePhoto.image;
    else
        img = self.takephotoOverlayView.snappetTakenPhoto.image;
//    UIImage* img1 = [PhotoPreviewViewController saveSnappetOutputImageWithSource:img];
    NSString *pth = [[PhotoLibraryManager sharedInstance] saveImageToTmp:img];
    
    NSURL *url = [NSURL fileURLWithPath:pth];
    [objectsToShare addObject:url];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}

- (void)deletePressed {
    [[PhotoLibraryManager sharedInstance] deletePhotoWithIndex:(int)([[PhotoLibraryManager sharedInstance].imageList count]-1)];
    [self.takephotoOverlayView.phoneTakePhoto setImage:nil];
     [self.takephotoOverlayView.bottomBtnUIViewCover setHidden:YES];
}

-(void)cancelTimerPressed {
    [self.takephotoOverlayView.bottomBtnUIViewCover setHidden:YES];
    if (self.cameraTimer != nil) {
        self.takephotoOverlayView.timerLabel.hidden = YES;
        self.takephotoOverlayView.timerLabelSnap.hidden = YES;
        [self.cameraTimer invalidate];
        self.cameraTimer = nil;
        if (self.takephotoOverlayView.isPhoneShutterMode) {
            [self.takephotoOverlayView.viewPhotoModePinkBar setHidden:NO];
            [self.takephotoOverlayView.takePhotoSettingBtnView setHidden:NO];
        }
        else {
            self.takephotoOverlayView.viewSnappetTutorial.hidden = NO;
        }
        [self.view makeToast:[[LocalizationManager getInstance] getStringWithKey:@"Taking photo timer canceled"]];
    }
}

// To write the image to file //
-(void)writeImgToPath:(id)sender
{
    

    UIImage *image = sender;
    NSArray *pathArr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                           NSUserDomainMask,
                                                           YES);
    CGSize size;
    int currentProfileIndex = 1;
    NSString *path = [[pathArr objectAtIndex:0]
                      stringByAppendingPathComponent:[NSString stringWithFormat:@"Img_%d.png",currentProfileIndex]];
    
//    size = CGSizeMake(120, 120);
//    UIGraphicsBeginImageContext(size);
//    [image drawInRect:CGRectMake(0, 0, 120, 120)];
//    image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
    NSLog(@"Saved.....");
    
    CGRect r = CGRectMake(0, 0, 80, 80);
    UIGraphicsBeginImageContext(r.size);
    
    UIImage *img1;
    
    [image drawInRect:r];
    img1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
//    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
}

+ (NSString *) applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (void)flashView{
    self.takephotoOverlayView.flashView.hidden = YES;
}

- (void)resizePhoto
{
//
    
    //        NSData *webData = UIImagePNGRepresentation(self.capturedImg);
    //        [webData writeToFile:imagePath atomically:YES];
    
    UIImageOrientation orientation = self.capturedImg.imageOrientation;
    CGFloat x, y, side; //We will see how to calculate those values later.
    UIImage* imageCropped;
    side = MIN(self.capturedImg.size.width, self.capturedImg.size.height);
    x = self.capturedImg.size.width / 2 - side / 2;
    //        y = self.capturedImg.size.height / 2 - side / 2 - 70*2;
    y = 0;
    CGRect cropRect = CGRectMake(x,y,side,side);
    CGImageRef imageRef = CGImageCreateWithImageInRect([self.capturedImg CGImage], cropRect);
    imageCropped = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);

//    imageCropped = [UIImage imageWithCGImage:imageCropped.CGImage scale:imageCropped.scale orientation:orientation];

    if (self.takephotoOverlayView.isSelfie){
        orientation = UIImageOrientationLeftMirrored;
    }
    imageCropped = [self fixRotation:imageCropped withOrientation:orientation];
    
    //        [[PhotoLibraryManager sharedInstance] saveImage:imageCropped];
    
    //        [[NSFileManager defaultManager] createFileAtPath:imagePath
    //                                                contents:webData
    //                                              attributes:nil];
    
    //        NSMutableArray *imageDataList = [NSMutableArray new];
    //            [imageDataList addObject:webData];
    //        NSFileManager *fileManager = [NSFileManager defaultManager];
    //        [fileManager removeItemAtPath:paths error:NULL];
    //        [imageDataList writeToFile:paths atomically:YES];
    
    
    self.takephotoOverlayView.timerLabel.hidden = YES;
    self.takephotoOverlayView.takenPhotoSettingBtnView.hidden = NO;
    [self.takephotoOverlayView showPreview:imageCropped];

    self.takephotoOverlayView.photoOkBtn.hidden = NO;
    self.takephotoOverlayView.deleteBtn.hidden = NO;
    if (!self.takephotoOverlayView.isUnderTutorial)
        self.takephotoOverlayView.closeBtn.hidden = NO;
    self.takephotoOverlayView.takenPhotoSettingBtnView.hidden = NO;
}


#pragma mark - lock orientation
- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
//    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
//    [NSThread detachNewThreadSelector:@selector(writeImgToPath:) toTarget:self withObject:img];
    
//    NSString* paths = [PhotoLibraryManager applicationDocumentsDirectory];
//    NSString* paths = [[TakePhotoViewController applicationDocumentsDirectory] stringByAppendingPathComponent:@"snaplist"];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];

//    NSString *imagePath = [paths stringByAppendingFormat:@"/Snappet_%d.png",([PhotoLibraryManager sharedInstance].latestPhotoIndex + 1)];
    
//    NSString *imagePath = [[PhotoLibraryManager sharedInstance] getNextImagePath];
    
    [SVProgressHUD dismiss];
    [self.takephotoOverlayView.viewPhotoModePinkBar setHidden:NO];
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]){
        self.capturedImg = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self performSelectorOnMainThread:@selector(resizePhoto) withObject:nil waitUntilDone:YES];
        
    }
    
    if (self.isPhoneShutterMode){
        [[PhotoLibraryManager sharedInstance] saveImage:self.takephotoOverlayView.phoneTakePhoto.image];
    }
    
    [AppConfig sharedInstance].donePhotoCount += 1;
    
    if ( [AppConfig sharedInstance].donePhotoCount == 2 )
    {
        [self showModalViewControllerWithType:kModalScreenTypeRate success:^(BOOL rdy) {
            
        }];
    }
    
    if ( self.takephotoOverlayView.photoSetLevel > 0 )
    {
//        [self.takephotoOverlayView.viewPhotoModePinkBar setHidden:YES];
        [self.takephotoOverlayView setCameraButton:NO];
        self.takephotoOverlayView.phoneTakePhoto.image = nil;
        [self takePhoto];
    }
//    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//    NSDictionary *metadata = [info objectForKey:UIImagePickerControllerMediaMetadata];
//    [library writeImageToSavedPhotosAlbum:[image CGImage] metadata:metadata completionBlock:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIImage *)fixRotation:(UIImage *)image withOrientation:(UIImageOrientation) orientation
{
    if (orientation == UIImageOrientationUp)
    {
        return image;
    }
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (orientation)
    {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (orientation)
    {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (orientation)
    {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.height, image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

#pragma mark - private
- (void)resetPhoto {
    self.receivedData = [NSMutableData new];
    self.photoLength = 0;
    [self.takephotoOverlayView.snappetTakenPhoto setImage:nil];
//    [self.photoImage setImage:nil];
    [self.takephotoOverlayView.blobProgress setProgress:0 animated:NO];
//    [_lbProgress setText:@"0 / 0"];
}

#pragma mark - SnappetRobotDelegate
-(void) Snappet:(SnappetRobot *)snappet didReceiveButtonPressed:(kSnappetPressType)pressType {
    NSLog(@"TakePhotoViewController didReceiveButtonPressed: %d", pressType);
    if(pressType == kSnappetShortPress) {
//        [self.view makeToast:@"Short pressed"];
        
        [self takePhoto];
    }
    else if(pressType == kSnappetLongPress) {
//        [self.view makeToast:@"Long pressed"];
    }
    
    
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveNewPhoto:(uint8_t)photoId {
    NSLog(@"CamTest didReceiveNewPhoto: %d", photoId);
    [self.view makeToast:[NSString stringWithFormat:@"New photo with ID: %d", photoId]];
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoFull:(NSDate *)timestamp {
    NSLog(@"CamTest didReceivePhotoFull: %@", timestamp);
    [self.view makeToast:@"Photo full"];
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoLength:(int)photoLength checksum:(uint8_t)checksum {
//    [self.lbLength setText:[NSString stringWithFormat:@"Length: %d", photoLength]];
//    [self.lbChecksum setText:[NSString stringWithFormat:@"Checksum: %d", checksum]];
    [self resetPhoto];
    self.takephotoOverlayView.viewSnapPetDownloading.hidden = NO;
    self.takephotoOverlayView.blobProgress.hidden = NO;
    self.takephotoOverlayView.viewSnappetTutorial.hidden = YES;
    if (timerAnimatingDownloadImages == nil)
        timerAnimatingDownloadImages = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self.takephotoOverlayView selector:@selector(animateSnapPetsDownloading) userInfo:nil repeats:YES];
    self.photoLength = photoLength;
    [SVProgressHUD dismiss];
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoBlob:(NSData *)blobData {
    [_receivedData appendData:blobData];

    int receivedLength = (int)[_receivedData length];
    NSLog(@"TakePhotoViewController received data %d", receivedLength);
    float progress = (float)receivedLength / (float)_photoLength;
    
//    if (self.isTakePhoto){
//        self.takephotoOverlayView.viewSnapPetDownloading.hidden = NO;
//        self.isTakePhoto = NO;
//        self.takephotoOverlayView.blobProgress.hidden = NO;
//        self.takephotoOverlayView.viewSnappetTutorial.hidden = YES;
//        if (timerAnimatingDownloadImages == nil)
//            timerAnimatingDownloadImages = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self.takephotoOverlayView selector:@selector(animateSnapPetsDownloading) userInfo:nil repeats:YES];
//    }
    
    [self.takephotoOverlayView.blobProgress setProgress:progress animated:NO];
    
    [self.takephotoOverlayView.progressLabel setText:[NSString stringWithFormat:@"%d / %d", receivedLength, _photoLength]];
//
    if(receivedLength == _photoLength) {

          [self.takephotoOverlayView.bottomBtnUIViewCover setHidden:YES];
        NSLog(@"TakePhotoViewController Receive photo data finished. Creating image now..");

        NSMutableData *finalData = [NSMutableData new];
        NSData *headerData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jpgheader.dat" ofType:nil]];
        [finalData appendData:headerData];
        [finalData appendData:_receivedData];

        // Finish receiving photo
        UIImage *image = [UIImage imageWithData:finalData];
        //        UIImage *image = [UIImage imageWithData:_receivedData];
        if(!image) {
            NSLog(@"image = nil");
        }
        
        [SVProgressHUD dismiss];
        [self.takephotoOverlayView.snappetTakenPhoto setHidden:NO];
        [self.takephotoOverlayView.snappetTakenPhoto setImage:image];
        self.takephotoOverlayView.snapptOkBtn.hidden = NO;
        self.takephotoOverlayView.snapptRetakeBtn.hidden = NO;
        self.takephotoOverlayView.deleteSnappetBtn.hidden = NO;
        if (!self.takephotoOverlayView.isUnderTutorial)
            self.takephotoOverlayView.closeBtn.hidden = NO;
        
        self.isTakingPhotoFromSnap = NO;
        [timerAnimatingDownloadImages invalidate];
        timerAnimatingDownloadImages = nil;
        self.takephotoOverlayView.viewSnapPetDownloading.hidden = YES;
        
        [[PhotoLibraryManager sharedInstance] saveImage:self.takephotoOverlayView.snappetTakenPhoto.image];

//        [_photoImage setImage:image];
//
//        // Save data for debug purpose
//        //        [self saveReceivedDataAction:nil];
    }
    
    
}

-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
//    [self.imagePickerController dismissViewControllerAnimated:NO completion:nil];
//    [self.navigationController popToRootViewControllerAnimated:YES];
    
    if (!self.takephotoOverlayView.isPhoneShutterMode)
        [self.takephotoOverlayView modePressed:nil];
    
    [self.takephotoOverlayView showConnectionScreen];
    if (self.takephotoOverlayView.isInUserSettingController) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        self.takephotoOverlayView.isInUserSettingController = NO;
    }
    
    [self viewUnloadAnimation];
    
    [self.view makeToast:[[LocalizationManager getInstance] getStringWithKey:@"SnapPet Disconnected"]];
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveNumberOfPhoto:(int)numberOfPhoto {
    NSLog(@"TakePhotoViewController numOfPhoto = %d", numberOfPhoto);
    [PhotoLibraryManager sharedInstance].numOfPhotoInSnappet = numberOfPhoto;
    
    [[PhotoLibraryManager sharedInstance] loadPhotoFromSnappet];
}
//-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsPhotoQuality:(int)photoQuality{
//    self.photoQuality = photoQuality;
//}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingLargePhotoInfo:(kSnappetPhotoQuality)quality resolution:(kSnappetPhotoResolution)resolution{
    self.photoQuality = quality;
//    self.photoResolution.text = [NSString stringWithFormat:@"%d", resolution];
}
@end
