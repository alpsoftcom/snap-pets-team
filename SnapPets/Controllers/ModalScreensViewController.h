//
//  ModalScreensViewController.h
//  SnapPets
//
//  Created by Andrew Medvedev on 15/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ENUM(NSInteger, ModalScreenType)
{
    kModalScreenTypeRate,
    kModalScreenTypeBuyPet,
    kModalScreenTypePetFullStorage,
    kModalScreenTypePetLowBattery,
    kModalScreenTypePetConnecting,
    kModalScreenTypePetSearching,
    kModalScreenTypePetPin,
    kModalScreenTypePetName,
    kModalScreenTypePetInvalidPin
};

typedef void(^ModalScreenCallBackBlock)(BOOL rdyForRate);

@interface ModalScreensViewController : UIViewController

@property (copy, nonatomic) ModalScreenCallBackBlock callBackHandler;
@property (assign, nonatomic) enum ModalScreenType modalScreenType;

@end
