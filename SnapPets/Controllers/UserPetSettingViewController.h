//
//  UserSettingViewController.h
//  SnapPets
//
//  Created by David Chan on 5/5/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnapPetSettingPopupView.h"
#import "UserSettingViewControllerRedesign.h"
#import "ParentViewController.h"



@class UserSettingViewControllerRedesign;

@interface UserPetSettingViewController : ParentViewController <UITextFieldDelegate,SnappetRobotDelegate> {
  SnappetRobot* robot;
    int number;
}
@property (nonatomic, assign)SettingsCellType type;
@property (nonatomic, weak)IBOutlet UILabel * titleLabel;
@property (nonatomic, weak)IBOutlet UITextField * namepintextField;
@property (nonatomic, weak)IBOutlet UIButton * checkMarkButton;
@property (nonatomic, weak)IBOutlet UIImageView * mainImageView;

@property (nonatomic, weak)IBOutlet UIView * updateView;
@property (nonatomic, weak)IBOutlet UIImageView * updateImageView;
@property (nonatomic, weak)IBOutlet UILabel * updateLabel;
@property (nonatomic, weak)IBOutlet UIButton * updateCancelButton;

-(void)setPetSettingsMode:(SettingsCellType)celltype;
-(IBAction)backButtonPressed:(id)sender;
-(IBAction)checkmarkButton:(id)sender;

@end
