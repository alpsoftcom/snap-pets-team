//
//  BootloaderViewController.h
//  SnapPets
//
//  Created by Katy Pun on 28/1/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DFUOperations.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "ParentViewController.h"
//#import "SnappetRobot.h"

typedef enum : NSUInteger {
    BootloaderPrepareDFU = 0,
    BootloaderScanning,
    BootloaderConnect,
    BootloaderUpdate,
    BootloaderUpdateFinished,
} BootloaderState;

@interface BootloaderViewController : ParentViewController <UINavigationControllerDelegate, DFUOperationsDelegate, SnappetRobotDelegate>

@property (nonatomic, strong) SnappetRobot *snappet;
@property (nonatomic, assign) BootloaderState bootloaderState;

@end
