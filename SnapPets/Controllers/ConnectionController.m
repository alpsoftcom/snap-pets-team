//
//  ConnectionController.m
//  MipPhotoTest
//
//  Created by Forrest Chan on 26/9/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "ConnectionController.h"
#import "DeviceCell.h"
#import "SelectCamController.h"
//#import "SnappetRobot.h"
//#import "SnappetRobotPrivate.h"
#import "CamTestController.h"
#import "GoldenImageManager.h"
#import "PhotoLibraryViewController.h"
#import "TestServiceViewController.h"
#import "ConnectionManager.h"
#import "TakePhotoViewController.h"
#import "SVProgressHUD.h"

#import "BluetoothConnectionManager.h"
#import "AppConfig.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

#define IS_RATIO_3_2    (( double )[[UIScreen mainScreen] bounds].size.height / [[UIScreen mainScreen] bounds].size.width == ( double )3 / 2)

@interface ConnectionController ()

@property(nonatomic, strong) UARTPeripheral *currentPeripheral;

@end

@implementation ConnectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.currentPeripheral = nil;
    self.deviceList = [NSMutableArray new];
    
    // config CBCentralManager
    self.cm = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    NSString *versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.lbVersion.text = [NSString stringWithFormat:@"App version: %@  ", versionString];
    
    
    if (IS_RATIO_3_2){
        CGRect rect = self.deviceTable.frame;
        rect.size.height -= 88;
        self.deviceTable.frame = rect;
        rect = self.lbVersion.frame;
        rect.origin.y -= 88;
        self.lbVersion.frame = rect;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSnappetRobotFinderNotification:) name:SnappetRobotFinderNotificationID object:nil];
    [[SnappetRobotFinder sharedInstance] cbCentralManagerState];
    [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
    
    if(self.currentPeripheral) {
        [self.cm cancelPeripheralConnection:self.currentPeripheral.peripheral];
        self.currentPeripheral = nil;
    }
  
    // Start scanning
    [self startScan];
    
    //testing
    [self checkForExistingScreenAndInitializeIfPresent];
}

-(void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SnappetRobotFinderNotificationID object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)showImageToTV {
    UIScreen *externalScreen = [[UIScreen screens] objectAtIndex:1];
    UIWindow *externalWindow = [[UIWindow alloc] initWithFrame:externalScreen.bounds];
    externalWindow.screen = externalScreen;
    
    // This is a custom constructor that takes in the window size and uses
    // it to size the controller's view on load. Accessing the view's
    // window size on load didn't work for us.
//    ImageTVOutViewController *externalViewController = [[ImageTVOutViewController alloc] initWithFrame:externalWindow.frame];
    
//    externalWindow.rootViewController = externalViewController;
//    externalWindow.hidden = NO;
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (IBAction)settingPressed:(id)sender {
}

#pragma mark - AirPlay and extended display

- (void)checkForExistingScreenAndInitializeIfPresent
{
    if ([[UIScreen screens] count] > 1)
    {
        
        // Get the screen object that represents the external display.
        UIScreen *secondScreen = [[UIScreen screens] lastObject];
        secondScreen.overscanCompensation = 2;//UIScreenOverscanCompensationScale;
        
        // Get the screen's bounds so that you can create a window of the correct size.
        UIScreenMode *screenMode = [[secondScreen availableModes] lastObject];
        CGRect screenBounds = (CGRect){.size = screenMode.size};
        
        self.secondWindow = [[UIWindow alloc] initWithFrame:screenBounds];
        self.secondWindow.screen = secondScreen;
        
        UIImage *img = [UIImage imageNamed:@"cover.png"];
        
//        self.secondWindow.rootViewController = externalViewController;

        img = [self scaleToSizeKeepAspect:img.size tvSize:screenBounds.size imgView:img];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        [self.secondWindow addSubview:imgView];
        
        
        // Set up initial content to display...
//        self.secondWindow.backgroundColor = [UIColor redColor];
        
        
        // Show the window.
        self.secondWindow.hidden = NO;
    }
}

- (UIImage*)scaleToSizeKeepAspect:(CGSize)size tvSize:(CGSize)tvSize imgView:(UIImage *)img
{
    UIGraphicsBeginImageContext( tvSize );
    
//    float ws = size.width/tvSize.width;
//    float hs = size.height/tvSize.height;
    CGFloat ws = tvSize.width/size.width;
    CGFloat hs = tvSize.height/size.height;
    
    CGFloat ratio = MIN(ws, hs);
    [img drawInRect:CGRectMake(tvSize.width/2-(size.width*ratio)/2,
                                     tvSize.height/2-(size.height*ratio)/2, size.width*ratio,
                                     size.height*ratio)];

    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - lock orientation
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - private
- (void)startScan {
//    [self.cm scanForPeripheralsWithServices:@[UARTPeripheral.uartServiceUUID] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: [NSNumber numberWithBool:NO]}];
    [[SnappetRobotFinder sharedInstance] scanForSnappets];
//    [[BluetoothConnectionManager sharedInstance] startScan];
}

- (void)stopScan {
//    [self.cm stopScan];
    [[SnappetRobotFinder sharedInstance] stopScanForSnappets];
}

- (void)_timerFired {
    [SVProgressHUD dismiss];
    
    [self.connectionTimer invalidate];
    self.connectionTimer = nil;
}

#pragma mark - button actions
- (IBAction)refreshAction:(id)sender {
//    PhotoLibraryViewController *pcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoLibraryViewController"];
//    [self.navigationController pushViewController:pcontroller animated:YES];
    
#if !defined(TARGET_IPHONE_SIMULATOR) || TARGET_IPHONE_SIMULATOR == 0
    [self stopScan];
    [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
    [self.deviceList removeAllObjects];
    [self.deviceTable reloadData];
    self.scanLabel.hidden = NO;
    [self startScan];
//    [[BluetoothConnectionManager sharedInstance] startScan];
#else
//    CamTestController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CamTestController"];
//    [self.navigationController pushViewController:controller animated:YES];
    
    TakePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TakePhotoViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    
    
//    SelectCamController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCamController"];
//    //    controller.currentPeripheral = self.currentPeripheral;
//    self.currentPeripheral.delegate = controller;
//    [self.navigationController pushViewController:controller animated:YES];

#endif
}

- (IBAction)manageGoldenImageAction:(id)sender {
    [[GoldenImageManager sharedInstance] showGoldenImageManagerInController:self passwordProtected:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[SnappetRobotFinder sharedInstance] snappetsFound] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DeviceCell *cell = [[DeviceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"spdevicecell"];
    
//    CBPeripheral *peripheral = [self.deviceList objectAtIndex:indexPath.row];
//    cell.peripheral = peripheral;
    
    SnappetRobot *snappet = [[SnappetRobotFinder sharedInstance] snappetsFound][indexPath.row];
    cell.snappet = snappet;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return DeviceCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self stopScan];
    
    SnappetRobot *snappet = [[SnappetRobotFinder sharedInstance] snappetsFound][indexPath.row];
    snappet.delegate = self;
    [snappet connect];
    
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:239.0f/255.0f green:70.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD show];
    
    self.connectionTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                       target:self
                                                     selector:@selector(_timerFired)
                                                     userInfo:nil
                                                      repeats:NO];
    
//    CBPeripheral *peripheral = [self.deviceList objectAtIndex:indexPath.row];
//    self.currentPeripheral = [[UARTPeripheral alloc] initWithPeripheral:peripheral delegate:self];
//    [self.cm connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: [NSNumber numberWithBool:YES]}];
}

#pragma mark - UARTPeripheralDelegate
- (void) didReceiveData:(NSString *) string {}
- (void) didReceiveImage:(UIImage *)image {}
- (void) didReceiveByteProgress:(int)currentByteLength fullLength:(int)fullByteLength {}
- (void) didProcessImageDataFailed {}

#pragma mark - CBCentralManagerDelegate
- (void) centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state == CBCentralManagerStatePoweredOn) {
        [self startScan];
    }
}

- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Did discover peripheral %@", peripheral.name);
//    [self.cm stopScan];
//    self.currentPeripheral = [[UARTPeripheral alloc] initWithPeripheral:peripheral delegate:self];
//    [self.cm connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: [NSNumber numberWithBool:YES]}];
    
//    self.scanLabel.hidden = YES;
//    [self.deviceList addObject:peripheral];
//    [self.deviceTable reloadData];
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Did connect peripheral %@", peripheral.name);
    
    if ([self.currentPeripheral.peripheral isEqual:peripheral]) {
        [self.currentPeripheral didConnect];
    }
    
    SelectCamController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCamController"];
//    controller.currentPeripheral = self.currentPeripheral;
    self.currentPeripheral.delegate = controller;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Did disconnect peripheral %@", peripheral.name);
    if ([self.currentPeripheral.peripheral isEqual:peripheral]) {
        [self.currentPeripheral didDisconnect];
        self.currentPeripheral = nil;
    }
    
    if([self.navigationController.topViewController isKindOfClass:[SelectCamController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - SnappetRobotFinder callback
- (void)onSnappetRobotFinderNotification: (NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    if(info){
        NSNumber *code = [info objectForKey: @"code"];
        //id data = [info objectForKey: @"data"];
        if (code.intValue == SnappetRobotFinderNote_SnappetFound){
            self.deviceList = [[SnappetRobotFinder sharedInstance] snappetsFound];
            [self.deviceTable reloadData];
            self.scanLabel.hidden = YES;
        } else if (code.intValue == SnappetRobotFinderNote_SnappetListCleared) {
            self.deviceList = [[SnappetRobotFinder sharedInstance] snappetsFound];
            [self.deviceTable reloadData];
            self.scanLabel.hidden = NO;
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothError) {
            NSLog(@"Bluetooth error!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsOff) {
            NSLog(@"Bluetooth is off!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsAvailable) {
            NSLog(@"SnappetRobotFinderNote_BluetoothIsAvailable");
        }
    }
}

#pragma mark - SnappetRobotDelegate
-(void) SnappetDeviceReady:(SnappetRobot *)snappet {
    NSLog(@"SnappetDeviceReady");
//    SelectCamController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCamController"];
//    controller.snappet = snappet;
//    [self.navigationController pushViewController:controller animated:YES];

     //   [NSThread sleepForTimeInterval:2];
    
//    CamTestController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CamTestController"];
//    controller.snappet = snappet;
//    snappet.delegate = controller;
//    snappet.autoReconnect = NO;     // Disable auto reconnect for now
//    [self.navigationController pushViewController:controller animated:YES];
    

//    TestServiceViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TestServiceViewController"];
//    controller.snappet = snappet;
//    snappet.delegate = controller;
//    snappet.autoReconnect = NO;     // Disable auto reconnect for now
//    [self.navigationController pushViewController:controller animated:YES];
    
    TakePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TakePhotoViewController"];

    [self.navigationController pushViewController:controller animated:YES];
    
    [ConnectionManager sharedInstance].snappet = snappet;
    [ConnectionManager sharedInstance].snappet.autoReconnect = NO;
    
    [self.connectionTimer invalidate];
    self.connectionTimer = nil;
    
    [SVProgressHUD dismiss];
}


@end
