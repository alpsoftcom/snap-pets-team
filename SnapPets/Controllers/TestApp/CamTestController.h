//
//  CamTestController.h
//  SnapPets
//
//  Created by Forrest Chan on 27/11/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
//#import "SnappetRobot.h"
#import "GoldenImageController.h"
#import "ParentViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface CamTestController : ParentViewController <SnappetRobotDelegate, UITextFieldDelegate, GoldenImageControllerDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) SnappetRobot *snappet;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *lbNumOfPhoto;
@property (weak, nonatomic) IBOutlet UITextField *tfGetPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbLength;
@property (weak, nonatomic) IBOutlet UILabel *lbChecksum;
@property (weak, nonatomic) IBOutlet UIProgressView *blobProgress;
@property (weak, nonatomic) IBOutlet UILabel *lbProgress;
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property (weak, nonatomic) IBOutlet UIButton *btnGoldenImage;
@property (weak, nonatomic) IBOutlet UITextField *tfMarking;
@property (weak, nonatomic) IBOutlet UILabel *firmwareVersion;

@property (weak, nonatomic) IBOutlet UIView *extraView;

@property (strong, nonatomic) UIScrollView *imageScrollView;
@property (strong, nonatomic) UIImageView *snappetImageLarge;
@property (strong, nonatomic) UIImageView *goldenImageLarge;

@end
