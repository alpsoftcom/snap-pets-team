//
//  GoldenImageManager.m
//  SnapPets
//
//  Created by Forrest Chan on 8/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "GoldenImageManager.h"

#define SNAPPET_QAQC_PASSWORD    @"wwqa2014"

@interface GoldenImageManager ()

@property (nonatomic, strong) NSMutableArray *imageList;
@property (nonatomic, strong) NSString *imageListPath;
@property (nonatomic, strong) UIViewController *parentController;
@property (nonatomic, strong) NSMutableDictionary *goldenImageSettings;
@property (nonatomic, assign) int selectedIndex;

@end

@implementation GoldenImageManager

static GoldenImageManager *instance = nil;

+ (GoldenImageManager *)sharedInstance {
    if(!instance) {
        instance = [GoldenImageManager new];
    }
    return instance;
}

- (id)init {
    self = [super init];
    
    self.imageListPath = [[GoldenImageManager applicationDocumentsDirectory] stringByAppendingPathComponent:@"gilist"];
    NSLog(@"imageListPath = %@", _imageListPath);
    NSMutableArray *imageDataList = [NSMutableArray arrayWithContentsOfFile:_imageListPath];
    self.imageList = [NSMutableArray new];
    if(!imageDataList) {
        // TODO: Remove late
        // Insert testing images
        [_imageList addObject:[UIImage imageNamed:@"golden_01.jpg"]];
//        [_imageList addObject:[UIImage imageNamed:@"golden_02.jpg"]];
//        [_imageList addObject:[UIImage imageNamed:@"golden_03.jpg"]];
        [self save];
    }
    else {
        for(NSData *imgData in imageDataList) {
            [_imageList addObject:[UIImage imageWithData:imgData]];
        }
    }
    
    // get golden image settings
    NSString *goldenImageSettingsPath = [[GoldenImageManager applicationDocumentsDirectory] stringByAppendingPathComponent:@"gisettings"];
    self.goldenImageSettings = [[NSMutableDictionary alloc] initWithContentsOfFile:goldenImageSettingsPath];
    if([_goldenImageSettings objectForKey:@"selectedIndex"] != nil) {
        self.selectedIndex = [_goldenImageSettings[@"selectedIndex"] intValue];
    }
    
    return self;
}

+ (NSString *) applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark - private
- (void)save {
    NSMutableArray *imageDataList = [NSMutableArray new];
    for(UIImage *img in _imageList) {
        [imageDataList addObject:UIImagePNGRepresentation(img)];
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:_imageListPath error:NULL];
    [imageDataList writeToFile:_imageListPath atomically:YES];
}

#pragma mark - public
- (void)showGoldenImageManagerInController:(UIViewController *)controller passwordProtected:(BOOL)passwordProtected {
    self.parentController = controller;
    UIAlertView *passwordAlert = [[UIAlertView alloc] initWithTitle:@"Manage Golden Photos"
                                                        message:@"Please fill in the password"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Confirm", nil];
    passwordAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    passwordAlert.tag = 10;
    [passwordAlert show];
}

- (void)removeImage:(UIImage *)image {
    [_imageList removeObject:image];
    [self save];
}

- (void)addImage:(UIImage *)image {
    [_imageList addObject:image];
    [self save];
}

- (NSMutableArray *)getGoldenImages {
    return _imageList;
}

- (void)setGoldenImageIndex:(int)giIndex {
    self.selectedIndex = giIndex;
    [_goldenImageSettings setObject:[NSNumber numberWithInt:_selectedIndex] forKey:@"selectedIndex"];
    NSString *goldenImageSettingsPath = [[GoldenImageManager applicationDocumentsDirectory] stringByAppendingPathComponent:@"gisettings"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:goldenImageSettingsPath error:NULL];
    [_goldenImageSettings writeToFile:goldenImageSettingsPath atomically:YES];
}

- (UIImage *)getSelectedGoldenImage {
    if(_selectedIndex < [_imageList count]) {
        return _imageList[_selectedIndex];
    }
    return nil;
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 10 && buttonIndex == 1) {
        UITextField *textField = [alertView textFieldAtIndex:0];
        if([textField.text isEqualToString:SNAPPET_QAQC_PASSWORD]) {
            // Proceed to Golden Image Controller
            UIViewController *controller = [self.parentController.storyboard instantiateViewControllerWithIdentifier:@"GoldenImageController"];
            [self.parentController.navigationController pushViewController:controller animated:YES];
        }
        else {
            UIAlertView *incorrectPasswordAlert = [[UIAlertView alloc] initWithTitle:@"Manage Golden Photos"
                                                                    message:@"Incorrect password"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Close"
                                                          otherButtonTitles:nil];
            [incorrectPasswordAlert show];
        }
    }
}

@end
