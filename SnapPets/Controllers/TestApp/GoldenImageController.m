//
//  GoldenImageController.m
//  SnapPets
//
//  Created by Forrest Chan on 4/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "GoldenImageController.h"
#import "GoldenImageManager.h"
#import "UIAlertView+Blocks.h"

@interface GoldenImageController ()

@property (nonatomic, strong) NSMutableArray *imageList;
@property (nonatomic, strong) MWPhotoBrowser *photoBrowser;
@property (nonatomic, assign) NSUInteger currentImageIndex;

@end

@implementation GoldenImageController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Get image list
    self.imageList = [[GoldenImageManager sharedInstance] getGoldenImages];
    
    // Create photo browser
    self.photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    _photoBrowser.displayActionButton = YES;
    _photoBrowser.displayNavArrows = YES;
    _photoBrowser.displaySelectionButtons = NO;
    _photoBrowser.alwaysShowControls = NO;
    _photoBrowser.zoomPhotosToFill = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    _photoBrowser.wantsFullScreenLayout = YES;
#endif
    _photoBrowser.enableGrid = YES;
    _photoBrowser.startOnGrid = YES;
    _photoBrowser.enableSwipeToDismiss = YES;
    [_photoBrowser setCurrentPhotoIndex:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Add browser
    [self.view addSubview:_photoBrowser.view];
    
    [self.view bringSubviewToFront:self.btnBack];
    [self.view bringSubviewToFront:self.btnDelete];
    [self.view bringSubviewToFront:self.btnAdd];
    
    if(_pickerMode) {
        self.btnAdd.hidden = YES;
        [self.btnDelete setTitle:@"Select" forState:UIControlStateNormal];
    }
    else {
        [self.btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
    }
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return NO;
}

#pragma mark - button actions
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteAction:(id)sender {
    if(_pickerMode) {
        // Photo picker mode
        if([_delegate respondsToSelector:@selector(didPickImage:withIndex:)]) {
            [_delegate didPickImage:_imageList[_currentImageIndex] withIndex:(int)_currentImageIndex];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [UIAlertView showWithTitle:@"Delete Golden Image"
                           message:@"Confirm delete?"
                 cancelButtonTitle:@"NO"
                 otherButtonTitles:@[@"YES"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"YES"]) {
                                  // Delete golden image
                                  [[GoldenImageManager sharedInstance] removeImage:_imageList[_currentImageIndex]];
                                  self.imageList = [[GoldenImageManager sharedInstance] getGoldenImages];
                                  [_photoBrowser reloadData];
                              }
                           }];
    }
}

- (IBAction)addAction:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:NULL];
//    NSLog(@"info = %@", info);
    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    if(!image) {
//        NSURL *imageURL = info[UIImagePickerControllerReferenceURL];
//        NSLog(@"imageURL = %@", imageURL);
//        NSData *data = [NSData dataWithContentsOfURL:imageURL];
//        image = [UIImage imageWithData:data];
//    }
    if(image) {
        [[GoldenImageManager sharedInstance] addImage:image];
        [_photoBrowser reloadData];
    }
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return [_imageList count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return [MWPhoto photoWithImage:_imageList[index]];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    return [MWPhoto photoWithImage:_imageList[index]];
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    self.currentImageIndex = index;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser gridViewHidden:(BOOL)gridViewHidden {
    if(gridViewHidden) {
        self.btnBack.hidden = YES;
        self.btnAdd.hidden = YES;
        self.btnDelete.hidden = NO;
    }
    else {
        self.btnBack.hidden = NO;
        self.btnAdd.hidden = NO;
        self.btnDelete.hidden = YES;
    }
    
    if(_pickerMode) {
        self.btnAdd.hidden = YES;
    }
}

@end
