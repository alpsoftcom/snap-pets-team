//
//  GoldenImageManager.h
//  SnapPets
//
//  Created by Forrest Chan on 8/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoldenImageManager : NSObject <UIAlertViewDelegate>

+ (GoldenImageManager *)sharedInstance;

#pragma mark - private
- (void)save;

#pragma mark - public
- (void)showGoldenImageManagerInController:(UIViewController *)controller passwordProtected:(BOOL)passwordProtected;
- (void)removeImage:(UIImage *)image;
- (void)addImage:(UIImage *)image;
- (NSMutableArray *)getGoldenImages;
- (void)setGoldenImageIndex:(int)giIndex;
- (UIImage *)getSelectedGoldenImage;

@end
