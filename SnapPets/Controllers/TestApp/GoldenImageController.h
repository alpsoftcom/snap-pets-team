//
//  GoldenImageController.h
//  SnapPets
//
//  Created by Forrest Chan on 4/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MWPhotoBrowser.h"
#import "ParentViewController.h"

@protocol GoldenImageControllerDelegate <NSObject>

@optional
- (void)didPickImage:(UIImage *)image withIndex:(int)index;

@end

@interface GoldenImageController : ParentViewController </*MWPhotoBrowserDelegate,*/ UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, assign) BOOL pickerMode;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (nonatomic, strong) id<GoldenImageControllerDelegate> delegate;

@end
