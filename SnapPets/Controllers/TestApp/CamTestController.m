//
//  CamTestController.m
//  SnapPets
//
//  Created by Forrest Chan on 27/11/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "CamTestController.h"
#import "UIView+Toast.h"
#import "GoldenImageManager.h"
#import "SVProgressHUD.h"
//#import "UIAlertView+Blocks.h"
#import "BootloaderViewController.h"

@interface CamTestController ()

@property (nonatomic, assign) int photoLength;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, assign) kSnappetPhotoQuality photoQuality;

@end

@implementation CamTestController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self refreshGoldenImage];
//    [_snappet snappetReadPhotoQuality];
    [_snappet snappetReadLargeSizePhotoInfo];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_scrollView setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _photoImage.frame.origin.y + _photoImage.frame.size.height)];
    
    self.firmwareVersion.text = [NSString stringWithFormat:@"firm %@", self.snappet.bleModuleSoftwareVersion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSLog(@"willAnimateRotationToInterfaceOrientation");
    if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        // Landscape
        if(!(self.imageScrollView)) {
            self.imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            [self.view addSubview:_imageScrollView];
            
            self.snappetImageLarge = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            _snappetImageLarge.contentMode = UIViewContentModeScaleAspectFit;
            _snappetImageLarge.backgroundColor = [UIColor blackColor];
            [_imageScrollView addSubview:_snappetImageLarge];
            
            self.goldenImageLarge = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
            _goldenImageLarge.contentMode = UIViewContentModeScaleAspectFit;
            _goldenImageLarge.backgroundColor = [UIColor blackColor];
            [_imageScrollView addSubview:_goldenImageLarge];
            
            [_imageScrollView setContentSize:CGSizeMake(self.view.frame.size.width*2, self.view.frame.size.height)];
            _imageScrollView.scrollEnabled = YES;
            
            // Create labels
            UILabel *lbGolden = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
            lbGolden.backgroundColor = [UIColor clearColor];
            lbGolden.textColor = [UIColor whiteColor];
            [lbGolden setText:@"Golden sample"];
            [_goldenImageLarge addSubview:lbGolden];
            
            UILabel *lbTarget = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
            lbTarget.backgroundColor = [UIColor clearColor];
            lbTarget.textColor = [UIColor whiteColor];
            [lbTarget setText:@"Target"];
            [_snappetImageLarge addSubview:lbTarget];
        }
        
        UIImage *goldenImage = [[GoldenImageManager sharedInstance] getSelectedGoldenImage];
        [_goldenImageLarge setImage:goldenImage];
        [_snappetImageLarge setImage:_photoImage.image];
        
        _imageScrollView.hidden = NO;
    }
    else {
        // Portrait
        _imageScrollView.hidden = YES;
    }
}

#pragma mark - private
- (void)resetPhoto {
    self.receivedData = [NSMutableData new];
    self.photoLength = 0;
    [self.photoImage setImage:nil];
    [_blobProgress setProgress:0 animated:NO];
    [_lbProgress setText:@"0 / 0"];
}

- (void)refreshGoldenImage {
    UIImage *goldenImage = [[GoldenImageManager sharedInstance] getSelectedGoldenImage];
    if(goldenImage) {
        [_btnGoldenImage setImage:goldenImage forState:UIControlStateNormal];
    }
}

-(void)createPDFfromUIView:(UIView*)aView {
    if(![MFMailComposeViewController canSendMail]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Please setup an email account first." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSString *fileName = [NSString stringWithFormat:@"Snappets_test_case_%@.pdf", [_tfMarking text]];
    
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    [aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    // instructs the mutable data object to write its context to a file on disk
//    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
//    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
//    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:fileName];
//    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
//    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
    
    // Mail composer
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setSubject:[NSString stringWithFormat:@"Snet Pets QA testing case #%@", [_tfMarking text]]];
    [picker setMessageBody:[NSString stringWithFormat:@"Snappets test case marking #%@", [_tfMarking text]] isHTML:NO];
    [picker addAttachmentData:pdfData mimeType:@"application/pdf" fileName:fileName];
    [picker addAttachmentData:UIImageJPEGRepresentation([[GoldenImageManager sharedInstance] getSelectedGoldenImage], 1) mimeType:@"image/jpeg" fileName:@"golden_image.jpg"];
    if(_photoImage.image != nil) {
        [picker addAttachmentData:UIImageJPEGRepresentation(_photoImage.image, 1) mimeType:@"image/jpeg" fileName:@"snappets_image.jpg"];
    }
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)dismissLoading {
    [SVProgressHUD dismiss];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - button actions
- (IBAction)readNumOfPhotosAction:(id)sender {
    [self.snappet snappetReadNumberOfPhotos];
}

- (IBAction)getPhotoAction:(id)sender {
    int photoId = [_tfGetPhoto.text intValue];
    uint8_t photoIdByte = (uint8_t)photoId;
    [self resetPhoto];
    [self.snappet snappetGetPhoto:kSnappetPhotoLargeSize photoID:photoIdByte];
}

- (IBAction)deletePhotoAction:(id)sender {
    int photoId = [_tfGetPhoto.text intValue];
    uint8_t photoIdByte = (uint8_t)photoId;
    [self.snappet snappetDeletePhoto:photoIdByte];
}

- (IBAction)takePhotoAndSaveToFlashAction:(id)sender {
    [self.snappet snappetTakePhotoAndSaveToFlash];
}

- (IBAction)takePhotoOnlyAction:(id)sender {
    uint8_t quality = self.photoQuality;
    [self.snappet snappetTakePhotoWithoutSavingToFlash:kSnappetPhotoResolutionVGA quality:quality];
    
    // Show loading for 1.5 seconds
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self performSelector:@selector(dismissLoading) withObject:nil afterDelay:1.5f];
}

- (IBAction)deleteAllPhotoAction:(id)sender {
    [self.snappet snappetDeleteAllPhoto];
}

- (IBAction)stopTransferAction:(id)sender {
    [self.snappet snappetStopTransfer];
}

- (IBAction)disconnectAction:(id)sender {
//    [self.snappet disconnect];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveReceivedDataAction:(id)sender {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd_MM_yyyy_hh_mm_ss"];
    NSString *fileName = [NSString stringWithFormat:@"image_%@.jpg", [dateFormatter stringFromDate:[NSDate date]]];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    [_receivedData writeToFile:filePath atomically:YES];
    NSLog(@"Save image as: %@", [NSString stringWithFormat:@"image_%@.jpg", [dateFormatter stringFromDate:[NSDate date]]]);
}

- (IBAction)manageGoldenImagesAction:(id)sender {
    [[GoldenImageManager sharedInstance] showGoldenImageManagerInController:self passwordProtected:YES];
}

- (IBAction)goldenImageAction:(id)sender {
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"GoldenImageController"];
    ((GoldenImageController *)controller).pickerMode = YES;
    ((GoldenImageController *)controller).delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)moreAction:(id)sender {
    _extraView.hidden = !(_extraView.hidden);
    if(_extraView.hidden) {
        [((UIButton *)sender) setTitle:@"More" forState:UIControlStateNormal];
    }
    else {
        [((UIButton *)sender) setTitle:@"Less" forState:UIControlStateNormal];
    }
}

- (IBAction)exportPDFAction:(id)sender {
    [self createPDFfromUIView:_scrollView];
}

- (IBAction)saveToPhotoAlbumAction:(id)sender {
    if(_photoImage.image) {
        UIImageWriteToSavedPhotosAlbum(_photoImage.image, self, @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), NULL);
    }
    else {
//        [UIAlertView showWithTitle:@"Cannot save"
//                           message:@"You must take a photo first"
//                 cancelButtonTitle:@"Close"
//                 otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                     
//                 }];
    }
}

- (IBAction)powerOffAction:(id)sender {
    [_snappet snappetReboot:kSnappetRebootPowerOff];
}

- (IBAction)sleepAction:(id)sender {
    [_snappet snappetReboot:kSnappetRebootSleep];
}

- (IBAction)deepSleepAction:(id)sender {
    
//    [UIAlertView showWithTitle:@"Warning"
//                       message:@"Please reset first before go to deep sleep mode"
//             cancelButtonTitle:@"Close"
//             otherButtonTitles:@[@"Deep sleep"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                 if (buttonIndex == 1){
//                     [_snappet snappetReboot:kSnappetDeepSleep];
//                     [_snappet disconnect];
//                 }
//             }];
    
    
}
- (IBAction)setPhotoLowQuality:(id)sender {
    self.photoQuality = kSnappetPhotoQualityLow;
//    [_snappet snappetSetPhotoQuality:0];
    [_snappet snappetSetLargeSizePhotoInfo:kSnappetPhotoQualityLow resolution:kSnappetPhotoResolutionVGA];
    [self.view makeToast:@"Low photo quality"];
}

- (IBAction)setPhotoHighQuality:(id)sender {
    self.photoQuality = kSnappetPhotoQualityHigh;
//    [_snappet snappetSetPhotoQuality:100];
    [_snappet snappetSetLargeSizePhotoInfo:kSnappetPhotoQualityHigh resolution:kSnappetPhotoResolutionVGA];
    [self.view makeToast:@"High photo quality"];
}

- (IBAction)firmwareUpdateAction:(id)sender {
//    [_snappet rebootToMode:kDeviceDFUMode];
    BootloaderViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BootloaderViewController"];
    controller.snappet = _snappet;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Save photo
- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
    if (error) {
//        [UIAlertView showWithTitle:@"Save error"
//                           message:[NSString stringWithFormat:@"Failed with error: %@", error]
//                 cancelButtonTitle:@"Close"
//                 otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                     
//                 }];
    } else {
//        [UIAlertView showWithTitle:@"Save success"
//                           message:@"Photo has been saved to your photo album"
//                 cancelButtonTitle:@"Close"
//                 otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                     
//                 }];
    }
}

- (IBAction)testBootloaderMode:(id)sender {
    [_snappet snappetReboot:kSnappetRebootDFU];
}

#pragma mark - GoldenImageControllerDelegate
- (void)didPickImage:(UIImage *)image withIndex:(int)index {
    [[GoldenImageManager sharedInstance] setGoldenImageIndex:index];
    [_btnGoldenImage setImage:image forState:UIControlStateNormal];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0) {
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - SnappetRobotDelegate
-(void) Snappet:(SnappetRobot *)snappet didReceiveButtonPressed:(kSnappetPressType)pressType {
    NSLog(@"CamTest didReceiveButtonPressed: %d", pressType);
    if(pressType == kSnappetShortPress) {
        [self.view makeToast:@"Short pressed"];
    }
    else if(pressType == kSnappetLongPress) {
        [self.view makeToast:@"Long pressed"];
    }
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveNewPhoto:(uint8_t)photoId {
    NSLog(@"CamTest didReceiveNewPhoto: %d", photoId);
    [self.view makeToast:[NSString stringWithFormat:@"New photo with ID: %d", photoId]];
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoFull:(NSDate *)timestamp {
    NSLog(@"CamTest didReceivePhotoFull: %@", timestamp);
    [self.view makeToast:@"Photo full"];
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveNumberOfPhoto:(int)numberOfPhoto {
    [self.lbNumOfPhoto setText:[NSString stringWithFormat:@"%d", numberOfPhoto]];
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoLength:(int)photoLength checksum:(uint8_t)checksum {
    [self.lbLength setText:[NSString stringWithFormat:@"Length: %d", photoLength]];
    [self.lbChecksum setText:[NSString stringWithFormat:@"Checksum: %d", checksum]];
    [self resetPhoto];
    self.photoLength = photoLength;
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoBlob:(NSData *)blobData {
    [_receivedData appendData:blobData];
    int receivedLength = (int)[_receivedData length];
    float progress = (float)receivedLength / (float)_photoLength;
    [_blobProgress setProgress:progress animated:NO];
    [_lbProgress setText:[NSString stringWithFormat:@"%d / %d", receivedLength, _photoLength]];
    
    if(receivedLength == _photoLength) {
        NSLog(@"Receive photo data finished. Creating image now..");
        NSMutableData *finalData = [NSMutableData new];
        NSData *headerData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jpgheader.dat" ofType:nil]];
        [finalData appendData:headerData];
        [finalData appendData:_receivedData];
        
        // Finish receiving photo
        UIImage *image = [UIImage imageWithData:finalData];
//        UIImage *image = [UIImage imageWithData:_receivedData];
        if(!image) {
            NSLog(@"image = nil");
        }
        [_photoImage setImage:image];
        
        // Save data for debug purpose
//        [self saveReceivedDataAction:nil];
    }
}

-(void) Snappet:(SnappetRobot *)snappet deletedPhoto:(NSArray *)photoIds {
    // Toast deleted photo
    if([photoIds count] > 0) {
        NSNumber *photoId = photoIds[0];
        int value = (int)[photoId charValue];
        [self.view makeToast:[NSString stringWithFormat:@"Deleted photo with ID: %d", value]];
    }
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsPhotoQuality:(int)photoQuality{
    self.photoQuality = photoQuality;
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingLargePhotoInfo:(kSnappetPhotoQuality)quality resolution:(kSnappetPhotoResolution)resolution{
    self.photoQuality = quality;
}

-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
