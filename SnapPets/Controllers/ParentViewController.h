//
//  ParentViewController.h
//  SnapPets
//
//  Created by Andrew Medvedev on 15/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalScreensViewController.h"

@interface ParentViewController : UIViewController

- (void)showModalViewControllerWithType:(enum ModalScreenType)type success:(void(^)(BOOL rdy))success;

@end
