//
//  ParentViewController.m
//  SnapPets
//
//  Created by Andrew Medvedev on 15/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "ParentViewController.h"

@interface ParentViewController ()

@property (strong, nonatomic) ModalScreensViewController *modalScreensVC;

@end

@implementation ParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showModalViewControllerWithType:(enum ModalScreenType)type success:(void (^)(BOOL))success
{
    if ( !_modalScreensVC )
    {
        self.modalScreensVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalScreensViewController"];
        self.modalScreensVC.providesPresentationContextTransitionStyle = YES;
        self.modalScreensVC.definesPresentationContext = YES;
        [self.modalScreensVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        self.modalScreensVC.callBackHandler = ^(BOOL rdy){
            success(rdy);
        };
    }
    
    __weak typeof (self) wself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wself presentViewController:_modalScreensVC animated:NO completion:^{
            wself.modalScreensVC.modalScreenType = type;
        }];
    });
    
    NSLog(@"MODAL: %ld", (long)_modalScreensVC.modalScreenType);
}

@end
