//
//  RateAppViewController.h
//  SnapPets
//
//  Created by Andrew Medvedev on 14/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

typedef void(^RateCallBackBlock)(BOOL rdyForRate);

@interface RateAppViewController : ParentViewController

@property (copy, nonatomic) RateCallBackBlock callBackHandler;

@end
