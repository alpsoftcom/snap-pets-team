//
//  GeneralSettingViewController.h
//  SnapPets
//
//  Created by Andrew Medvedev on 14/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserSettingViewControllerRedesign.h"
#import "ParentViewController.h"

typedef void(^GeneralSettingsHandlerBlock)(NSString *action);

@interface GeneralSettingViewController : ParentViewController

@property (copy, nonatomic) GeneralSettingsHandlerBlock generalSettingsHandler;
@property (nonatomic, assign)SettingsCellType cellType;

@end
