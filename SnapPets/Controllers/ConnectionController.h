//
//  ConnectionController.h
//  MipPhotoTest
//
//  Created by Forrest Chan on 26/9/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UARTPeripheral.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "ParentViewController.h"

//#import "SnappetRobotFinder.h"
//#import "SnappetRobotFinderPrivate.h"

@interface ConnectionController : ParentViewController <UITableViewDataSource, UITableViewDelegate, CBCentralManagerDelegate, UARTPeripheralDelegate, SnappetRobotDelegate> {
    
}
#pragma mark - button actions
- (IBAction)refreshAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *deviceTable;
@property (strong, nonatomic) CBCentralManager *cm;
@property (strong, nonatomic) NSMutableArray *deviceList;
@property (weak, nonatomic) IBOutlet UILabel *scanLabel;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;

@property (nonatomic, strong) UIWindow *mirroredWindow;
@property (nonatomic, strong) UIScreen *mirroredScreen;

@property (nonatomic, strong) UIWindow *secondWindow;

@property (weak, nonatomic) IBOutlet UILabel *lbVersion;

@property (nonatomic, strong) NSTimer* connectionTimer;

@end
