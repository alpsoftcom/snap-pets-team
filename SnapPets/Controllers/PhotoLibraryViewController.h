//
//  PhotoLibraryViewController.h
//  SnapPets
//
//  Created by Katy Pun on 12/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "PhotoDownloader.h"

@interface PhotoLibraryViewController : BaseViewController <UICollectionViewDataSource,UINavigationControllerDelegate, SnappetRobotDelegate, PhotoDownloaderDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *photoCollection;
@property (nonatomic, strong) IBOutlet UIImageView *imgBG;

@property (strong, nonatomic) IBOutlet UIView *bottomBarView;
@property (strong, nonatomic) IBOutlet UIButton *snapWithPetBtn;
@property (strong, nonatomic) IBOutlet UIButton *snapWithCamBtn;
@property (strong, nonatomic) IBOutlet UIButton *settingBtn;
@property (strong, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UILabel *currentPhotoIndex;
@property (weak, nonatomic) IBOutlet UILabel *totalPhoto;
@property (weak, nonatomic) IBOutlet UILabel *photoLabel;

@property (strong, nonatomic) IBOutlet UILabel *downloadLabel;

@property (readwrite) BOOL isMultipleSelectOn;

@property (nonatomic, strong) IBOutlet UIView* deleteView;
@property (nonatomic, strong) IBOutlet UIView* deleteContentView;
@property (nonatomic, strong) IBOutlet UIButton* deleteConfirmBtn;
@property (nonatomic, strong) IBOutlet UIButton* cancelBtn;

@property (nonatomic, strong) IBOutlet UIButton* selectPhotosBtn;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintDeleteView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* constraintBottomCollectionView;
@property (readwrite) NSMutableArray* m_arrSelected;
@property (weak, nonatomic) IBOutlet UILabel *deleteContent;

@end
