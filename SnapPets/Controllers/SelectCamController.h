//
//  SelectCamController.h
//  MipPhotoTest
//
//  Created by Forrest Chan on 4/8/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UARTPeripheral.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>
#import "ParentViewController.h"
//#import "SnappetRobot.h"
//#import "SnappetRobotPrivate.h"

@interface SelectCamController : ParentViewController <UARTPeripheralDelegate>

@property(nonatomic, weak) IBOutlet UIButton *connectButton;
@property(nonatomic, weak) IBOutlet UIButton *takePictureButton;
@property(nonatomic, weak) IBOutlet UIImageView *imageView;
@property(nonatomic, weak) IBOutlet UIButton *hexDumpButton;
@property(nonatomic, weak) IBOutlet UIButton *dropboxButton;
@property(nonatomic, weak) IBOutlet UILabel *progressLabel;
@property(nonatomic, weak) IBOutlet UIProgressView *progressBar;
@property(nonatomic, weak) IBOutlet UISwitch *predefinedHeaderSwitch;
@property(nonatomic, weak) IBOutlet UIView *settingPageView;
@property(nonatomic, weak) IBOutlet UIView *indicatorBg;
@property(nonatomic, weak) IBOutlet UIImageView *indatorImgView;
//@property(nonatomic, strong) UARTPeripheral *currentPeripheral;


@property (nonatomic, strong) UIWindow *mirroredWindow;
@property (nonatomic, strong) UIScreen *mirroredScreen;

@property (nonatomic, strong) UIWindow *secondWindow;

@property (nonatomic, strong) SnappetRobot *snappet;

@end
