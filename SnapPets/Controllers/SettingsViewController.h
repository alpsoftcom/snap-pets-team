//
//  SettingsViewController.h
//  SnapPets
//
//  Created by Katy Pun on 16/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
//#import "SnappetRobot.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

@interface SettingsViewController : BaseViewController <SnappetRobotDelegate>

@end
