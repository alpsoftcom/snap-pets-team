//
//  PhotoLibraryViewController.m
//  SnapPets
//
//  Created by Katy Pun on 12/12/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "PhotoLibraryViewController.h"
#import "PhotoLibraryViewCell.h"
#import "PhotoPreviewViewController.h"
#import "SelectCamController.h"
#import "SettingsViewController.h"
#import "TakePhotoViewController.h"
#import "PhotoLibraryManager.h"
#import "TestServiceViewController.h"
#import "ConnectionManager.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "LocalizationManager.h"

@interface PhotoLibraryViewController () <UICollectionViewDelegate>
@property (nonatomic, assign) int currentTransferredPhoto;
@property (nonatomic, assign) int photoLength;
@property (nonatomic, strong) NSMutableData *receivedData;

@end

static NSString * const PhotoLibraryViewCellIdentifier = @"photoviewcell";

@implementation PhotoLibraryViewController 

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.m_arrSelected = [NSMutableArray new];
    [[PhotoLibraryManager sharedInstance] loadPhoto];
    
    
    self.constraintBottomCollectionView.constant =  0.f;
    [self.photoCollection updateConstraints];
    [self.photoCollection layoutIfNeeded];

    self.photoCollection.delegate = self;
    self.photoCollection.dataSource = self;
    [self.photoCollection reloadData];
    [self.deleteConfirmBtn setTitle:[[LocalizationManager getInstance] getStringWithKey:@"BtnDelete"] forState:UIControlStateNormal];
    [self.deleteContent setText:[[LocalizationManager getInstance] getStringWithKey:@"BtnDeleteContent"]];
    [self.downloadLabel setText:[[LocalizationManager getInstance] getStringWithKey:@"DownloadImage"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.downloadLabel setHidden:YES];
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        UIFont* font30 = [UIFont systemFontOfSize:30];
        self.selectPhotosBtn.titleLabel.font = font30;
        self.currentPhotoIndex.font = font30;
        self.totalPhoto.font = font30;
        self.photoLabel.font = font30;
    }
//    else if (screenBounds.height == 480) {
//        self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen.png"];
//    }
//    else if (screenBounds.height == 667) {
//        self.imgBG.image = [UIImage imageNamed:@"bg_connectionscreen.png"];
//    }
    self.currentPhotoIndex.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
    self.totalPhoto.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
}
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    if([self.bottomBarView isHidden])
        self.constraintBottomCollectionView.constant =  0.f;
    else
        self.constraintBottomCollectionView.constant =  55.f;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [ConnectionManager sharedInstance].snappet.delegate = self;
    [[ConnectionManager sharedInstance].snappet snappetReadNumberOfPhotos];

    [self.photoCollection reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}


- (void)loadPhotoArray
{
    
}

+ (NSString *) applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

//-(void)showPics {
////    NSString* imageListPath = [[PhotoLibraryViewController applicationDocumentsDirectory] stringByAppendingPathComponent:@"snaplist"];
//    self.imageList = [NSMutableArray new];
//    
//    NSString* imageListPath = [PhotoLibraryViewController applicationDocumentsDirectory];
//    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:imageListPath error:NULL];
//    for (NSString* file in files) {
//        if ([file.pathExtension compare:@"png" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
//            NSString *fullPath = [imageListPath stringByAppendingPathComponent:file];
//            [self.imageList addObject:[UIImage imageWithContentsOfFile:fullPath]];
//        }
//    }
//    NSMutableArray *imageDataList = [NSMutableArray arrayWithContentsOfFile:imageListPath];
//
//    if(imageDataList) {
//        for(NSData *imgData in imageDataList) {
//            [self.imageList addObject:[UIImage imageWithData:imgData]];
//        }
//        
//    }
//    
//    
////    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject] error:NULL];
////    NSArray *PhotoArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:@"Snap_Image"];
////    NSMutableArray *imgQueue = [[NSMutableArray alloc] initWithCapacity:PhotoArray.count];
////    for (NSString* path in PhotoArray) {
////        [imgQueue addObject:[UIImage imageWithContentsOfFile:path]];
////    }
//    
////    UIImage *currentPic = _imgView.image;
////    int i = -1;
////    
////    if (currentPic != nil && [imgQueue containsObject:currentPic]) {
////        i = [imgQueue indexOfObject:currentPic];
////    }
////    
////    i++;
////    if(i < imgQueue.count) {
////        _imgView.image = [imgQueue objectAtIndex:1];
////    }
//}

- (IBAction)snapWithPet:(id)sender
{
    SelectCamController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCamController"];
    //    controller.currentPeripheral = self.currentPeripheral;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)snapWithCam:(id)sender
{
//    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
//    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
//    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
//    imagePickerController.delegate = self;
//    imagePickerController.showsCameraControls = NO;
//    [self.navigationController pushViewController:imagePickerController animated:YES];
    
    TakePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TakePhotoViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)settings:(id)sender
{
//    SettingsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
    
    TestServiceViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TestServiceViewController"];
    controller.snappet = [ConnectionManager sharedInstance].snappet;
    [ConnectionManager sharedInstance].snappet.delegate = controller;
    [ConnectionManager sharedInstance].snappet.autoReconnect = YES;
//    controller.snappet = snappet;
//    snappet.delegate = controller;
//    snappet.autoReconnect = NO;     // Disable auto reconnect for now
//    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)editPhoto:(id)sender
{
    TakePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TakePhotoViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)resetPhoto {
    self.receivedData = [NSMutableData new];
    self.photoLength = 0;
}

- (IBAction)deletePhoto:(id)sender {
    
//    self.receivedData = [NSMutableData new];
//    self.photoLength = 0;
}

- (IBAction)backButtonAction:(id)sender {
    [[PhotoDownloader sharedInstance] stop];
    [self.navigationController popViewControllerAnimated:YES];
//    [self.view removeFromSuperview];
}

- (IBAction)selectPhotosAction:(id)sender {
//    [[PhotoDownloader sharedInstance] stop];
//    [self.navigationController popViewControllerAnimated:YES];
    //    [self.view removeFromSuperview];
    if (self.photoCollection.allowsMultipleSelection) {
        self.photoCollection.allowsMultipleSelection = NO;
        [self.m_arrSelected removeAllObjects];
        UIButton *button = sender;
//        [button setTitle:@"" forState:UIControlStateNormal];
        CGSize screenBounds = [UIScreen mainScreen].bounds.size;
        if (screenBounds.height == 1024 || screenBounds.height == 2048) {
            [button setTitle:@"Select" forState:UIControlStateNormal];
            //[button setImage:[UIImage imageNamed:@"btn_selectall_theme1.png"] forState:UIControlStateNormal];
        }
        else {
            [button setTitle:@"Select" forState:UIControlStateNormal];
            //[button setImage:[UIImage imageNamed:@"btn_selectall_theme1.png"] forState:UIControlStateNormal];
        }
        
        for (int i=0; i<[[PhotoLibraryManager sharedInstance].imageList count];i++) {
            [self.photoCollection deselectItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO];
            PhotoLibraryViewCell *photoCell = (PhotoLibraryViewCell *)[self.photoCollection cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            [photoCell unselect];
        }
        self.bottomBarView.hidden = YES;
    }
    else {
        self.photoCollection.allowsMultipleSelection = YES;
        [self.m_arrSelected removeAllObjects];
        UIButton *button = sender;
//        [button setTitle:@"Cancel" forState:UIControlStateNormal];
        CGSize screenBounds = [UIScreen mainScreen].bounds.size;
        if (screenBounds.height == 1024 || screenBounds.height == 2048) {
            [button setTitle:@"Cancel" forState:UIControlStateNormal];
//            [button setImage:[UIImage imageNamed:@"btn_close_theme1.png"] forState:UIControlStateNormal];
        }
        else {
            [button setTitle:@"Cancel" forState:UIControlStateNormal];
//            [button setImage:[UIImage imageNamed:@"btn_close_theme1.png"] forState:UIControlStateNormal];
        }
    }
    [self viewWillLayoutSubviews];
}

- (IBAction)deleteAction:(id)sender {
    self.constraintDeleteView.constant = -200.0f;
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.constraintDeleteView.constant = 0.0f;
                         [self.deleteView layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         self.constraintDeleteView.constant = 0.0f;
                     }
     ];
}

- (IBAction)deleteConfirmAction:(id)sender {
//    [[PhotoLibraryManager sharedInstance] deletePhotoWithIndex:self.selectedPhotoIndex];
//    [self.navigationController popViewControllerAnimated:NO];
    if (self.photoCollection.allowsMultipleSelection) {
        NSArray* arr = [self.photoCollection indexPathsForSelectedItems];
        arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSInteger r1 = [obj1 row];
            NSInteger r2 = [obj2 row];
            if (r1 > r2) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            if (r1 < r2) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        for (int i=[arr count]-1; i>=0; i--) {
            NSIndexPath *path = [arr objectAtIndex:i];
            NSLog(@"%d", path.row);
            [[PhotoLibraryManager sharedInstance] deletePhotoWithIndex:(int)path.row];
        }
        [self.photoCollection reloadData];
        
        self.currentPhotoIndex.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
        self.totalPhoto.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
        
        self.bottomBarView.hidden = YES;
        [self.m_arrSelected removeAllObjects];
        [self viewWillLayoutSubviews];
    }
    
    [self performSelectorOnMainThread:@selector(cancelAction:) withObject:nil waitUntilDone:YES];
    [self performSelectorOnMainThread:@selector(selectPhotosAction:) withObject:self.selectPhotosBtn waitUntilDone:YES];
}

- (IBAction)cancelAction:(id)sender {
    self.constraintDeleteView.constant = 0.0f;
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.constraintDeleteView.constant = -200.0f;
                         [self.deleteView layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         self.constraintDeleteView.constant = -200.0f;
                     }
     ];
}

- (IBAction)shareAction:(id)sender {
//    NSURL *url = [NSURL fileURLWithPath:pth];
//    NSArray *objectsToShare = @[url];
    NSMutableArray* objectsToShare = [NSMutableArray new];
    NSArray* array = [self.photoCollection indexPathsForSelectedItems];
    for (int i=0;i<[array count];i++) {
        NSIndexPath* index = [array objectAtIndex:i];
        NSString* path = [[PhotoLibraryManager sharedInstance].imagenameList objectAtIndex:index.row];
        NSURL *url = [NSURL fileURLWithPath:path];
        [objectsToShare addObject:url];
    }
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}

- (void)longPress:(UILongPressGestureRecognizer*)gesture
{
    if ( gesture.state == UIGestureRecognizerStateBegan ) {
        if (!self.photoCollection.allowsMultipleSelection) {
            [self selectPhotosAction:self.selectPhotosBtn];
            PhotoLibraryViewCell* cell = (PhotoLibraryViewCell*)gesture.view;
            [self.photoCollection selectItemAtIndexPath:[NSIndexPath indexPathForRow:cell.tag inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
            [self collectionView:self.photoCollection didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:cell.tag inSection:0]];
        }
    }
    [self viewWillLayoutSubviews];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.constraintDeleteView.constant == 200.0f) {
        [self cancelAction:nil];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    

    return [[PhotoLibraryManager sharedInstance].imageList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoLibraryViewCell *photoCell = [collectionView dequeueReusableCellWithReuseIdentifier:PhotoLibraryViewCellIdentifier forIndexPath:indexPath];

    if ([[photoCell viewWithTag:100] isKindOfClass:[UIImageView class]]) {
        UIImageView *recipeImageView = (UIImageView *)[photoCell viewWithTag:100];
        recipeImageView.image = [[PhotoLibraryManager sharedInstance].imageThumbnailList objectAtIndex:indexPath.row];
    }
    [photoCell unselect];
    for (int i=0;i<[self.m_arrSelected count];i++) {
        NSNumber* number = [self.m_arrSelected objectAtIndex:i];
        if ([number longValue] == indexPath.row) {
            [photoCell select];
            break;
        }
    }
    if (photoCell.gesture != nil)
        photoCell.gesture = nil;
    [photoCell setTag:indexPath.row];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [photoCell addGestureRecognizer:longPress];
    photoCell.gesture = longPress;
//    photoCell.photoImgView = [[UIImageView alloc] init];
//    photoCell.photoImgView = (UIImageView *)[photoCell viewWithTag:100];
//    photoCell.photoImgView = recipeImageView;//(UIImageView *)[photoCell viewWithTag:100];
//    photoCell.photoImgView.image = [UIImage imageNamed:[self.photoArray objectAtIndex:indexPath.row]];
    
    return photoCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        return CGSizeMake(200.0f, 200.0f);
    }
    else
        return CGSizeMake(100.0f, 100.0f);
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.photoCollection.allowsMultipleSelection) {
        PhotoLibraryViewCell *photoCell = (PhotoLibraryViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [photoCell select];
        [self.m_arrSelected addObject:[NSNumber numberWithLong:indexPath.row]];
        self.bottomBarView.hidden = NO;
        [self viewWillLayoutSubviews];
    }
    else {
        [[PhotoDownloader sharedInstance] stop];
    
        PhotoPreviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoPreviewViewController"];
//      controller.selectedPhotoPath = [self.imageList objectAtIndex:indexPath.row];
        controller.selectedPhotoIndex = indexPath.row;
        controller.isPreviewAll = NO;
        controller.selectedImg = [[PhotoLibraryManager sharedInstance].imageList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controller animated:NO];
        [SVProgressHUD dismiss];
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.photoCollection.allowsMultipleSelection) {
        PhotoLibraryViewCell *photoCell = (PhotoLibraryViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [photoCell unselect];
        if ([[self.photoCollection indexPathsForSelectedItems] count] == 0)
            self.bottomBarView.hidden = YES;
        for (int i=0;i<[self.m_arrSelected count];i++) {
            NSNumber* number = [self.m_arrSelected objectAtIndex:i];
            if ([number longValue] == indexPath.row) {
                [self.m_arrSelected removeObject:number];
                break;
            }
        }
    }
    [self viewDidLayoutSubviews];
}

#pragma mark - SnappetRobotDelegate
-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoLength:(int)photoLength checksum:(uint8_t)checksum {
    //    [self.lbLength setText:[NSString stringWithFormat:@"Length: %d", photoLength]];
    //    [self.lbChecksum setText:[NSString stringWithFormat:@"Checksum: %d", checksum]];
    [self resetPhoto];
    self.photoLength = photoLength;
//    NSLog(@"PhotoLibraryViewController photoLength %d", photoLength);
//    NSLog(@"PhotoLibraryViewController numOfPhoto = %d",[PhotoLibraryManager sharedInstance].numOfPhotoInSnappet);
    if (photoLength > 0 && [PhotoLibraryManager sharedInstance].numOfPhotoInSnappet > 0){
//        [SVProgressHUD show];
    }
}

-(void) Snappet:(SnappetRobot *)snappet didReceivePhotoBlob:(NSData *)blobData {
    [_receivedData appendData:blobData];
    int receivedLength = (int)[_receivedData length];
    //    float progress = (float)receivedLength / (float)_photoLength;
    //    [_blobProgress setProgress:progress animated:NO];
    //    [_lbProgress setText:[NSString stringWithFormat:@"%d / %d", receivedLength, _photoLength]];
    //
    
//    NSLog(@"PhotoLibraryViewController received data %d", receivedLength);
    if(receivedLength > 0 && receivedLength == _photoLength) {
//        NSLog(@"PhotoLibraryViewController Receive photo data finished. Creating image now..");
        
        NSMutableData *finalData = [NSMutableData new];
        NSData *headerData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jpgheader.dat" ofType:nil]];
        [finalData appendData:headerData];
        [finalData appendData:_receivedData];
        
        // Finish receiving photo
        UIImage *image = [UIImage imageWithData:finalData];
        //        UIImage *image = [UIImage imageWithData:_receivedData];
        if(!image) {
//            NSLog(@"image = nil");
        }
        
        
//        [SVProgressHUD dismiss];
        
        //save to app album
        [[PhotoLibraryManager sharedInstance] saveImage:image];
        //delete from snappet
        uint8_t photoIdByte = (uint8_t)(1);
//        NSLog(@"PhotoLibraryViewController delete photo");
        [[ConnectionManager sharedInstance].snappet snappetDeletePhoto:photoIdByte];
        
        [self.photoCollection reloadData];
        
        [SVProgressHUD dismiss];
        
        if ([PhotoLibraryManager sharedInstance].currentPhotoId+1 == [PhotoLibraryManager sharedInstance].numOfPhotoInSnappet){
        }else{
            //get next photo from snappet
//            [SVProgressHUD show];
            [PhotoLibraryManager sharedInstance].currentPhotoId++;
            
//            uint8_t photoIdByte = (uint8_t)([PhotoLibraryManager sharedInstance].currentPhotoId+1);
            uint8_t photoIdByte = (uint8_t)(1);
            [[ConnectionManager sharedInstance].snappet snappetGetPhoto:kSnappetPhotoLargeSize photoID:photoIdByte];
        }
        
        self.currentPhotoIndex.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
        
        //        [_photoImage setImage:image];
        //
        //        // Save data for debug purpose
        //        //        [self saveReceivedDataAction:nil];
    }
    
    
}


-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.view makeToast:[[LocalizationManager getInstance] getStringWithKey:@"SnapPet Disconnected"]];
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveNumberOfPhoto:(int)numberOfPhoto {
    
    
    [PhotoLibraryManager sharedInstance].numOfPhotoInSnappet = numberOfPhoto;
    
    if (numberOfPhoto > 0) {
        [self.downloadLabel setHidden:NO];
        [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
        [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
        [SVProgressHUD show];
        
        [PhotoLibraryManager sharedInstance].currentPhotoId = 0;
//        uint8_t photoIdByte = (uint8_t)([PhotoLibraryManager sharedInstance].currentPhotoId+1);
//        [[ConnectionManager sharedInstance].snappet snappetGetPhoto:kSnappetPhotoLargeSize photoID:photoIdByte];
        
        [[PhotoDownloader sharedInstance] setDelegate:self];
        [[PhotoDownloader sharedInstance] downloadAllPhotos];
        
        self.currentPhotoIndex.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
    }else{
        self.currentPhotoIndex.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
    }
    
    self.totalPhoto.text = [NSString stringWithFormat:@"%d", numberOfPhoto+(int)[[PhotoLibraryManager sharedInstance].imageList count]];
}

- (void)didDownloadImage {
    self.currentPhotoIndex.text = [NSString stringWithFormat:@"%d", (int)[[PhotoLibraryManager sharedInstance].imageList count]];
    [self.photoCollection reloadData];
    [[PhotoLibraryManager sharedInstance] loadPhoto];
}

- (void)didCompleteDownloadTask {
    [SVProgressHUD dismiss];
    [self.downloadLabel setHidden:YES];
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
//        NSLog(@"Disconnect");
        [[ConnectionManager sharedInstance].snappet disconnect];
        [ConnectionManager sharedInstance].snappet = nil;
        [self.navigationController popToRootViewControllerAnimated:NO];
        
    }
}

@end
