//
//  RateAppViewController.m
//  SnapPets
//
//  Created by Andrew Medvedev on 14/09/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "RateAppViewController.h"

@interface RateAppViewController ()
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@end

@implementation RateAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *bgView = [[UIView alloc] initWithFrame:(CGRect){0, 0, SCREEN_WIDTH, SCREEN_HEIGHT}];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.5;
    [self.view addSubview:bgView];
    [self.view sendSubviewToBack:bgView];
    
    self.view.backgroundColor = [UIColor clearColor];
    if ( IS_IOS7 )
    {
        self.modalPresentationStyle = UIModalPresentationFormSheet;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    
//    self.backgroundView.layer.cornerRadius = self.backgroundView.frame.size.height / 8;
//    self.backgroundView.layer.masksToBounds = YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)yesButtonPressed:(id)sender
{
    _callBackHandler(YES);
}

- (IBAction)remindButtonPressed:(id)sender
{
    _callBackHandler(NO);
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
