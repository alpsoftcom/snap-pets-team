//
//  ImageTVOutViewController.m
//  MipPhotoTest
//
//  Created by Katy Pun on 29/9/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import "ImageTVOutViewController.h"

@interface ImageTVOutViewController ()

@end

@implementation ImageTVOutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithFrame:(CGRect)rect {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    self = [storyboard instantiateViewControllerWithIdentifier:@"ImageTVOutViewController"];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(handleSyncSlideshowNotification:)
//                                                 name:@"SyncSlideshowNotification"
//                                               object:nil];
    
}

- (void)setImg:(UIImage *)img {
    [self.displayImage setImage:img];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
