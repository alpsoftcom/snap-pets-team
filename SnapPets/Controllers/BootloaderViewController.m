//
//  BootloaderViewController.m
//  SnapPets
//
//  Created by Katy Pun on 28/1/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "BootloaderViewController.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

//#import "SnappetRobotFinder.h"
//#import "SnappetRobotFinderPrivate.h"
//#import "BluetoothRobotFinderPrivate.h"
//#import "BluetoothRobotPrivate.h"

@interface BootloaderViewController ()

@property (nonatomic, retain) DFUOperations *dfuOperations;
@property (nonatomic, assign) DfuFirmwareTypes enumFirmwareType;
@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) CBPeripheral *selectedPeripheral;
@property (nonatomic, assign) BOOL isDfuVersionExist;

@end

@implementation BootloaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dfuOperations = [[DFUOperations alloc] initWithDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Register device finder notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSnappetRobotFinderNotification:) name:SnappetRobotFinderNotificationID object:nil];
    [[SnappetRobotFinder sharedInstance] cbCentralManagerState];
    [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
    
    // Start DFU
    [self performDFU];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Stop scan
    [[SnappetRobotFinder sharedInstance] stopScanForSnappets];
    
    // Remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - setter

- (void)setSnappet:(SnappetRobot *)snappet {
    _snappet = snappet;
    _snappet.delegate = self;
}

#pragma mark - DFU

- (void)setBootloaderState:(BootloaderState)bootloaderState {
    _bootloaderState = bootloaderState;
    
    switch (_bootloaderState) {
        default:
        case BootloaderPrepareDFU:
            if(_snappet.isDFUMode) {
                // Already in DFU mode, jump straight to BootloaderUpdate
                [self setBootloaderState:BootloaderConnect];
            }
            else {
                // Put device in DFU mode
                [_snappet nordicRebootToMode:kDeviceDFUMode];
            }
            break;
        case BootloaderScanning:
            // Start scanning
            [[SnappetRobotFinder sharedInstance] scanForSnappets];
            break;
        case BootloaderConnect:
            // Stop scanning
            [[SnappetRobotFinder sharedInstance] stopScanForSnappets];
            
            // Steal CBCentralManager's instance for DFU operation
            self.centralManager = [SnappetRobotFinder sharedInstance].btMgr.manager;
            self.selectedPeripheral = _snappet.ymsPeripheral.cbPeripheral;
            
            // Connect to DFU device
            [_dfuOperations setCentralManager:_centralManager];
            [_dfuOperations connectDevice:_selectedPeripheral];
            break;
        case BootloaderUpdate:
        {
            NSURL *fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"WowWee_Snap_Pets_V18.hex" ofType:nil]];
            if (self.isDfuVersionExist) {
                [_dfuOperations performDFUOnFileWithMetaData:fileURL firmwareMetaDataURL:nil firmwareType:APPLICATION];
            }
            else {
                [_dfuOperations performDFUOnFile:fileURL firmwareType:APPLICATION];
            }
        }
            break;
    }
}

- (void)performDFU {
    // Put device in DFU mode
    [self setBootloaderState:BootloaderPrepareDFU];
}

#pragma mark - Button actions

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - FileType Selector Delegate

-(void)centralManager:(CBCentralManager *)manager didPeripheralSelected:(CBPeripheral *)peripheral
{
//    selectedPeripheral = peripheral;
    [_dfuOperations setCentralManager:manager];
//    deviceName.text = peripheral.name;
    [_dfuOperations connectDevice:peripheral];
}

#pragma mark - SnappetRobotFinder

- (void)onSnappetRobotFinderNotification: (NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    if(info){
        NSNumber *code = [info objectForKey: @"code"];
        if (code.intValue == SnappetRobotFinderNote_SnappetFound){
            for(SnappetRobot *device in [[SnappetRobotFinder sharedInstance] snappetsFound]) {
                if(device.isDFUMode) {
                    self.snappet = device;
                    [self setBootloaderState:BootloaderConnect];
                    break;
                }
            }
        } else if (code.intValue == SnappetRobotFinderNote_SnappetListCleared) {
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothError) {
            NSLog(@"Bluetooth error!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsOff) {
            NSLog(@"Bluetooth is off!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsAvailable) {
            NSLog(@"SnappetRobotFinderNote_BluetoothIsAvailable");
        }
    }
}

#pragma mark - SnappetRobotDelegate

-(void) SnappetDeviceDisconnected:(SnappetRobot *)snappet error:(NSError *)error {
    NSLog(@"BootloaderViewController: SnappetDeviceDisconnected");
    
    _snappet.delegate = nil;
    self.snappet = nil;
    
    // Scan
    [self setBootloaderState:BootloaderScanning];
}

#pragma mark - DFUOperations delegate methods

-(void)onDeviceConnected:(CBPeripheral *)peripheral
{
    NSLog(@"onDeviceConnected %@",peripheral.name);
//    self.isConnected = YES;
//    [self enableUploadButton];
    self.isDfuVersionExist = NO;
    [self setBootloaderState:BootloaderUpdate];
}

-(void)onDeviceConnectedWithVersion:(CBPeripheral *)peripheral
{
    NSLog(@"onDeviceConnectedWithVersion %@",peripheral.name);
//    self.isConnected = YES;
//    [self enableUploadButton];
    self.isDfuVersionExist = YES;
    [self setBootloaderState:BootloaderUpdate];
}

-(void)onDeviceDisconnected:(CBPeripheral *)peripheral
{
    NSLog(@"device disconnected %@",peripheral.name);
//    self.isTransferring = NO;
//    self.isConnected = NO;
//    
//    // Scanner uses other queue to send events. We must edit UI in the main queue
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (self.dfuVersion != 1) {
//            [self clearUI];
//            
//            
//            if (!self.isTransfered && !self.isTransferCancelled && !self.isErrorKnown) {
//                [Utility showAlert:@"The connection has been lost"];
//            }
//            self.isTransferCancelled = NO;
//            self.isTransfered = NO;
//            self.isErrorKnown = NO;
//        }
//        else {
//            double delayInSeconds = 3.0;
//            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [dfuOperations connectDevice:peripheral];
//            });
//            
//        }
//    });
}

-(void)onReadDFUVersion:(int)version
{
    NSLog(@"onReadDFUVersion %d",version);
//    self.dfuVersion = version;
//    NSLog(@"DFU Version: %d",self.dfuVersion);
//    if (self.dfuVersion == 1) {
//        [dfuOperations setAppToBootloaderMode];
//    }
//    [self enableUploadButton];
}

-(void)onDFUStarted
{
    NSLog(@"onDFUStarted");
//    self.isTransferring = YES;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        uploadButton.enabled = YES;
//        [uploadButton setTitle:@"Cancel" forState:UIControlStateNormal];
//        NSString *uploadStatusMessage = [self getUploadStatusMessage];
//        uploadStatus.text = uploadStatusMessage;
//    });
}

-(void)onDFUCancelled
{
    NSLog(@"onDFUCancelled");
//    self.isTransferring = NO;
//    self.isTransferCancelled = YES;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self enableOtherButtons];
//    });
}

-(void)onSoftDeviceUploadStarted
{
    NSLog(@"onSoftDeviceUploadStarted");
}

-(void)onSoftDeviceUploadCompleted
{
    NSLog(@"onSoftDeviceUploadCompleted");
}

-(void)onBootloaderUploadStarted
{
    NSLog(@"onBootloaderUploadStarted");
//    dispatch_async(dispatch_get_main_queue(), ^{
//        uploadStatus.text = @"uploading bootloader ...";
//    });
}

-(void)onBootloaderUploadCompleted
{
    NSLog(@"onBootloaderUploadCompleted");
}

-(void)onTransferPercentage:(int)percentage
{
    NSLog(@"onTransferPercentage %d",percentage);
    // Scanner uses other queue to send events. We must edit UI in the main queue
//    dispatch_async(dispatch_get_main_queue(), ^{
//        progressLabel.text = [NSString stringWithFormat:@"%d %%", percentage];
//        [progress setProgress:((float)percentage/100.0) animated:YES];
//    });
}

-(void)onSuccessfulFileTranferred
{
    NSLog(@"OnSuccessfulFileTransferred");
    // Scanner uses other queue to send events. We must edit UI in the main queue
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self.isTransferring = NO;
//        self.isTransfered = YES;
//        NSString* message = [NSString stringWithFormat:@"%u bytes transfered in %u seconds", dfuOperations.binFileSize, dfuOperations.uploadTimeInSeconds];
//        [Utility showAlert:message];
//    });
}

-(void)onError:(NSString *)errorMessage
{
    NSLog(@"OnError %@",errorMessage);
//    self.isErrorKnown = YES;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [Utility showAlert:errorMessage];
//        [self clearUI];
//    });
}

@end
