//
//  TakePhotoWithSnapPetsScannerController.m
//  SnapPets
//
//  Created by David Chan on 21/4/15.
//  Copyright (c) 2015 ___WOWWEE___. All rights reserved.
//

#import "TakePhotoWithSnapPetsScannerController.h"
#import "DeviceCell.h"
//#import "SnappetRobotFinder.h"
#import "SVProgressHUD.h"
#import "ConnectionManager.h"
#import "DeviceTableCell.h"

#import "AppConfig.h"
#import "LocalizationManager.h"
#import "Flurry.h"
#import <WowweeSnappetSDK/WowweeSnappetSDK.h>

static NSString * kLastConnectedPet = @"LastConnectedPet";
static NSString * kLastConnectedPetPIN = @"LastConnectedPetPIN";

@interface TakePhotoWithSnapPetsScannerController ()

@end

@implementation TakePhotoWithSnapPetsScannerController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", [AppConfig applicationDocumentsDirectory]);
    //    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    //    statusBarView.backgroundColor  =  [UIColor yellowColor];
    //    [self.view addSubview:statusBarView];
    self.viewScanner.layer.cornerRadius = 15.0f;
    self.viewScanner.clipsToBounds = YES;
    
    self.btnClose.layer.cornerRadius = 5.0f;
    self.btnClose.clipsToBounds = YES;
    
    CGSize screenBounds = [UIScreen mainScreen].bounds.size;
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        self.viewScanner.layer.cornerRadius = 30.0f;
        self.viewScanner.clipsToBounds = YES;
        
        self.btnClose.layer.cornerRadius = 30.0f;
        self.btnClose.clipsToBounds = YES;
    }
    
    self.viewScannerBase.translatesAutoresizingMaskIntoConstraints = YES;
    [self.viewScannerBase setFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:self.viewScannerBase];
    [self.viewScannerBase setHidden:YES];
    
    self.popupView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.popupView setFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:self.popupView];
    [self.popupView setHidden:YES];
    
    self.tutorialView.translatesAutoresizingMaskIntoConstraints = NO;
    CGRect rect = [[UIScreen mainScreen] bounds];
    [self.tutorialView setFrame:rect];
    [self.view addSubview:self.tutorialView];
    [self.tutorialView setHidden:YES];
    
    self.tutorialView.deleagte = self;
    self.tutorialView.takePhotoOverlayDelegate = self.takephotoOverlayView;
    
    self.enterNameView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.enterNameView setFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:self.enterNameView];
    [self.enterNameView setHidden:YES];
    
    if (screenBounds.height == 1024 || screenBounds.height == 2048) {
        //        UIFont* fontButton = [UIFont systemFontOfSize:50.0f weight:2.0f];
        //        UIFont* fontLabel80 = [UIFont systemFontOfSize:80.0f weight:2.0f];
        //        UIFont* fontLabel50 = [UIFont systemFontOfSize:50.0f weight:2.0f];
        UIFont* fontLabel60 = [UIFont fontWithName:@"FontesqueSansPro-Bold" size:60.0f];
        self.labelTitleScanner.font = fontLabel60;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationEnteredBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationEnteredForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    NSMutableArray *menuanimationImages = [[NSMutableArray alloc] init];
    NSInteger animationImageCount = 2;// 45;
    for (int aniCount = 0; aniCount < animationImageCount; aniCount++) {
        NSString * str;
        if(aniCount < 10)
           str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"SnapPets_Loading_0000%d", aniCount] ofType: @"png"];
        else
            str = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat: @"SnapPets_Loading_000%d", aniCount] ofType: @"png"];

        // here is the code to pre-render the image
        UIImage *frameImage = [UIImage imageWithContentsOfFile: str];
        UIGraphicsBeginImageContext(frameImage.size);
        CGRect rect = CGRectMake(0, 0, frameImage.size.width, frameImage.size.height);
        [frameImage drawInRect:rect];
        UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [menuanimationImages addObject:renderedImage];
    }
    
    self.conntectingImageView.animationImages = menuanimationImages;
    self.conntectingImageView.animationDuration = 2.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([ConnectionManager sharedInstance].snappet == nil) {
        [self.takephotoOverlayView showConnectionScreen];
    }
    else {
        [self.takephotoOverlayView showSnappetTakePhotoScreen];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SnappetRobotFinderNotificationID object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSnappetRobotFinderNotification:) name:SnappetRobotFinderNotificationID object:nil];
    [[SnappetRobotFinder sharedInstance] cbCentralManagerState];
    [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
    
    //    if(self.currentPeripheral) {
    //        [self.cm cancelPeripheralConnection:self.currentPeripheral.peripheral];
    //        self.currentPeripheral = nil;
    //    }
    //
    // Start scanning
    [self startScan];
}

- (void)applicationEnteredBackground:(NSNotification *)notification {
    NSLog(@"Application Entered Background");
    if (isInCurrentViewController) {
        [self viewUnloadAnimation];
    }
}

- (void)applicationEnteredForeground:(NSNotification *)notification {
    NSLog(@"Application Entered Foreground");
    //    [self startScan];
    if (isInCurrentViewController) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSnappetRobotFinderNotification:) name:SnappetRobotFinderNotificationID object:nil];
        [[SnappetRobotFinder sharedInstance] cbCentralManagerState];
        [[SnappetRobotFinder sharedInstance] clearFoundSnappetList];
        
        [self startScan];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [timerAnimatingScannerButton invalidate];
    timerAnimatingScannerButton = nil;
    
    [CATransaction begin];
    [self.takephotoOverlayView.scanBtn.layer removeAllAnimations];
    [CATransaction commit];
    self.takephotoOverlayView.scanBtn.tag = 0;
}

- (void)viewUnloadAnimation {
    if (self.takephotoOverlayView.scanBtn.tag != 0) {
        [CATransaction begin];
        [self.takephotoOverlayView.scanBtn.layer removeAllAnimations];
        [CATransaction commit];
        self.takephotoOverlayView.scanBtn.tag = 0;
        [self.takephotoOverlayView.scanBtn setAlpha:1.0f];
    }
}

#pragma mark - IBAction
- (IBAction)buttonSelector:(id)sender {
    if (sender == self.btnClose) {
        if( [ConnectionManager sharedInstance].snappet)
            [[ConnectionManager sharedInstance].snappet disconnect];
        [self.viewScannerBase setHidden:YES];
        [self.conntectingImageView stopAnimating];
        [self.conntectingImageView setHidden:YES];
        
    }
}

-(IBAction)buyPetButtonPressed:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LocalizationManager getInstance] getStringWithKey:@"Open URL"]
                                                    message:[[LocalizationManager getInstance] getStringWithKey:@"Would you like to open SnapPets webpage?"]
                                                   delegate:self
                                          cancelButtonTitle:[[LocalizationManager getInstance] getStringWithKey:@"NO"]
                                          otherButtonTitles:[[LocalizationManager getInstance] getStringWithKey:@"YES"], nil];
    alert.delegate = self;
    [alert show];
}
#pragma mark - private
- (void)startScan {
    //    [self.cm scanForPeripheralsWithServices:@[UARTPeripheral.uartServiceUUID] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: [NSNumber numberWithBool:NO]}];
    [[SnappetRobotFinder sharedInstance] scanForSnappets];
    //    [[BluetoothConnectionManager sharedInstance] startScan];
}

- (void)stopScan {
    //    [self.cm stopScan];
    [[SnappetRobotFinder sharedInstance] stopScanForSnappets];
}

- (void)_timerFired {
    [SVProgressHUD dismiss];
    
    [self.connectionTimer invalidate];
    self.connectionTimer = nil;
}

- (void)checkFirmwareVersion {
    // [[ConnectionManager sharedInstance].snappet snappetResetASCIIPinCodeAndEraseFlash];
    
    [NSThread sleepForTimeInterval:0.2f];
    [AppConfig sharedInstance].strRobotName = [ConnectionManager sharedInstance].snappet.name;
    [[ConnectionManager sharedInstance].snappet snappetReadActivationStatus];

//    NSString* strVersion = [ConnectionManager sharedInstance].snappet.bleModuleSoftwareVersion;
//    if ([strVersion length] <= 5) {
//    }
    
 
    
    if(self.conntectingImageView.hidden)
        return;
    SnappetRobot * robot =[ConnectionManager sharedInstance].snappet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *lastConnectedPetUUID = [defaults objectForKey:kLastConnectedPet];
    NSString * petUUID = [NSString stringWithFormat:@"%@",robot.uuid.UUIDString];
    if(lastConnectedPetUUID){
        if([lastConnectedPetUUID isEqualToString: petUUID]){
            NSLog(@"This pet was connected");
             NSString *lastConnectedPetPIN = [defaults objectForKey:kLastConnectedPetPIN];
            int value0 = [[NSString stringWithFormat:@"%c", [lastConnectedPetPIN characterAtIndex:0]] intValue];
            int value1 = [[NSString stringWithFormat:@"%c", [lastConnectedPetPIN characterAtIndex:1]] intValue];
            int value2 = [[NSString stringWithFormat:@"%c", [lastConnectedPetPIN characterAtIndex:2]] intValue];
            int value3 = [[NSString stringWithFormat:@"%c", [lastConnectedPetPIN characterAtIndex:3]] intValue];

              [robot snappetAuthenticateWithASCIIPinCode:value0 digit2:value1 digit3:value2 digit4:value3];
//            [defaults setObject:petUUID forKey:@"LastConnectedPet"];
//            [defaults setObject:_petPinTextField.text forKey:@"LastConnectedPetPIN"];
//            [defaults synchronize];
            return;
        }
            
    }
    
    [[AppConfig sharedInstance] saveDeviceWithName:[ConnectionManager sharedInstance].snappet.name Identity:[[ConnectionManager sharedInstance].snappet.uuid UUIDString] ColorID:[ConnectionManager sharedInstance].snappet.colorId];
    
    [self showmodalPetNameScreen];

    

    //    [[ConnectionManager sharedInstance].snappet snappetReadTakePhotoMode];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger count = [[[SnappetRobotFinder sharedInstance] snappetsFound] count];
    if (count > 0)
        m_isSnapPetFound = YES;
    else
        m_isSnapPetFound = NO;
    
    if (m_isSnapPetFound)
        return [[[SnappetRobotFinder sharedInstance] snappetsFound] count];
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DeviceTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceTableCell"];
    
    if (m_isSnapPetFound) {
        SnappetRobot *snappet = [[SnappetRobotFinder sharedInstance] snappetsFound][indexPath.row];
        cell.snappet = snappet;
        [cell updateView:indexPath.row%3];
    }
    else {
        cell.snappet = nil;
        [cell updateView:indexPath.row%3];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGRect rect = [self.view bounds];
    if (rect.size.height == 1024 || rect.size.height == 2048)
        return 120.0f;
    else
        return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self stopScan];
    
    if (m_isSnapPetFound) {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            self.labelTitleScanner.text = [[LocalizationManager getInstance] getStringWithKey:@"Connecting"];
            [tableView setHidden:YES];
            [self.conntectingImageView setHidden:NO];
            [self.conntectingImageView startAnimating];
            [self.snapPetLabel setHidden:YES];
              });
            SnappetRobot *snappet = [[SnappetRobotFinder sharedInstance] snappetsFound][indexPath.row];
            snappet.delegate = self;
            [snappet connect];
//            [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
//            [SVProgressHUD setForegroundColor:[UIColor colorWithRed:239.0f/255.0f green:70.0f/255.0f blue:125.0f/255.0f alpha:1]];
            //[SVProgressHUD show];
            self.connectionTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                                    target:self
                                                                  selector:@selector(_timerFired)
                                                                  userInfo:nil
                                                                   repeats:NO];
      
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LocalizationManager getInstance] getStringWithKey:@"Open URL"]
                                                        message:[[LocalizationManager getInstance] getStringWithKey:@"Would you like to open SnapPets webpage?"]
                                                       delegate:self
                                              cancelButtonTitle:[[LocalizationManager getInstance] getStringWithKey:@"NO"]
                                              otherButtonTitles:[[LocalizationManager getInstance] getStringWithKey:@"YES"], nil];
        alert.delegate = self;
        [alert show];
    }
}

#pragma mark - SnappetRobotFinder callback
- (void)onSnappetRobotFinderNotification: (NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    if(info){
        NSNumber *code = [info objectForKey: @"code"];
        //id data = [info objectForKey: @"data"];
        if (code.intValue == SnappetRobotFinderNote_SnappetFound){
            //            self.deviceList = [[SnappetRobotFinder sharedInstance] snappetsFound];
            NSArray* arr = [[SnappetRobotFinder sharedInstance] snappetsFound];
            m_arrSorted = [arr sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                NSNumber *first = ((SnappetRobot*)a).snappetRSSI;
                NSNumber *second = ((SnappetRobot*)b).snappetRSSI;
                return ![first compare:second];
            }];
            
            
            [self.tableView reloadData];
            [self.takephotoOverlayView animateScanButton];
            //            if (timerAnimatingScannerButton == nil)
            //                timerAnimatingScannerButton = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self.takephotoOverlayView selector:@selector(animateScanButton) userInfo:nil repeats:YES];
            
            //            self.scanLabel.hidden = YES;
        } else if (code.intValue == SnappetRobotFinderNote_SnappetListCleared) {
            //            self.deviceList = [[SnappetRobotFinder sharedInstance] snappetsFound];
            [self.tableView reloadData];
            //            self.scanLabel.hidden = NO;
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothError) {
            NSLog(@"Bluetooth error!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsOff) {
            NSLog(@"Bluetooth is off!");
        } else if (code.intValue == SnappetRobotFinderNote_BluetoothIsAvailable) {
            NSLog(@"SnappetRobotFinderNote_BluetoothIsAvailable");
        }
    }
}

#pragma mark - SnappetRobotDelegate
-(void) SnappetDeviceReady:(SnappetRobot *)snappet {
    NSLog(@"SnappetDeviceReady");
 
    
    [NSThread sleepForTimeInterval:0.1];
    

    [[AppConfig sharedInstance] saveDeviceWithName:[ConnectionManager sharedInstance].snappet.name Identity:
     [[ConnectionManager sharedInstance].snappet.uuid UUIDString] ColorID:[ConnectionManager sharedInstance].snappet.colorId];
    [SVProgressHUD dismiss];

    [ConnectionManager sharedInstance].snappet = snappet;
    [ConnectionManager sharedInstance].snappet.autoReconnect = YES;
    
  
    //[self showModalViewControllerWithType:kModalScreenTypePetName];
    
    //    [[ConnectionManager sharedInstance].snappet snappetSetUserPhoneName:@""];
    [NSThread detachNewThreadSelector:@selector(checkFirmwareVersion) toTarget:self withObject:nil];
    
    //    NSLog(@"%@", snappet.bleModuleSoftwareVersion);
    

    
}

-(void) Snappet:(SnappetRobot *)snappet didReceiveModuleParameterUserPhoneName:(NSString *)userPhoneName {
    //    NSLog(@"%@", userPhoneName);
  
//    if ([userPhoneName isEqualToString:@""]) {
//        
//        [self.viewScannerBase setHidden:YES];
//        
//        [[AppConfig sharedInstance] saveDeviceWithName:snappet.name Identity:[snappet.uuid UUIDString] ColorID:snappet.colorId];
//        
//        if(self.conntectingImageView.hidden)
//            return;
//        
//        [self showModalViewControllerWithType:kModalScreenTypePetName success:^(BOOL rdy) {
//            
//            //snappet.delegate = self;
//            //[snappet snappetReadChargingStatus];
//            
//                [self.conntectingImageView stopAnimating];
//                [self.conntectingImageView setHidden:YES];
//                                [self.takephotoOverlayView.labelName setText:snappet.name];
//                                [self.takephotoOverlayView showSnappetTakePhotoScreen];
//                                [self.connectionTimer invalidate];
//                                self.connectionTimer = nil;
//            
//            if (self.takephotoOverlayView.isPhoneShutterMode) {
//                [self.takephotoOverlayView performSelectorOnMainThread:@selector(modePressed:) withObject:self.takephotoOverlayView.modeBtn waitUntilDone:YES];
//            }
//            
//        }];
//
//        
//        
////        [self.takephotoOverlayView.labelName setText:snappet.name];
////        [self.viewScannerBase setHidden:YES];
////        [self.takephotoOverlayView showSnappetTakePhotoScreen];
////        
////        [ConnectionManager sharedInstance].snappet.autoReconnect = NO;
////        [self.connectionTimer invalidate];
////        self.connectionTimer = nil;
////        
////        [[AppConfig sharedInstance] saveDeviceWithName:snappet.name Identity:[snappet.uuid UUIDString] ColorID:snappet.colorId];
////        
////        [self.takephotoOverlayView modePressed:self.takephotoOverlayView.modeBtn];
////        [self.takephotoOverlayView showNewSnappetScreen];
//        //        [snappet snappetWriteDeviceName:[[UIDevice currentDevice] name]];
//    }
//    else {
//        
////        [self.takephotoOverlayView.labelName setText:snappet.name];
////        [self.viewScannerBase setHidden:YES];
////        [self.takephotoOverlayView showSnappetTakePhotoScreen];
////        
////        [self.connectionTimer invalidate];
////        self.connectionTimer = nil;
////
//        if(self.conntectingImageView.hidden)
//            return;
//         [[AppConfig sharedInstance] saveDeviceWithName:snappet.name Identity:[snappet.uuid UUIDString] ColorID:snappet.colorId];
//        [self showModalViewControllerWithType:kModalScreenTypePetName success:^(BOOL rdy) {
//            
//            //snappet.delegate = self;
//            //[snappet snappetReadChargingStatus];
//            
//            [self.conntectingImageView stopAnimating];
//            [self.conntectingImageView setHidden:YES];
//            [self.takephotoOverlayView.labelName setText:snappet.name];
//            [self.takephotoOverlayView showSnappetTakePhotoScreen];
//            [self.connectionTimer invalidate];
//            self.connectionTimer = nil;
//            
//            if (self.takephotoOverlayView.isPhoneShutterMode) {
//                [self.takephotoOverlayView performSelectorOnMainThread:@selector(modePressed:) withObject:self.takephotoOverlayView.modeBtn waitUntilDone:YES];
//            }
//            
//        }];
//
//        
//
////        if (self.takephotoOverlayView.isPhoneShutterMode)
////            [self.takephotoOverlayView modePressed:self.takephotoOverlayView.modeBtn];
//        
//    }
//    [self.conntectingImageView stopAnimating];
//    self.conntectingImageView.animationImages = nil;
    [SVProgressHUD dismiss];
}
-(void)showmodalPetNameScreen{
    
    [self showModalViewControllerWithType:kModalScreenTypePetName success:^(BOOL rdy) {
        
        
        [self.conntectingImageView stopAnimating];
        //self.conntectingImageView.animationImages = nil;
        [SVProgressHUD dismiss];
        [self.viewScannerBase setHidden:YES];
        [self.takephotoOverlayView showSnappetTakePhotoScreen];
        if (self.takephotoOverlayView.isPhoneShutterMode) {
            [self.takephotoOverlayView performSelectorOnMainThread:@selector(modePressed:) withObject:self.takephotoOverlayView.modeBtn waitUntilDone:YES];
        }
        [ConnectionManager sharedInstance].snappet.autoReconnect = YES;
        
        [self.connectionTimer invalidate];
        self.connectionTimer = nil;
        
        
        
        
    }];
    

}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSecurityAuthenticateResponse:(int)response{
    BOOL pinAuthen;
    
    // _petPinBlock(_petPinTextField.text, YES);
    // [self.snappet snappetReadChargingStatus ];
    
    
    if (response == 0){
        [self  showmodalPetNameScreen];
    }
    else
    {
        
        [self.conntectingImageView stopAnimating];
        //self.conntectingImageView.animationImages = nil;
        [SVProgressHUD dismiss];
        [self.viewScannerBase setHidden:YES];
        [self.takephotoOverlayView showSnappetTakePhotoScreen];
        if (self.takephotoOverlayView.isPhoneShutterMode) {
            [self.takephotoOverlayView performSelectorOnMainThread:@selector(modePressed:) withObject:self.takephotoOverlayView.modeBtn waitUntilDone:YES];
        }
        [ConnectionManager sharedInstance].snappet.autoReconnect = YES;
        
        [self.connectionTimer invalidate];
        self.connectionTimer = nil;

    

        
    }
    
}


-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingsActivationStatus:(int)status {
    if (status == 0 || status == 1) {
        [Flurry logEvent:@"Snappet Activated" withParameters:@{@"Color" : [NSNumber numberWithLong:snappet.colorId]}];
        [[ConnectionManager sharedInstance].snappet snappetSetProductActivationUploaded];
    }
    else {
        
    }
}
-(void) Snappet:(SnappetRobot *)snappet didReceiveSettingChargingResponse:(int)response{
    NSLog(@"");
    if (response == kSnappetNotCharging){
        NSLog(@"not charging");
    }else if (response == kSnappetCharging){
        NSLog(@"charging");
    }else if (response == kSnappetFullyCharge){
        NSLog(@"power full");
    }
//    [self showModalViewControllerWithType:kModalScreenTypePetLowBattery success:^(BOOL rdy) {
//
//        
//    }];

}
#pragma mark - TakePhotoOverlayViewDelegate
- (void)scannerPressed {
#if TARGET_IPHONE_SIMULATOR
    [self.takephotoOverlayView showSnappetTakePhotoScreen];
    /*
     [self.viewScannerBase setHidden:YES];
     [self.takephotoOverlayView showSnappetTakePhotoScreen];
     
     [ConnectionManager sharedInstance].snappet.autoReconnect = NO;
     [self.connectionTimer invalidate];
     self.connectionTimer = nil;
     
     [self.takephotoOverlayView modePressed:self.takephotoOverlayView.modeBtn];
     [self.takephotoOverlayView showNewSnappetScreen];
     //    */
#else
    [self.viewScannerBase setHidden:NO];
    self.labelTitleScanner.text = [[LocalizationManager getInstance] getStringWithKey:@"Searching for"];
    [self.conntectingImageView setHidden:YES];
    [self.snapPetLabel setHidden:NO];
    [self.tableView setHidden:NO];
    [self startScan];
    self.labelTitleScanner.numberOfLines = 2;
    //    [self.popupView showQuitSettingView];
#endif
}

#pragma mark - UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://qrs.ly/y84pxxu"]];
    }
}

@end
