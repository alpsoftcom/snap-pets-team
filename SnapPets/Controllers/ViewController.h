//
//  ViewController.h
//  MipPhotoTest
//
//  Created by Forrest Chan on 25/4/14.
//  Copyright (c) 2014 ___WOWWEE___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnappetRobotFinder.h"

@interface ViewController : UIViewController <SnappetRobotDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *mips;
@property (strong, nonatomic) IBOutlet UITableView *mipTableView;
@property (strong, nonatomic) NSMutableData *imageData;
@property (strong, nonatomic) NSData *previousData;
@property (strong, nonatomic) IBOutlet UIImageView *mipImage;
@property (strong, nonatomic) UIAlertView *alert;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;
@property (strong, nonatomic) IBOutlet UILabel *readyLabel;
@property (strong, nonatomic) IBOutlet UILabel *countdownLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UIButton *cameraButton;


@end
